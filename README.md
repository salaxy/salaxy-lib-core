General plain JavaScript / TypeScript libraries
-----------------------------------------------

Plain JavaScript / TypeScript stack for Salaxy. 
These libraries are independent from libraries such as Angular, Angular 2, ReactJS etc.
They may be used inside these libraries.

Usage:

- Run `npm install @salaxy/core --save --ignore-scripts`
- Reference or copy TypeScript files from `node_modules/@salaxy/core`

For more information and documentation about Salaxy platform, please go to https://developers.salaxy.com
