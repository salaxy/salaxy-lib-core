/* eslint-disable @typescript-eslint/no-var-requires */
/**
 * salaxy-lib-core: Grunt dev and build configuration v2.0
 * @param {*} grunt The Grunt object
 */
module.exports = (grunt) => {
  require('jit-grunt')(grunt);
  grunt.initConfig({
    concat: {
      core: {
        options: {
          banner: '"use strict";\n\n/***** SALAXY CORE library, See https://developers.salaxy.com for details *****/\n\nvar require;\n',
          process: (src, filepath) => {
            return '// Source: ' + filepath + '\n' +
              src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
          },
        },
        src: [
          'node_modules/babel-polyfill/dist/polyfill.js',
          'node_modules/moment/moment.js',
          'node_modules/js-base64/base64.js',
          'dist/npm/@salaxy/core/salaxy-core.js',
        ],
        dest: 'dist/npm/@salaxy/core/salaxy-core-all.js',
        nonull: true
      },
      jquery: {
        options: {
          banner: "'use strict';\n\n/***** SALAXY FULL LIBRARY (salaxy-lib-jquery, salaxy-lib-core and external dependencies), See https://developers.salaxy.com for details *****/\n\n",
          process: (src, filepath) => {
            return '// Source: ' + filepath + '\n' + src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
          }
        },
        src: [
          'dist/npm/@salaxy/core/salaxy-core-all.js',
          'dist/npm/@salaxy/jquery/salaxy-lib-jquery.js',

        ],
        dest: 'dist/npm/@salaxy/jquery/salaxy-lib-jquery-all.js',
        nonull: true
      },
      reports: {
        options: {
          banner: '"use strict";\n\n/***** SALAXY REPORTS library, See https://developers.salaxy.com for details *****/\n',
          process: (src, filepath) => {
            return '// Source: ' + filepath + '\n' +
              src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
          },
        },
        src: [
          'node_modules/handlebars/dist/handlebars.js',
          'dist/npm/@salaxy/reports/salaxy-lib-reports.js',
        ],
        dest: 'dist/npm/@salaxy/reports/salaxy-lib-reports.js',
        nonull: true
      },
    },
    ts: {
      core: {
        tsconfig: {
          tsconfig: 'src/@salaxy/core/tsconfig.json',
          passThrough: true
        }
      },
      node: {
        tsconfig: {
          tsconfig: 'src/@salaxy/node/tsconfig.json',
          passThrough: true
        }
      },
      jquery: {
        tsconfig: {
          tsconfig: 'src/@salaxy/jquery/tsconfig.json',
          passThrough: true
        }
      },
      reports: {
        tsconfig: {
          tsconfig: 'src/@salaxy/reports/tsconfig.json',
          passThrough: true
        }
      },
    },
    browserify: {
      core: {
        src: ['dist/npm/@salaxy/core/index.js'],
        dest: 'dist/npm/@salaxy/core/salaxy-core.js',
        options: {
          alias: { '@salaxy/core': "./dist/npm/@salaxy/core/index.js" },
          browserifyOptions: {
            bundleExternal: true,
          },
          transform: ['browserify-shim']
        }
      },
      jquery: {
        src: ['dist/npm/@salaxy/jquery/index.js'],
        dest: 'dist/npm/@salaxy/jquery/salaxy-lib-jquery.js',
        options: {
          alias: { '@salaxy/jquery': "./dist/npm/@salaxy/jquery/index.js" },
          external: ["@salaxy/core"],
          browserifyOptions: {
            bundleExternal: false,
          },
          transform: ['browserify-shim']
        }
      },
      reports: {
        src: ["dist/npm/@salaxy/reports/index.js"],
        dest: 'dist/npm/@salaxy/reports/salaxy-lib-reports.js',
        options: {
          alias: { '@salaxy/reports': "./dist/npm/@salaxy/reports/index.js" },
          external: ["@salaxy/core"],
          browserifyOptions: {
            bundleExternal: false,
          },
          transform: ['browserify-shim']
        }
      }
    },
    terser: {
      options: {},
      core: {
        files: {
          'dist/npm/@salaxy/core/salaxy-core-all.min.js': ['dist/npm/@salaxy/core/salaxy-core-all.js'],
        }
      },
      all: {
        files: {
          'dist/npm/@salaxy/core/salaxy-core-all.min.js': ['dist/npm/@salaxy/core/salaxy-core-all.js'],
          'dist/npm/@salaxy/jquery/salaxy-lib-jquery.min.js': ['dist/npm/@salaxy/jquery/salaxy-lib-jquery.js'],
          'dist/npm/@salaxy/jquery/salaxy-lib-jquery-all.min.js': ['dist/npm/@salaxy/jquery/salaxy-lib-jquery-all.js'],
          'dist/npm/@salaxy/reports/salaxy-lib-reports.min.js': ['dist/npm/@salaxy/reports/salaxy-lib-reports.js']
        }
      }
    },
    copy: {
      npm: {
        files: [
          { expand: true, cwd: 'src/@salaxy', src: ['core/package.json', 'core/readme.md'], dest: 'dist/npm/@salaxy/' },
          { expand: true, cwd: 'src/@salaxy', src: ['node/package.json', 'node/readme.md'], dest: 'dist/npm/@salaxy/' },
          { expand: true, cwd: 'src/@salaxy', src: ['jquery/package.json', 'jquery/readme.md'], dest: 'dist/npm/@salaxy/' },
          { expand: true, cwd: 'dist/npm/@salaxy/core', src: 'salaxy-core-all.*', dest: 'dist/npm/@salaxy/jquery' },
          { expand: true, cwd: 'src/@salaxy', src: ['reports/package.json', 'reports/readme.md'], dest: 'dist/npm/@salaxy/' },
          { expand: true, cwd: 'src/@salaxy', src: ['reports/hbs/**/*.hbs'], dest: 'dist/npm/@salaxy/' },
          { expand: true, cwd: 'src/@salaxy', src: ['reports/i18n/*.json'], dest: 'dist/npm/@salaxy/reports', flatten: "true" }
        ]
      }
    },
    clean: {
      npm: {
        src: ['dist/npm/@salaxy/core', 'dist/npm/@salaxy/node', 'dist/npm/@salaxy/jquery', 'dist/npm/@salaxy/reports']
      },
    },
    eslint: {
      options: {
        configFile: ".eslintrc.js",
        extensions: [".ts"],
      },
      core: ['./src/@salaxy/core'],
      all: ['./src/@salaxy']
    },
    handlebars: {
      commonjs: {
        options: {
          namespace: 'salaxy.reports.templates',
          processName: (filePath) => {
            const pieces = filePath.split('/');
            return pieces[pieces.length - 1].split('.')[0];
          },
          node: true,
        },
        files: {
          'src/@salaxy/reports/hbs/index.js': 'src/@salaxy/reports/hbs/**/*.hbs',
          'dist/npm/@salaxy/reports/hbs/index.js': 'src/@salaxy/reports/hbs/**/*.hbs',
        }
      }
    },
    less: {
      reports: {
        options: {
          sourceMap: true,
          sourceMapFileInline: false,
          sourceMapRootpath: "/",
        },
        files: {
          "dist/npm/@salaxy/reports/salaxy-lib-reports.css": "src/@salaxy/reports/less/salaxy-reports.less",
        }
      }
    },
    yearlyNumbers: {
      default: {
        url: "http://localhost:82/v02/api/calculator/yearlyNumbers", // TODO: to be changed to test-api.
        years: ["2020", "2021"],
        dest: "src/@salaxy/core/codegen/yearlyChangingNumbersYears.ts"
      }
    }
  });

  // Custom tasks
  grunt.registerMultiTask('yearlyNumbers', 'Creates yearly changing numbers array into codegen', () => {
    const YearlyNumbers = require("./grunt-tasks/YearlyNumbers");
    new YearlyNumbers(grunt).getYearlyNumbers();
  });

  // ===========================================================================
  // BUILD JOBS 		    ========================================================
  // ===========================================================================

  const coreTasks = ['ts:core', 'browserify:core', 'concat:core', 'terser:core', 'copy'];
  const allTasks = ['clean:npm', 'handlebars', 'ts', 'browserify', 'concat', 'terser:all', 'less', 'copy', 'eslint:all'];

  grunt.registerTask('default', coreTasks);
  grunt.registerTask('core', coreTasks);
  grunt.registerTask('build', allTasks);
  grunt.registerTask('build-full', allTasks);
};
