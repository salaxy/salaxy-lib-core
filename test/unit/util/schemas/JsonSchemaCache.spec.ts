import { OpenAPIV3 } from "openapi-types";
import chai from "chai";
import dirtyChai from "dirty-chai";

import { JsonSchemaCache, JsonSchemaUtils } from "@salaxy/core";

// Sample: https://github.com/OAI/OpenAPI-Specification/blob/master/examples/v3.0/api-with-examples.json
import * as irSchema from "./ir.schema.json";

describe("Unit tests", () => {
  describe("JSON Schema cache", () => {
    const expect = chai.expect;
    chai.use(dirtyChai);

    const sampleUrl = "https://FOOBAR.com/ir-schema";
    const sampleSchema = irSchema as any;

    it("Loads a schema to cache", () => {
      const cache = new JsonSchemaCache(null);
      expect(cache.schemaCache.length).equals(0);
      cache.addSchemaDocument(sampleUrl, sampleSchema);
      expect(cache.schemaCache.length, "1 schema loaded").equals(1);
      const schemaInCache = cache.findSchemaDoc(sampleUrl, true);
      expect(JsonSchemaUtils.isOpenAPIV2(schemaInCache.doc)).false("Not OpenAPIV2");
      expect(JsonSchemaUtils.isOpenAPIV3(schemaInCache.doc)).true("OpenAPIV3 type guard");
      expect(schemaInCache.url).equals(sampleUrl.toLowerCase());
      expect(schemaInCache.type).equals("OpenAPIV3");
    });

    it("Finds a schema in document", () => {
      const cache = new JsonSchemaCache(null);
      cache.addSchemaDocument(sampleUrl, sampleSchema);
      const target = cache.findSchema(sampleUrl, "IncomeTypeCode");
      expect(target).exist();
      expect(target.type).equals("object");
      expect(Object.keys(target.properties)[0]).equals("code");
    });

    it("Resolves internal references", () => {
      const cache = new JsonSchemaCache(null);
      cache.addSchemaDocument(sampleUrl, sampleSchema);
      const target = cache.findSchema(sampleUrl, "IncomeTypeCode").properties.calcGrouping as OpenAPIV3.SchemaObject;
      expect(target).exist();
      expect(JsonSchemaUtils.isReferenceObject(target)).false();
      expect(JsonSchemaUtils.isNonArraySchemaObject(target)).true();
      expect(target.type).equals("string");
      expect(target.enum).exist("Is enum");
      expect(target.enum[0]).equals("undefined");
    });

    it("Gets simple property", () => {
      const cache = new JsonSchemaCache(null);
      const doc = cache.addSchemaDocument(sampleUrl, sampleSchema);
      const schema = cache.findSchema(sampleUrl, "IncomeTypeCode");
      const target = JsonSchemaUtils.getProperty(schema, "calcGrouping");
      expect(target).exist();
      expect(JsonSchemaUtils.isReferenceObject(target.schema)).false();
      expect(JsonSchemaUtils.isNonArraySchemaObject(target.schema)).true();
      expect(target.schema.type).equals("string");
      expect(target.schema.enum).exist("Is enum");
      expect(target.schema.enum[0]).equals("undefined");
      expect(target.propertyName).equals("calcGrouping");
      expect(target.parentName).equals("IncomeTypeCode");
    });

    it("Gets property with complex path", () => {
      const cache = new JsonSchemaCache(null);
      const doc = cache.addSchemaDocument(sampleUrl, sampleSchema);
      const schema = cache.findSchema(sampleUrl, "EarningsPayment");
      expect(schema).exist("schema found");
      const target = JsonSchemaUtils.getProperty(schema, "deliveryData.payer.payerBasic");
      expect(target.schema).exist("deliveryData.payer.payerBasic");
      expect(JsonSchemaUtils.isReferenceObject(target.schema)).false();
      expect(JsonSchemaUtils.isNonArraySchemaObject(target.schema)).true();
      expect(target.schema.type).equals("object");
      expect((target.schema.properties.missingId as OpenAPIV3.SchemaObject).type).equals("boolean");
      expect(target.propertyName).equals("payerBasic");
      expect(target.parentName).equals("Payer");
    });

    it("SchemaObject typeguard tests", () => {
      const cache = new JsonSchemaCache(null);
      expect(JsonSchemaUtils.isArraySchemaObject({ type: "array" } as any)).true("ArraySchemaObject positive");
      expect(JsonSchemaUtils.isArraySchemaObject({ type: "foobar" } as any)).false("ArraySchemaObject negative");
      expect(JsonSchemaUtils.isNonArraySchemaObject({ type: "object" } as any)).true("NonArraySchemaObject object");
      expect(JsonSchemaUtils.isNonArraySchemaObject({ type: "string" } as any)).true("NonArraySchemaObject string");
      expect(JsonSchemaUtils.isNonArraySchemaObject({ type: "array" } as any)).false("NonArraySchemaObject negative");
      expect(JsonSchemaUtils.isNonArraySchemaObject({ } as any)).false("NonArraySchemaObject empty");
      expect(JsonSchemaUtils.isReferenceObject({ $ref: "foobar" } as any)).true("ReferenceObject positive");
      expect(JsonSchemaUtils.isReferenceObject({ type: "foobar" } as any)).false("ArraySchemaObject negative");
    });

    it("SchemaDocument typeguard tests", () => {
      const cache = new JsonSchemaCache(null);
      expect(JsonSchemaUtils.isOpenAPIV2({ swagger: "2.0" } as any)).true("OpenAPIV2 positive");
      expect(JsonSchemaUtils.isOpenAPIV2({ swagger: "3.0" } as any)).false("OpenAPIV2 negative");
      expect(JsonSchemaUtils.isOpenAPIV2({ version: "3.0.3" } as any)).false("OpenAPIV2 negative, V3");

      expect(JsonSchemaUtils.isOpenAPIV3({ openapi: "3.0.3" } as any)).true("OpenAPIV3 positive");
      expect(JsonSchemaUtils.isOpenAPIV3({ openapi: "3.1.9" } as any)).true("OpenAPIV3 positive, future");
      expect(JsonSchemaUtils.isOpenAPIV3({ openapi: "3.0.0" } as any)).true("OpenAPIV3 positive, old");

      expect(JsonSchemaUtils.isOpenAPIV3({ swagger: "3.0" } as any)).false("OpenAPIV3 negative");
      expect(JsonSchemaUtils.isOpenAPIV3({ swagger: "2.0" } as any)).false("OpenAPIV3 negative, V2");
    });
  });
});
