import chai from "chai";
import dirtyChai from "dirty-chai";

import {
    Numeric,
} from "@salaxy/core";

describe("Unit tests", () => {

  describe("Numeric util tests", () => {
    const expect = chai.expect;
    chai.use(dirtyChai);

    it("Should round null to null, using 0 decimal rounding", () => {
      expect(Numeric.round(null,0)).equals(null);
    });

    it("Should round undefined to undefined, using 0 decimal rounding", () => {
      expect(Numeric.round(undefined,0)).equals(undefined);
    });

    it("Should round  0.5 to 1, using 0 decimal rounding", () => {
      expect(Numeric.round(0.5,0)).equals(1);
    });

    it("Should round  -0.5 to -1, using 0 decimal rounding", () => {
      expect(Numeric.round(-0.5,0)).equals(-1);
    });

    it("Should round  1.005 to 1.01, using 2 decimal rounding", () => {
      expect(Numeric.round(1.005,2)).equals(1.01);
    });

    it("Should round  -1.005 to -1.01, using 2 decimal rounding", () => {
      expect(Numeric.round(-1.005,2)).equals(-1.01);
    });

    it("Should round  2.175 to 2.18, using 2 decimal rounding", () => {
      expect(Numeric.round(2.175,2)).equals(2.18);
    });

    it("Should round  -2.175 to -2.18, using 2 decimal rounding", () => {
      expect(Numeric.round(-2.175,2)).equals(-2.18);
    });

    it("Should round  5.015 to 5.02, using 2 decimal rounding", () => {
      expect(Numeric.round(5.015,2)).equals(5.02);
    });

    it("Should round  -5.015 to -5.02, using 2 decimal rounding", () => {
      expect(Numeric.round(-5.015,2)).equals(-5.02);
    });

    it("Should round  1.0005 to 1, using 0 decimal rounding", () => {
      expect(Numeric.round(1.0005,0)).equals(1);
    })

    it("Should round  1.0005 to 1, using 1 decimal rounding", () => {
      expect(Numeric.round(1.0005,1)).equals(1);
    })
    it("Should round  1.0005 to 1, using 2 decimal rounding", () => {
      expect(Numeric.round(1.0005,2)).equals(1);
    });

    it("Should round  1.0005 to 1.001, using 3 decimal rounding", () => {
      expect(Numeric.round(1.0005,3)).equals(1.001);
    });

    it("Should round  1.0005 to 1.0005, using 4 decimal rounding", () => {
      expect(Numeric.round(1.0005,4)).equals(1.0005);
    });

    it("Should round  1.0005 to 1.0005, using 5 decimal rounding", () => {
      expect(Numeric.round(1.0005,5)).equals(1.0005);
    });

    it("Should round  1.005 to 1.01, using 2 decimal rounding", () => {
      expect(Numeric.round(1.005,2)).equals(1.01);
    });

    it("Should round  -1.005 to -1.01, using 2 decimal rounding", () => {
      expect(Numeric.round(-1.005,2)).equals(-1.01);
    });

    it("Should round  39.425 to 39.43, using 2 decimal rounding", () => {
      expect(Numeric.round(39.425,2)).equals(39.43);
    });

    it("Should round  -39.425 to -39.43, using 2 decimal rounding", () => {
      expect(Numeric.round(-39.425,2)).equals(-39.43);
    });

    it("Should round  1005e-3 to 1.01, using 2 decimal rounding", () => {
      expect(Numeric.round(1005e-3,2)).equals(1.01);
    });

    it("Should round  -1005e-3 to -1.01, using 2 decimal rounding", () => {
      expect(Numeric.round(-1005e-3,2)).equals(-1.01);
    });
    it("Should round  5.684341886080802e-14 to 0, using 2 decimal rounding", () => {
      expect(Numeric.round(5.684341886080802e-14,2)).equals(0);
    });
    it("Should round  5.015 * 100 to 501.2, using 1 decimal rounding", () => {
      expect(Numeric.round(5.015 * 100 ,1)).equals(501.5);
    });

    it("Should round  -5.015 * 100 to -501.2, using 1 decimal rounding", () => {
      expect(Numeric.round(-5.015 * 100 ,1)).equals(-501.5);
    });

    it("Should round  1.499999999999999 to 1.5, using 2 decimal rounding", () => {
      expect(Numeric.round(1.499999999999999,2)).equals(1.5);
    });

    it("Should round  1.499999999999999 to 1.5, using 1 decimal rounding", () => {
      expect(Numeric.round(1.499999999999999,1)).equals(1.5);
    });

    it("Should round  1.499999999999999 to 1, using 0 decimal rounding", () => {
      expect(Numeric.round(1.499999999999999,0)).equals(1);
    });

    it("Should round  1.4999999999999999 to 2 (more than 15 digits), using 0 decimal rounding", () => {
      expect(Numeric.round(1.4999999999999999,0)).equals(2);
    });

    it("Should round  -1.4999999999999999 to -2 (more than 15 digits), using 0 decimal rounding", () => {
      expect(Numeric.round(-1.4999999999999999,0)).equals(-2);
    });

    it("Should round  1.499999999999999e0 to 1, using 0 decimal rounding", () => {
      expect(Numeric.round(1.499999999999999e0,0)).equals(1);
    });

    it("Should round  1.4999999999999999e0 to 2 (more than 15 digits), using 0 decimal rounding", () => {
      expect(Numeric.round(1.4999999999999999e0,0)).equals(2);
    });
  });
});
