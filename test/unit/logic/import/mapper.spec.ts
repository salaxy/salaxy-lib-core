import chai from "chai";
import dirtyChai from "dirty-chai";

import {
  Map,
  Mapper,
  MapSelector,
  Objects,
  WorkerAccount,
  WorkerLogic,
} from "@salaxy/core";

describe("Unit tests", () => {
  describe("Logic, Import, Mapper", () => {
    const expect = chai.expect;
    chai.use(dirtyChai);

    it("Should map json to table and back with default blank object", () => {

      const workerObjects: WorkerAccount[] = [];
      const worker1 = WorkerLogic.getBlank();
      worker1.avatar.firstName = "John";
      worker1.avatar.lastName = "Doe";
      worker1.contact.street = "New Street 3";
      workerObjects.push(worker1);
      const worker2 = WorkerLogic.getBlank();
      worker2.avatar.firstName = "Jane";
      worker2.avatar.lastName = "Doe";
      worker2.contact.street = "Old Street 3";
      workerObjects.push(worker2);

      // console.log(workerObjects);

      const workerMap: Map<WorkerAccount, any> = {
        firstName: "avatar.firstName",
        lastName: "avatar.lastName",
        street: "contact.street",
      };

      const workerMapper = new Mapper(workerMap, WorkerLogic.getBlank());

      const tableData = workerMapper.fromObjects(workerObjects);
      // console.log(tableData);
      expect(tableData.length, "Table contains all objects").equal(workerObjects.length);

      const objects = workerMapper.fromRows((tableData));
      // console.log(objects);

      expect(Objects.equal(workerObjects, objects), "From/To roundtrip does not change data").equal(true);

    });

    it("Should map json to table and back without default blank object", () => {

      const workerObjects: WorkerAccount[] = [];
      const worker1 = WorkerLogic.getBlank();
      worker1.avatar.firstName = "John";
      worker1.avatar.lastName = "Doe";
      worker1.contact.street = "New Street 3";
      workerObjects.push(worker1);
      const worker2 = WorkerLogic.getBlank();
      worker2.avatar.firstName = "Jane";
      worker2.avatar.lastName = "Doe";
      worker2.contact.street = "Old Street 3";
      workerObjects.push(worker2);

      // console.log(workerObjects);

      const workerMap: Map<WorkerAccount, any> = {
        firstName: "avatar.firstName",
        lastName: "avatar.lastName",
        street: "contact.street",
      };

      const workerMapper = new Mapper(workerMap);

      const tableData = workerMapper.fromObjects(workerObjects);
      // console.log(tableData);
      expect(tableData.length, "Table contains all objects").equal(workerObjects.length);

      const objects = workerMapper.fromRows(tableData);
      // console.log(objects);
      expect(objects.length, "No objects dropped in roundtrip").equal(workerObjects.length);

    });

    it("Should map json to table and back with default blank object and map selectors", () => {

      const workerObjects: WorkerAccount[] = [];
      const worker1 = WorkerLogic.getBlank();
      worker1.avatar.firstName = "John";
      worker1.avatar.lastName = "Doe";
      worker1.contact.street = "New Street 3";
      workerObjects.push(worker1);
      const worker2 = WorkerLogic.getBlank();
      worker2.avatar.firstName = "Jane";
      worker2.avatar.lastName = "Doe";
      worker2.contact.street = "Old Street 3";
      workerObjects.push(worker2);

      // console.log(workerObjects);

      const workerMap: Map<WorkerAccount, any> = {
        firstName: "avatar.firstName",
        lastName: {
          path: "avatar.lastName",
        } as MapSelector<WorkerAccount, any>,
        street: {
          fromObject: (workerObject: WorkerAccount, row: any) => {
            row.street = workerObject.contact.street;
          },
          fromRow: (row: any, workerObject: WorkerAccount) => {
            workerObject.contact.street = row.street;
          },
        } as MapSelector<WorkerAccount, any>,
      };

      const workerMapper = new Mapper(workerMap, WorkerLogic.getBlank());

      const tableData = workerMapper.fromObjects(workerObjects);
      // console.log(tableData);
      expect(tableData.length, "Table contains all objects").equal(workerObjects.length);

      const objects = workerMapper.fromRows(tableData);
      // console.log(objects);

      expect(Objects.equal(workerObjects, objects), "From/To roundtrip does not change data").equal(true);
    });
  });
});
