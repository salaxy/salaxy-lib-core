import chai from "chai";
import dirtyChai from "dirty-chai";

import {
  ImportBatch,
  ImportBatches,
  Map,
  Mapper,
  MapSelector,
  Objects,
  TableFormat,
  Tables,
  WorkerAccount,
  WorkerLogic,
  Workers,
} from "@salaxy/core";

/** Only for storage testing */
class SimpleStorage implements Storage {
  private storage: Array<
  {
    /** item key */
    key: string,
    /** item value */
    value: string,
  }> =  [];

  /** Returns the key for the index. */
  public key(index: number): string {
    return (index >= 0 && index < this.storage.length ? this.storage[index] : {}).key;
  }

  /** Clears the storage. */
  public clear() {
    this.storage = [];
  }
  /** Returns the item value for the key. */
  public getItem(keyName: string): string {
    const value = (this.storage.find( (x) => x.key === keyName) || {}).value;
    return value;
  }

  /** Sets the item value. */
  public setItem(keyName: string, keyValue: string) {
    let item = this.storage.find( (x) => x.key === keyName);
    if (item) {
      item.value = keyValue;
    } else {
      item = {key: keyName, value: keyValue};
      this.storage.push(item);
    }
  }

  /** Removes the item. */
  public removeItem(keyName: string) {
    this.storage = this.storage.filter( (x) => x.key !== keyName);
  }

  /** Returns the number of items in the storage */
  public get length() {
    return this.storage.length;
  }

}

describe("Unit tests", () => {
  describe("Logic, Import, Batches", () => {
    const expect = chai.expect;
    chai.use(dirtyChai);

    it("Should read, map, validate and import ", async () => {

      const workerObjects: WorkerAccount[] = [];
      const worker1 = WorkerLogic.getBlank();
      worker1.avatar.firstName = "John";
      worker1.avatar.lastName = "Doe";
      worker1.contact.street = "New Street 3";
      workerObjects.push(worker1);
      const worker2 = WorkerLogic.getBlank();
      worker2.avatar.firstName = "Jane";
      worker2.avatar.lastName = "Doe";
      worker2.contact.street = "Old Street 3";
      workerObjects.push(worker2);

      // console.log(workerObjects);

      const workerMap: Map<WorkerAccount, any> = {
        firstName: "avatar.firstName",
        lastName: "avatar.lastName",
        street: "contact.street",
      };

      const workerMapper = new Mapper(workerMap, WorkerLogic.getBlank());
      const tableData = workerMapper.fromObjects(workerObjects);
      const csv = Tables.export(tableData, TableFormat.Csv, "fi") as string;
      // console.log(csv);

      const workers = new Workers(null);
      workers.save =  (workerAccount: WorkerAccount): Promise<WorkerAccount> => {
        return Promise.resolve(workerAccount);
      };

      const batch = await ImportBatches.open(null, null, workerMapper, workers);
      // console.log(batch.id);

      await batch.read(csv, TableFormat.Csv, "fi");
      // console.log(batch.phase, batch.status);

      await batch.map();

      const notify = (msg, index) => {
        // console.log(msg, index);
      };

      await batch.validate(notify);
      // console.log(batch.phase, batch.status);

      await batch.import(notify);
      // console.log(batch.phase, batch.status);

      expect(batch.phase).equal("imported");
      expect(batch.status).equal("success");
    });

    it("Should use storage if passed", async () => {

      const workerObjects: WorkerAccount[] = [];
      const worker1 = WorkerLogic.getBlank();
      worker1.avatar.firstName = "John";
      worker1.avatar.lastName = "Doe";
      worker1.contact.street = "New Street 3";
      workerObjects.push(worker1);
      const worker2 = WorkerLogic.getBlank();
      worker2.avatar.firstName = "Jane";
      worker2.avatar.lastName = "Doe";
      worker2.contact.street = "Old Street 3";
      workerObjects.push(worker2);

      const workerMap: Map<WorkerAccount, any> = {
        firstName: "avatar.firstName",
        lastName: "avatar.lastName",
        street: "contact.street",
      };

      const workerMapper = new Mapper(workerMap, WorkerLogic.getBlank());
      const tableData = workerMapper.fromObjects(workerObjects);
      const csv = Tables.export(tableData, TableFormat.Csv, "fi") as string;

      const workers = new Workers(null);
      workers.save =  (workerAccount: WorkerAccount): Promise<WorkerAccount> => {
        return Promise.resolve(workerAccount);
      };

      const simpleStorage = new SimpleStorage();

      const batch = await ImportBatches.open(simpleStorage, null, workerMapper, workers);

      await batch.read(csv, TableFormat.Csv, "fi");

      await batch.map();

      await batch.validate();

      await batch.import();

      const batches1 = await ImportBatches.getAll(simpleStorage);
      // console.log(batches1);

      const batchCopy = await ImportBatches.open(simpleStorage, batch.id, workerMapper, workers);
      // console.log(batchCopy);

      batchCopy.id = null;
      await batchCopy.save();

      const batches2 = await ImportBatches.getAll(simpleStorage);

      expect(batches2.length, "Total number of batches is 2.").equal(2);
      // console.log(batches2);

      await ImportBatches.delete(simpleStorage, batch.id);
      await ImportBatches.delete(simpleStorage, batchCopy.id);

      const batches3 = await ImportBatches.getAll(simpleStorage);

      expect(batches3.length, "All batches are deleted.").equal(0);

    });
  });
});
