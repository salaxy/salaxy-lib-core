import chai from "chai";
import dirtyChai from "dirty-chai";

import {
    AppStatus,
    Arrays,
    Config,
    Role,
    RoleLogic,
    UserSession,
    SystemRole,
} from "@salaxy/core";

describe("Unit tests", () => {

  describe("Role membership tests", () => {
    const expect = chai.expect;
    chai.use(dirtyChai);

    it("Role tests, all roles", () => {
        const session: UserSession = {
            currentAccount: {
                identity: {
                    roles: [Role.Accountant, Role.TrustedPartner],
                },
            },
        };
        const appStatus: AppStatus = {
          sessionCheckInProgress: false,
          clientRoles: ["faa"],
          isTestData: true,
        };

        expect(RoleLogic.isInAllRoles(session, appStatus, [Role.Accountant, Role.TrustedPartner])).equal(true);
        expect(RoleLogic.isInAllRoles(session, appStatus, ["accountant", "trustedPartner"])).equal(true);
        expect(RoleLogic.isInAllRoles(session, appStatus, Arrays.assureArray("accountant,trustedPartner"))).equal(true);
        expect(RoleLogic.isInAllRoles(session, appStatus, null)).equal(true);
        expect(RoleLogic.isInAllRoles(session, appStatus, null)).equal(true);
        expect(RoleLogic.isInAllRoles(null, appStatus, Arrays.assureArray("foobar"))).equal(false);
        expect(RoleLogic.isInAllRoles(null, appStatus, Arrays.assureArray("faa"))).equal(true);

        expect(RoleLogic.isInSomeRole(session, appStatus, [Role.Accountant])).equal(true);
        expect(RoleLogic.isInSomeRole(session, appStatus, ["accountant"])).equal(true);
        expect(RoleLogic.isInSomeRole(session, appStatus, Arrays.assureArray("accountant"))).equal(true);
        expect(RoleLogic.isInSomeRole(session, appStatus, null)).equal(true);
        expect(RoleLogic.isInSomeRole(session, appStatus, null)).equal(true);
        expect(RoleLogic.isInSomeRole(null, appStatus, Arrays.assureArray("foobar"))).equal(false);
        expect(RoleLogic.isInSomeRole(null, appStatus, Arrays.assureArray("faa"))).equal(true);

    });

    it("Role tests, missing roles", () => {
        const session: UserSession = {
            currentAccount: {
                identity: {
                    roles: [Role.TrustedPartner],
                },
            },
        };

        const appStatus: AppStatus = {
          sessionCheckInProgress: false,
          clientRoles: [],
          isTestData: true,
        };

        expect(RoleLogic.isInAllRoles(session, appStatus, [Role.Accountant, Role.TrustedPartner])).equal(false);
        expect(RoleLogic.isInAllRoles(session, appStatus, ["accountant", "trustedPartner"])).equal(false);
        expect(RoleLogic.isInAllRoles(session, appStatus, Arrays.assureArray("accountant,trustedPartner"))).equal(false);

        expect(RoleLogic.isInSomeRole(session, appStatus, [Role.Accountant])).equal(false);
        expect(RoleLogic.isInSomeRole(session, appStatus, ["accountant"])).equal(false);
        expect(RoleLogic.isInSomeRole(session, appStatus, Arrays.assureArray("accountant"))).equal(false);

    });

    it("Role tests, is test data", () => {
      const session: UserSession = {
          currentAccount: {
              identity: {
                  roles: [],
              },
          },
      };
      const appStatus: AppStatus = {
        sessionCheckInProgress: false,
        clientRoles: [],
        isTestData: true,
      };

      expect(RoleLogic.isInSomeRole(session, appStatus, ["test"])).equal(true);
      expect(RoleLogic.isInSomeRole(session, appStatus, [SystemRole.Test])).equal(true);
      expect(RoleLogic.isInSomeRole(session, appStatus, ["!test"])).equal(false);
      expect(RoleLogic.isInSomeRole(session, appStatus, [SystemRole.Prod])).equal(false);
      expect(RoleLogic.isInSomeRole(session, appStatus, ["prod"])).equal(false);
      expect(RoleLogic.isInSomeRole(session, appStatus, ["!prod"])).equal(true);

      appStatus.isTestData = false;
      expect(RoleLogic.isInSomeRole(session, appStatus, ["test"])).equal(false);
      expect(RoleLogic.isInSomeRole(session, appStatus, [SystemRole.Test])).equal(false);
      expect(RoleLogic.isInSomeRole(session, appStatus, ["!test"])).equal(true);
      expect(RoleLogic.isInSomeRole(session, appStatus, [SystemRole.Prod])).equal(true);
      expect(RoleLogic.isInSomeRole(session, appStatus, ["prod"])).equal(true);
      expect(RoleLogic.isInSomeRole(session, appStatus, ["!prod"])).equal(false);

      appStatus.isTestData = null;
      expect(RoleLogic.isInSomeRole(session, appStatus, ["test"])).equal(false);
      expect(RoleLogic.isInSomeRole(session, appStatus, [SystemRole.Test])).equal(false);
      expect(RoleLogic.isInSomeRole(session, appStatus, ["!test"])).equal(true);
      expect(RoleLogic.isInSomeRole(session, appStatus, [SystemRole.Prod])).equal(true);
      expect(RoleLogic.isInSomeRole(session, appStatus, ["prod"])).equal(true);
      expect(RoleLogic.isInSomeRole(session, appStatus, ["!prod"])).equal(false);

  });

  });
});
