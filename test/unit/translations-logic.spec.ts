import chai from "chai";
import dirtyChai from "dirty-chai";

import {
    EnumerationsLogic,
    ReportsLogic,
    Translations,
    CalcRowsLogic,
} from "@salaxy/core";

describe("Unit tests", () => {
  describe("Translations, basic functionality", () => {

    const expect = chai.expect;
    chai.use(dirtyChai);

    it("CalculationStatus: Translations", () => {
        Translations.setLanguage("fi");
        let text = Translations.get("SALAXY.ENUM.CalculationStatus.draft.label");
        expect(text).equal("Luonnos");

        Translations.setLanguage("en");
        text = Translations.get("SALAXY.ENUM.CalculationStatus.draft.label");
        expect(text).equal("Draft");

        Translations.setLanguage("sv");
        text = Translations.get("SALAXY.ENUM.CalculationStatus.draft.label");
        expect(text).equal("Utkast");
    });

    it("CalculationStatus: EnumerationsLogic", () => {
        Translations.setLanguage("fi");
        let meta = EnumerationsLogic.getEnumMetadata("CalculationStatus");

        expect(meta.values.find((x) => x.name === "draft").label).equal("Luonnos");

        Translations.setLanguage("en");
        meta = EnumerationsLogic.getEnumMetadata("CalculationStatus");

        expect(meta.values.find((x) => x.name === "draft").label).equal("Draft");

        Translations.setLanguage("sv");
        meta = EnumerationsLogic.getEnumMetadata("CalculationStatus");

        expect(meta.values.find((x) => x.name === "draft").label).equal("Utkast");

    });

    it("CalculationRowType:  CalculatorLogic", () => {
        Translations.setLanguage("fi");
        let meta = new CalcRowsLogic("household").getRowConfigs();

        expect(meta.find((x) => x.name === "hourlySalary").label).equal("Tuntipalkka");

        Translations.setLanguage("en");
        meta = new CalcRowsLogic("household").getRowConfigs();

        expect(meta.find((x) => x.name === "hourlySalary").label).equal("Hourly salary");

        Translations.setLanguage("sv");
        meta = new CalcRowsLogic("household").getRowConfigs();

        expect(meta.find((x) => x.name === "hourlySalary").label).equal("Timlön");
    });

    it("Reports:  ReportsLogic", () => {
        Translations.setLanguage("fi");
        const reportTypes = ReportsLogic.getReportTypes();

        expect(reportTypes.find((x) => x.id === "insurance").title).equal("Vakuutus");
    });
  });
});
