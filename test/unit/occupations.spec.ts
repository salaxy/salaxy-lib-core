import chai from "chai";
import dirtyChai from "dirty-chai";

import {
    Occupations,
} from "@salaxy/core";

describe("Unit tests", () => {
  describe("Occupations Logic", () => {
    const expect = chai.expect;
    chai.use(dirtyChai);

    it("Get single test", () => {
      const data = Occupations.getByCode("11122");
      expect(data).to.not.null();
      expect(data.label)
        .equals("Alue- ja paikallishallinnon johtajat ja ylimmät virkamiehet", "");
    });

    it("GetById fetches by ID", () => {
      // NOTE: Doesn't work if empty spaces around ID's
      const data = Occupations.getById("53112-temp");
      expect(data).to.not.null();
      expect(data.label)
        .equals("Tilapäinen lastenhoito", "");
    });

    it("GetByIds fetches by multiple ID's", () => {
      // NOTE: gives error on expectations if a bigger numer (later found on occupation data array) is given first
      const data = Occupations.getByIds("53112-temp, 11121");
      expect(data).to.have.length(2);
      expect(data[1].label)
        .equals("Tilapäinen lastenhoito", "");
      expect(data[0].label)
        .equals("Valtion keskushallinnon johtajat", "");
    });

    it("GetById fetches by keyword household ", () => {
      // TODO test with occupations with both keywords as keywords
      const data = Occupations.getByIds("household");
      expect(data.length).to.be.greaterThan(10);
      expect(data.find((x) => x.id === "51530")).to.not.be.null();
    });

    it("GetById fetches by keyword company", () => {
      const data = Occupations.getByIds("company");
      expect(data.length).to.be.greaterThan(10);
      expect(data.find((x) => x.id === "11200")).to.not.be.null();
    });
    it("GetById fetches by both keywords as keywords ", () => {
      const data = Occupations.getByIds("company,household");
      expect(data.length).to.be.greaterThan(30);
      expect(data.find((x) => x.id === "51530")).to.not.be.null();
      expect(data.find((x) => x.id === "11200")).to.not.be.null();
    });

    it("GetById returns empty array if unknown keyword", () => {
      const data = Occupations.getByIds("worker");
      expect([]).to.be.empty();
    });

    it("GetById returns empty array if unknown ID's", () => {
      const data = Occupations.getByIds("22222,11111");
      expect([]).to.be.empty();
    });

    it("GetById returns all if search text is null or empty", () => {
      const data = Occupations.search("");
      expect(data.length).to.be.greaterThan(482);
      expect(data[0].label).equals("Upseerit", "");
    });

    it("Search test with hakusana 1", () => {
        const data = Occupations.search("mll");
        expect(data[0].search).includes("mll", "");

    });
  });
});
