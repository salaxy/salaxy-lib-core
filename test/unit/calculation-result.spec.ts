import chai from "chai";
import dirtyChai from "dirty-chai";

import {
    CalculationResult,
    CalculationResultLogic,
} from "@salaxy/core";

describe("Unit tests", () => {
  describe("CalculationResult calculations", () => {
    const expect = chai.expect;
    chai.use(dirtyChai);
    it("Should convert null and empty array to zero as well as accept null items", () => {

      const result1 = CalculationResultLogic.add([]);
      expect(result1.totals.total).equals(0);

      const result2 = CalculationResultLogic.add(null);
      expect(result2.totals.total).equals(0);

      const result3 = CalculationResultLogic.add([null]);
      expect(result3.totals.total).equals(0);

    });

    it("Should add simple totals together", () => {
      const calculationResult1: CalculationResult =  {
        totals: {
          total : 101,
        },
      };
      const calculationResult2: CalculationResult =  {
        totals: {
          total : 202,
        },
      };

      const result = CalculationResultLogic.add([calculationResult1, calculationResult2]);

      expect(result.totals.total).equals(101 + 202);
    });
  });
});
