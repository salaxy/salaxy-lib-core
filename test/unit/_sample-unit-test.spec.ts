// If you copy-paste, remove the comments (or use _template file).
// In real tests, there should never be comments:
// You should explain the expectation in describe(), it() and expect().

// Test framework import.
import chai from "chai";
import dirtyChai from "dirty-chai";
// For pure unit tests, we do not need Config or TestHelper.

// Import the classes, you want to test
import {
    CalculatorLogic, CalculationStatus,
} from "@salaxy/core";

// Add a describe() for each folder under tests to make a tree structure.
// Describe contents may be more verbose than the folder name.
describe("Unit tests", () => {

  // Describe the actual test set / group.
  describe("_Sample", () => {
    // Initialize the test framework
    const expect = chai.expect;
    chai.use(dirtyChai);
    // For pure unit tests, we do not need Config or TestHelper.

    // Describe your test - start with "it" if possible. "It" referes to the "describe" above.
    it("should show how to write unit tests.", () => {
      // Initialize your objects / do operations
      const calc = CalculatorLogic.getBlank();

      // Run expects, explain only if necessary (would not really be necessary in this example)
      expect(calc.workflow.status).equals(CalculationStatus.Draft, "Default status is Draft.");
      // Use dirty-chai to keep lint happy in these cases
      expect(calc.framework).to.not.be.null();
    });
  });
});
