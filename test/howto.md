Running and authoring tests in @salaxy/core project
===================================================

Running the unit tests
----------------------

Unit tests are located in two folders:

- `unit`: Pure unit tests testing the TypeScript logic.  
  - These tests do not connect to server and are very fast to run.
- `unit-api`: Unit tests for the API logic.
  - These tests should not write anything to the database, but they may be a bit slower than pure unit tests as they connect to the API.

The `unit-api` tests use connection to the server, so they need a config file. However, as they do not use authentication, you can simply copy the `test/config-template.txt` => `test/config.ts` and get started.

These tests are set as default in NPM, so you can run them simply by:

```
> npm test
```

Create a config file
--------------------

For `unit-api` and `system` tests, you must create a `test/config.ts` 
file that specifies how to connect to the server. 

For example (see `test/config-template.txt`):

```
import { TestHelperConfig } from "@salaxy/node";

export const config: TestHelperConfig = {
  server: "auto",
  username: "your.account@email.fi",
  password: "password_here",
};
```

For `unit-api` tests, the credentials are not used, but for `system` tests you also need real credentials to the server.

You can create your own account in https://test.palkkaus.fi. Typically, you want to create a company account (get an available Company id from https://www.ytj.fi). For digital signature, you may select any personal ID and any bank, then in the last screen, you are presented an option to bypass the bank authentication / signature.


Other ways of running the tests
-------------------------------

- `> npm test` runs all unit tests: `unit` and `unit-api` folders
- `> npm run test-path` runs tests based on a path
  - `> npm run test-path test/unit/getting-started/hello-samples.spec.ts` runs single test
  - You may also use wildcards glob: `> npm run test-path **/hello-samples.spec.ts`
- install Mocha sidebar: https://marketplace.visualstudio.com/items?itemName=maty.vscode-mocha-sidebar
- `> npm run test-system` runs all system tests
  - These will write and read to the database.
  - Typically, they should do a full clean-up, but this is not guaranteed if there are programming errors
  - Also system tests require authentication (valid account) and running them will typically take longer.
- `> npm run test-unit` runs all pure unit tests (`unit` folder)
- `> npm run test-api` runs all API unit tests (`unit-api` folder)
- `> npm run test-all` runs all tests
  - Even the ones in `disabled` and `misc` folder
  - Note that by design, some test sets in `misc` folder will throw errors

Currently the Mocha sidebar does not support debugging with ts-node/register and tsconfig-paths/register arguments
https://github.com/maty21/mocha-sidebar/blob/master/provider-extensions/runDebug.js

Creating examples and disabling tests
-------------------------------------

Usage examples / code demos are often best written usin unit test framework. In this case, 
you do not typically use expect/assert and these are not real tests that should be run in CI.
Adde examples to `/examples` folder.

Id you need to disable an individual test, you can simply mark it `it()` => `xit()` 
or `describe()` => `xdescribe()`. Additionally, you can move the disabled file to `disabled` folder,
so that it is potentially looked at at some point later.

Creating unit tests for @salaxy/core
------------------------------------

Pure unit tests for logic written in @salaxy/core project should be written in `test/unit` folder.
If you have several test files that are related, add a subfolder for them.

These tests should not connect to the server as they should only test the logic in @salaxy/core library.
As such they should not need config or TestHelper. See `test/unit/_sample-unit-test.spec.ts` 
for an up-to-date example of a proper unit test.

You can easily start writing a test by copy-pasting `test/unit/_template-unit-test.spec.ts`.

Use best normal practices for writing unit tests in Mocha / Chai / TypeScript.

Creating unit tests for Salaxy API
----------------------------------

API unit tests for API logic that can be tested without data storage should be written in `test/unit-api` folder.
If you have several test files that are related, add a subfolder for them.

These tests should not write anything to the database, but they may be a bit slower than pure unit tests as they connect to the API.
As they use connection to the server, they need a config file and typically use TestHelper.

Currently, none of the API tests use authentication, which has the advantage of not needing the credentials in the config file:
you can simply copy the `config-template.txt` as `config.ts` and get started.
However, we may reconsider this later: If we had more injectable API, 
we could run some tests that require authentication, but not storage.

See `test/unit-api/_sample-api-test.spec.ts` for an up-to-date example of a proper API test.

You can easily start writing a test by copy-pasting `test/unit-api/_template-api-test.spec.ts`.

Creating System tests
----------------------------------

System tests for e.g. API CRUD methods should be written in `test/system` folder.
If you have several test files that are related, add a subfolder for them.

System tests are tests that require reading and writing to/from the data storage:
By definition they have side effects and also they require a valid user credentials.
Also, they are typically slower than the unit tests.

Even for system tests, we try to avoid lasting side effects by doing clean-up:
At the end of the test the test code should remove all the data that it added.
This is so that the system tests can be run repeatedly - e.g. every night.

You can easily start writing a system test by copy-pasting `test/system/crud/_template-CRUD.spec.ts`.

TODO: Further documentation and perhaps a separate template for plain system tests.
