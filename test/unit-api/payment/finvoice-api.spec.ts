import {
  Calculation,
  CalculationRowType,
  CalculatorLogic,
  SalaryKind,
} from "@salaxy/core";
import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../../config";

import {
  TestHelper,
} from "@salaxy/node";

xdescribe("API tests", () => {
  xdescribe("Payment: Finvoice api", function () {

    const expect = chai.expect;
    chai.use(dirtyChai);

    const sxyHelper = new TestHelper(config, this);

    // before("authenticate, will be removed eventually", async () => {
    //    await sxyHelper.authenticate();
    // });

    // npm run test-path "test/unit-api/payment/finvoice-api.spec.ts"

    it("Fetches payment object for given calculations", async () => {
      const calc = CalculatorLogic.getBlank();
      calc.worker.accountId = "example-default";
      calc.salary = {
        price: 100,
        kind: SalaryKind.FixedSalary,
      };
      calc.rows.push({
        message: "Expenses should not affect Unemployment insurance payments.",
        price: 20,
        rowType: CalculationRowType.Expenses,
      });
      calc.workflow.paidAt = "2018-08-01";

      const finvoice = await sxyHelper.api.calculator.getFinvoice([calc]);

      /* eslint-disable-next-line */
      console.log(JSON.stringify(finvoice));

      expect(finvoice).to.not.be.null();
    });
  });
});
