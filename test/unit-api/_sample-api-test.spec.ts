// If you copy-paste, remove the comments (or use _template file).
// In real tests, there should never be comments:
// You should explain the expectation in describe(), it() and expect().

// Test framework and configuration import.
import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../config";

// Import the classes, you want to test
// Typically, also the TestHelper
import {
  Calculation,
  SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

// Add a describe() for each folder under tests to make a tree structure.
// Describe contents may be more verbose than the folder name.
describe("API tests", () => {

  // Describe the actual test set / group.
  // NOTE: You have to use "function()" and not "() =>" here (https://mochajs.org/#arrow-functions)
  //       if you want "this" to refer to suite callback context in "TestHelper(config, this)" below.
  describe("_Sample", function() {
    // Initialize the test framework
    const expect = chai.expect;
    chai.use(dirtyChai);

    // Initialize the test helper (see describe.this note above).
    const sxyHelper = new TestHelper(config, this);

    // Describe your test
    it("should show how to write API tests.", async () => {
      // Inititialize your test data - you may also have shared veriables/functions directly under describe()
      const calculation: Calculation = {
        salary: {
          amount: 5,
          price: 6,
          kind: SalaryKind.FixedSalary,
        },
      };

      // Make the API call(s) and await
      // DO NOT USE METHODS THAT UPDATE DATA!
      const resultCalculation = await sxyHelper.api.calculator.recalculate(calculation);

      // Run expects

      // Add the property name to message if there are several expects - this ...
      expect(resultCalculation.result.totals.totalGrossSalary, "totalGrossSalary")
        .equal(30);
      // ...or real message if there is something to that may not be immediately clear.
      expect(resultCalculation.result.totals.totalSocialSecurityBase, "Longer explanation of what we are testing and why.")
        .equal(30);
      // Use dirty-chai to keep lint happy in these cases
      expect(resultCalculation.result).to.not.be.null();
    });
  });

});
