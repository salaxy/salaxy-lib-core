
import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../../config";

import {
  Calculation,
  DateOfBirthAccuracy,
  SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

describe("API tests", () => {
  describe("Calculator: Pension (TyEL) calculation", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    it("Age 15, no pension", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        worker: {
          dateOfBirth: new Date(2003, 1, 2) as any, // Date of birth is 2nd of Feb
          dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
        },
        workflow: {
          paidAt: new Date(2018, 2, 3) as any, // Payment date is 3rd of March
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation).then((resultCalculation: Calculation) => {
        expect(resultCalculation.result.totals.totalGrossSalary).equal(
          calculation.salary.amount * calculation.salary.price);
        expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age_15");
        expect(resultCalculation.result.employerCalc.pension).equal(0);
        expect(resultCalculation.result.workerCalc.pension).equal(0);
      });
    });

    it("Age 16, no pension", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        worker: {
          dateOfBirth: new Date(2002, 1, 2) as any, // Date of birth is 2nd of Feb
          dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
        },
        workflow: {
          paidAt: new Date(2018, 2, 3) as any, // Payment date is 3rd of March
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation).then((resultCalculation: Calculation) => {
        expect(resultCalculation.result.totals.totalGrossSalary).equal(
          calculation.salary.amount * calculation.salary.price);
        expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age16");
        expect(resultCalculation.result.employerCalc.pension).equal(0);
        expect(resultCalculation.result.workerCalc.pension).equal(0);
      });
    });

    it("Age 17, worker pension percent 6.35", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        worker: {
          dateOfBirth: new Date(2001, 1, 2) as any, // Date of birth is 2nd of Feb
          dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
        },
        workflow: {
          paidAt: new Date(2018, 2, 3) as any, // Payment date is 3rd of March
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation).then((resultCalculation: Calculation) => {
        expect(resultCalculation.result.totals.totalGrossSalary).equal(
          calculation.salary.amount * calculation.salary.price);
        expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age17");
        expect(resultCalculation.result.workerCalc.pension).equal(6.35);
        expect(resultCalculation.result.employerCalc.pension).equal(18.95);
      });
    });

    it("Age 52, worker pension percent 6.35", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        worker: {
          dateOfBirth: new Date(1966, 1, 2) as any, // Date of birth is 2nd of Feb
          dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
        },
        workflow: {
          paidAt: new Date(2018, 2, 3) as any, // Payment date is 3rd of March
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation).then((resultCalculation: Calculation) => {
        expect(resultCalculation.result.totals.totalGrossSalary).equal(
          calculation.salary.amount * calculation.salary.price);
        expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age18_52");
        expect(resultCalculation.result.workerCalc.pension).equal(6.35);
        expect(resultCalculation.result.employerCalc.pension).equal(18.95);
      });
    });

    it("Age 53, worker pension percent 7.85", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        worker: {
          dateOfBirth: new Date(1965, 1, 2) as any, // Date of birth is 2nd of Feb
          dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
        },
        workflow: {
          paidAt: new Date(2018, 2, 3) as any, // Payment date is 3rd of March
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation).then((resultCalculation: Calculation) => {
        expect(resultCalculation.result.totals.totalGrossSalary).equal(
          calculation.salary.amount * calculation.salary.price);
        expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age53_62");
        expect(resultCalculation.result.workerCalc.pension).equal(7.85);
        expect(resultCalculation.result.employerCalc.pension).equal(17.45);
      });
    });

    it("Age 62, worker pension percent 7.85", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        worker: {
          dateOfBirth: new Date(1956, 1, 2) as any, // Date of birth is 2nd of Feb
          dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
        },
        workflow: {
          paidAt: new Date(2018, 2, 3) as any, // Payment date is 3rd of March
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation).then((resultCalculation: Calculation) => {
        expect(resultCalculation.result.totals.totalGrossSalary).equal(
          calculation.salary.amount * calculation.salary.price);
        expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age53_62");
        expect(resultCalculation.result.workerCalc.pension).equal(7.85);
        expect(resultCalculation.result.employerCalc.pension).equal(17.45);
      });
    });

    it("Age 63, worker pension percent 6.35", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        worker: {
          dateOfBirth: new Date(1955, 1, 2) as any, // Date of birth is 2nd of Feb
          dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
        },
        workflow: {
          paidAt: new Date(2018, 2, 3) as any, // Payment date is 3rd of March
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation).then((resultCalculation: Calculation) => {
        expect(resultCalculation.result.totals.totalGrossSalary).equal(
          calculation.salary.amount * calculation.salary.price);
        expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age63_64");
        expect(resultCalculation.result.workerCalc.pension).equal(6.35);
        expect(resultCalculation.result.employerCalc.pension).equal(18.95);
      });
    });

    it("Age 67, worker pension percent 6.35", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        worker: {
          dateOfBirth: new Date(1951, 1, 2) as any, // Date of birth is 2nd of Feb
          dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
        },
        workflow: {
          paidAt: new Date(2018, 2, 3) as any, // Payment date is 3rd of March
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation)
        .then((resultCalculation: Calculation) => {
          expect(resultCalculation.result.totals.totalGrossSalary).equal(
            calculation.salary.amount * calculation.salary.price);
          expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age65_67");
          expect(resultCalculation.result.workerCalc.pension).equal(6.35);
          expect(resultCalculation.result.employerCalc.pension).equal(18.95);
        });
    });

    it("Age 68, worker pension percent 0", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        worker: {
          dateOfBirth: new Date(1950, 1, 2) as any, // Date of birth is 2nd of Feb
          dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
        },
        workflow: {
          paidAt: new Date(2018, 2, 3) as any, // Payment date is 3rd of March
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation)
        .then((resultCalculation: Calculation) => {
          expect(resultCalculation.result.totals.totalGrossSalary).equal(
            calculation.salary.amount * calculation.salary.price);
          expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age68AndOVer");
          expect(resultCalculation.result.workerCalc.pension).equal(0);
          expect(resultCalculation.result.employerCalc.pension).equal(0);
        });
    });
  });
});
