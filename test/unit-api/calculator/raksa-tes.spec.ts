import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../../config";

import {
  Calculation,
  CalculationRowType,
  SalaryKind,
  UnionPaymentType,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

describe("API tests", () => {
  describe("Calculator: Raksa TES", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    it("Union membership payment, normal", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
      };
      return sxyHelper.api.calculator.setUnionPayment(calculation, UnionPaymentType.RaksaNormal, 0.0)
        .then((resultCalculation: Calculation) => {
          expect(resultCalculation.result.workerCalc.unionPayment).equal(1.49);
        });
    });

    it("Union membership payment, unemployed rate", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
      };
      return sxyHelper.api.calculator.setUnionPayment(calculation, UnionPaymentType.RaksaUnemploymentOnly, 0.0)
        .then((resultCalculation: Calculation) => {
          expect(resultCalculation.result.workerCalc.unionPayment).equal(0.52);
        });
    });

    it("Daily expenses", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        framework: {
          numberOfDays: 5,
          dailyExpenses: 6,
        },
      };
      return sxyHelper.api.calculator.addRow(calculation, CalculationRowType.Expenses, "Matkakustannukset",
        calculation.framework.dailyExpenses, calculation.framework.numberOfDays)
        .then(
          (resultCalculation: Calculation) => {
            expect(resultCalculation.result.rows[1].total).equal(30);
            expect(resultCalculation.result.rows[1].count).equal(5);
            expect(resultCalculation.result.rows[1].price).equal(6);
            expect(resultCalculation.result.rows[1].rowType).equal("expenses");
            expect(resultCalculation.result.rows[1].rowSource).equal("salaryPage");
          });
    });

    it("Daily travel expenses", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        framework: {
          numberOfDays: 5,
          dailyTravelExpenses: 6,
        },
      };
      return sxyHelper.api.calculator.addRow(calculation, CalculationRowType.MilageDaily, "Matkakustannukset",
        calculation.framework.dailyTravelExpenses, calculation.framework.numberOfDays)
        .then((resultCalculation: Calculation) => {
          expect(resultCalculation.result.rows[1].total).equal(30);
          expect(resultCalculation.result.rows[1].count).equal(5);
          expect(resultCalculation.result.rows[1].price).equal(6);
          expect(resultCalculation.result.rows[1].rowType).equal("milageDaily");
          expect(resultCalculation.result.rows[1].rowSource).equal("salaryPage");
        });
    });

    it("Daily travel expenses km", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        framework: {
          numberOfDays: 5,
          dailyTravelExpensesKm: 6,
        },
      };
      return sxyHelper.api.calculator.addRow(calculation, CalculationRowType.Expenses, "Matkakustannukset",
        calculation.framework.dailyTravelExpensesKm * 0.41, calculation.framework.numberOfDays)
        .then((resultCalculation: Calculation) => {
          expect(resultCalculation.result.rows[1].total).equal(12.3);
          expect(resultCalculation.result.rows[1].count).equal(5);
          expect(resultCalculation.result.rows[1].price).equal(2.46);
          expect(resultCalculation.result.rows[1].rowType).equal("expenses");
          expect(resultCalculation.result.rows[1].rowSource).equal("salaryPage");
        });
    });

    it("Tool compensation", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        framework: {
          numberOfDays: 5,
          dailyExpenses: 1.01,
        },
      };
      return sxyHelper.api.calculator.addRow(calculation, CalculationRowType.ToolCompensation, "Työkalukorvaus",
        calculation.framework.dailyExpenses, calculation.framework.numberOfDays,
      ).then((resultCalculation: Calculation) => {
        expect(resultCalculation.result.rows[1].total).equal(5.05);
        expect(resultCalculation.result.rows[1].count).equal(5);
        expect(resultCalculation.result.rows[1].price).equal(1.01);
        expect(resultCalculation.result.rows[1].rowType).equal("toolCompensation");
        expect(resultCalculation.result.rows[1].rowSource).equal("salaryPage");
      });
    });

  });
});
