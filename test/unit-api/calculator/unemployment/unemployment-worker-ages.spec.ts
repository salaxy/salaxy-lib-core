import chai from "chai";
import chaiAlmost from "chai-almost";
import dirtyChai from "dirty-chai";
import { config } from "../../../config";

import {
  Calculation,
  DateOfBirthAccuracy,
  SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

import { UnemploymentTestHelpers } from "./UnemploymentTestHelpers";

describe("API tests", () => {
  describe("Calculator - Unemployment Insurance (TVR), Worker age limits logic", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    chai.use(chaiAlmost(0.02));
    const sxyHelper = new TestHelper(config, this);
    const tvrHelper = new UnemploymentTestHelpers(sxyHelper.api.calculator.getYearlyChangingNumbers("today"));

    it("calculates no Payment for Age 16", async () => {
      const input = tvrHelper.getCalculation(null);
      input.worker = {
        dateOfBirth: "2002-02-02",
        dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
      };
      input.workflow.paidAt = "2018-03-03";

      const resultCalculation = await sxyHelper.api.calculator.recalculate(input);

      expect(resultCalculation.result.totals.totalGrossSalary).equal(100);
      expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age16");
      expect(resultCalculation.result.employerCalc.unemployment).equal(0);
      expect(resultCalculation.result.workerCalc.unemploymentInsurance).equal(0);
    });

    it("calculates payment 0.65 (empl) & 1.90 (worker) for Age 17", async () => {
      const input = tvrHelper.getCalculation(null);
      input.worker = {
        dateOfBirth: "2001-02-02",
        dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
      };
      input.workflow.paidAt = "2018-03-03";

      const resultCalculation = await sxyHelper.api.calculator.recalculate(input);

      expect(resultCalculation.result.totals.totalGrossSalary).equal(100);
      expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age17");
      expect(resultCalculation.result.employerCalc.unemployment).equal(0.65);
      expect(resultCalculation.result.workerCalc.unemploymentInsurance).equal(1.9);
    });

    it("calculates payment 0.65(empl) & 1.9 (worker) for Age 52", async () => {
      const input = tvrHelper.getCalculation(null);
      input.worker = {
        dateOfBirth: "1966-02-02",
        dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
      };
      input.workflow.paidAt = "2018-03-03";

      const resultCalculation = await sxyHelper.api.calculator.recalculate(input);

      expect(resultCalculation.result.totals.totalGrossSalary).equal(100);
      expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age18_52");
      expect(resultCalculation.result.employerCalc.unemployment).equal(0.65);
      expect(resultCalculation.result.workerCalc.unemploymentInsurance).equal(1.9);
    });

    it("calculates payment 0.65 (empl) & 1.90 (worker) for Age 64", async () => {
      const input = tvrHelper.getCalculation(null);
      input.worker = {
        dateOfBirth: "1954-02-02",
        dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
      };
      input.workflow.paidAt = "2018-03-03";

      const resultCalculation = await sxyHelper.api.calculator.recalculate(input);

      expect(resultCalculation.result.totals.totalGrossSalary).equal(100);
      expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age63_64");
      expect(resultCalculation.result.employerCalc.unemployment).equal(0.65);
      expect(resultCalculation.result.workerCalc.unemploymentInsurance).equal(1.9);
    });

    it("calculates no payment for Age 65", async () => {
      const input = tvrHelper.getCalculation(null);
      input.worker = {
        dateOfBirth: "1953-02-02",
        dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
      };
      input.workflow.paidAt = "2018-03-03";

      const resultCalculation = await sxyHelper.api.calculator.recalculate(input);

      expect(resultCalculation.result.totals.totalGrossSalary).equal(100);
      expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age65_67");
      expect(resultCalculation.result.employerCalc.unemployment).equal(0);
      expect(resultCalculation.result.workerCalc.unemploymentInsurance).equal(0);
    });
  });
});
