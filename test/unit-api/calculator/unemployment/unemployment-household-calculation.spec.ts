import chai from "chai";
import chaiAlmost from "chai-almost";
import dirtyChai from "dirty-chai";
import { config } from "../../../config";

import {
  LegalEntityType, Numeric,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

import { UnemploymentTestHelpers } from "./UnemploymentTestHelpers";

describe("API tests", () => {
  describe("Calculator - Unemployment Insurance (TVR), HOUSEHOLD", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    chai.use(chaiAlmost(0.02));
    const sxyHelper = new TestHelper(config, this);
    const tvrHelper = new UnemploymentTestHelpers(sxyHelper.api.calculator.getYearlyChangingNumbers("today"));

    // TODO: Should we check these and if so how? Should we add recalculateExt()?

    // it("Employer should have TVR responsibility");
    // Responsibilities.NoTvr=false
    // it("Unemployment percentages have correct values");
    // UnemploymentEmployer, UnemploymentWorker, UnemploymentFull
    // it("EmploymentGroups.IsPrivatePerson has correct value");
    // EmployerGroups.IsPrivatePerson

    it("should be able to check that the Employer is a Person after recalculation.", async () => {
      const input = tvrHelper.getCalculation(false);
      const target = await sxyHelper.api.calculator.recalculate(input);

      expect(target.employer.avatar.entityType).to.equal(LegalEntityType.Person);
    });

    it("should deduct payments from Worker", async () => {
      const input = tvrHelper.getCalculation(false);
      const target = await sxyHelper.api.calculator.recalculate(input);

      expect(target.result).to.not.be.null();
      expect(target.result.workerCalc.unemploymentInsurance, "unemploymentInsurance").to.equal(tvrHelper.refAmounts.worker);
      expect(target.result.workerCalc.fullUnemploymentInsurance, "fullUnemploymentInsurance").to.equal(tvrHelper.refAmounts.worker);

      const expectedSideCosts = target.result.workerCalc.pension + tvrHelper.refAmounts.worker;
      expect(target.result.workerCalc.workerSideCosts, "workerSideCosts").to.equal(expectedSideCosts);

      const expectedSalaryPayment = 100 - expectedSideCosts - target.result.workerCalc.tax;
      expect(target.result.workerCalc.salaryPayment, "salaryPayment").to.equal(expectedSalaryPayment);
      expect(target.result.workerCalc.totalWorkerPayment, "totalWorkerPayment").to.equal(expectedSalaryPayment + 20);
    });

    it("should add payments to totals", async () => {
      const input = tvrHelper.getCalculation(false);
      const target = await sxyHelper.api.calculator.recalculate(input);

      expect(target.result).to.not.be.null();
      expect(target.result.totals.total).to.equal(120);
      expect(target.result.totals.totalGrossSalary).to.equal(100);
      expect(target.result.totals.totalSocialSecurityBase).to.equal(100);
      expect(target.result.totals.totalExpenses).to.equal(20);
      expect(target.result.totals.totalBaseSalary).to.equal(100);
      expect(target.result.totals.totalTaxable).to.equal(100);
      expect(target.result.totals.unemployment).to.equal(tvrHelper.refAmounts.total);
    });

    it("adds payment to Employer", async () => {
      const input = tvrHelper.getCalculation(false);
      const target = await sxyHelper.api.calculator.recalculate(input);

      expect(target.result).to.not.be.null();
      expect(target.result.employerCalc.unemployment).to.equal(tvrHelper.refAmounts.employer);
    });

    it("adds payment as deduction", async () => {
      const input = tvrHelper.getCalculation(false);
      const target = await sxyHelper.api.calculator.recalculate(input);

      expect((target.result.employerCalc as any).deductionUnemploymentSelfPayment).to.not.be.null("deductionUnemploymentSelfPayment null implies an old version");
      expect((target.result.employerCalc as any).deductionUnemploymentSelfPayment).to.equal(tvrHelper.refAmounts.total);
      expect(target.result.employerCalc.totalDeductions).to.equal(tvrHelper.refAmounts.total);
    });

    it("adds Unemployment payment to Employer side costs", async () => {
      const input = tvrHelper.getCalculation(false);
      const target = await sxyHelper.api.calculator.recalculate(input);

      const mandatorySideCosts = target.result.employerCalc.pension
        + target.result.employerCalc.socialSecurity
        + tvrHelper.refAmounts.employer;
      expect(target.result.employerCalc.mandatorySideCosts).to.be.be.almost(mandatorySideCosts);
      expect(target.result.employerCalc.allSideCosts).to
        .be.almost(mandatorySideCosts + target.result.employerCalc.palkkaus);
      expect(target.result.employerCalc.householdDeduction).to.equal(0);

      const totalSalaryCost = target.result.workerCalc.salaryPayment
        + target.result.workerCalc.deductions
        + mandatorySideCosts
        + target.result.employerCalc.palkkaus;
      expect(target.result.employerCalc.totalSalaryCost).to.be.almost(totalSalaryCost);

      const totalPayment = totalSalaryCost + 20 - tvrHelper.refAmounts.total;
      expect(target.result.employerCalc.totalPayment).to.be.almost(totalPayment);
      expect(target.result.employerCalc.totalDeductions).to.equal(tvrHelper.refAmounts.total);
      expect(target.result.employerCalc.deductionUnemploymentSelfPayment).to.equal(tvrHelper.refAmounts.total);
      expect(target.result.employerCalc.finalCost).to.be.almost(totalSalaryCost + 20);

    });

    it("adds Unemployment payment to Household deduction.", async () => {
      const input = tvrHelper.getCalculation(false);
      input.salary.isHouseholdDeductible = true;
      const target = await sxyHelper.api.calculator.recalculate(input);

      // Pension + SocialSecurity + Unemployment
      const mandatorySideCosts = target.result.employerCalc.pension
        + target.result.employerCalc.socialSecurity
        + tvrHelper.refAmounts.employer;
      expect(target.result.employerCalc.mandatorySideCosts).to.be.be.almost(mandatorySideCosts);

      const householdDeduction = mandatorySideCosts + tvrHelper.refAmounts.householdDeductionPercent * 100;
      expect(target.result.employerCalc.householdDeduction).to.be.almost(householdDeduction);

      const totalSalaryCost = target.result.workerCalc.salaryPayment
        + target.result.workerCalc.deductions
        + mandatorySideCosts
        + target.result.employerCalc.palkkaus;
      expect(target.result.employerCalc.totalSalaryCost).to.be.almost(totalSalaryCost);

      const totalPayment = totalSalaryCost + 20 - tvrHelper.refAmounts.total;
      expect(target.result.employerCalc.totalPayment).to.be.almost(totalPayment);
      expect(target.result.employerCalc.totalDeductions).to.equal(tvrHelper.refAmounts.total);
      expect(target.result.employerCalc.deductionUnemploymentSelfPayment).to.equal(tvrHelper.refAmounts.total);
      const finalCost = totalPayment
        + tvrHelper.refAmounts.total // TotalDeductions
        - householdDeduction;
      expect(target.result.employerCalc.finalCost).to.be.almost(finalCost);

    });

    // TODO: Implement before going to production with new TVR

    // it("Worker Unemployment payment may be less than Unemployment payment if there is not enough money");
    // WorkerCalc.UnemploymentInsurance vs FullUnemploymentInsurance
    // it("If not enough money, Unemployment deduction comes after SalaryAfterTaxAndForeclosure - SalaryAdvance - FullPension");
    // it("If not enough money, Employer pays workers part.");
  });
});
