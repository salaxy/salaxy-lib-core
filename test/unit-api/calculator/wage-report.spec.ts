
/*
import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../../config";

import {
  Calculation,
  DateOfBirthAccuracy,
  SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

describe("API tests", () => {
  describe("Calculator: Wage report", () => {
    describe("Worker age", function () {
      const expect = chai.expect;
      chai.use(dirtyChai);
      const sxyHelper = new TestHelper(config, this);

      it("Age 15, should contain exceptions to health, unemployment and pension insurances", async () => {
        const calculation: Calculation = {
          salary: {
            amount: 10,
            price: 10,
            kind: SalaryKind.FixedSalary,
          },
          worker: {
            dateOfBirth: "2003-02-02", // Date of birth is 2nd of Feb
            dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
          },
          workflow: {
            paidAt: "2018-03-02", // Payment date is 3rd of March
          },
        };
        const wageReport = await sxyHelper.api.calculator.getEarningsPayment(calculation);
        expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).contains("noObligationToHealthInsurance");
        expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).contains("noObligationToUnemploymentInsurance");
        expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).contains("noObligationToEarningsRelatedPensionInsurance");
      });

      it("Age 16, should contain exceptions to unemployment and pension insurances", async () => {
        const calculation: Calculation = {
          salary: {
            amount: 10,
            price: 10,
            kind: SalaryKind.FixedSalary,
          },
          worker: {
            dateOfBirth: "2002-02-02", // Date of birth is 2nd of Feb
            dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
          },
          workflow: {
            paidAt: "2018-03-02", // Payment date is 3rd of March
          },
        };
        const wageReport = await sxyHelper.api.calculator.getEarningsPayment(calculation);
        expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).not.contain("noObligationToHealthInsurance");
        expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).contains("noObligationToUnemploymentInsurance");
        expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).contains("noObligationToEarningsRelatedPensionInsurance");
      });

      it("Age 17, shoud contain no age related exceptions to insurances", async () => {
        const calculation: Calculation = {
          salary: {
            amount: 10,
            price: 10,
            kind: SalaryKind.FixedSalary,
          },
          worker: {
            dateOfBirth: "2001-02-02", // Date of birth is 2nd of Feb
            dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
          },
          workflow: {
            paidAt: "2018-03-02", // Payment date is 3rd of March
          },
        };
        const wageReport = await sxyHelper.api.calculator.getEarningsPayment(calculation);
        expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).to.be.null();
        // Alternatively:
        // expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).not.contain("noObligationToHealthInsurance");
        // expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).not.contain("noObligationToUnemploymentInsurance");
        // expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).not.contain("noObligationToEarningsRelatedPensionInsurance");
      });

      it("Age 52, shoud contain no age related exceptions to insurances", async () => {
        const calculation: Calculation = {
          salary: {
            amount: 10,
            price: 10,
            kind: SalaryKind.FixedSalary,
          },
          worker: {
            dateOfBirth: "1966-02-02", // Date of birth is 2nd of Feb
            dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
          },
          workflow: {
            paidAt: "2018-03-02", // Payment date is 3rd of March
          },
        };
        const wageReport = await sxyHelper.api.calculator.getEarningsPayment(calculation);
        expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).to.be.null();
        // Alternatively:
        // expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).not.contain("noObligationToHealthInsurance");
        // expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).not.contain("noObligationToUnemploymentInsurance");
        // expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).not.contain("noObligationToEarningsRelatedPensionInsurance");
      });

      it("Age 65, should contain exceptions to unemployment insurances", async () => {
        const calculation: Calculation = {
          salary: {
            amount: 10,
            price: 10,
            kind: SalaryKind.FixedSalary,
          },
          worker: {
            dateOfBirth: "1953-02-02", // Date of birth is 2nd of Feb
            dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
          },
          workflow: {
            paidAt: "2018-03-02", // Payment date is 3rd of March
          },
        };
        const wageReport = await sxyHelper.api.calculator.getEarningsPayment(calculation);
        expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).not.contain("noObligationToHealthInsurance");
        expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).contains("noObligationToUnemploymentInsurance");
        expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).not.contain("noObligationToEarningsRelatedPensionInsurance");
      });
      it("Age 68, should contain exceptions to health, unemployment and pension insurances", async () => {
        const calculation: Calculation = {
          salary: {
            amount: 10,
            price: 10,
            kind: SalaryKind.FixedSalary,
          },
          worker: {
            dateOfBirth: "1950-02-02", // Date of birth is 2nd of Feb
            dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
          },
          workflow: {
            paidAt: "2018-03-02", // Payment date is 3rd of March
          },
        };
        const wageReport = await sxyHelper.api.calculator.getEarningsPayment(calculation);
        expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).contains("noObligationToHealthInsurance");
        expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).contains("noObligationToUnemploymentInsurance");
        expect(wageReport.deliveryData.reports[0].incomeEarner.insuranceExceptions).contains("noObligationToEarningsRelatedPensionInsurance");
      });
    });
  });
});
*/