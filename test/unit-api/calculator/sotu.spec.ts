import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../../config";

import {
  Calculation,
  DateOfBirthAccuracy,
  SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

describe("API tests", () => {
  describe("Calculator: Social security payment (SOTU)", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    it("Age 15, no Sotu", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        worker: {
          dateOfBirth: new Date(2003, 1, 2) as any, // Date of birth is 2nd of Feb
          dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
        },
        workflow: {
          paidAt: new Date(2018, 2, 3) as any, // Payment date is 3rd of March
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation)
        .then((resultCalculation: Calculation) => {
          expect(resultCalculation.result.totals.totalGrossSalary).equal(
            calculation.salary.amount * calculation.salary.price);
          expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age_15");
          expect(resultCalculation.result.employerCalc.socialSecurity).equal(0);
        });
    });

    it("Age 16, Sotu 0.86", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        worker: {
          dateOfBirth: new Date(2002, 1, 2) as any, // Date of birth is 2nd of Feb
          dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
        },
        workflow: {
          paidAt: new Date(2018, 2, 3) as any, // Payment date is 3rd of March
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation)
        .then((resultCalculation: Calculation) => {
          expect(resultCalculation.result.totals.totalGrossSalary).equal(
            calculation.salary.amount * calculation.salary.price);
          expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age16");
          expect(resultCalculation.result.employerCalc.socialSecurity).equal(0.86);
        });
    });

    it("Age 52, Sotu 0.86", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        worker: {
          dateOfBirth: new Date(1966, 1, 2) as any, // Date of birth is 2nd of Feb
          dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
        },
        workflow: {
          paidAt: new Date(2018, 2, 3) as any, // Payment date is 3rd of March
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation)
        .then((resultCalculation: Calculation) => {
          expect(resultCalculation.result.totals.totalGrossSalary).equal(
            calculation.salary.amount * calculation.salary.price);
          expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age18_52");
          expect(resultCalculation.result.employerCalc.socialSecurity).equal(0.86);
        });
    });

    it("Age 67, Sotu 0.86", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        worker: {
          dateOfBirth: new Date(1951, 1, 2) as any, // Date of birth is 2nd of Feb
          dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
        },
        workflow: {
          paidAt: new Date(2018, 2, 3) as any, // Payment date is 3rd of March
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation)
        .then((resultCalculation: Calculation) => {
          expect(resultCalculation.result.totals.totalGrossSalary).equal(
            calculation.salary.amount * calculation.salary.price);
          expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age65_67");
          expect(resultCalculation.result.employerCalc.socialSecurity).equal(0.86);
        });
    });

    it("Age 68, no Sotu", () => {
      const calculation: Calculation = {
        salary: {
          amount: 10,
          price: 10,
          kind: SalaryKind.FixedSalary,
        },
        worker: {
          dateOfBirth: new Date(1950, 1, 2) as any, // Date of birth is 2nd of Feb
          dateOfBirthAccuracy: DateOfBirthAccuracy.Exact,
        },
        workflow: {
          paidAt: new Date(2018, 2, 3) as any, // Payment date is 3rd of March
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation)
        .then((resultCalculation: Calculation) => {
          expect(resultCalculation.result.totals.totalGrossSalary).equal(
            calculation.salary.amount * calculation.salary.price);
          expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age68AndOVer");
          expect(resultCalculation.result.employerCalc.socialSecurity).equal(0);
        });
    });

  });
});
