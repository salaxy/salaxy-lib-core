import chai from "chai";
import chaiAlmost from "chai-almost";
import dirtyChai from "dirty-chai";
import { config } from "../../config";

import {
  Dates,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

describe("API tests", () => {
  describe("Calculator: Yearly changing numbers", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    chai.use(chaiAlmost(0.02));

    const sxyHelper = new TestHelper(config, this);

    it("should always fetch the numbers for the current year.", async () => {
      const target = sxyHelper.api.calculator.getYearlyChangingNumbers(Dates.getToday());
      expect(target).to.not.be.empty();
      expect(target.sideCosts.taxFreeDailyHalfAllowance).to.equal(20);
    });

    it("should fetch the numbers for the previous year if today is Jan-Nov.", async () => {
      if (Dates.getMonth("today") < 12) {
        const thisYear = Dates.getYear("today");
        const target = sxyHelper.api.calculator.getYearlyChangingNumbers((thisYear - 1) + "-06-01");
        expect(target).to.not.be.empty();
        expect(target.sideCosts.taxFreeDailyHalfAllowance).to.equal(19);
      }
    });

    it("should fetch the numbers for the next year if today is December.", async () => {
      if (Dates.getMonth("today") === 12) {
        const thisYear = Dates.getYear("today");
        const target = sxyHelper.api.calculator.getYearlyChangingNumbers((thisYear + 1) + "-06-01");
        expect(target).to.not.be.empty();
        expect(target.sideCosts.taxFreeDailyHalfAllowance).to.equal(19);
      }
    });

    it("should fail with message \"Year 2021 not supported.\" for year 2021 (until implemented in Dec 2020).", async () => {
      try {
        const target = sxyHelper.api.calculator.getYearlyChangingNumbers("2021-01-01");
        expect("").to.equal("Should never come here"); // TODO: How to fail in Chai?
      } catch (err) {
        expect(err).to.not.be.null();
        expect(err.message).to.equal("Year 2021 not supported.");
      }
    });
  });
});
