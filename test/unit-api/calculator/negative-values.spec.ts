import chai from "chai";
import chaiAlmost from "chai-almost";
import dirtyChai from "dirty-chai";
import { config } from "../../config";

import {
  CalculationRowType, CalculationRowUnit, CalculatorLogic, SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

import { IncomesRegisterTestHelper } from "./incomes-register/incomesRegisterTestHelper";

describe("API tests", () => {
  describe("Calculator: Negative values (deductions)", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    chai.use(chaiAlmost(0.02));
    const sxyHelper = new TestHelper(config, this);
    const irHelper = new IncomesRegisterTestHelper(sxyHelper, expect);

    it("deducts monthly salary directly, not using deduction rows.", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const salary = 100;
      const deduction = 20;
      const actualSalary = salary - deduction;
      inputCalc.rows = [
        { price: salary, rowType: CalculationRowType.MonthlySalary },
        {
          price: -1 * deduction,
          rowType: CalculationRowType.MonthlySalary,
          data: {
            kind: "monthlySalary",
          },
        },
      ];

      const calc = await sxyHelper.api.calculator.recalculate(inputCalc);
      const numbers = await irHelper.yearlyChangingNumbers;
      const resultRows = calc.result.irRows.filter((x) => x.irData.code === 201);
      expect(resultRows, "Two result rows with value").to.have.length(2);
      const salaryRow = resultRows[0];
      expect(salaryRow.total, "Salary row has correct total").equals(salary);
      const deductionRow = resultRows[1];
      expect(deductionRow.total, "Deduction row has correct total").equals(-1 * deduction);

      const calcResult = calc.result;
      expect(calcResult.totals.total, "total").to.equal(actualSalary);
      expect(calcResult.totals.totalBaseSalary, "total").to.equal(actualSalary);
      expect(calcResult.totals.totalGrossSalary, "total").to.equal(actualSalary);
      expect(calcResult.totals.totalTaxable, "total").to.equal(actualSalary);

      // Has no effect in actual deduction rows.
      expect(calcResult.employerCalc.deductionOtherDeductions, "deductionOtherDeductions").equals(0);
      expect(calcResult.employerCalc.totalDeductions, "Employer deductions").equals((numbers.sideCosts.unemploymentEmployerPercent + numbers.sideCosts.unemploymentWorkerPercent) * actualSalary);
      expect(calcResult.workerCalc.fullOtherDeductions, "fullOtherDeductions").equals(0);
      expect(calcResult.workerCalc.otherDeductions, "otherDeductions").equals(0);
      expect(calcResult.workerCalc.deductions, "Worker deductions").almost(
        numbers.sideCosts.unemploymentWorkerPercent * actualSalary
        + numbers.sideCosts.tyelWorkerPercent * actualSalary,
      );
    });

    it("Deducts the given row type and leaves other salary rows intact.", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const salary = 1000;
      const bonus = 300;
      const deduction = 150;
      const actualSalary = salary + bonus - deduction;
      inputCalc.rows = [
        { price: salary, rowType: CalculationRowType.MonthlySalary },
        { price: bonus, rowType: CalculationRowType.Salary },
        {
          price: -1 * deduction,
          rowType: CalculationRowType.Salary,
          data: {
            kind: "salary",
          },
        },
      ];

      const calc = await sxyHelper.api.calculator.recalculate(inputCalc);
      const resultRows = calc.result.irRows.filter((x) => x.irData.code === 227);
      expect(resultRows, "Two result rows").to.have.length(2);
      const bonusRow = resultRows[0];
      expect(bonusRow.total, "Bonus row has correct total").equals(bonus);
      const deductionRow = resultRows[1];
      expect(deductionRow.total, "Deduction row has correct total").equals(-1 * deduction);
      const salaryRows = calc.result.irRows.filter((x) => x.irData.code === 201);
      expect(salaryRows, "Single salary row").to.have.length(1);
      expect(salaryRows[0].total, "Salary row has correct value").to.have.equals(salary);

      const calcResult = calc.result;
      expect(calcResult.totals.total, "total").to.equal(actualSalary);
      expect(calcResult.totals.totalBaseSalary, "total").to.equal(actualSalary);
      expect(calcResult.totals.totalGrossSalary, "total").to.equal(actualSalary);
      expect(calcResult.totals.totalTaxable, "total").to.equal(actualSalary);
    });

    it("Throws a validation error if the the total after deduction becomes negative.");

    it("Sorts the rows so that the deductions are after the additions.");

    it("Sums deductions to original values in the report that goes to Incomes Register.");

    it("Does not sum the manually added IR rows (only CalculationRows).");

  });
});
