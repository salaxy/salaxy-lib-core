import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../../config";

import {
  Calculation,
  SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

describe("API tests", () => {
  describe("Calculator: TotalGrossSalary", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    it("Salary amount and price greater than zero", () => {
      const calculation: Calculation = {
        salary: {
          amount: 5,
          price: 6,
          kind: SalaryKind.FixedSalary,
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation)
        .then((resultCalculation: Calculation) => {
          expect(resultCalculation.result.totals.totalGrossSalary).equal(
            calculation.salary.amount * calculation.salary.price);
        });
    });

    it("Salary.amount is zero", () => {
      const calculation: Calculation = {
        salary: {
          amount: 0,
          price: 6,
          kind: SalaryKind.FixedSalary,
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation)
        .then((resultCalculation: Calculation) => {
          expect(resultCalculation.result.totals.totalGrossSalary).equal(0);
        });
    });

    it("Salary.price is zero", () => {
      const calculation: Calculation = {
        salary: {
          amount: 5,
          price: 0,
          kind: SalaryKind.FixedSalary,
        },
      };
      sxyHelper.api.calculator.recalculate(calculation)
        .then((resultCalculation: Calculation) => {
          expect(resultCalculation.result.totals.totalGrossSalary).equal(0);
        });
    });

  });
});
