import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../../config";

import {
  Calculation,
  Dates,
  PaymentChannel,
  SalaryDateLogic,
  SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

describe("API tests", () => {

  describe("Calculator: SalaryDate", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    it("Should return paidDate +2 for SalaryDate paid on Monday", async () => {
      const calculation: Calculation = {
        salary: {
          amount: 5,
          price: 6,
          kind: SalaryKind.FixedSalary,
        },
        workflow: {
          paidAt: "2019-04-01",
        },
      };

      const resultCalculation = await sxyHelper.api.calculator.recalculate(calculation);

      expect((resultCalculation.workflow as any).salaryDate, "salaryDate")
        .equal("2019-04-03");
      expect((resultCalculation.workflow as any).requestedSalaryDate, "requestedSalaryDate")
        .to.be.null();

      const clientSalaryDate = SalaryDateLogic.CalculateSalaryDate(null, calculation.workflow.paidAt);
      expect(clientSalaryDate, "client salaryDate").equal("2019-04-03");
    });

    it("Should return paidDate +3 for SalaryDate paid on Monday evening", async () => {
      const calculation: Calculation = {
        salary: {
          amount: 5,
          price: 6,
          kind: SalaryKind.FixedSalary,
        },
        workflow: {
          paidAt: "2019-04-01T13:00:00.000Z",
        },
      };
      const clientSalaryDate = SalaryDateLogic.CalculateSalaryDate(null, calculation.workflow.paidAt);
      expect(clientSalaryDate, "client salaryDate").equal("2019-04-04");
    });

    it("Should return paidDate +2 for SalaryDate paid on Monday morning", async () => {
      const calculation: Calculation = {
        salary: {
          amount: 5,
          price: 6,
          kind: SalaryKind.FixedSalary,
        },
        workflow: {
          paidAt: "2019-04-01T12:00:00.000Z",
        },
      };
      const clientSalaryDate = SalaryDateLogic.CalculateSalaryDate(null, calculation.workflow.paidAt);
      expect(clientSalaryDate, "client salaryDate").equal("2019-04-03");
    });

    it("Should return paidDate +4 for SalaryDate, when paid on Friday ", async () => {
      const calculation: Calculation = {
        salary: {
          amount: 5,
          price: 6,
          kind: SalaryKind.FixedSalary,
        },
        workflow: {
          paidAt: "2019-04-05",
        },
      };

      const resultCalculation = await sxyHelper.api.calculator.recalculate(calculation);

      expect(resultCalculation.workflow.salaryDate, "salaryDate")
        .equal("2019-04-09");
      expect(resultCalculation.workflow.requestedSalaryDate, "requestedSalaryDate")
        .to.be.null();

      const clientSalaryDate = SalaryDateLogic.CalculateSalaryDate(null, calculation.workflow.paidAt);
      expect(clientSalaryDate, "client salaryDate").equal("2019-04-09");
    });

    it("Should return paidDate +5 for SalaryDate, when paid on Good Friday ", async () => {
      const calculation: Calculation = {
        salary: {
          amount: 5,
          price: 6,
          kind: SalaryKind.FixedSalary,
        },
        workflow: {
          paidAt: "2019-04-19",
        },
      };

      const resultCalculation = await sxyHelper.api.calculator.recalculate(calculation);

      expect(resultCalculation.workflow.salaryDate, "salaryDate")
        .equal("2019-04-25");
      expect(resultCalculation.workflow.requestedSalaryDate, "requestedSalaryDate")
        .to.be.null();

      const clientSalaryDate = SalaryDateLogic.CalculateSalaryDate(null, calculation.workflow.paidAt);
      expect(clientSalaryDate, "client salaryDate").equal("2019-04-25");
    });

    it("Should accept requestedSalaryDate for Thursday but not for Wednesday if paid on Monday  ", async () => {
      const calculation: Calculation = {
        salary: {
          amount: 5,
          price: 6,
          kind: SalaryKind.FixedSalary,
        },
        workflow: {
          paidAt: "2019-04-01",
          requestedSalaryDate: "2019-04-04",
        } as any,
      };

      let isValid = SalaryDateLogic.isValidSalaryDate("2019-04-04", null, calculation.workflow.paidAt);
      expect(isValid, "client side validation 1").equal(true);

      let resultCalculation = await sxyHelper.api.calculator.recalculate(calculation);

      expect(resultCalculation.workflow.salaryDate, "salaryDate")
        .equal("2019-04-04");
      expect(resultCalculation.workflow.requestedSalaryDate, "requestedSalaryDate")
        .equal("2019-04-04");

      calculation.workflow.requestedSalaryDate = "2019-04-03";

      resultCalculation = await sxyHelper.api.calculator.recalculate(calculation);

      expect(resultCalculation.workflow.salaryDate, "salaryDate")
        .equal("2019-04-03");
      expect(resultCalculation.workflow.requestedSalaryDate, "requestedSalaryDate")
        .to.be.null();

      isValid = SalaryDateLogic.isValidSalaryDate("2019-04-03", null, calculation.workflow.paidAt);
      expect(isValid, "client side validation 2").equal(false);
    });

    it("Should not accept salary date  on Saturday ", async () => {
      const calculation: Calculation = {
        salary: {
          amount: 5,
          price: 6,
          kind: SalaryKind.FixedSalary,
        },
        workflow: {
          paidAt: "2019-04-01",
          requestedSalaryDate: "2019-04-06",
        } as any,
      };

      const resultCalculation = await sxyHelper.api.calculator.recalculate(calculation);

      expect(resultCalculation.workflow.requestedSalaryDate, "requestedSalaryDate")
        .to.be.null();

      let isValid = SalaryDateLogic.isValidSalaryDate("2019-04-06", null, calculation.workflow.paidAt);
      expect(isValid, "client side validation 1").equal(false);

      isValid = SalaryDateLogic.isValidSalaryDate("2019-04-05", null, calculation.workflow.paidAt);
      expect(isValid, "client side validation 2").equal(true);

    });

    it("Should return something for current day and both server and client should return the same result.", async () => {
      const calculation: Calculation = {
        salary: {
          amount: 5,
          price: 6,
          kind: SalaryKind.FixedSalary,
        },
        workflow: {
          paidAt: "2019-08-14T21:00:00Z",
        },
      };

      const resultCalculation = await sxyHelper.api.calculator.recalculate(calculation);

      expect(resultCalculation.workflow.salaryDate, "server salaryDate")
        .not.to.be.null();

      const clientSalaryDate = SalaryDateLogic.CalculateSalaryDate(null, calculation.workflow.paidAt);
      // console.log(clientSalaryDate);
      expect(clientSalaryDate, "client salaryDate").not.to.be.null();

      expect(clientSalaryDate, "client").equal(resultCalculation.workflow.salaryDate);
    });

    it("Should calculate workdate, absence day and vacation days", async () => {
      expect(Dates.getWorkdays("2019-04-01", "2019-04-30"), "work days").to.have.length(20);
      expect(Dates.getVacationDays("2019-04-01", "2019-04-30"), "vacation days days").to.have.length(23);
    });

    it("Calculation and validation should give the same results", async () => {
  
      let salaryDate = SalaryDateLogic.CalculateSalaryDate(null, null, true , null);
      let isValid = SalaryDateLogic.isValidSalaryDate(salaryDate, null, null, true, null);

      expect(isValid, "default channel").equal(true);

      salaryDate = SalaryDateLogic.CalculateSalaryDate(null, null, false , null);
      isValid = SalaryDateLogic.isValidSalaryDate(salaryDate, null, null, false, null);

      expect(isValid, "default channel").equal(true);

      salaryDate = SalaryDateLogic.CalculateSalaryDate(null, null, true, PaymentChannel.AccountorGo);
      isValid = SalaryDateLogic.isValidSalaryDate(salaryDate, null, null, true, PaymentChannel.AccountorGo);

      expect(isValid, "accountor go channel").equal(true);

      salaryDate = SalaryDateLogic.CalculateSalaryDate(null, null, null, PaymentChannel.AccountorGo);
      isValid = SalaryDateLogic.isValidSalaryDate(salaryDate, null, null, false, PaymentChannel.AccountorGo);

      expect(isValid, "accountor go channel").equal(true);

    });

    it("Should accept requestedSalaryDate for Tuesday but not for Monday if paid on Monday in Accountor", async () => {
      const calculation: Calculation = {
        salary: {
          amount: 5,
          price: 6,
          kind: SalaryKind.FixedSalary,
        },
        workflow: {
          paidAt: "2019-04-01",
          requestedSalaryDate: "2019-04-02",
        } as any,
        info: {
          paymentChannel: PaymentChannel.AccountorGo,
        } as any,
      };

      let isValid = SalaryDateLogic.isValidSalaryDate("2019-04-02", null, calculation.workflow.paidAt, true, PaymentChannel.AccountorGo);
      expect(isValid, "client side validation 1").equal(true);

      let resultCalculation = await sxyHelper.api.calculator.recalculate(calculation);

      expect(resultCalculation.workflow.salaryDate, "salaryDate")
        .equal("2019-04-02");
      expect(resultCalculation.workflow.requestedSalaryDate, "requestedSalaryDate")
        .equal("2019-04-02");

      calculation.workflow.requestedSalaryDate = "2019-04-01";

      resultCalculation = await sxyHelper.api.calculator.recalculate(calculation);

      expect(resultCalculation.workflow.salaryDate, "salaryDate")
        .equal("2019-04-02");
      expect(resultCalculation.workflow.requestedSalaryDate, "requestedSalaryDate")
        .to.be.null();

      isValid = SalaryDateLogic.isValidSalaryDate("2019-04-01", null, calculation.workflow.paidAt);
      expect(isValid, "client side validation 2").equal(false);
    });
  });
});
