import chai from "chai";
import chaiAlmost from "chai-almost";
import dirtyChai from "dirty-chai";
import { config } from "../../../config";

import { CalculationRowType, CalculatorLogic } from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

import { IncomesRegisterTestHelper } from "./IncomesRegisterTestHelper";

describe("API tests", () => {
  describe("Incomes Register (IR): 300 series basic tests", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    chai.use(chaiAlmost(0.02));
    const sxyHelper = new TestHelper(config, this);
    const irHelper = new IncomesRegisterTestHelper(sxyHelper, expect);

    it("301: Accommodation benefit", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(301, 500, 1000);
      // TODO: Additional tests.
    });

    it("302: Interest benefit for a housing loan", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(302, 500, 1000);
      // TODO: Additional tests.
    });

    it("303: Meal allowance", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(303, 500, 1000);
      // TODO: Additional tests.
    });

    it("304: Car benefit", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(304, 500, 1000);
      // TODO: Additional tests.
    });

    it("308: Compensation for membership of a governing body", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(308, 500, 1000);
      // TODO: Additional tests.
    });

    it("309: Share of reserve and surplus drawn from personnel fund (taxable 80%)", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(309, 500, 1000);
      // TODO: Additional tests.
    });

    it("310: Monetary gift for employees", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(310, 500, 1000);
      // TODO: Additional tests.
    });

    it("311: Kilometre allowance (tax-exempt)", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(311, 500, 1000);
      // TODO: Additional tests.
    });

    it("313: Compensation for use, earned income", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(313, 500, 1000);
      // TODO: Additional tests.
    });

    it("314: Compensation for use, capital income", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(314, 500, 1000);
      // TODO: Additional tests.
    });

    it("315: Other taxable benefit for employees", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(315, 500, 1000);
      // TODO: Additional tests.
    });

    it("316: Other taxable income deemed earned income", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(316, 500, 1000);
      // TODO: Additional tests.
    });

    it("317: Other fringe benefit", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(317, 500, 1000);
      // TODO: Additional tests.
    });

    it("320: Stock options and grants", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(320, 500, 1000);
      // TODO: Additional tests.
    });

    it("321: Wages paid by substitute payer: the employer pays for the employer's social insurance contributions", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(321, 500, 1000, null, false,  true);
      // TODO: Additional tests.
    });

    it("322: Wages paid by substitute payer: employer pays for employer's earnings-related pension insurance contribution", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(322, 500, 1000, null, false,  true);
      // TODO: Additional tests.
    });

    it("323: Wages paid by substitute payer: employer pays for employer's unemployment insurance contribution", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(323, 500, 1000, null, false,  true);
      // TODO: Additional tests.
    });

    it("324: Wages paid by substitute payer, employer pays for accident and occupational disease insurance contribution", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(324, 500, 1000, null, false,  true);
      // TODO: Additional tests.
    });

    it("325: Wages paid by substitute payer, employer pays for employer's health insurance contribution", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(325, 500, 1000, null, false,  true);
      // TODO: Additional tests.
    });

    it("326: Compensation for employee invention", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(326, 500, 1000);
      // TODO: Additional tests.
    });

    it("330: Telephone benefit", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(330, 500, 1000);
      // TODO: Additional tests.
    });

    it("331: Daily allowance", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(331, 500, 1000);
      // TODO: Additional tests.
    });

    it("332: Capital income payment", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(332, 500, 1000);
      // TODO: Additional tests.
    });

    it("334: Meal benefit", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(334, 500, 1000);
      // TODO: Additional tests.
    });

    it("335: Reimbursement of costs, paid to conciliator", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(335, 500, 1000);
      // TODO: Additional tests.
    });

    it("336: Non-wage compensation for work", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(336, 500, 1000);
      // TODO: Additional tests.
    });

    it("338: Pension paid by employer", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(338, 500, 1000);
      // TODO: Additional tests.
    });

    it("339: Dividends/profit surplus based on work effort (wages)", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(339, 500, 1000);
      // TODO: Additional tests.
    });

    it("340: Dividends/profit surplus (wages) based on work effort (non-wage)", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(340, 500, 1000);
      // TODO: Additional tests.
    });

    it("341: Employer-subsidised commuter ticket, tax-exempt share", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(341, 500, 1000, null, false,  true);
      // TODO: Additional tests.
    });

    it("342: Employer-subsidised commuter ticket, taxable share", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(342, 500, 1000, null, false,  true);
      // TODO: Additional tests.
    });

    it("343: Employee stock option", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(343, 500, 1000);
      // TODO: Additional tests.
    });

    it("350: Wages transferred to athletes' special fund", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(350, 500, 1000);
      // TODO: Additional tests.
    });

    it("353: Taxable reimbursement of expenses", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(353, 500, 1000);
      // TODO: Additional tests.
    });

    it("357: Kilometre allowance paid by non-profit organisation", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(357, 500, 1000);
      // TODO: Additional tests.
    });

    it("358: Daily allowance paid by non-profit organisation", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(358, 500, 1000);
      // TODO: Additional tests.
    });

    it("361: Employee stock option with a subscription price lower than the market price at the time of issue", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(361, 500, 1000);
      // TODO: Additional tests.
    });

  });
});
