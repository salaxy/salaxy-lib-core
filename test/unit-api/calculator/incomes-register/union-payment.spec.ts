import chai from "chai";
import chaiAlmost from "chai-almost";
import dirtyChai from "dirty-chai";
import { config } from "../../../config";

import { CalculationRowType, CalculatorLogic, Numeric, UnionPaymentType } from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

import { IncomesRegisterTestHelper } from "./IncomesRegisterTestHelper";

describe("API tests", () => {
  describe("Incomes Register (IR): Union payment", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    chai.use(chaiAlmost(0.02));
    const sxyHelper = new TestHelper(config, this);
    const irHelper = new IncomesRegisterTestHelper(sxyHelper, expect);

    it("Basic company case", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const salary = 1000;
      inputCalc.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
        {
          price: 1000, count: 0.2345, rowType: CalculationRowType.UnionPayment, data: {
            unionPaymentType: "other",
          }
        },
      ];
      const deduction = 234.5;
      const results = await irHelper.runCommonTestsForUnionPayment(inputCalc, salary, deduction, deduction);
      expect(results.resultRow.count, "Result count should still have 4 decimals.").equals(0.2345);
    });

    it("updates price automatically from Salary", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const salary = 1000;
      inputCalc.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
        {
          price: 4321, count: 0.345, rowType: CalculationRowType.UnionPayment, data: {
            unionPaymentType: "other",
          }
        },
      ];
      const deduction = salary * 0.345;
      const results = await irHelper.runCommonTestsForUnionPayment(inputCalc, salary, deduction, deduction);
    });

    it("does not remove the row when price is zero and auto-update is on.", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const salary = 1000;
      inputCalc.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
        {
          price: 0, count: 0.345, rowType: CalculationRowType.UnionPayment, data: {
            unionPaymentType: "other",
          }
        },
      ];
      const deduction = salary * 0.345;
      const results = await irHelper.runCommonTestsForUnionPayment(inputCalc, salary, deduction, deduction);
    });

    it("removes the row when count is zero", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const salary = 1000;
      inputCalc.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
        {
          price: 4321, count: 0, rowType: CalculationRowType.UnionPayment, data: {
            unionPaymentType: "other",
          }
        },
      ];
      const results = await irHelper.runCommonTestsForUnionPayment(inputCalc, salary, 0, 0);
      expect(results.resultRow, "No result row").to.not.exist();
    });

    it("does not update price automatically from Salary when isFixedSum is set to true", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const salary = 1000;
      inputCalc.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
        {
          price: 200, count: 0.9, rowType: CalculationRowType.UnionPayment, data: {
            unionPaymentType: "other",
            isFixedSum: true,
          }
        },
      ];
      const deduction = 0.9 * 200;
      const results = await irHelper.runCommonTestsForUnionPayment(inputCalc, salary, deduction, deduction);
      expect(results.resultRow.count, "result row count").equals(0.9);
      expect(results.resultRow.price, "result row price").equals(200);
    });

    it("Basic Raksa case", async () => {
      const yearlyNumbers = irHelper.yearlyChangingNumbers;
      const inputCalc = CalculatorLogic.getBlank();
      const salary = 2345;
      inputCalc.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
        {
          price: salary, count: yearlyNumbers.sideCosts.unionPaymentRaksaA, rowType: CalculationRowType.UnionPayment, data: {
            unionPaymentType: "raksaNormal",
          }
        },
      ];
      const deduction = Numeric.round(salary * yearlyNumbers.sideCosts.unionPaymentRaksaA);
      const results = await irHelper.runCommonTestsForUnionPayment(inputCalc, salary, deduction, deduction);
      expect(results.resultRow.count, "Count should be the raksa percentage").equals(yearlyNumbers.sideCosts.unionPaymentRaksaA);
      expect(results.resultRow.price, "Price should be the salary").equals(salary);
    });

    it("Basic Unemployement only case", async () => {
      const yearlyNumbers = irHelper.yearlyChangingNumbers;
      const inputCalc = CalculatorLogic.getBlank();
      const salary = 2345;
      inputCalc.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
        // Price and Count here should be ignored
        {
          price: salary, count: yearlyNumbers.sideCosts.unionPaymentRaksaAoTa, rowType: CalculationRowType.UnionPayment, data: {
            unionPaymentType: "raksaUnemploymentOnly",
          }
        },
      ];
      const deduction = Numeric.round(salary * yearlyNumbers.sideCosts.unionPaymentRaksaAoTa);
      const results = await irHelper.runCommonTestsForUnionPayment(inputCalc, salary, deduction, deduction);
      expect(results.resultRow.count, "Count should be the raksa percentage").equals(yearlyNumbers.sideCosts.unionPaymentRaksaAoTa);
      expect(results.resultRow.price, "Price should be the salary").equals(salary);
    });

    it("API method - basic case", async () => {
      const salary = 3456;
      const percent = 0.0132;
      const deduction = Numeric.round(salary * percent);

      const inputCalc1 = CalculatorLogic.getBlank();
      inputCalc1.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
      ];
      const taxPercent = 0.214;
      inputCalc1.worker.tax.taxPercentForVisualization = taxPercent * 100;
      const result1 = await sxyHelper.api.calculator.setUnionPayment(inputCalc1, UnionPaymentType.Other, percent * 100);

      const inputCalc2 = CalculatorLogic.getBlank();
      inputCalc2.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
        {
          price: 456, count: percent, rowType: CalculationRowType.UnionPayment, data: {
            unionPaymentType: "other",
          }
        },
      ];
      const result2 = await irHelper.runCommonTestsForUnionPayment(inputCalc2, salary, deduction, deduction);

      expect(result1.result.totals.totalGrossSalary, "totalGrossSalary").to.equal(result2.calc.result.totals.totalGrossSalary);
      expect(result1.result.workerCalc.unionPayment, "unionPayment").to.equal(result2.calc.result.workerCalc.unionPayment);
      expect(result1.result.workerCalc.fullUnionPayment, "fullUnionPayment").to.equal(result2.calc.result.workerCalc.fullUnionPayment);
      expect(result1.result.workerCalc.totalWorkerPayment, "totalWorkerPayment").to.equal(result2.calc.result.workerCalc.totalWorkerPayment);
      expect(result1.result.employerCalc.deductionUnionPayment, "deductionUnionPayment").to.equal(result2.calc.result.employerCalc.deductionUnionPayment);
      expect(result1.result.employerCalc.totalPayment, "totalPayment").to.equal(result2.calc.result.employerCalc.totalPayment);
      expect(result1.result.employerCalc.finalCost, "finalCost").to.equal(result2.calc.result.employerCalc.finalCost);

    });

    it("API method - Raksa case", async () => {
      const salary = 1000;
      const yearlyNumbers = irHelper.yearlyChangingNumbers;
      const percent = yearlyNumbers.sideCosts.unionPaymentRaksaA;
      const deduction = Numeric.round(salary * percent);

      const inputCalc1 = CalculatorLogic.getBlank();
      inputCalc1.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
      ];
      const taxPercent = 0.214;
      inputCalc1.worker.tax.taxPercentForVisualization = taxPercent * 100;
      const result1 = await sxyHelper.api.calculator.setUnionPayment(inputCalc1, UnionPaymentType.RaksaNormal, 0.3); // Percent should be ignored (fetched from the usecase)

      const inputCalc2 = CalculatorLogic.getBlank();
      inputCalc2.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
        {
          price: 1000, count: yearlyNumbers.sideCosts.unionPaymentRaksaA, rowType: CalculationRowType.UnionPayment, data: {
            unionPaymentType: "raksaNormal",
          }
        },
      ];
      const result2 = await irHelper.runCommonTestsForUnionPayment(inputCalc2, salary, deduction, deduction);

      expect(result1.result.totals.totalGrossSalary, "totalGrossSalary").to.equal(result2.calc.result.totals.totalGrossSalary);
      expect(result1.result.workerCalc.unionPayment, "unionPayment").to.equal(result2.calc.result.workerCalc.unionPayment);
      expect(result1.result.workerCalc.fullUnionPayment, "fullUnionPayment").to.equal(result2.calc.result.workerCalc.fullUnionPayment);
      expect(result1.result.workerCalc.totalWorkerPayment, "totalWorkerPayment").to.equal(result2.calc.result.workerCalc.totalWorkerPayment);
      expect(result1.result.employerCalc.deductionUnionPayment, "deductionUnionPayment").to.equal(result2.calc.result.employerCalc.deductionUnionPayment);
      expect(result1.result.employerCalc.totalPayment, "totalPayment").to.equal(result2.calc.result.employerCalc.totalPayment);
      expect(result1.result.employerCalc.finalCost, "finalCost").to.equal(result2.calc.result.employerCalc.finalCost);

    });

    it("Backward compatibility: Raksa basic case (Framework)", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const salary = 2000;
      inputCalc.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
      ];
      (inputCalc.framework as any).unionPayment = "raksaNormal";
      const yearlyNumbers = irHelper.yearlyChangingNumbers;
      const deduction = Numeric.round(salary * yearlyNumbers.sideCosts.unionPaymentRaksaA);
      const results = await irHelper.runCommonTestsForUnionPayment(inputCalc, salary, deduction, deduction);
    });

    it("Backward compatibility: Raksa OA- tai TA case (Framework)", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const salary = 2000;
      inputCalc.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
      ];
      (inputCalc.framework as any).unionPayment = "raksaUnemploymentOnly";
      const yearlyNumbers = irHelper.yearlyChangingNumbers;
      const deduction = Numeric.round(salary * yearlyNumbers.sideCosts.unionPaymentRaksaAoTa);
      const results = await irHelper.runCommonTestsForUnionPayment(inputCalc, salary, deduction, deduction);
    });

    it("Backward compatibility: Other", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const salary = 1000;
      inputCalc.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
      ];
      (inputCalc.framework as any).unionPayment = "other";
      (inputCalc.framework as any).unionPaymentPercent = 1.23;
      const deduction = 12.3;
      const results = await irHelper.runCommonTestsForUnionPayment(inputCalc, salary, deduction, deduction);
      expect(results.resultRow.count, "Result count should still have 4 decimals.").equals(0.0123);
    });

    it("Backward compatibility: NotSelected", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const salary = 1000;
      inputCalc.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
      ];
      (inputCalc.framework as any).unionPayment = "notSelected";
      (inputCalc.framework as any).unionPaymentPercent = 0.0123;
      const results = await irHelper.runCommonTestsForUnionPayment(inputCalc, salary, 0, 0);
    });
  });
});
