
import {
  Calculation, CalculationFlag, CalculationRowType, CalculatorLogic, IncomeTypesLogic, IrRow, LedgerEntry, Numeric, ResultRow, YearlyChangingNumbers,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

/** Test helper for incomes register methods. */
export class IncomesRegisterTestHelper {

  /** Yearly changing numbers */
  public yearlyChangingNumbers: YearlyChangingNumbers;

  constructor(private sxyHelper: TestHelper, private expect: Chai.ExpectStatic) {
    this.yearlyChangingNumbers = sxyHelper.api.calculator.getYearlyChangingNumbers(new Date());
  }

  /** Gets the "standard" additions: Pension, HealthInsurance and Tax minus Worker UnemploymentInsurance */
  public getStandardEmployerAdditions(calc: Calculation): number {
    return calc.result.employerCalc.pension + calc.result.employerCalc.socialSecurity
      + calc.result.employerCalc.palkkaus - calc.result.workerCalc.unemploymentInsurance;
  }

  /** Gets the "standard" worker deductions from salary: Pension, Tax plus UnemploymentInsurance */
  public getStandardWorkerDeductions(calc: Calculation): number {
    return calc.result.workerCalc.pension + calc.result.workerCalc.tax + calc.result.workerCalc.unemploymentInsurance;
  }

  /**
   * Checks the new totaPayment calculations against the legacy values.
   * @param calc Calculation to check
   * @param incomeTypeCode Used for excluding some income type codes from the test.
   */
  public checkTotalPayments(calc: Calculation, incomeTypeCode: number = null): void {
    // These income types will not be calculated correctly by the legacy code.
    switch (incomeTypeCode) {
      case 417:
      case 408:
      case 411:
      case 415:
        // Deductions were deducted separately, not by group. Does not make sense to recreate this functionality
        return;
      case 419:
      case 401:
      case 407:
        // Deductions from net salary did not exist before.
        return;
    }
    this.expect(calc.result.employerCalc.totalPayment, "totalPayment differs from totalPaymentLegacy").equals((calc.result.employerCalc as any).totalPaymentLegacy);
    this.expect(calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment differs from totalWorkerPaymentLegacy").equals((calc.result.workerCalc as any).totalWorkerPaymentLegacy);
  }

  /**
   * Runs a common set of tests for a given Incomes Register (IR row) income type.
   * @param incomeTypeCode Income type code.
   * @param sum Sum for the IR-row
   * @param otherSalary Other salary in the calculation - needed when testing benefits, deductions etc. because the row alone will yield to unexpected results.
   * @param inputCalc Calculation being tested. Set the tax percent to 21.4%
   * If not set, will create it using incomeTypeCode, sum and otherSalary (CalculationRowType Salary).
   * @param isInsuranceDeduction If true, "sum" deducts the pensions that are set by "otherSalary".
   * @param noAccountingEntry If true, no accounting entry should be created for the income type.
   */
  public async runCommonTestsForIncomeType(incomeTypeCode: number, sum: number, otherSalary = 0, inputCalc: Calculation = null, isInsuranceDeduction = false, noAccountingEntry = false):
      Promise<{ calc: Calculation, resultRow: IrRow, reportRow: LedgerEntry, numbers: YearlyChangingNumbers }> {

    if (!inputCalc) {
      inputCalc = CalculatorLogic.getBlank();
      if (otherSalary > 0) {
        inputCalc.rows = [
          { price: otherSalary, rowType: CalculationRowType.Salary },
        ];
      }
      inputCalc.result.irRows.push({
        price: sum,
        irData: {
          code: incomeTypeCode,
        },
      });
    }

    const expect = this.expect;
    const incomeType = IncomeTypesLogic.allTypes.find((x) => x.code === incomeTypeCode);
    const totalSum = sum + otherSalary;
    const numbers = this.yearlyChangingNumbers;

    const taxPercent = 0.214;
    inputCalc.worker.tax.taxPercentForVisualization = taxPercent * 100;
    const calc = await this.sxyHelper.api.calculator.recalculate(inputCalc);
    this.checkTotalPayments(calc, incomeTypeCode);

    const resultRows = calc.result.irRows.filter((x) => x.irData.code === incomeTypeCode);
    expect(resultRows, "One result row with value").to.have.length(1);
    const resultRow = resultRows[0];
    expect(resultRow.total, "Result row has correct total").equals(sum);
    expect(resultRow.irData.code, "Result row has correct code").equals(incomeTypeCode);

    let totalTaxable: number;
    switch (incomeType.taxDefault) {
      case 1:
        totalTaxable = totalSum;
        expect(resultRow.calcData.behavior, "tax flag").contains(CalculationFlag.Tax);
        break;
      case -1:
        totalTaxable = otherSalary - sum;
        expect(resultRow.calcData.behavior, "tax flag").contains(CalculationFlag.TaxDeduction);
        break;
      case 0:
        totalTaxable = otherSalary;
        expect(resultRow.calcData.behavior, "tax flag").contains(CalculationFlag.NoTax);
        break;
      default:
        throw new Error("Unsupported taxDefault: " + incomeType.taxDefault);
    }
    expect(calc.result.totals.totalTaxable, "totalTaxable").equals(totalTaxable);
    expect(calc.result.workerCalc.tax, "tax").almost(totalTaxable * taxPercent);

    let pensionBase;
    let unemploymentBase;
    if (isInsuranceDeduction) {
      expect(resultRow.calcData.behavior, "insurancesDeduction flag").contains("insurancesDeduction");
      pensionBase = otherSalary - sum;
      unemploymentBase = otherSalary - sum;
      expect(calc.result.totals.totalSocialSecurityBase, "totalSocialSecurityBase").almost(pensionBase);
      expect(calc.result.totals.pension, "pension").almost(pensionBase * numbers.sideCosts.tyelBasePercent);
      expect(calc.result.totals.unemployment, "unemployment").almost(unemploymentBase * (numbers.sideCosts.unemploymentEmployerPercent + numbers.sideCosts.unemploymentWorkerPercent));
      expect(calc.result.employerCalc.socialSecurity, "socialSecurity").almost(pensionBase * numbers.sideCosts.socSecPercent);
    } else {
      if (incomeType.pensionInsurance) {
        expect(resultRow.calcData.behavior, "PensionInsurance flag").contains(CalculationFlag.PensionInsurance);
        expect(calc.result.totals.totalSocialSecurityBase, "totalSocialSecurityBase").almost(totalSum);
        expect(calc.result.totals.pension, "pension").almost(totalSum * numbers.sideCosts.tyelBasePercent);
        pensionBase = totalSum;
      } else {
        expect(resultRow.calcData.behavior, "No PensionInsurance flag").to.not.contain(CalculationFlag.PensionInsurance);
        expect(calc.result.totals.totalSocialSecurityBase, "totalSocialSecurityBase").almost(otherSalary);
        expect(calc.result.totals.pension, "pension").almost(otherSalary * numbers.sideCosts.tyelBasePercent);
        pensionBase = otherSalary;
      }

      if (incomeType.unemploymentInsurance) {
        expect(resultRow.calcData.behavior, "UnemploymentInsurance flag").contains(CalculationFlag.UnemploymentInsurance);
        expect(calc.result.totals.unemployment, "unemployment").almost(totalSum * (numbers.sideCosts.unemploymentEmployerPercent + numbers.sideCosts.unemploymentWorkerPercent));
        unemploymentBase = totalSum;
      } else {
        expect(resultRow.calcData.behavior, "No UnemploymentInsurance flag").to.not.contain(CalculationFlag.UnemploymentInsurance);
        expect(calc.result.totals.unemployment, "unemployment").almost(otherSalary * (numbers.sideCosts.unemploymentEmployerPercent + numbers.sideCosts.unemploymentWorkerPercent));
        unemploymentBase = otherSalary;
      }

      if (incomeType.healthInsurance) {
        expect(resultRow.calcData.behavior, "HealthInsurance flag").contains(CalculationFlag.HealthInsurance);
        expect(calc.result.employerCalc.socialSecurity, "socialSecurity").almost(totalSum * numbers.sideCosts.socSecPercent);
      } else {
        expect(resultRow.calcData.behavior, "No HealthInsurance flag").to.not.contain(CalculationFlag.HealthInsurance);
        expect(calc.result.employerCalc.socialSecurity, "socialSecurity").almost(otherSalary * numbers.sideCosts.socSecPercent);
      }
    }

    const allSideCosts = calc.result.employerCalc.pension + calc.result.employerCalc.socialSecurity
      + calc.result.employerCalc.palkkaus - calc.result.workerCalc.unemploymentInsurance;
    const workerSideCosts = totalTaxable * taxPercent + unemploymentBase * numbers.sideCosts.unemploymentWorkerPercent + pensionBase * numbers.sideCosts.tyelWorkerPercent;
    switch (incomeType.paymentDefault) {
      case 1:
        expect(resultRow.calcData.behavior, "CF flag").contains(CalculationFlag.CfPayment);
        expect(calc.result.employerCalc.totalPayment, "totalPayment - CfPayment").almost(totalSum + allSideCosts);
        expect(calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment - CfPayment")
          .almost(totalSum - workerSideCosts);
        break;
      case -1:
        expect(resultRow.calcData.behavior, "CF flag").contains(CalculationFlag.CfDeduction);
        expect(calc.result.employerCalc.totalPayment, "totalPayment - CfDeduction").almost(otherSalary - sum + allSideCosts);
        expect(calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment - CfDeduction")
          .almost(otherSalary - sum - workerSideCosts);
        break;
      case 0:
        expect(resultRow.calcData.behavior, "CF flag").contains(CalculationFlag.CfNoPayment);
        expect(calc.result.employerCalc.totalPayment, "totalPayment - CfNoPayment").almost(otherSalary + allSideCosts);
        expect(calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment - CfDeduction")
          .almost(otherSalary - workerSideCosts);
        break;
      default:
        throw new Error("Unsupported paymentDefault: " + incomeType.paymentDefault);
    }

    if (incomeType.accidentInsurance) {
      // TODO: Accident insurance not implemented on this level
    } else {
      // TODO: Accident insurance not implemented on this level
    }

    const accountingData = await this.sxyHelper.api.reports.getAccountingDataForCalculations([calc]);
    const allEntries = accountingData.ledgerAccounts.map( (acc) => acc.entries).reduce( (all, items) => all.concat(...items), []);
    const debitEntries = allEntries.filter( (x) => x.isDebit && x.dimension.entryCodeGroup === "incomeType" && x.dimension.entryCode === ("" + incomeTypeCode));
    let reportRow = null;
    if (noAccountingEntry) {
      expect(debitEntries, "There should not be a reporting row matching the type.").to.have.length(0);
    } else {
      expect(debitEntries, "There should be a reporting row matching the type.").to.have.length(1);
      reportRow = debitEntries[0];
      expect(reportRow.amount).to.almost(sum);
    }

    return {calc, resultRow, reportRow, numbers};
  }

  public async runExpensesEtcPlaceholder(inputCalc: Calculation, rowType: CalculationRowType, sum: number)
    : Promise<{ calc: Calculation, resultRow: ResultRow, reportRow: LedgerEntry, numbers: YearlyChangingNumbers }> {
      throw new Error("Not implemented");
  }

  public async runCommonTestsForSalaries(inputCalc: Calculation, rowType: CalculationRowType, sum: number,
                                         isAddition = false, isBenefit = false, socSecSum: number = null, expectedIrCode = 0)
    : Promise<{ calc: Calculation, resultRow: ResultRow, reportRow: LedgerEntry, numbers: YearlyChangingNumbers }> {
      const expect = this.expect;
      const numbers = this.yearlyChangingNumbers;
      const taxPercent = 0.214;
      inputCalc.worker.tax.taxPercentForVisualization = taxPercent * 100;

      const calc = await this.sxyHelper.api.calculator.recalculate(inputCalc);
      if (socSecSum == null) {
        socSecSum = sum;
      }

      const resultRows = calc.result.rows.filter((x) => x.total !== 0);
      expect(resultRows, "One result row with value").to.have.length(1);
      const resultRow = resultRows[0];
      expect(resultRow.rowType, "Row type as specified").to.equal(rowType);
      expect(resultRow.total, "Result row total").to.almost(sum);
      expect(resultRow.totalBaseSalary, "Result row totalBaseSalary").to.almost(isAddition ? 0 : sum);
      expect(resultRow.totalDeduction, "Result row totalDeduction").to.almost(0);
      expect(resultRow.totalExpenses, "Result row totalExpenses").to.almost(0);
      expect(resultRow.totalGrossSalary, "Result row totalGrossSalary").to.almost(isBenefit ? 0 : sum);
      expect(resultRow.totalSocialSecurityBase, "Result row totalSocialSecurityBase").to.almost(socSecSum);
      expect(resultRow.totalTaxable, "Result row totalTaxable").to.almost(sum);

      // Totals
      expect(calc.result.totals.total, "total").to.almost(sum);
      expect(calc.result.totals.totalExpenses, "totalExpenses").to.almost(0);
      expect(calc.result.totals.totalBaseSalary, "totalBaseSalary").to.almost(isAddition ? 0 : sum);
      expect(calc.result.totals.totalGrossSalary, "totalGrossSalary").to.almost(isBenefit ? 0 : sum);
      expect(calc.result.totals.totalSocialSecurityBase, "totalSocialSecurityBase").to.almost(socSecSum);
      expect(calc.result.totals.totalTaxable, "totalTaxable").to.almost(sum);
      expect(calc.result.totals.pension, "pension").to.almost(socSecSum * numbers.sideCosts.tyelBasePercent);
      expect(calc.result.totals.unemployment, "pension").to.almost(socSecSum * (numbers.sideCosts.unemploymentEmployerPercent + numbers.sideCosts.unemploymentWorkerPercent));

      // Employer numbers
      expect(calc.result.employerCalc.socialSecurity, "socialSecurity").to.almost(socSecSum * numbers.sideCosts.socSecPercent);
      const basePrice = Math.max(4, Math.min(sum * 0.01, 10));
      expect(calc.result.employerCalc.palkkaus, "palkkaus").to.almost(basePrice * 1.24);
      expect(calc.result.employerCalc.totalDeductions, "totalDeductions").to.almost(socSecSum * (numbers.sideCosts.unemploymentEmployerPercent + numbers.sideCosts.unemploymentWorkerPercent));

      const netSalary = Numeric.round(sum - (sum * taxPercent));
      if (isBenefit) {
        expect(calc.result.employerCalc.pension, "pension").to.almost(socSecSum * numbers.sideCosts.tyelBasePercent);
        expect(calc.result.employerCalc.unemployment, "unemployment").to.almost(socSecSum * (numbers.sideCosts.unemploymentEmployerPercent + numbers.sideCosts.unemploymentWorkerPercent));
        expect(calc.result.workerCalc.tax, "tax").to.almost(0);
        expect(calc.result.workerCalc.salaryAfterTax, "salaryAfterTax").to.almost(0);
        expect(calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment").to.almost(0);
        expect(calc.result.workerCalc.benefits, "benefits").to.almost(sum);
      } else {
        expect(calc.result.employerCalc.pension, "pension").to.almost(socSecSum * (numbers.sideCosts.tyelBasePercent - numbers.sideCosts.tyelWorkerPercent));
        expect(calc.result.employerCalc.unemployment, "unemployment").to.almost(socSecSum * numbers.sideCosts.unemploymentEmployerPercent);
        expect(calc.result.workerCalc.tax, "tax").to.almost(Numeric.round(sum) * taxPercent);
        expect(calc.result.workerCalc.salaryAfterTax, "salaryAfterTax").to.almost(netSalary);
        expect(calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment").to
          .almost(Numeric.round(netSalary - socSecSum * numbers.sideCosts.tyelWorkerPercent - socSecSum * numbers.sideCosts.unemploymentWorkerPercent));
        expect(calc.result.workerCalc.benefits, "benefits").to.equal(0);
      }

      expect(calc.result.workerCalc.fullPension, "fullPension").to.almost(socSecSum * numbers.sideCosts.tyelWorkerPercent);
      expect(calc.result.workerCalc.fullUnemploymentInsurance, "fullUnemploymentInsurance").to.almost(socSecSum * numbers.sideCosts.unemploymentWorkerPercent);

      // All deductions should be 0 - Can't be checked from calc.result.workerCalc.deductions, because that is zero if there is not enough to deduct
      expect(calc.result.workerCalc.foreclosure, "foreclosure").to.equal(0);
      expect(calc.result.workerCalc.fullSalaryAdvance, "fullSalaryAdvance").to.equal(0);
      expect(calc.result.workerCalc.fullPrepaidExpenses, "fullPrepaidExpenses").to.equal(0);
      expect(calc.result.workerCalc.fullOtherDeductions, "fullOtherDeductions").to.equal(0);
      expect(calc.result.workerCalc.fullUnionPayment, "fullUnionPayment").to.equal(0);

      // Reports
      const accountingData = await this.sxyHelper.api.reports.getAccountingDataForCalculations([calc]);
      const allEntries = accountingData.ledgerAccounts.map( (acc) => acc.entries).reduce( (all, items) => all.concat(...items), []);
      const debitEntries = allEntries.filter( (x) => x.isDebit && x.dimension.entryCodeGroup === "incomeType" && x.dimension.entryCode === ("" + expectedIrCode));
      expect(debitEntries, "There should be a reporting row matching the type.").to.have.length(1);
      const reportRow = debitEntries[0];
      expect(reportRow.amount).to.almost(sum);
      return {calc, resultRow, reportRow, numbers};
    }

  public async runCommonTestsForExpenses(inputCalc: Calculation, rowType: CalculationRowType, sum: number, expectedIrCode = 0)
    : Promise<{ calc: Calculation, resultRow: ResultRow, reportRow: LedgerEntry }> {
    const expect = this.expect;
    const taxPercent = 0.214;
    inputCalc.worker.tax.taxPercentForVisualization = taxPercent * 100;

    const calc = await this.sxyHelper.api.calculator.recalculate(inputCalc);

    const resultRows = calc.result.rows.filter((x) => x.total !== 0);
    expect(resultRows, "One result row with value").to.have.length(1);
    const resultRow = resultRows[0];
    expect(resultRow.rowType, "Row type as specified").to.equal(rowType);
    expect(resultRow.total, "Result row total").to.equal(sum);
    expect(resultRow.totalBaseSalary, "Result row totalBaseSalary").to.equal(0);
    expect(resultRow.totalDeduction, "Result row totalDeduction").to.equal(0);
    expect(resultRow.totalExpenses, "Result row totalExpenses").to.equal(sum);
    expect(resultRow.totalGrossSalary, "Result row totalGrossSalary").to.equal(0);
    expect(resultRow.totalSocialSecurityBase, "Result row totalSocialSecurityBase").to.equal(0);
    expect(resultRow.totalTaxable, "Result row totalTaxable").to.equal(0);

    // Totals
    expect(calc.result.totals.total, "total").to.equal(sum);
    expect(calc.result.totals.totalExpenses, "totalExpenses").to.equal(sum);
    expect(calc.result.totals.totalBaseSalary, "totalBaseSalary").to.equal(0);
    expect(calc.result.totals.totalGrossSalary, "totalGrossSalary").to.equal(0);
    expect(calc.result.totals.totalSocialSecurityBase, "totalSocialSecurityBase").to.equal(0);
    expect(calc.result.totals.totalTaxable, "totalTaxable").to.equal(0);
    expect(calc.result.totals.pension, "pension").to.equal(0);
    expect(calc.result.totals.unemployment, "pension").to.equal(0);

    // Employer numbers
    expect(calc.result.employerCalc.socialSecurity, "socialSecurity").to.equal(0);
    const price = 1.24 * (Math.max(4, Math.min(sum * 0.01, 10)));
    expect(calc.result.employerCalc.palkkaus, "palkkaus").to.equal(price);
    expect(calc.result.employerCalc.totalDeductions, "totalDeductions").to.equal(0);
    expect(calc.result.employerCalc.totalPayment, "totalPayment").to.equal(sum + price);
    expect(calc.result.employerCalc.pension, "pension").to.equal(0);
    expect(calc.result.employerCalc.unemployment, "unemployment").to.equal(0);

    expect(calc.result.workerCalc.tax, "tax").to.equal(0);
    expect(calc.result.workerCalc.salaryAfterTax, "salaryAfterTax").to.equal(0);
    expect(calc.result.workerCalc.benefits, "benefits").to.equal(0);
    expect(calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment").to.equal(sum);

    expect(calc.result.workerCalc.fullPension, "fullPension").to.equal(0);
    expect(calc.result.workerCalc.fullUnemploymentInsurance, "fullUnemploymentInsurance").to.almost(0);

    expect(calc.result.workerCalc.deductions, "foreclosure").to.equal(0);
    expect(calc.result.workerCalc.foreclosure, "foreclosure").to.equal(0);
    expect(calc.result.workerCalc.fullSalaryAdvance, "fullSalaryAdvance").to.equal(0);
    expect(calc.result.workerCalc.fullPrepaidExpenses, "fullPrepaidExpenses").to.equal(0);
    expect(calc.result.workerCalc.fullOtherDeductions, "fullOtherDeductions").to.equal(0);
    expect(calc.result.workerCalc.fullUnionPayment, "fullUnionPayment").to.equal(0);

    // Reports
    const accountingData = await this.sxyHelper.api.reports.getAccountingDataForCalculations([calc]);
    const allEntries = accountingData.ledgerAccounts.map( (acc) => acc.entries).reduce( (all, items) => all.concat(...items), []);
    const debitEntries = allEntries.filter( (x) => x.isDebit && x.dimension.entryCodeGroup === "incomeType" && x.dimension.entryCode === ("" + expectedIrCode));
    expect(debitEntries, "There should be a reporting row matching the type.").to.have.length(1);
    const reportRow = debitEntries[0];
    expect(reportRow.amount).to.almost(sum);
    return {calc, resultRow, reportRow};
  }

  /**
   * Runs the tests for Union payments. There is a lot of legacy with this method.
   * @param inputCalc Calculation that is tested
   * @param salary Salary that is set to the calculation.
   * @param workerDeduction Sum deducted from Worker. If this is zero, we test also that there is no rows (input, result or report) for the union payment.
   * @param employerDeduction Sum deducted from the employer payment. This is zero when Palkkaus does the deduction / payment at the backoffice.
   */
  public async runCommonTestsForUnionPayment(inputCalc: Calculation, salary: number, workerDeduction: number, employerDeduction: number)
    : Promise<{ calc: Calculation, resultRow: ResultRow, reportRow: LedgerEntry, numbers: YearlyChangingNumbers }> {
    const expect = this.expect;
    const numbers = this.yearlyChangingNumbers;
    const taxPercent = 0.214;
    inputCalc.worker.tax.taxPercentForVisualization = taxPercent * 100;

    const calc = await this.sxyHelper.api.calculator.recalculate(inputCalc);
    const socSecSum = salary;

    const resultRows = calc.result.rows.filter((x) => x.total !== 0);
    const resultRow = resultRows.find((x) => x.rowType === CalculationRowType.UnionPayment);
    if (workerDeduction === 0) {
      expect(resultRows, "1 row").to.have.length(1);
      expect(resultRow, "no union payment row").to.be.undefined();
    } else {
      expect(resultRows, "2 rows").to.have.length(2);
      expect(resultRow, "UnionPayment row to exist (Result row)").to.exist();
      expect(resultRow.total, "Result row total").to.almost(workerDeduction);
      expect(resultRow.totalBaseSalary, "Result row totalBaseSalary").to.almost(0);
      expect(resultRow.totalDeduction, "Result row totalDeduction").to.almost(workerDeduction);
      expect(resultRow.totalExpenses, "Result row totalExpenses").to.almost(0);
      expect(resultRow.totalGrossSalary, "Result row totalGrossSalary").to.almost(0);
      expect(resultRow.totalSocialSecurityBase, "Result row totalSocialSecurityBase").to.almost(0);
      expect(resultRow.totalTaxable, "Result row totalTaxable").to.almost(0);
      expect(resultRow.message).to.contain("Ay-jäsenmaksu");
    }

    const inputRow = calc.rows.find((x) => x.rowType === CalculationRowType.UnionPayment);
    if (workerDeduction === 0) {
      expect(calc.rows, "1 Input row").to.have.length(1);
      expect(inputRow, "no union payment Input row").to.be.undefined();
    } else {
      expect(calc.rows, "2 rows").to.have.length(2);
      expect(inputRow, "UnionPayment row to exist (Input row)").to.exist();
      expect(inputRow.count * inputRow.price, "Input row total").to.almost(workerDeduction);
      expect(inputRow.message).to.contain("Ay-jäsenmaksu");
    }

    const salaryRow = resultRows.find((x) => x.rowType === CalculationRowType.Salary);
    expect(salaryRow, "Salary row exists").to.exist();
    expect(salaryRow.total, "Salary row total").to.equal(salary);
    expect(salaryRow.totalBaseSalary, "Salary row totalBaseSalary").to.equal(salary);
    expect(salaryRow.totalDeduction, "Salary row totalDeduction").to.equal(0);
    expect(salaryRow.totalExpenses, "Salary row totalExpenses").to.equal(0);
    expect(salaryRow.totalGrossSalary, "Salary row totalGrossSalary").to.equal(salary);
    expect(salaryRow.totalSocialSecurityBase, "Salary row totalSocialSecurityBase").to.equal(socSecSum);
    expect(salaryRow.totalTaxable, "Salary row totalTaxable").to.equal(salary);

    // Totals
    expect(calc.result.totals.total, "total").to.almost(salary + workerDeduction);
    expect(calc.result.totals.totalExpenses, "totalExpenses").to.equal(0);
    expect(calc.result.totals.totalBaseSalary, "totalBaseSalary").to.equal(salary);
    expect(calc.result.totals.totalGrossSalary, "totalGrossSalary").to.equal(salary);
    expect(calc.result.totals.totalSocialSecurityBase, "totalSocialSecurityBase").to.equal(socSecSum);
    expect(calc.result.totals.totalTaxable, "totalTaxable").to.equal(salary);
    expect(calc.result.totals.pension, "pension").to.almost(socSecSum * numbers.sideCosts.tyelBasePercent);
    expect(calc.result.totals.unemployment, "pension").to.almost(socSecSum * (numbers.sideCosts.unemploymentEmployerPercent + numbers.sideCosts.unemploymentWorkerPercent));

    // Employer numbers
    const allSideCosts = calc.result.employerCalc.pension + calc.result.employerCalc.socialSecurity
      + calc.result.employerCalc.palkkaus - calc.result.workerCalc.unemploymentInsurance;
    expect(calc.result.employerCalc.socialSecurity, "socialSecurity").to.almost(socSecSum * numbers.sideCosts.socSecPercent);
    expect(calc.result.employerCalc.deductionUnionPayment, "deductionUnionPayment").to.equal(employerDeduction);
    expect(calc.result.employerCalc.totalDeductions, "totalDeductions").to.almost(salary * (numbers.sideCosts.unemploymentEmployerPercent + numbers.sideCosts.unemploymentWorkerPercent) + employerDeduction);
    const basePrice = Math.max(4, Math.min(salary * 0.01, 10));
    expect(calc.result.employerCalc.palkkaus, "palkkaus").to.almost(basePrice * 1.24);
    expect(calc.result.employerCalc.totalPayment, "totalPayment").to.almost(salary + allSideCosts - employerDeduction);
    expect(calc.result.employerCalc.finalCost, "finalCost").to.almost(salary + allSideCosts + calc.result.totals.unemployment);

    const salaryAfterTax = salary - (salary * taxPercent);
    expect(calc.result.employerCalc.pension, "pension").to.almost(socSecSum * (numbers.sideCosts.tyelBasePercent - numbers.sideCosts.tyelWorkerPercent));
    expect(calc.result.employerCalc.unemployment, "unemployment").to.almost(socSecSum * numbers.sideCosts.unemploymentEmployerPercent);
    expect(calc.result.workerCalc.tax, "tax").to.almost(salary * taxPercent);
    expect(calc.result.workerCalc.salaryAfterTax, "salaryAfterTax").to.almost(salaryAfterTax);

    const socSecDeductions = socSecSum * numbers.sideCosts.tyelWorkerPercent + socSecSum * numbers.sideCosts.unemploymentWorkerPercent;
    expect(calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment").to.almost((salaryAfterTax) - workerDeduction - socSecDeductions);
    expect(calc.result.workerCalc.benefits, "benefits").to.equal(0);
    expect(calc.result.workerCalc.fullPension, "fullPension").to.almost(socSecSum * numbers.sideCosts.tyelWorkerPercent);
    expect(calc.result.workerCalc.fullUnemploymentInsurance, "fullUnemploymentInsurance").to.almost(socSecSum * numbers.sideCosts.unemploymentWorkerPercent);

    expect(calc.result.workerCalc.fullUnionPayment, "fullUnionPayment").to.almost(workerDeduction);
    expect(calc.result.workerCalc.unionPayment, "unionPayment").to.almost(workerDeduction);
    expect(calc.result.workerCalc.workerSideCosts, "workerSideCosts").to
      .almost(workerDeduction + calc.result.workerCalc.pension + calc.result.workerCalc.unemploymentInsurance);
    expect(calc.result.workerCalc.deductions, "Worker deductions").to.almost(calc.result.workerCalc.tax + calc.result.workerCalc.workerSideCosts);
    expect(calc.result.workerCalc.otherDeductions, "Worker otherDeductions").to.equal(0);

    // Reports
    const accountingData = await this.sxyHelper.api.reports.getAccountingDataForCalculations([calc]);
    const allEntries = accountingData.ledgerAccounts.map( (acc) => acc.entries).reduce( (all, items) => all.concat(...items), []);
    const debitEntries = allEntries.filter( (x) => x.isDebit && x.dimension.entryCodeGroup === "incomeType" && x.dimension.entryCode === ("408"));
    let reportRow = null;
    if (workerDeduction === 0) {
      expect(debitEntries, "There should not be a reporting row matching the type.").to.have.length(0);
    } else {
      expect(debitEntries, "There should be a reporting row matching the type.").to.have.length(1);
      reportRow = debitEntries[0];
      expect(reportRow.amount).to.almost(workerDeduction);
    }
    return {calc, resultRow, reportRow, numbers};
  }

  /** Runs common test for backward compatibility in Deductions. */
  public async runCommonTestsForDeductions(inputCalc: Calculation, rowType: CalculationRowType, salary: number, deduction: number, expenses = 0, expectedIrCode = 0, expectedReportRows = 1)
    : Promise<{ calc: Calculation, resultRow: ResultRow, reportRows: LedgerEntry[], numbers: YearlyChangingNumbers }> {
    const expect = this.expect;
    const numbers = this.yearlyChangingNumbers;
    const taxPercent = 0.214;
    inputCalc.worker.tax.taxPercentForVisualization = taxPercent * 100;

    const calc = await this.sxyHelper.api.calculator.recalculate(inputCalc);
    const socSecSum = salary;

    const resultRows = calc.result.rows.filter((x) => x.total !== 0);
    expect(resultRows, "2 or 3 rows with value").to.have.length(expenses > 0 ? 3 : 2);

    const resultRow = resultRows.find((x) => x.rowType === rowType);
    expect(resultRow, "Row type as specified").to.exist();
    expect(resultRow.total, "Result row total").to.equal(deduction);
    expect(resultRow.totalBaseSalary, "Result row totalBaseSalary").to.equal(0);
    expect(resultRow.totalDeduction, "Result row totalDeduction").to.equal(deduction);
    expect(resultRow.totalExpenses, "Result row totalExpenses").to.equal(0);
    expect(resultRow.totalGrossSalary, "Result row totalGrossSalary").to.equal(0);
    expect(resultRow.totalSocialSecurityBase, "Result row totalSocialSecurityBase").to.equal(0);
    expect(resultRow.totalTaxable, "Result row totalTaxable").to.equal(0);

    const salaryRow = resultRows.find((x) => x.rowType === CalculationRowType.Salary);
    expect(salaryRow, "Salary row exists").to.exist();
    expect(salaryRow.total, "Salary row total").to.equal(salary);
    expect(salaryRow.totalBaseSalary, "Salary row totalBaseSalary").to.equal(salary);
    expect(salaryRow.totalDeduction, "Salary row totalDeduction").to.equal(0);
    expect(salaryRow.totalExpenses, "Salary row totalExpenses").to.equal(0);
    expect(salaryRow.totalGrossSalary, "Salary row totalGrossSalary").to.equal(salary);
    expect(salaryRow.totalSocialSecurityBase, "Salary row totalSocialSecurityBase").to.equal(socSecSum);
    expect(salaryRow.totalTaxable, "Salary row totalTaxable").to.equal(salary);

    if (expenses > 0) {
      const expensesRow = resultRows.find((x) => x.rowType === CalculationRowType.Expenses);
      expect(expensesRow, "Expenses row exists").to.exist();
      expect(expensesRow.total, "Expenses row total").to.equal(expenses);
      expect(expensesRow.totalBaseSalary, "Expenses row totalBaseSalary").to.equal(0);
      expect(expensesRow.totalDeduction, "Expenses row totalDeduction").to.equal(0);
      expect(expensesRow.totalExpenses, "Expenses row totalExpenses").to.equal(expenses);
      expect(expensesRow.totalGrossSalary, "Expenses row totalGrossSalary").to.equal(0);
      expect(expensesRow.totalSocialSecurityBase, "Expenses row totalSocialSecurityBase").to.equal(0);
      expect(expensesRow.totalTaxable, "Expenses row totalTaxable").to.equal(0);
    }

    // Totals
    expect(calc.result.totals.totalExpenses, "totalExpenses").to.equal(expenses);
    expect(calc.result.totals.totalBaseSalary, "totalBaseSalary").to.equal(salary);
    expect(calc.result.totals.totalGrossSalary, "totalGrossSalary").to.equal(salary);
    expect(calc.result.totals.totalSocialSecurityBase, "totalSocialSecurityBase").to.equal(socSecSum);
    expect(calc.result.totals.totalTaxable, "totalTaxable").to.equal(salary);
    expect(calc.result.totals.pension, "pension").to.almost(socSecSum * numbers.sideCosts.tyelBasePercent);
    expect(calc.result.totals.unemployment, "pension").to.almost(socSecSum * (numbers.sideCosts.unemploymentEmployerPercent + numbers.sideCosts.unemploymentWorkerPercent));

    // Employer numbers
    expect(calc.result.employerCalc.socialSecurity, "socialSecurity").to.almost(socSecSum * numbers.sideCosts.socSecPercent);
    const basePrice = Math.max(4, Math.min((salary + expenses) * 0.01, 10));
    expect(calc.result.employerCalc.palkkaus, "palkkaus").to.almost(basePrice * 1.24);

    const salaryAfterTax = salary - (salary * taxPercent);
    expect(calc.result.employerCalc.pension, "pension").to.almost(socSecSum * (numbers.sideCosts.tyelBasePercent - numbers.sideCosts.tyelWorkerPercent));
    expect(calc.result.employerCalc.unemployment, "unemployment").to.almost(socSecSum * numbers.sideCosts.unemploymentEmployerPercent);
    expect(calc.result.workerCalc.tax, "tax").to.almost(salary * taxPercent);
    expect(calc.result.workerCalc.salaryAfterTax, "salaryAfterTax").to.almost(salaryAfterTax);

    const socSecDeductions = socSecSum * numbers.sideCosts.tyelWorkerPercent + socSecSum * numbers.sideCosts.unemploymentWorkerPercent;
    expect(calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment").to.almost((salaryAfterTax + expenses) - deduction - socSecDeductions);
    expect(calc.result.workerCalc.benefits, "benefits").to.equal(0);

    expect(calc.result.workerCalc.fullPension, "fullPension").to.almost(socSecSum * numbers.sideCosts.tyelWorkerPercent);
    expect(calc.result.workerCalc.fullUnemploymentInsurance, "fullUnemploymentInsurance").to.almost(socSecSum * numbers.sideCosts.unemploymentWorkerPercent);

    // Reports

    const accountingData = await this.sxyHelper.api.reports.getAccountingDataForCalculations([calc]);
    const allEntries = accountingData.ledgerAccounts.map( (acc) => acc.entries).reduce( (all, items) => all.concat(...items), []);
    const creditEntries = allEntries.filter( (x) => !x.isDebit && x.dimension.entryCodeGroup === "incomeType" && x.dimension.entryCode === ("" + expectedIrCode));
    expect(creditEntries, "There should be a reporting row matching the type.").to.have.length(expectedReportRows);
    return {calc, resultRow, reportRows: creditEntries, numbers};
  }
}
