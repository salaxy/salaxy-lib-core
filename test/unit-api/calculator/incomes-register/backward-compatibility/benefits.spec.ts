import chai from "chai";
import chaiAlmost from "chai-almost";
import dirtyChai from "dirty-chai";
import { config } from "../../../../config";

import {
  CalculationRowType, CalculationRowUnit, CalculatorLogic, SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

import { IncomesRegisterTestHelper } from "../IncomesRegisterTestHelper";

describe("API tests", () => {
  describe("Incomes Register (IR): Backward compatibility, Benefits", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    chai.use(chaiAlmost(0.02));
    const sxyHelper = new TestHelper(config, this);
    const irHelper = new IncomesRegisterTestHelper(sxyHelper, expect);

    it("CalculationRowType.AccomodationBenefit", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.AccomodationBenefit,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.AccomodationBenefit, 100, true, true, null, 301);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Asuntoetu");
      expect(results.reportRow.accountNumber).to.equal("5400");
    });

    it("CalculationRowType.MealBenefit", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.MealBenefit,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.MealBenefit, 100, true, true, null, 334);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Ravintoetu");
      expect(results.reportRow.accountNumber).to.equal("5410");
    });

    it("CalculationRowType.PhoneBenefit", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.PhoneBenefit,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.PhoneBenefit, 100, true, true, null, 330);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Puhelinetu");
      expect(results.reportRow.accountNumber).to.equal("5430");
    });

    it("CalculationRowType.OtherBenefit", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.OtherBenefit,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.OtherBenefit, 100, true, true, null, 317);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Muu etu");
      expect(results.reportRow.accountNumber).to.equal("5440");
    });

  });
});
