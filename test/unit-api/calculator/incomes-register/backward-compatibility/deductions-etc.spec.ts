import chai from "chai";
import chaiAlmost from "chai-almost";
import dirtyChai from "dirty-chai";
import { config } from "../../../../config";

import {
  CalculationRowType, CalculationRowUnit, CalculatorLogic, SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

import { IncomesRegisterTestHelper } from "../IncomesRegisterTestHelper";

describe("API tests", () => {
  describe("Incomes Register (IR): Backward compatibility, Deductions etc.", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    chai.use(chaiAlmost(0.02));
    const sxyHelper = new TestHelper(config, this);
    const irHelper = new IncomesRegisterTestHelper(sxyHelper, expect);

    it("CalculationRowType.UnionPayment", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const salary = 500;
      inputCalc.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
        {
          count: 0.015, price: 100, rowType: CalculationRowType.UnionPayment, data: {
            unionPaymentType: "other",
          }
        },
      ];
      const deduction = salary * 0.015;
      const results = await irHelper.runCommonTestsForDeductions(inputCalc, CalculationRowType.UnionPayment, salary, deduction, 0, 408);
      // Dedutions depending on the case
      const calcResult = results.calc.result;
      // This is a bug server-side and needs to be fixed (though it does not really affect anything)
      expect(calcResult.totals.total, "total").to.oneOf([salary, salary + deduction]); // => Should be salary + deduction
      expect(calcResult.employerCalc.deductionUnionPayment, "deductionUnionPayment").to.equal(deduction);
      expect(calcResult.employerCalc.totalDeductions, "Employer deductions").to.almost(deduction + calcResult.totals.unemployment);
      expect(calcResult.workerCalc.fullUnionPayment, "fullUnionPayment").to.almost(deduction);
      expect(calcResult.workerCalc.unionPayment, "unionPayment").to.almost(deduction);
      expect(calcResult.workerCalc.workerSideCosts, "workerSideCosts").to
        .almost(deduction + calcResult.workerCalc.pension + calcResult.workerCalc.unemploymentInsurance);
      expect(calcResult.workerCalc.deductions, "Worker deductions").to.almost(calcResult.workerCalc.tax + calcResult.workerCalc.workerSideCosts);
      // Reporting
      expect(results.resultRow.message).to.equal("Ay-jäsenmaksu");
      expect(results.reportRows[0].accountNumber).to.equal("2925");
      expect(results.reportRows[0].amount, "Report row amount").to.equal(deduction);
    });

    it("CalculationRowType.Foreclosure", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const deduction = 20;
      inputCalc.rows = [
        { price: 100, rowType: CalculationRowType.Salary },
        { price: deduction, rowType: CalculationRowType.Foreclosure },
      ];

      const results = await irHelper.runCommonTestsForDeductions(inputCalc, CalculationRowType.Foreclosure, 100, deduction, 0, 417);
      // Dedutions depending on the case
      const calcResult = results.calc.result;
      expect(calcResult.totals.total, "total").to.equal(100 + deduction);
      expect(calcResult.employerCalc.totalDeductions, "Employer deductions").to.almost(deduction + calcResult.totals.unemployment);
      expect(calcResult.workerCalc.deductions, "Worker deductions").to.almost(deduction + calcResult.workerCalc.tax + calcResult.workerCalc.workerSideCosts);

      expect(calcResult.workerCalc.foreclosure, "Foreclosure from worker").to.almost(deduction);
      expect(calcResult.workerCalc.salaryAfterTaxAndForeclosure, "salaryAfterTaxAndForeclosure").to
        .almost(calcResult.workerCalc.salaryAfterTax - deduction);
      expect(calcResult.employerCalc.deductionForeclosure, "Foreclosure from employer").to.almost(deduction);
      expect(calcResult.employerCalc.foreclosureByPalkkaus, "Foreclosure paid by Palkkaus").to.almost(0);

      // Reporting
      expect(results.resultRow.message).to.equal("Ulosmittaus (työnantaja)");
      expect(results.reportRows[0].accountNumber).to.equal("2933");
      expect(results.reportRows[0].amount, "Report row amount").to.equal(deduction);
    });

    it("CalculationRowType.ForeclosureByPalkkaus", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const deduction = 20;
      inputCalc.rows = [
        { price: 100, rowType: CalculationRowType.Salary },
        { price: deduction, rowType: CalculationRowType.ForeclosureByPalkkaus },
      ];
      const results = await irHelper.runCommonTestsForDeductions(inputCalc, CalculationRowType.ForeclosureByPalkkaus, 100, deduction, 0, 417);
      // Dedutions depending on the case
      const calcResult = results.calc.result;
      expect(calcResult.totals.total, "total").to.equal(100 + deduction);
      expect(calcResult.employerCalc.totalDeductions, "Employer deductions").to.almost(calcResult.totals.unemployment);
      expect(calcResult.workerCalc.deductions, "Worker deductions").to.almost(deduction + calcResult.workerCalc.tax + calcResult.workerCalc.workerSideCosts);

      expect(calcResult.workerCalc.foreclosure, "Foreclosure from worker").to.almost(deduction);
      expect(calcResult.workerCalc.salaryAfterTaxAndForeclosure, "salaryAfterTaxAndForeclosure").to
        .almost(calcResult.workerCalc.salaryAfterTax - deduction);
      expect(calcResult.employerCalc.deductionForeclosure, "Foreclosure from employer").to.almost(0);
      expect(calcResult.employerCalc.foreclosureByPalkkaus, "Foreclosure paid by Palkkaus").to.almost(deduction);

      // Reporting
      expect(results.resultRow.message).to.equal("Ulosmittaus (Palkkaus.fi)");
      expect(results.reportRows[0].accountNumber).to.equal("2933");
      expect(results.reportRows[0].amount, "Report row amount").to.equal(deduction);
    });

    it("CalculationRowType.Advance", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows = [
        { price: 100, rowType: CalculationRowType.Salary },
        { price: 20, rowType: CalculationRowType.Advance },
      ];
      const deduction = 20;
      const results = await irHelper.runCommonTestsForDeductions(inputCalc, CalculationRowType.Advance, 100, deduction, 0, 1);
      // Dedutions depending on the case
      const calcResult = results.calc.result;
      expect(calcResult.totals.total, "total").to.equal(100 + deduction);
      expect(calcResult.employerCalc.totalDeductions, "Employer deductions").to.almost(deduction + calcResult.totals.unemployment);
      expect(calcResult.workerCalc.fullSalaryAdvance, "fullSalaryAdvance").to.almost(deduction);
      expect(calcResult.workerCalc.salaryAdvance, "salaryAdvance").to.almost(deduction);
      expect(calcResult.workerCalc.deductions, "Worker deductions").to.almost(deduction + calcResult.workerCalc.tax + calcResult.workerCalc.workerSideCosts);
      // Reporting
      expect(results.resultRow.message).to.equal("Palkkaennakko");
      expect(results.reportRows[0].accountNumber).to.equal("2961");
      expect(results.reportRows[0].amount, "Report row amount").to.equal(deduction);
    });

    it("CalculationRowType.PrepaidExpenses", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const deduction = 20;
      inputCalc.rows = [
        { price: 100, rowType: CalculationRowType.Salary },
        { price: 50, rowType: CalculationRowType.Expenses },
        { price: deduction, rowType: CalculationRowType.PrepaidExpenses },
      ];
      const results = await irHelper.runCommonTestsForDeductions(inputCalc, CalculationRowType.PrepaidExpenses, 100, deduction, 50, 1, 2);
      // Dedutions depending on the case
      const calcResult = results.calc.result;
      expect(calcResult.totals.total, "total").to.equal(100 + deduction + 50);
      expect(calcResult.employerCalc.totalDeductions, "Employer deductions").to.almost(calcResult.totals.unemployment);
      expect(calcResult.employerCalc.totalPayment, "Employer totalPayment").to
        .almost(calcResult.employerCalc.totalSalaryCost - calcResult.employerCalc.deductionUnemploymentSelfPayment + 50 - deduction);
      expect(calcResult.workerCalc.deductions, "Worker deductions").to.almost(calcResult.workerCalc.tax + calcResult.workerCalc.workerSideCosts);
      expect(calcResult.workerCalc.prepaidExpenses, "prepaidExpenses").equals(deduction);
      expect(calcResult.workerCalc.fullPrepaidExpenses, "fullPrepaidExpenses").equals(deduction);
      expect(calcResult.workerCalc.totalWorkerPayment, "fullPrepaidExpenses")
        .almost(calcResult.workerCalc.salaryPayment + 50 - deduction);
      // Reporting
      expect(results.resultRow.message).to.equal("Ennakkoon maksettu kulukorvaus");
      const reportRow = results.reportRows.find( (x) => x.accountNumber === "7860");
      expect(reportRow).not.to.be.null();
      expect(reportRow.amount, "Report row amount").to.equal(deduction);
    });

    it("CalculationRowType.OtherDeductions", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows = [
        { price: 100, rowType: CalculationRowType.Salary },
        { price: 20, rowType: CalculationRowType.OtherDeductions },
      ];
      const deduction = 20;
      const results = await irHelper.runCommonTestsForDeductions(inputCalc, CalculationRowType.OtherDeductions, 100, deduction, 0, 408);
      // Dedutions depending on the case
      const calcResult = results.calc.result;
      expect(calcResult.totals.total, "total").to.equal(100 + deduction);
      expect(calcResult.employerCalc.deductionOtherDeductions, "deductionOtherDeductions").to.almost(deduction);
      expect(calcResult.employerCalc.totalDeductions, "Employer deductions").to.almost(deduction + calcResult.totals.unemployment);
      expect(calcResult.workerCalc.fullOtherDeductions, "fullOtherDeductions").to.almost(deduction);
      expect(calcResult.workerCalc.otherDeductions, "otherDeductions").to.almost(deduction);
      expect(calcResult.workerCalc.deductions, "Worker deductions").to.almost(deduction + calcResult.workerCalc.tax + calcResult.workerCalc.workerSideCosts);

      // Reporting
      expect(results.resultRow.message).to.equal("Muu vähennys");
      expect(results.reportRows[0].accountNumber).to.equal("2979");
      expect(results.reportRows[0].amount, "Report row amount").to.equal(deduction);
    });

    it("CalculationRowType.ChildCareSubsidy", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      const salary = 100;
      const subsidy = 50;
      inputCalc.rows = [
        { price: salary, rowType: CalculationRowType.Salary },
        { price: subsidy, rowType: CalculationRowType.ChildCareSubsidy },
      ];
      const numbers = irHelper.yearlyChangingNumbers;
      const taxPercent = 0.214;
      inputCalc.worker.tax.taxPercentForVisualization = taxPercent * 100;

      const calc = await sxyHelper.api.calculator.recalculate(inputCalc);

      const resultRows = calc.result.rows.filter((x) => x.total !== 0);
      expect(resultRows, "2 rows with value").to.have.length(2);

      const resultRow = resultRows.find((x) => x.rowType === CalculationRowType.ChildCareSubsidy);
      expect(resultRow, "Row type as specified").to.exist();
      expect(resultRow.total, "Result row total").to.equal(subsidy);
      expect(resultRow.totalBaseSalary, "Result row totalBaseSalary").to.equal(0);
      expect(resultRow.totalDeduction, "Result row totalDeduction").to.equal(0);
      expect(resultRow.totalExpenses, "Result row totalExpenses").to.equal(0);
      expect(resultRow.totalGrossSalary, "Result row totalGrossSalary").to.equal(0);
      expect(resultRow.totalSocialSecurityBase, "Result row totalSocialSecurityBase").to.equal(subsidy);
      expect(resultRow.totalTaxable, "Result row totalTaxable").to.equal(0);

      const salaryRow = resultRows.find((x) => x.rowType === CalculationRowType.Salary);
      expect(salaryRow, "Salary row exists").to.exist();
      expect(salaryRow.total, "Salary row total").to.equal(salary);
      expect(salaryRow.totalBaseSalary, "Salary row totalBaseSalary").to.equal(salary);
      expect(salaryRow.totalDeduction, "Salary row totalDeduction").to.equal(0);
      expect(salaryRow.totalExpenses, "Salary row totalExpenses").to.equal(0);
      expect(salaryRow.totalGrossSalary, "Salary row totalGrossSalary").to.equal(salary);
      expect(salaryRow.totalSocialSecurityBase, "Salary row totalSocialSecurityBase").to.equal(salary);
      expect(salaryRow.totalTaxable, "Salary row totalTaxable").to.equal(salary);

      // Totals
      const socSecSum = salary + subsidy;
      expect(calc.result.totals.total, "total").to.equal(socSecSum);
      expect(calc.result.totals.totalExpenses, "totalExpenses").to.equal(0);
      expect(calc.result.totals.totalBaseSalary, "totalBaseSalary").to.equal(salary);
      expect(calc.result.totals.totalGrossSalary, "totalGrossSalary").to.equal(salary);
      expect(calc.result.totals.totalSocialSecurityBase, "totalSocialSecurityBase").to.equal(socSecSum);
      expect(calc.result.totals.totalTaxable, "totalTaxable").to.equal(salary);
      expect(calc.result.totals.pension, "pension").to.almost(socSecSum * numbers.sideCosts.tyelBasePercent);
      expect(calc.result.totals.unemployment, "pension").to.almost(socSecSum * (numbers.sideCosts.unemploymentEmployerPercent + numbers.sideCosts.unemploymentWorkerPercent));

      // Employer numbers
      expect(calc.result.employerCalc.socialSecurity, "socialSecurity").to.almost(socSecSum * numbers.sideCosts.socSecPercent);
      const basePrice = Math.max(4, Math.min(socSecSum * 0.01, 10));
      expect(calc.result.employerCalc.palkkaus, "palkkaus").to.almost(basePrice * 1.24);
      expect(calc.result.employerCalc.totalDeductions, "totalDeductions").to.almost(socSecSum * (numbers.sideCosts.unemploymentEmployerPercent + numbers.sideCosts.unemploymentWorkerPercent));

      const netSalary = salary - (salary * taxPercent);
      expect(calc.result.employerCalc.pension, "pension").to.almost(socSecSum * (numbers.sideCosts.tyelBasePercent - numbers.sideCosts.tyelWorkerPercent));
      expect(calc.result.employerCalc.unemployment, "unemployment").to.almost(socSecSum * numbers.sideCosts.unemploymentEmployerPercent);
      expect(calc.result.workerCalc.tax, "tax").to.almost(salary * taxPercent);
      expect(calc.result.workerCalc.salaryAfterTax, "salaryAfterTax").to.almost(netSalary);
      expect(calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment").to.almost(netSalary - socSecSum * numbers.sideCosts.tyelWorkerPercent - socSecSum * numbers.sideCosts.unemploymentWorkerPercent);
      expect(calc.result.workerCalc.benefits, "benefits").to.equal(0);

      expect(calc.result.workerCalc.fullPension, "fullPension").to.almost(socSecSum * numbers.sideCosts.tyelWorkerPercent);
      expect(calc.result.workerCalc.fullUnemploymentInsurance, "fullUnemploymentInsurance").to.almost(socSecSum * numbers.sideCosts.unemploymentWorkerPercent);

      // All deductions should be 0 - Can't be checked from calc.result.workerCalc.deductions, because that is zero if there is not enough to deduct
      expect(calc.result.workerCalc.foreclosure, "foreclosure").to.equal(0);
      expect(calc.result.workerCalc.fullSalaryAdvance, "fullSalaryAdvance").to.equal(0);
      expect(calc.result.workerCalc.fullPrepaidExpenses, "fullPrepaidExpenses").to.equal(0);
      expect(calc.result.workerCalc.fullOtherDeductions, "fullOtherDeductions").to.equal(0);
      expect(calc.result.workerCalc.fullUnionPayment, "fullUnionPayment").to.equal(0);

      // Reports
      const accountingData = await sxyHelper.api.reports.getAccountingDataForCalculations([calc]);
      const allEntries = accountingData.ledgerAccounts.map( (acc) => acc.entries).reduce( (all, items) => all.concat(...items), []);
      const debitEntries = allEntries.filter( (x) => x.isDebit && x.dimension.entryCodeGroup === "incomeType" && x.dimension.entryCode === ("" + 321));
      expect(debitEntries, "There should not be a reporting row matching the type.").to.have.length(0);

     });
  });
});
