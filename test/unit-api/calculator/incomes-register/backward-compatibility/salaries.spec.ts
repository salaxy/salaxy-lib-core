import chai from "chai";
import chaiAlmost from "chai-almost";
import dirtyChai from "dirty-chai";
import { config } from "../../../../config";

import {
  CalculationRowType, CalculationRowUnit, CalculatorLogic, SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

import { IncomesRegisterTestHelper } from "../IncomesRegisterTestHelper";

describe("API tests", () => {
  describe("Incomes Register (IR): Backward compatibility, Salaries", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    chai.use(chaiAlmost(0.02));
    const sxyHelper = new TestHelper(config, this);
    const irHelper = new IncomesRegisterTestHelper(sxyHelper, expect);

    it("CalculationRowType.Unknown becomes a zero-row", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.Unknown,
      });
      const target = await sxyHelper.api.calculator.recalculate(inputCalc);
      expect(target.result.rows).to.have.length(1);
      expect(target.result.rows[0].total).to.equal(0);
      expect(target.result.totals.total).to.equal(0);
      expect(target.result.totals.totalGrossSalary).to.equal(0);
      expect(target.result.totals.totalSocialSecurityBase).to.equal(0);
      expect(target.result.totals.totalTaxable).to.equal(0);
      expect(target.result.workerCalc.totalWorkerPayment).to.equal(0);
      expect(target.result.employerCalc.totalPayment).to.equal(4.96);
    });

    it("CalculationRowType.Salary", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.Salary,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.Salary, 100, false, false, null, 227);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Palkka tai palkkio");
      expect(results.reportRow.accountNumber).to.equal("5000");
    });

    it("CalculationRowType.HourlySalary", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 5,
        price: 20,
        rowType: CalculationRowType.HourlySalary,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.HourlySalary, 100, false, false, null, 201);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Hours);
      expect(results.resultRow.message).to.equal("Tuntipalkka");
      expect(results.reportRow.accountNumber).to.equal("5000");
    });

    it("CalculationRowType.MonthlySalary", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 1000,
        rowType: CalculationRowType.MonthlySalary,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.MonthlySalary, 1000, false, false, null, 201);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Kuukausipalkka");
      expect(results.reportRow.accountNumber).to.equal("5000");
    });

    it("CalculationRowType.HolidaySalary", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 1000,
        rowType: CalculationRowType.HolidaySalary,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.HolidaySalary, 1000, false, false, null, 201);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Loma-ajan palkka");
      expect(results.reportRow.accountNumber).to.equal("5000");
    });

    it("CalculationRowType.TotalWorkerPayment", async () => {

      // TODO: check tax calculation, rounding error in backend?

      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 300,
        rowType: CalculationRowType.TotalWorkerPayment,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.Salary, 427.5, false, false, null, 227);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Palkka tai palkkio");
      expect(results.reportRow.accountNumber).to.equal("5000");
    });

    it("CalculationRowType.TotalEmployerPayment", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 200,
        rowType: CalculationRowType.TotalEmployerPayment,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.Salary, 162.62, false, false, null, 227);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Palkka tai palkkio");
      expect(results.reportRow.accountNumber).to.equal("5000");
    });

    it("CalculationRowType.Compensation", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 500,
        rowType: CalculationRowType.Compensation,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.Compensation, 500, false, false, 0, 336);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Työkorvaus");
      expect(results.reportRow.accountNumber).to.equal("5130");
    });

    it("SalaryKind.Undefined becomes CalculationRowType.Salary", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.salary = {
        amount: 1,
        price: 100,
        kind: SalaryKind.Undefined,
      };
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.Salary, 100, false, false, null, 227);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Palkka tai palkkio");
      expect(results.reportRow.accountNumber).to.equal("5000");
    });

    it("SalaryKind.FixedSalary becomes CalculationRowType.Salary", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.salary = {
        amount: 1,
        price: 100,
        kind: SalaryKind.FixedSalary,
      };
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.Salary, 100, false, false, null, 227);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Palkka tai palkkio");
      expect(results.reportRow.accountNumber).to.equal("5000");
    });

    it("SalaryKind.HourlySalary becomes CalculationRowType.HourlySalary", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.salary = {
        amount: 10,
        price: 10,
        kind: SalaryKind.HourlySalary,
      };
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.HourlySalary, 100, false, false, null, 201);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Tuntipalkka");
      expect(results.reportRow.accountNumber).to.equal("5000");
    });

    it("SalaryKind.MonthlySalary becomes CalculationRowType.MonthlySalary", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.salary = {
        amount: 1,
        price: 1000,
        kind: SalaryKind.MonthlySalary,
      };
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.MonthlySalary, 1000, false, false, null, 201);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Kuukausipalkka");
      expect(results.reportRow.accountNumber).to.equal("5000");
    });

    it("SalaryKind.TotalWorkerPayment becomes CalculationRowType.TotalWorkerPayment", async () => {

      // TODO: check tax calculation, rounding error in backend?

      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.salary = {
        amount: 1,
        price: 300,
        kind: SalaryKind.TotalWorkerPayment,
      };
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.Salary, 427.5, false, false, null, 227);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Palkka tai palkkio");
      expect(results.reportRow.accountNumber).to.equal("5000");
    });

    it("SalaryKind.TotalEmployerPayment becomes CalculationRowType.TotalEmployerPayment", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.salary = {
        amount: 1,
        price: 200,
        kind: SalaryKind.TotalEmployerPayment,
      };
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.Salary, 162.62, false, false, null, 227);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Palkka tai palkkio");
      expect(results.reportRow.accountNumber).to.equal("5000");
    });

    it("SalaryKind.Compensation becomes CalculationRowType.Compensation", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.salary = {
        amount: 1,
        price: 500,
        kind: SalaryKind.Compensation,
      };
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.Compensation, 500, false, false, 0, 336);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Työkorvaus");
      expect(results.reportRow.accountNumber).to.equal("5130");
    });

  });
});
