import chai from "chai";
import chaiAlmost from "chai-almost";
import dirtyChai from "dirty-chai";
import { config } from "../../../../config";

import {
  CalculationRowType, CalculationRowUnit, CalculatorLogic, SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

import { IncomesRegisterTestHelper } from "../IncomesRegisterTestHelper";

describe("API tests", () => {
  describe("Incomes Register (IR): Backward compatibility, Expenses", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    chai.use(chaiAlmost(0.02));
    const sxyHelper = new TestHelper(config, this);
    const irHelper = new IncomesRegisterTestHelper(sxyHelper, expect);

    it("CalculationRowType.DailyAllowance", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.DailyAllowance,
      });
      const results = await irHelper.runCommonTestsForExpenses(inputCalc, CalculationRowType.DailyAllowance, 100, 331);
      expect(results.resultRow.message).to.equal("Kokopäiväraha");
      expect(results.reportRow.accountNumber).to.equal("7880");
    });

    it("CalculationRowType.DailyAllowanceHalf", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.DailyAllowanceHalf,
      });
      const results = await irHelper.runCommonTestsForExpenses(inputCalc, CalculationRowType.DailyAllowanceHalf, 100, 331);
      expect(results.resultRow.message).to.equal("Osapäiväraha");
      expect(results.reportRow.accountNumber).to.equal("7880");
    });

    it("CalculationRowType.MealCompensation", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.MealCompensation,
      });
      const results = await irHelper.runCommonTestsForExpenses(inputCalc, CalculationRowType.MealCompensation, 100, 303);
      expect(results.resultRow.message).to.equal("Ateriakorvaus");
      expect(results.reportRow.accountNumber).to.equal("7910");
    });

    it("CalculationRowType.MilageOwnCar", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.MilageOwnCar,
      });
      const results = await irHelper.runCommonTestsForExpenses(inputCalc, CalculationRowType.MilageOwnCar, 100, 311);
      expect(results.resultRow.message).to.equal("Kilometrikorvaus, oma auto");
      expect(results.reportRow.accountNumber).to.equal("7870");
    });

    it("CalculationRowType.ToolCompensation", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.ToolCompensation,
      });
      const results = await irHelper.runCommonTestsForExpenses(inputCalc, CalculationRowType.ToolCompensation, 100, 353);
      expect(results.resultRow.message).to.equal("Työvälinekorvaus");
      expect(results.reportRow.accountNumber).to.equal("7760");
    });

    it("CalculationRowType.Expenses", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.Expenses,
      });
      const results = await irHelper.runCommonTestsForExpenses(inputCalc, CalculationRowType.Expenses, 100, 1);
      expect(results.resultRow.message).to.equal("Kulukorvaus");
      expect(results.reportRow.accountNumber).to.equal("7860");
    });

    it("CalculationRowType.MilageDaily", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.MilageDaily,
      });
      const results = await irHelper.runCommonTestsForExpenses(inputCalc, CalculationRowType.MilageDaily, 100, 311);
      expect(results.resultRow.message).to.equal("Kilometrikorvaus, päiväkohtainen");
      expect(results.reportRow.accountNumber).to.equal("7870");
    });

    it("CalculationRowType.MilageOther", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.MilageOther,
      });
      const results = await irHelper.runCommonTestsForExpenses(inputCalc, CalculationRowType.MilageOther, 100, 311);
      expect(results.resultRow.message).to.equal("Kilometrikorvaus, muu peruste");
      expect(results.reportRow.accountNumber).to.equal("7870");
    });

  });
});
