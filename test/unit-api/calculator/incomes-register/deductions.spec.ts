import chai from "chai";
import chaiAlmost from "chai-almost";
import dirtyChai from "dirty-chai";
import { config } from "../../../config";

import { CalculationRowType, CalculatorLogic } from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

import { IncomesRegisterTestHelper } from "./IncomesRegisterTestHelper";

describe("API tests", () => {
  describe("Incomes Register (IR): Deductions", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    chai.use(chaiAlmost(0.02));
    const sxyHelper = new TestHelper(config, this);
    const irHelper = new IncomesRegisterTestHelper(sxyHelper, expect);

    it("401: Compensation collected for car benefit basic test", async () => {
      const salary = 2000;
      const deduction = 500;
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.result.irRows = [
        { price: salary, irData: { code: 220 } },
        { price: deduction, irData: { code: 401 } },
      ];
      const results = await irHelper.runCommonTestsForIncomeType(401, deduction, salary, inputCalc, true);
      expect(results.calc.result.totals.totalSocialSecurityBase, "totalSocialSecurityBase").almost(salary - deduction);
      expect(results.calc.result.totals.totalTaxable, "totalTaxable").almost(salary - deduction);
      expect(results.calc.result.employerCalc.totalPayment, "totalPayment")
        .almost(salary - deduction + irHelper.getStandardEmployerAdditions(results.calc));
      expect(results.calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment")
        .almost(salary - deduction - irHelper.getStandardWorkerDeductions(results.calc));
    });

    xit("401: When compensation for CarBenefit is larger than the car benefit, deduction should be gapped to Benefit amount.", async () => {
      // TODO - later
    });

    it("407: Reimbursement collected for other fringe benefits basic test", async () => {
      const salary = 800;
      const deduction = 150;
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.result.irRows = [
        { price: salary, irData: { code: 220 } },
        { price: deduction, irData: { code: 407 } },
      ];
      const results = await irHelper.runCommonTestsForIncomeType(407, deduction, salary, inputCalc, true);
      expect(results.calc.result.totals.totalSocialSecurityBase, "totalSocialSecurityBase").almost(salary - deduction);
      expect(results.calc.result.totals.totalTaxable, "totalTaxable").almost(salary - deduction);
      expect(results.calc.result.employerCalc.totalPayment, "totalPayment")
        .almost(salary - deduction + irHelper.getStandardEmployerAdditions(results.calc));
      expect(results.calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment")
        .almost(salary - deduction - irHelper.getStandardWorkerDeductions(results.calc));
    });

    xit("407: Reimbursement for fringe benefit that has insurance exceptions should also have insurance exceptions", async () => {
      // TODO - later
    });

    it("419 Deduction before withholding", async () => {
      const salary = 1000;
      const deduction = 300;
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.result.irRows = [
        { price: salary, irData: { code: 220 } },
        { price: deduction, irData: { code: 419 } },
      ];
      const results = await irHelper.runCommonTestsForIncomeType(419, deduction, salary, inputCalc, true, true);
      expect(results.calc.result.totals.totalSocialSecurityBase, "totalSocialSecurityBase").almost(salary - deduction);
      expect(results.calc.result.totals.totalTaxable, "totalTaxable").almost(salary - deduction);
      expect(results.calc.result.employerCalc.totalPayment, "totalPayment")
        .almost(salary + irHelper.getStandardEmployerAdditions(results.calc));
      expect(results.calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment")
        .almost(salary - irHelper.getStandardWorkerDeductions(results.calc));
    });

    it("408: Other item deductible from net wage or salary basic test", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(408, 100, 500);
      expect(results.calc.result.workerCalc.otherDeductions, "otherDeductions").equals(100);
      expect(results.calc.result.employerCalc.deductionOtherDeductions, "deductionOtherDeductions").equals(100);
      expect(results.calc.result.employerCalc.totalPayment, "totalPayment")
        .almost(500 - 100 + irHelper.getStandardEmployerAdditions(results.calc));
      expect(results.calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment")
        .almost(500 - 100 - irHelper.getStandardWorkerDeductions(results.calc));
    });

    it("408: CalculationRowType.OtherDeductions should become 408-row", async () => {
      const calc = CalculatorLogic.getBlank();
      calc.rows = [
        { rowType: CalculationRowType.Salary, price: 500 },
        { rowType: CalculationRowType.OtherDeductions, price: 100 },
      ];
      const results = await irHelper.runCommonTestsForIncomeType(408, 100, 500, calc);
      expect(results.calc.result.workerCalc.otherDeductions, "otherDeductions").equals(100);
      expect(results.calc.result.employerCalc.deductionOtherDeductions, "deductionOtherDeductions").equals(100);
      irHelper.checkTotalPayments(results.calc);
      expect(results.calc.result.employerCalc.totalPayment, "totalPayment")
        .almost(500 - 100 + irHelper.getStandardEmployerAdditions(results.calc));
      expect(results.calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment")
        .almost(500 - 100 - irHelper.getStandardWorkerDeductions(results.calc));
    });

    it("411: Employer-paid premium for collective additional pension insurance, employee's contribution basic test", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(411, 200, 1000);
      expect(results.calc.result.employerCalc.totalPayment, "totalPayment")
        .almost(1000 - 200 + irHelper.getStandardEmployerAdditions(results.calc));
      expect(results.calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment")
        .almost(1000 - 200 - irHelper.getStandardWorkerDeductions(results.calc));
    });

    it("415: Reimbursement for employer-subsidised commuter ticket basic test", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(415, 300, 800);
      expect(results.calc.result.employerCalc.totalPayment, "totalPayment")
        .almost(800 - 300 + irHelper.getStandardEmployerAdditions(results.calc));
      expect(results.calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment")
        .almost(800 - 300 - irHelper.getStandardWorkerDeductions(results.calc));
    });

    it("417: Distraint basic test", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(417, 100, 500);
      expect(results.calc.result.workerCalc.foreclosure, "foreclosure").equals(100);
      expect(results.calc.result.employerCalc.deductionForeclosure, "deductionForeclosure").equals(100);
      expect(results.calc.result.employerCalc.foreclosureByPalkkaus, "deductionForeclosure").equals(0);
    });

    it("417: CalculationRowType.Foreclosure should become 417-row", async () => {
      const calc = CalculatorLogic.getBlank();
      calc.rows = [
        { rowType: CalculationRowType.Salary, price: 500 },
        { rowType: CalculationRowType.Foreclosure, price: 100 },
      ];
      const results = await irHelper.runCommonTestsForIncomeType(417, 100, 500, calc);
      irHelper.checkTotalPayments(results.calc);
      expect(results.calc.result.workerCalc.foreclosure, "foreclosure").equals(100);
      expect(results.calc.result.employerCalc.deductionForeclosure, "deductionForeclosure").equals(100);
      expect(results.calc.result.employerCalc.foreclosureByPalkkaus, "foreclosureByPalkkaus").equals(0);
      expect(results.calc.result.employerCalc.totalPayment, "totalPayment")
        .almost(500 - 100 + irHelper.getStandardEmployerAdditions(results.calc));
      expect(results.calc.result.workerCalc.totalWorkerPayment, "totalWorkerPayment")
        .almost(500 - 100 - irHelper.getStandardWorkerDeductions(results.calc));
    });

    it("417: CalculationRowType.ForeclosureByPalkkaus should become 417-row", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows = [
        { rowType: CalculationRowType.Salary, price: 500 },
        { rowType: CalculationRowType.ForeclosureByPalkkaus, price: 100 },
      ];
      const calcResult = await sxyHelper.api.calculator.recalculate(inputCalc);
      irHelper.checkTotalPayments(calcResult);
      expect(calcResult.result.workerCalc.foreclosure, "foreclosure").equals(100);
      expect(calcResult.result.employerCalc.deductionForeclosure, "deductionForeclosure").equals(0);
      expect(calcResult.result.employerCalc.foreclosureByPalkkaus, "foreclosureByPalkkaus").equals(100);
      expect(calcResult.result.employerCalc.totalPayment, "totalPayment")
        .almost(500 + irHelper.getStandardEmployerAdditions(calcResult));
      expect(calcResult.result.workerCalc.totalWorkerPayment, "totalWorkerPayment")
        .almost(500 - 100 - irHelper.getStandardWorkerDeductions(calcResult));
    });

  });
});
