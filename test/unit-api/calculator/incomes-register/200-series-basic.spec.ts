import chai from "chai";
import chaiAlmost from "chai-almost";
import dirtyChai from "dirty-chai";
import { config } from "../../../config";

import { CalculationRowType, CalculatorLogic } from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

import { IncomesRegisterTestHelper } from "./IncomesRegisterTestHelper";

describe("API tests", () => {
  describe("Incomes Register (IR): 200 series basic tests", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    chai.use(chaiAlmost(0.02));
    const sxyHelper = new TestHelper(config, this);
    const irHelper = new IncomesRegisterTestHelper(sxyHelper, expect);

    it("201: Time-rate pay", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(201, 100);
      // TODO: Additional tests.
    });

    it("202: Initiative fee", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(202, 100);
      // TODO: Additional tests.
    });

    it("203: Bonus pay", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(203, 100);
      // TODO: Additional tests.
    });

    it("204: Complementary wage/salary paid during benefit period", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(204, 100);
      // TODO: Additional tests.
    });

    it("205: Emergency work compensation", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(205, 100);
      // TODO: Additional tests.
    });

    it("206: Evening work compensation", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(206, 100);
      // TODO: Additional tests.
    });

    it("207: Evening shift allowance", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(207, 100);
      // TODO: Additional tests.
    });

    it("208: Notice period compensation", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(208, 100);
      // TODO: Additional tests.
    });

    it("209: Kilometre allowance (taxable)", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(209, 100);
      // TODO: Additional tests.
    });

    it("210: Meeting fee", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(210, 100);
      // TODO: Additional tests.
    });

    it("211: Saturday pay", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(211, 100);
      // TODO: Additional tests.
    });

    it("212: Extra work premium", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(212, 100);
      // TODO: Additional tests.
    });

    it("213: Holiday bonus", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(213, 100);
      // TODO: Additional tests.
    });

    it("214: Lecture fee", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(214, 100);
      // TODO: Additional tests.
    });

    it("215: Compensation for acting in a position of trust", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(215, 100);
      // TODO: Additional tests.
    });

    it("216: Other compensation", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(216, 100);
      // TODO: Additional tests.
    });

    it("217: Waiting time compensation", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(217, 100);
      // TODO: Additional tests.
    });

    it("218: Working condition compensation", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(218, 100);
      // TODO: Additional tests.
    });

    it("219: Partial pay during sick leave", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(219, 100);
      // TODO: Additional tests.
    });

    it("220: Commission", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(220, 100);
      // TODO: Additional tests.
    });

    it("221: Sunday work compensation", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(221, 100);
      // TODO: Additional tests.
    });

    it("222: Benefit arising from synthetic option", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(222, 100);
      // TODO: Additional tests.
    });

    it("223: Performance bonus", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(223, 100);
      // TODO: Additional tests.
    });

    it("224: Monetary compensation from a working time bank", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(224, 100);
      // TODO: Additional tests.
    });

    it("225: Compensation for accrued time off", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(225, 100);
      // TODO: Additional tests.
    });

    it("226: Share issue for employees", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(226, 500, 1000);
      // TODO: Additional tests.
    });

    it("227: Contract pay ", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(227, 100);
      // TODO: Additional tests.
    });

    it("229: Damages in conjunction with termination and lay-off", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(229, 100);
      // TODO: Additional tests.
    });

    it("230: Stand-by compensation", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(230, 100);
      // TODO: Additional tests.
    });

    it("231: Voluntary compensation in conjunction with termination of employment", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(231, 100);
      // TODO: Additional tests.
    });

    it("232: Weekly rest compensation", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(232, 100);
      // TODO: Additional tests.
    });

    it("233: Profit-sharing bonus", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(233, 100);
      // TODO: Additional tests.
    });

    it("234: Annual holiday compensation", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(234, 100);
      // TODO: Additional tests.
    });

    it("235: Overtime compensation", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(235, 100);
      // TODO: Additional tests.
    });

    it("236: Night work allowance", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(236, 100);
      // TODO: Additional tests.
    });

    it("237: Night shift compensation", async () => {
      const results = await irHelper.runCommonTestsForIncomeType(237, 100);
      // TODO: Additional tests.
    });

  });
});
