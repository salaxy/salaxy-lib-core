import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../config";

import {
    ApiTestErrorType,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

describe("API tests", () => {
  describe("Exception handling tests", function() {
    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    it("In Node.js, API methods have then method with success and error methods.",  (done) => {
        sxyHelper.api.test.throwException(ApiTestErrorType.Default)
            .then(
                (data) => { done("Error, execution should not come to Then block when there is an exception."); },
                (data) => { done(); },
            );
    });

    it("In Node.js, API methods have then AND catch methods.",  (done) => {
      sxyHelper.api.test.throwException(ApiTestErrorType.Default)
            .then((data) => { done("Error, execution should not come to Then block when there is an exception."); })
            .catch((data) => { done(); });
    });

  });
});
