import {
  WorkerAccount,
  WorkerLogic,
} from "@salaxy/core";
import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../../config";

import {
  TestHelper,
} from "@salaxy/node";

xdescribe("API tests", () => {
  xdescribe("Workers: Assure and modify", function() {

    const expect = chai.expect;
    chai.use(dirtyChai);

    const sxyHelper = new TestHelper(config, this);

    before("authenticate, will be removed eventually", async () => {
      await sxyHelper.authenticate();
    });

    // npm run test-path "test/unit-api/workers/assure-api.spec.ts"

    it("Assures worker account", async () => {

      let method = "/partner/assureworker";
      method += "?officialId=" + encodeURIComponent("281056-405M");
      method += "&firstName=" + encodeURIComponent("John");
      method += "&lastName=" + encodeURIComponent("Worker");
      method += "&email=" + encodeURIComponent("john.x.worker@palkkaus.fi");
      method += "&telephone=" + encodeURIComponent("+3585655666777");
      method += "&bankAccountIban=" + encodeURIComponent("FI9517453000181869");

      const workerAccount = await sxyHelper.ajax.postJSON(method, "") as WorkerAccount;

      expect(workerAccount).to.not.be.null();
      expect(workerAccount.contact.email).equal("john.x.worker@palkkaus.fi");

      workerAccount.contact.email = "john.worker@palkkaus.fi";

      const modifiedWorkerAccount = await sxyHelper.api.workers.save(workerAccount);
      expect(modifiedWorkerAccount.contact.email).equal("john.worker@palkkaus.fi");

    });
  });
});
