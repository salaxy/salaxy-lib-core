import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../config";

import {
    Calculation,
    Dates,
    Numeric,
    SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

describe("API tests", () => {
  describe("Rounding error cases", function() {
    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    it("should calculate employer unemployment using total minus worker part, which looks like rounding error, but is not.", () => {
      const calculation: Calculation = {
        salary: {
          amount: 1,
          price: 30,
          kind: SalaryKind.FixedSalary,
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation).then((resultCalculation: Calculation) => {
        expect(resultCalculation.result.totals.totalGrossSalary).equal(
            calculation.salary.amount * calculation.salary.price);
        expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age18_52");
        const numbers = sxyHelper.api.calculator.getYearlyChangingNumbers(Dates.getToday());
        const roundedSum = Numeric.round(30 * (numbers.sideCosts.unemploymentEmployerPercent + numbers.sideCosts.unemploymentWorkerPercent)) - Numeric.round(30 * numbers.sideCosts.unemploymentWorkerPercent);
        expect(resultCalculation.result.employerCalc.unemployment).equal(roundedSum);
      });
    });

    // TODO: Are these relevant after the numbers have changed. See version history - may be that these need to be some specific numbers.

    xit("Worker calculation mismatch", () => {
      const calculation: Calculation = {
        salary: {
          amount: 1,
          price: 30,
          kind: SalaryKind.FixedSalary,
        },
        worker: {
        },
      };
      return sxyHelper.api.calculator.recalculate(calculation)
        .then((resultCalculation: Calculation) => {
          expect(resultCalculation.result.totals.totalGrossSalary).equal(
              calculation.salary.amount * calculation.salary.price);
          expect(resultCalculation.result.responsibilities.pensionAgeRange).equal("age18_52");
          expect(resultCalculation.result.workerCalc.tax).equal(0);
          expect(resultCalculation.result.workerCalc.unemploymentInsurance).equal(0.57);
          expect(resultCalculation.result.workerCalc.pension).equal(1.905);
          expect(resultCalculation.result.workerCalc.salaryPayment).equal(27.525);
        });
    });
  });
});
