import { TestHelper, TestHelperConfig } from "@salaxy/node";

/** Provides helper methods for accessing the new API v03 */
export class ApiV03Helper {

  /** Gets a v03 URL */
  public static getUrl(method: string, query?: { [key: string]: string; }) {
    let url = "http://localhost:82/v03-rc/api" + method;
    if (query && Object.keys.length > 0) {
      url += "?" + Object.keys(query).map((key) => key + "=" + query[key]).join("&");
    }
    return url;
  }

  /**
   * Gets the Salaxy ID that is being used for testing.
   * Can we get this from token, or some kind of session object?
   */
  public static getSalaxyId() {
    return "FI29POYO0022336189";
  }

  /**
   * Helper for posting to the Serialization test method.
   * @returns the result object or if the call fails object with error=true and status.
   */
  public static postToSerialization(helper: TestHelper, data: any): Promise<any> {
    return helper.ajax.postJSON(ApiV03Helper.getUrl("/functionaltest/serialization"), data)
      .catch((error) => ({ error: true, status: (error.response && error.response.status ? error.response.status : "No response") }));
  }

  /**
   * Helper for sending a get call to the Serialization test method.
   * @returns the result object or if the call fails object with error=true and status.
   */
  public static getToSerialization(helper: TestHelper, key: string, value: string): Promise<any> {
    return helper.ajax.getJSON(ApiV03Helper.getUrl(`/functionaltest/serialization?${key}=${value}`))
      .catch((error) => ({ error: true, status: (error.response && error.response.status ? error.response.status : "No response") }));
  }

}
