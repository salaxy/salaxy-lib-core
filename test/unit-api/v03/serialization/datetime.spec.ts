import chai from "chai";
import dirtyChai from "dirty-chai";

import {
  TestHelper,
} from "@salaxy/node";

import { config } from "../../../config";
import { ApiV03Helper } from "../ApiV03Helper";

describe("API tests", () => {

  describe("v03: Serialization, DateTime", function() {

    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    it("handles a DateTime UTC value correctly in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "dateTimeValue", "2019-01-02T03:04:05Z");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.dateTimeValue).equals("2019-01-02T03:04:05Z");
    });

    it("interprets a non-timezone DateTime value as UTC in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "dateTimeValue", "2019-01-02T03:04:05");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.dateTimeValue).equals("2019-01-02T03:04:05Z");
    });

    it("handles a DateTime UTC value correctly in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "dateTimeValue", "2019-01-02T03:04:05Z");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.dateTimeValue).equals("2019-01-02T03:04:05Z");
    });

    it("converts a Date string in DateTime type as UTC time in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "dateTimeValue", "2019-01-02");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.dateTimeValue).equals("2019-01-02T00:00:00Z");
    });

    it("converts a local (Finnish) DateTime to UTC correctly in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "dateTimeValue", encodeURIComponent("2019-07-16T15:26:23+3:00"));
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.dateTimeValue).equals("2019-07-16T12:26:23Z");
    });
    it("ignores JavaScript time (int) in DateTime in GET (this could be an error).", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "dateTimeValue", "1563262329261");
      expect(target.status).to.be.undefined("Exception status code.");
      // This could also throw an error if switching server infra to Asp.Net Core.
      expect(target.dateTimeValue).to.be.null();
    });

    it("handles Date().toString() datetime format in GET (this could be an error).", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "dateTimeValue", encodeURIComponent("Mon, 15 Jul 2019 08:26:33 GMT"));
      expect(target.status).to.be.undefined("Exception status code.");
      // This could also throw an error if switching server infra to Asp.Net Core.
      expect(target.dateTimeValue).equals("2019-07-15T08:26:33Z");
    });

    it("should also be tested with http POST / JSON.");
  });

});
