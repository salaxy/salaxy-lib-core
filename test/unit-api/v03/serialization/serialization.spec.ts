import chai from "chai";
import dirtyChai from "dirty-chai";

import {
  TestHelper,
} from "@salaxy/node";

import { config } from "../../../config";
import { ApiV03Helper } from "../ApiV03Helper";

describe("API tests", () => {

  describe("v03: Serialization, general", function() {

    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    it("handles a string in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "stringValue", "Foobar");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.stringValue).equals("Foobar");
    });

    it("handles a string in POST.", async () => {
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { stringValue: "Foobar" });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.stringValue).equals("Foobar");
    });

    it("ignores case in property names in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "StrIngValUE", "Hello");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.stringValue).equals("Hello");
    });

    it("ignores case in property names in POST.", async () => {
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { StrIngValUE: "Hello" });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.stringValue).equals("Hello");
    });

    it("returns enum as string.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "enumValue", "valueThree");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.enumValue).equals("valueThree");
    });

    it("ignores case of enum value in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "enumValue", "ValueTWO");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.enumValue).equals("valueTwo");
    });

    it("ignores case of enum value in POST.", async () => {
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { enumValue: "ValueTWO" });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.enumValue).equals("valueTwo");
    });

    it("accepts enum as number in GET", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "enumValue", "2");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.enumValue).equals("valueTwo");
    });

    it("accepts enum as number in POST", async () => {
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { enumValue: 3 });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.enumValue).equals("valueThree");
    });

    it("handles dictionary first level.", async () => {
      const input = {
        numberVal: 2,
        boolVal: true,
        stringVal: "asde",
        /* eslint-disable-next-line quote-props */
        "stringKey": 4,
      };
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { dictValue: input });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.dictValue).to.not.be.null();
      expect(target.dictValue.numberVal).equals(input.numberVal);
      expect(target.dictValue.boolVal).equals(input.boolVal);
      expect(target.dictValue.stringVal).equals(input.stringVal);
      expect(target.dictValue.stringKey).equals(input.stringKey);
    });

    xit("handles dictionary second level data.", async () => {
      const input = {
        numberVal: 2,
        boolVal: true,
        stringVal: "asde",
        /* eslint-disable-next-line quote-props */
        "stringKey": 4,
      };
      const target = await ApiV03Helper.postToSerialization(sxyHelper, {
        mid: {
          dictValue: input,
        },
      });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.mid).to.exist();
      expect(target.mid.dictValue).to.exist();
      expect(target.mid.dictValue.numberVal).equals(input.numberVal);
      expect(target.mid.dictValue.boolVal).equals(input.boolVal);
      expect(target.mid.dictValue.stringVal).equals(input.stringVal);
      expect(target.mid.dictValue.stringKey).equals(input.stringKey);
    });

    it("handles an object with sub-values.", async () => {
      const input = {
        numberVal: 2,
        boolVal: true,
        stringVal: "asde",
        /* eslint-disable-next-line quote-props */
        "stringKey": 4,
      };
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { objectValue: input });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.objectValue).to.not.be.null();
      expect(target.objectValue.numberVal).equals(input.numberVal);
      expect(target.objectValue.boolVal).equals(input.boolVal);
      expect(target.objectValue.stringVal).equals(input.stringVal);
      expect(target.objectValue.stringKey).equals(input.stringKey);
    });

  });
});
