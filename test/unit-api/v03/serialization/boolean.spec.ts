import chai from "chai";
import dirtyChai from "dirty-chai";

import {
  TestHelper,
} from "@salaxy/node";

import { config } from "../../../config";
import { ApiV03Helper } from "../ApiV03Helper";

describe("API tests", () => {

  describe("v03: Serialization, Boolean", function() {
    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    it("handles a false in json post.", async () => {
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { boolValue: false });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.boolValue).equals(false);
    });
    it("handles a true in json post.", async () => {
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { boolValue: true });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.boolValue).equals(true);
    });
    it("handles a null when nullable in json post.", async () => {
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { boolValue: null });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.boolValue).to.be.null();
    });
    it("accepts a 1 as true in json post.", async () => {
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { boolValue: 1 });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.boolValue).equals(true);
    });
    it("accepts a 0 as false in json post.", async () => {
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { boolValue: 0 });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.boolValue).equals(false);
    });
    it("accepts a 'true' string as true in json post.", async () => {
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { boolValue: "true" });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.boolValue).equals(true);
    });
    it("ignores random string in json post (this could be an error).", async () => {
      // If switching to another environment, this could also be an error
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { boolValue: "asdf" });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.boolValue).to.be.null();
    });

    it("handles a false in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "boolValue", "false");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.boolValue).equals(false);
    });
    it("handles a true in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "boolValue", "true");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.boolValue).equals(true);
    });
    it("handles a null as empty string when nullable in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "boolValue", "");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.boolValue).to.be.null();
    });
    it("ignores string 'null' in GET (this could be an error).", async () => {
      // If switching to another environment, this could also be an error
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "boolValue", "null");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.boolValue).to.be.null();
    });
    it("ignores 0 in GET (this could be an error or false).", async () => {
      // If switching to another environment, this could also be an error or zero could be mapped to false.
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "boolValue", "0");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.boolValue).to.be.null();
    });
    it("ignores 1 in GET (this could be an error).", async () => {
      // If switching to another environment, this could also be an error
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "boolValue", "1");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.boolValue).to.be.null();
    });
    it("ignores random string in GET (this could be an error).", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "boolValue", "asdf");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.boolValue).to.be.null();
    });
  });

});
