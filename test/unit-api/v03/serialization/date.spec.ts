import chai from "chai";
import dirtyChai from "dirty-chai";

import {
  TestHelper,
} from "@salaxy/node";

import { config } from "../../../config";
import { ApiV03Helper } from "../ApiV03Helper";

describe("API tests", () => {

  describe("v03: Serialization, Date", function() {
    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    it("Date returns just the date part in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "dateValue", "2019-01-02T00:00:00");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.dateValue).equals("2019-01-02");
    });

    it("handles a DateTime before midnight to correct date in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "dateValue", "2019-01-02T23:59:59");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.dateValue).equals("2019-01-02");
    });

    it("handles a DateTime after midnight to correct date in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "dateValue", "2019-01-02T00:00:01");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.dateValue).equals("2019-01-02");
    });

    it("JavaScript time (int) in GET is not recognized (this could be an error).", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "dateValue", "1563262329261");
      expect(target.dateValue).to.be.null();
      // This could also throw an error if switching server infra to Asp.Net Core.
    });

    it("Handles Date().toString() format in GET (this could be an error).", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "dateValue", encodeURIComponent("Mon, 15 Jul 2019 08:26:33 GMT"));
      expect(target.dateValue).equals("2019-07-15");
      // This could also throw an error (it would be better) if switching server infra to Asp.Net Core.
    });

    it("Finnish date format in GET is not recognized (this could be an error).", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "dateValue", encodeURIComponent("15.7.2019"));
      expect(target.dateValue).to.be.null();
      // This could also throw an error if switching server infra to Asp.Net Core.
    });

    it("should also be tested with http POST / JSON.");

  });

});
