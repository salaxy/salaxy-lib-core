import chai from "chai";
import dirtyChai from "dirty-chai";

import {
  TestHelper,
} from "@salaxy/node";

import { config } from "../../../config";
import { ApiV03Helper } from "../ApiV03Helper";

describe("API tests", () => {

  describe("v03: Serialization, Integer", function() {

    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    it("handles an integer correctly in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "doubleValue", 4 as undefined as string);
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.doubleValue).equals(4);
    });

    it("handles a one-decimal value in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "doubleValue", 0.1 as undefined as string);
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.doubleValue).equals(0.1);
    });

    it("converts a string to double in post json.", async () => {
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { doubleValue: "0.1" });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.doubleValue).equals(0.1);
    });

    it("converts comma in a string to double in post json.", async () => {
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { doubleValue: "0,1" });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.doubleValue).equals(0.1);
    });

    it("handles a two-decimal value in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "doubleValue", 0.03 as undefined as string);
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.doubleValue).equals(0.03);
    });

    it("handles a 3-decimal value in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "doubleValue", 0.007 as undefined as string);
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.doubleValue).equals(0.007);
    });

    it("handles a 4-decimal value in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "doubleValue", 0.1234 as undefined as string);
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.doubleValue).equals(0.1234);
    });

    xit("fixes a basic float precision problem in GET.", async () => {
      // This should be fixed latest when going to Asp.Net Core.
      const problemFloat = 0.1 + 0.2;
      const asString = problemFloat.toString();
      expect(asString).equals("0.30000000000000004", "JavaScript precision check");
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "doubleValue", asString);
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.doubleValue).equals(0.3, "API result precision check");
    });

    it("should have consistent handling between POST JSON and GET. Now, not all cases have been tested.");
  });

  describe("v03: Serialization, Double", function() {
    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    it("handles an integer in GET.", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "intValue", 4 as undefined as string);
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.intValue).equals(4);
    });

    it("ignores decimal value as integer in GET (this could be an error).", async () => {
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "intValue", "1.3");
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.intValue).to.be.null();
    });
  });

  describe("v03: Serialization, Decimal", function() {

    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    it("handles a decimal in GET.", async () => {
      const testValue = "1.23456789";
      const target = await ApiV03Helper.getToSerialization(sxyHelper, "decimalValue", testValue);
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.decimalValue).to.not.be.null();
      expect(target.decimalValue.toString()).equals(testValue);
    });

    it("converts a string to decimal in post json.", async () => {
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { decimalValue: "0.1" });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.decimalValue).equals(0.1);
    });

    it("handles comma in a string to decimal in post json.", async () => {
      const target = await ApiV03Helper.postToSerialization(sxyHelper, { decimalValue: "0,1" });
      expect(target.status).to.be.undefined("Exception status code.");
      expect(target.decimalValue).equals(0.1);
    });

    it("should have consistent decimal for int handling in POST / JSON. Preferably throw.");

    it("should have consistent handling between POST JSON and GET. Now, not all cases have been tested.");
  });
});
