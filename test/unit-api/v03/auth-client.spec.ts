import chai from "chai";
import dirtyChai from "dirty-chai";

import {
  TestHelper,
} from "@salaxy/node";

import { config } from "../../config";
import { ApiV03Helper } from "./ApiV03Helper";

describe("API tests", () => {
  describe("v03: Authenticated client", function() {
    const expect = chai.expect;
    chai.use(dirtyChai);

    const sxyHelper = new TestHelper(config, this);
    before(sxyHelper.authenticate);

    it("gets a response from anonymous method.", async () => {
      const target = await sxyHelper.ajax.getJSON(ApiV03Helper.getUrl("/functionaltest/hello-world-anon"));
      expect(target).equals("Hello world");
    });

    it("gets the user name from method requiring authorization.", async () => {
      const target = await sxyHelper.ajax.getJSON(ApiV03Helper.getUrl("/functionaltest/user-name"));
      expect(target).equals(ApiV03Helper.getSalaxyId());
    });

    it("gets username from a method that allows anon.", async () => {
      const target = await sxyHelper.ajax.getJSON(ApiV03Helper.getUrl("/functionaltest/user-name-allow-anon"));
      expect(target).equals(ApiV03Helper.getSalaxyId());
    });

    it("gets some calculations.", async () => {
      const target = await sxyHelper.ajax.getJSON(ApiV03Helper.getUrl("/calculations/all"));
      expect(target.items.length).to.be.greaterThan(0);
    });

  });
});
