import chai from "chai";
import dirtyChai from "dirty-chai";

import {
  TestHelper,
} from "@salaxy/node";

import { config } from "../../config";
import { ApiV03Helper } from "./ApiV03Helper";

describe("API tests", () => {
  describe("v03: Anonymous client", function() {
    const expect = chai.expect;
    chai.use(dirtyChai);

    const sxyHelper = new TestHelper(config, this);

    it("gets a response from anonymous method.", async () => {
      const target = await sxyHelper.ajax.getJSON(ApiV03Helper.getUrl("/functionaltest/hello-world-anon"));
      expect(target).equals("Hello world");
    });

    it("gets a 401 error from method requiring authorization.", async () => {
      const target = await sxyHelper.ajax.getJSON(ApiV03Helper.getUrl("/functionaltest/user-name"))
        .then((data) => "No error thrown")
        .catch((data) => data);
      expect(target).to.not.be.null();
      expect(target.response.status).to.equal(401);
    });

    it("gets 'Anonymous' from username method with allow anon.", async () => {
      const target = await sxyHelper.ajax.getJSON(ApiV03Helper.getUrl("/functionaltest/user-name-allow-anon"));
      expect(target).equals("Anonymous");
    });

  });
});
