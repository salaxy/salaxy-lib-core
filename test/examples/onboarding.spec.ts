import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../config";

import {
  Onboardings,
  Session,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

describe("Examples", () => {

  describe("Onboarding", function() {
    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);
    before("Authenticate", sxyHelper.authenticate);

    it("Send current account to Onboarding if not verified", async () => {
      const ajax = sxyHelper.ajax;
      const customReturnUrl = "https://mycustom.url.here/path";

      // You typically fetch session only once at the point when you fetch the token.
      const sessionApi = new Session(ajax);
      const currentSession = await sessionApi.getSession();
      // Check if the account has NOT been verified
      if (!currentSession.isVerified) {
        const onboardingApi = new Onboardings(ajax);
        let customOnboardingId = null;
        // This is example on how to customize the onboarding. You can skip this if you go with default values.
        if (customReturnUrl) {
          // Id null fetches the latest and creates a new if the account has no onboarding object
          const newOnboarding = await onboardingApi.getOnboarding(null);
          // If these are not set, URL referrer is used.
          newOnboarding.ui.successUrl = customReturnUrl;
          newOnboarding.ui.cancelUrl = customReturnUrl;
          const savedOnboarding = await onboardingApi.saveOnboarding(newOnboarding);
          customOnboardingId = savedOnboarding.id;
        }
        const launchUrl = onboardingApi.getLaunchUrl(customOnboardingId);
        expect(launchUrl).to.contain(ajax.getServerAddress() + "/onboarding/launch");
        if (customOnboardingId) {
          expect(launchUrl).to.contain("/onboarding/launch/" + customOnboardingId);
        }

        // To use in real life: Send the browser to launcUrl (No IFrame, should be "_blank" or "_top")
        // access_token will be added to return url so you do not necessarily need to store state

      }
    });

  });

});
