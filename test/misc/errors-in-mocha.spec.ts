import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../config";

import {
  ApiTestErrorType,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

describe("Misc / Other tests", () => {
  describe("Demonstration of errors", () => {
    describe("All of these tests should be shown RED", function() {
      const expect = chai.expect;
      chai.use(dirtyChai);
      const sxyHelper = new TestHelper(config, this);

      it("Traditional async: Runtime Error should be shown as a failed test.", (done) => {
        throw Error("Some runtime error");
      });

      it("Promise: Runtime Error should be shown as a failed test (in then).", () => {
        return sxyHelper.api.test.hello("Hello World!").then(() => {
          throw Error("Runtime error in promise.then()");
        });
      });

      it("Async: Runtime Error should be shown as a failed test.", async () => {
        const helloResult = await sxyHelper.api.test.hello("Hello World!");
        throw Error("Runtime error in async call");
      });

      it("Failed assert should be shown as a failed test.", (done) => {
        expect("yes").to.equal("no");
        done("This code should never be executed.");
      });

      it("Promise: Failed assert should be shown as a failed test.", () => {
        return sxyHelper.api.test.hello("Hello World!").then((text) => {
          expect(text).to.equal("This ain't hello world.");
          throw Error("This code should never be executed.");
        });
      });

      it("Async: Failed assert should be shown as a failed test.", async () => {
        const helloResult = await sxyHelper.api.test.hello("Hello World!");
        expect(helloResult).to.equal("This ain't hello world.");
        throw Error("This code should never be executed.");
      });

      it("Promise: Ajax Error should be shown as a failed test.", () => {
        return sxyHelper.api.test.throwException(ApiTestErrorType.Default).then((text) => {
          throw Error("This code should never be executed.");
        });
      });

      it("Async: Ajax Error should be shown as a failed test.", async () => {
        const helloResult = await sxyHelper.api.test.throwException(ApiTestErrorType.Default);
        throw Error("This code should never be executed.");
      });

    });
  });
});
