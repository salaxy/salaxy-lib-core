import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../../config";

import {
  CompanyType,
  Onboarding,
  TestAccountInfo,
  WebSiteUserRole,
  SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

describe("System tests", () => {

  describe("v03", () => {

    describe("CRUD", function() {
      const expect = chai.expect;
      chai.use(dirtyChai);
      const sxyHelper = new TestHelper(config, this);

      it("should add a new item");
      it("should update an item");
      it("should find an item by ID");
      it("should get all items");
      it("should search for an item");
      it("should delete an item");
    });

    describe("OData", function() {
      const expect = chai.expect;
      chai.use(dirtyChai);
      const sxyHelper = new TestHelper(config, this);

      it("should filter items");
      it("should page items");
      it("should support other features of OData");
      it("test with anonymous data (CMS)?");
    });

    describe("Infrastructure", function() {
      const expect = chai.expect;
      chai.use(dirtyChai);
      const sxyHelper = new TestHelper(config, this);

      it("should reject a non-SSL call in test");
      it("should allow a non-SSL call in localhost");
      it("should return current session after authorization.");

    });

  });

});
