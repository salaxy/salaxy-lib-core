import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../../config";

import {
    Calculation,
    SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

describe("System tests", () => {

  describe("Calculator, testing use cases", function() {

    const expect = chai.expect;
    chai.use(dirtyChai);
    const testHelper = new TestHelper(config, this);

    // Test setup and tear-down
    let calculation: Calculation = null;

    before("Authenticate", testHelper.authenticate);

    after("Delete the calculation if it was created", () => {
        if (calculation && calculation.id) {
            return testHelper.api.calculations.delete(calculation.id);
        }
    });

    const date1 = new Date();
    const date2 = "2018-05-08";

    it("Salary amount and price greater than zero", () => {
        calculation = {
            usecase: {
                uri: "uri here",
                label: "label here",
                data: {
                    foo: "Foo value",
                    numberVal: 123.23,
                    booleanHere: true,
                    date1,
                    date2,
                    complexObj: {
                        inner: "huu haa",
                        innerNum: 321.45,
                        dateDeep1: date1,
                        dateDeep2: date2,
                    },
                },
            },
            salary: {
                amount: 5,
                price: 6,
                kind: SalaryKind.FixedSalary,
            },
        };
        return testHelper.api.calculator.recalculate(calculation).then((resultCalculation: Calculation) => {
            expect(new Date(resultCalculation.usecase.data.date1).getTime()).to.equal(date1.getTime());
            expect(new Date(resultCalculation.usecase.data.date2).getTime()).to.equal(new Date(date2).getTime());
            expect(new Date(resultCalculation.usecase.data.complexObj.dateDeep1).getTime()).to.equal(date1.getTime());
            expect(new Date(resultCalculation.usecase.data.complexObj.dateDeep2).getTime()).to.equal(new Date(date2).getTime());
            calculation = resultCalculation;
        });
    });

    it("Save draft", () => {
        return testHelper.api.calculations.save(calculation).then((resultCalculation: Calculation) => {
            expect(new Date(resultCalculation.usecase.data.date1).getTime()).to.equal(date1.getTime());
            expect(new Date(resultCalculation.usecase.data.date2).getTime()).to.equal(new Date(date2).getTime());
            expect(new Date(resultCalculation.usecase.data.complexObj.dateDeep1).getTime()).to.equal(date1.getTime());
            expect(new Date(resultCalculation.usecase.data.complexObj.dateDeep2).getTime()).to.equal(new Date(date2).getTime());

            calculation = resultCalculation;
        });
    });

    it("Get draft", () => {
        return testHelper.api.calculations.getSingle(calculation.id).then((resultCalculation: Calculation) => {
            /* eslint-disable-next-line */
            expect(new Date(resultCalculation.usecase.data.date1).getTime()).to.equal(date1.getTime());
            expect(new Date(resultCalculation.usecase.data.date2).getTime()).to.equal(new Date(date2).getTime());
            expect(new Date(resultCalculation.usecase.data.complexObj.dateDeep1).getTime()).to.equal(date1.getTime());
            expect(new Date(resultCalculation.usecase.data.complexObj.dateDeep2).getTime()).to.equal(new Date(date2).getTime());

            calculation = resultCalculation;
        });
    });
  });

});
