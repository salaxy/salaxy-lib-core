import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../../../config";

import {
   TestHelper,
} from "@salaxy/node";

import {
  AccountingPeriodClosingOption, BlobFile, Dates, WorkflowEvent, WorkflowEventUi,
} from "@salaxy/core";

describe("API tests", () => {
  describe("Accounting period closing tests", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);
    let reportId: string = null;
    let blobFile: BlobFile = null;
    const messageType: "PartnerMessageAssignedTo" | "PartnerMessageIssue" | "PartnerMessageClosed" = "PartnerMessageIssue";

    before("Authenticate", sxyHelper.authenticate);

    it("Close current accounting period", async () => {
      // NOTE: Only month and year are considered in date, day is irrelevant.
      // Hers, we are getting the accounting data for this months salaries ("closing the month early").
      const accountingPeriodDate = Dates.getToday();
      const id = await sxyHelper.api.reports.closeAccountingPeriod(accountingPeriodDate, AccountingPeriodClosingOption.Default );
      expect(id).to.not.be.null();
      reportId = id;
    });

    it("Retrieve report data", async () => {
      expect(reportId).to.not.be.null();
      const data = await sxyHelper.api.reports.getAccountingData(reportId);
      expect(data).to.not.be.null();
    });

    it("Retrieve report file", async () => {
      expect(reportId).to.not.be.null();
      const file = await sxyHelper.api.files.getSingle(reportId);
      expect(file).to.not.be.null();
      blobFile = file;
    });

    it("Add workflow message to report file", async () => {

      expect(blobFile).to.not.be.null();
      // Set new event, only one instance of each event type is possible to set.
      const event: WorkflowEvent = {
        type: messageType,
        ui: WorkflowEventUi.Danger,
        message: "test",
      };
      const workflowData = await sxyHelper.api.files.saveWorkflowEvent(blobFile, event);
      expect(workflowData.events.filter( (x) => x.type === messageType).length === 1);
    });
  });
});
