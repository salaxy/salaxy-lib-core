import chai from "chai";
import dirtyChai from "dirty-chai";
import { config } from "../config";

import {
  CompanyType,
  Onboarding,
  TestAccountInfo,
  WebSiteUserRole,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

describe("System tests", () => {

  describe("Test account creation tests", function() {
    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    // Test variables
    let personalWorkerAccountInfo: TestAccountInfo = null;
    let companyAccountInfo: TestAccountInfo = null;

    it("Create a personal account for testing purposes.", () => {
        const onboarding: Onboarding = {
            accountType: WebSiteUserRole.Worker,
            person: {
                firstName: "John",
                lastName: "Doe",
                personalId: "200180-765*",
            },
        };
        return sxyHelper.api.test.createTestAccount(onboarding)
          .then((testAccountInfo: TestAccountInfo) => {
            expect(testAccountInfo.userCredential.avatar.firstName).equal("John");
            personalWorkerAccountInfo = testAccountInfo;
        });
    });

    it("Delete the test account", () => {
      return sxyHelper.api.test.deleteCurrentAccount()
        .then((msg) => {
            expect(msg).have.string("deleted");
        });
    });

    it("Create company account for testing purposes", () => {
        const onboarding: Onboarding = {
              accountType: WebSiteUserRole.Company,
              // Add mandtory fields for company and primary contact person
              company: {
                businessId: "1255567*",
                name: "DJ Corporation",
                companyType: CompanyType.FiOy,
              },
              person: {
                firstName: "John",
                lastName: "Doe",
                personalId: "200180-765*",
              },
        };

        return sxyHelper.api.test.createTestAccount(onboarding)
          .then((testAccountInfo: TestAccountInfo) => {
            companyAccountInfo = testAccountInfo;
        });
    });

    it("Delete company account", () => {
      return sxyHelper.api.test.deleteCurrentAccount()
        .then((msg) => {
            expect(msg).have.string("deleted");
        });
    });

  });

});
