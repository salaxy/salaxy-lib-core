/* eslint-disable no-undef */
module.exports = {
  root: true,
  parser: "@typescript-eslint/parser",
  plugins: [
    "@typescript-eslint",
    "jsdoc",
  ],
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:jsdoc/recommended",
  ],
  "ignorePatterns": ["codegen", "templates", "i18n"],
  "settings": {
    "jsdoc": {
      "maxLines": 3,
      "ignorePrivate": true,
      "ignoreInternal": true,
    }
  },
  // NOTE: When adding rules, check rules performance: https://eslint.org/docs/developer-guide/working-with-rules#per-rule-performance
  //       Consider moving slow rules to the typed-long-running.eslintrc.js
  rules: {
    /*****************
     * Added rules
     *****************/
    "no-bitwise": "error",
    "no-trailing-spaces": "error",

    /*****************
     * Recommended rules => customizations
     *****************/
    // Explicit any is widely used (169 issues in core). We should check could we reduce the use of it?
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/explicit-module-boundary-types": ["error", {
      allowArgumentsExplicitlyTypedAsAny: true,
    }],
    "@typescript-eslint/no-unused-vars": ["error", {
      argsIgnorePattern: "^reason$|^error$", // reason and error used for promise catch.
    }],
    /*****************
     * JSDoc
     *****************/
    "jsdoc/require-description": "error",
    "jsdoc/require-jsdoc": [
      // TODO: We have a problem here that documentation missing is public class properties is currently not reported.
      "warn",
      {
        publicOnly: true,
        checkConstructors: false,
        require: {
          ClassDeclaration: true,
          FunctionDeclaration: true,
        },
        contexts: [
          // These can be tested in https://astexplorer.net/, but it is currently not working (https://github.com/fkling/astexplorer/issues/524)
          "TSEnumDeclaration",
          "TSInterfaceDeclaration",
          "TSPropertySignature",
          "MethodDefinition:not([kind=\"set\"])[accessibility=\"public\"] > FunctionExpression",
          "ClassProperty[accessibility=\"public\"]",
          "ClassProperty[accessibility=\"public\"] TSPropertySignature",
          "TSTypeAliasDeclaration",
        ],
        checkGetters: true,
        enableFixer: false
      }
    ],
    // excludeTags:["example"] does not seem to work (should even be default), have to use e.g. "```ts" or "```html"
    "jsdoc/check-indentation": ["warn", { "excludeTags": ["example"] }],
    "jsdoc/newline-after-description": "off",
    "jsdoc/require-returns": "off", // Currently, 266 errors in core project => Not seen as worth it.
    // TODO: 86 issues => Should be added back
    // When you add back, DISABLE FIXER!
    "jsdoc/require-param": "off", // TODO: 86 issues => Should be added back
    // As of writing, we think we do not need type checks in JSDoc => Types are only in TypeScript, not in JSDoc.
    "jsdoc/no-undefined-types": "off", // Enabling this will take a very long time (20sec)
    "jsdoc/check-types": "off",
    "jsdoc/valid-types": "off",
    "jsdoc/require-param-type": "off",
    "jsdoc/require-returns-type": "off",
    "jsdoc/require-property-type": "off",
    "jsdoc/check-access": "off",
  },
};