/*!
 * Custom task for getting yearly changing numbers and holidays from the api
 * and serializing them as a class for the client side library.
 */

'use strict';

/**
 * Creates a new YearlyNumbers fetcher.
 * @param grunt Grunt instance that is being run.
 */
function YearlyNumbers(grunt) {
  this.grunt = grunt;
}

YearlyNumbers.prototype.getYearlyNumbers = function () {
  var config = this.data;
  var done = this.async();
  var request = require('request');
  var numbers = [];

  var finish = function () {
    this.grunt.log.writeln("Saving to codegen.");
    var tsContent = JSON.stringify(numbers, null, "  ");
    var header = "/* eslint:disable */\n\n";
    header += "import {YearlyChangingNumbers} from \"../model\";\n\n";
    header += "/**\n";
    header += " * Yearly changing numbers. Contains side cost percentages and holidays.\n";
    header += " *\n";
    header += " */\n\n";
    header += "export const yearlyChangingNumbersYears =\n";
    var footer = " as YearlyChangingNumbers[];\n"

    this.grunt.file.write(config.dest, header + tsContent + footer);
    this.grunt.log.writeln("Saved.");
    done();
  }

  var getYear = function (i, years) {
    this.grunt.log.writeln("Getting yearly numbers for year: " + years[i]);
    request(config.url + "/" + years[i] + "-01-01", function (error, response, body) {
      if (error || !response || response.statusCode != 200) {
        if (error) {
          this.grunt.log.error("error:", error);
        }
        if (response) {
          this.grunt.log.writeln("statusCode:", response.statusCode);
        }
        console.error("No files saved.");
        done();
      } else {
        numbers.push(JSON.parse(body));
        i++;
        if (i == years.length) {
          finish();
        } else {
          getYear(i, years);
        }
      }
    });
  }
  var start = function () {
    getYear(0, config.years)
  }

  start();
};

module.exports = YearlyNumbers;
