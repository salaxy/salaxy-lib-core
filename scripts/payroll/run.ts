/* eslint-disable */

import {
  Calculations,
  IrIncomeTypeKind,
  IrIncomeTypeUsecase,
  Payrolls,
  Workers,
  CalculationRowType,
} from "@salaxy/core";

import {
  AjaxNode,
} from "@salaxy/node";

// Configuration
import * as config from "./config.json";

// Data for payroll
import rows from "./rows.json";
import header from "./header.json";

declare var process:any;
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";


const main = async() => {

  // Create ajax with token
  const ajax = new AjaxNode();
  ajax.serverAddress = config.apiServer;
  ajax.setCurrentToken(config.token);

  // Create workers api
  const workers = new Workers(ajax);

  // Get all workers
  console.log("Checking workers...");
  const workerData = await workers.getAll();

  // Check missing workers
  const missingWorkers = rows.filter(x => !workerData.find(w => w.id === x.workerId));
  if (missingWorkers.length > 0) {
    missingWorkers.forEach(x => console.log("Missing worker: " + x.workerId));
    return;
  }
  console.log("Workers ok.");

  // Create Payrolls api
  const payrolls = new Payrolls(ajax);

  // Get blank Payroll
  let payroll = payrolls.getBlank();

  // Set header
  payroll.input = {
     noUpdateFromEmployment: true,
     title: header.title,
     salaryDate: header.salaryDate,
     period: {
        start: header.workStartDate,
        end: header.workEndDate,
     }
  }
  payroll.info = {};

  // Save blank payroll
  console.log("Saving blank payroll...");
  payroll = await payrolls.save(payroll);
  console.log("Saved.");

  // Create calculation api
  const calculations = new Calculations(ajax);

  let counter = 0;
  // Save each row as calculation
  for(const row of rows) {
    counter ++;
    console.log("Saving calculation " + counter + "/" + rows.length + "...");
    const calc = calculations.getBlank();

    // Set period
    calc.info.workStartDate = header.workStartDate;
    calc.info.workEndDate = header.workEndDate;

    // Set worker
    calc.worker.accountId = row.workerId;

    // Add row
    const data: IrIncomeTypeUsecase = {
        irData: {
          code: row.incomeTypeCode,
        },
        kind: IrIncomeTypeKind.Undefined
    };
   
    calc.rows.push( {
       count: row.count,
       price: row.price,
       message: row.message,
       rowType: CalculationRowType.IrIncomeType,
       data,
    });

    // Save payroll with calculation
    const newCalc = await payrolls.calculationSave(calc, payroll.id);
    payroll.input.calculations.push(newCalc.id);
    console.log("Saved.");
  }

   // Save payroll with all calcs
   console.log("Saving payroll with all calcs...");
   payroll = await payrolls.save(payroll);
   console.log("Saved.");

  console.log("All Done!")
}


main().catch( ( reason) =>  {
  if (reason && reason.response) {
    console.log(reason.response.data);
  } else {
    console.log(reason);
  }
});

