/* eslint-disable */

import {
  WorkerAccount,
  Workers,
} from "@salaxy/core";

import {
  AjaxNode,
} from "@salaxy/node";

// Configuration
import * as config from "./config.json";

// Configuration
import workerData from "./workers.json";

declare var process:any;
// process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";


const main = async() => {

  // Create ajax with token
  const importAjax = new AjaxNode();
  importAjax.serverAddress = config.apiServer;
  importAjax.setCurrentToken(config.importToken);

  // Create workers api
  const workers = new Workers(importAjax);

  const mappings = [];
  const fs = require("fs");
  const workerItems = workerData as WorkerAccount[];
  // Save all workers
  for(const workerItem of workerItems) {
    const mapping = {
      owner: workerItem.owner,
      id: workerItem.id,
      employmentId: workerItem.employmentId,
      officialId: workerItem.officialPersonId,
      newOwner: null,
      newId: null,
      newEmploymentId: null,
    };
    workerItem.owner = null;
    workerItem.employerId = null;
    workerItem.id = null;
    workerItem.avatar.id = null;
    workerItem.avatar.owner = null;
    workerItem.employmentId = null;
    const newWorker = await workers.save(workerItem as any);
    mapping.newOwner = newWorker.owner;
    mapping.newEmploymentId = newWorker.employmentId;
    mapping.newId = newWorker.id;
    mappings.push(mapping);
    fs.appendFileSync("./scripts/worker/worker-mapping-rt.json", JSON.stringify(mapping), "utf-8");
    console.log("Imported " + mapping.newId);

  }

  fs.writeFileSync("./scripts/worker/worker-mappings.json", JSON.stringify(mappings), "utf-8");
  
  

  console.log("Workers imported.");

  console.log("All Done!")
}


main().catch( ( reason) =>  {
  if (reason && reason.response) {
    console.log(reason.response.data);
  } else {
    console.log(reason);
  }
});

