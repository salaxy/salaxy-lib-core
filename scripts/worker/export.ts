/* eslint-disable */

import {
  LegalEntityType,
  WorkerAccount,
  Workers,
} from "@salaxy/core";

import {
  AjaxNode,
} from "@salaxy/node";

// Configuration
import * as config from "./config.json";

declare var process:any;
// process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";


const main = async() => {

  // Create ajax with token
  const exportAjax = new AjaxNode();
  exportAjax.serverAddress = config.apiServer;
  exportAjax.setCurrentToken(config.exportToken);

  // Create workers api
  const workers = new Workers(exportAjax);

  // Get all workers
  console.log("Getting all workers...");
  let workerData = await workers.getAll();

  //remove employmentIds and ids:
  workerData = workerData.filter( (x) => x.entityType == LegalEntityType.PersonCreatedByEmployer);

  const fs = require("fs");
  fs.writeFileSync("./scripts/worker/workers.json", JSON.stringify(workerData), "utf-8");

  console.log("Workers exported.");

  console.log("All Done!")
}


main().catch( ( reason) =>  {
  if (reason && reason.response) {
    console.log(reason.response.data);
  } else {
    console.log(reason);
  }
});

