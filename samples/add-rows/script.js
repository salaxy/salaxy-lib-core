﻿/// <reference path="../../dist/npm/@salaxy/core/salaxy-core-all.d.ts" />

function sample6Calculate() {
    console.info("Running sample 6");
    salaxyApi.calculator.createNew(sample6emptyCalcObjectReceived);
}

function sample6emptyCalcObjectReceived(data) {
    console.info("Empty object received - doing the calculation");
    data.salary.kind = "FixedSalary";
    data.salary.price = $('#sample6-salary').val() || 100,
    data.rows = [{
        rowType: 'expenses',
        price: $('#sample6-expenses').val() || 30,
        // These two are really the only mandatory fields, the other properties that you can use:
        unit: 'count', // Unit will be guessed, but you are better off setting it
        count: 1, // Default for count is 1, so in this case we would not need to set it
        message: 'Some demo expenses' // Will use the Finnish description of rowType as default
    }, {
        rowType: 'phoneBenefit',
        price: 20,
        unit: 'month',
        count: 1,
        message: 'Puhelinetu'
    }
    ];
    salaxyApi.calculator.recalculate(data, sample6recalculationDone);
};

function sample6recalculationDone(data) {
    console.info(data);
    // NOTE: as-of-writing you would need to implement rounding here for this to be correct.
    // However, we are planning to round to 2 decimals inside API before release, so this should be fine in the release timeframe.
    $('#sample6-result-totalsalary').text(data.result.totals.totalGrossSalary.toFixed(2));
    $('#sample6-result-expenses').text(data.result.totals.totalExpenses.toFixed(2));
    $('#sample6-result-sidecosts').text(data.result.employerCalc.allSideCosts.toFixed(2));
    $('#sample6-result-totalpayment').text(data.result.employerCalc.totalPayment.toFixed(2));
    $('#sample6-result-benefits').text(data.result.workerCalc.benefits.toFixed(2));
}