﻿/// <reference path="../../dist/npm/@salaxy/core/salaxy-core-all.d.ts" />
function workerReport() {
    salaxyApi.reports.getReportHtmlById('worker',$('#sample4-id').val(), reportDone);
}

function workerReportPage2() {
    salaxyApi.reports.getReportHtmlById('workerpage2',$('#sample4-id').val(), reportDone);
}

function employerReport() {
    salaxyApi.reports.getReportHtmlById('employer',$('#sample4-id').val(), reportDone);
}

function employerReportPage2() {
    salaxyApi.reports.getReportHtmlById('employerpage2',$('#sample4-id').val(), reportDone);
}

function paymentReport() {
    salaxyApi.reports.getReportHtmlById('payments',$('#sample4-id').val(), reportDone);
}

function reportDone(html) {
    console.info(html);
    $('#report').html(html);
}