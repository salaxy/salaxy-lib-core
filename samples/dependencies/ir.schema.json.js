const sampleSchema = {
  "openapi": "3.0.1",
  "info": {
    "title": "API 1.0",
    "version": "1.0"
  },
  "paths": {
    "/v01/api/codes/incometype": {
      "get": {
        "tags": [
          "Codes"
        ],
        "summary": "Get income types and control rules.",
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/IncomeTypeCode"
                  }
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/codes/occupation": {
      "get": {
        "tags": [
          "Codes"
        ],
        "summary": "Get occupation codes.",
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/OccupationCode"
                  }
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/codes/cba": {
      "get": {
        "tags": [
          "Codes"
        ],
        "summary": "Get CBA codes and translations.",
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/CBACode"
                  }
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/epr": {
      "get": {
        "tags": [
          "Epr"
        ],
        "summary": "OData search supports e.g. calculation id.",
        "parameters": [
          {
            "name": "$filter",
            "in": "query",
            "schema": {
              "type": "string",
              "nullable": true
            }
          },
          {
            "name": "$orderby",
            "in": "query",
            "schema": {
              "type": "string",
              "nullable": true
            }
          },
          {
            "name": "$select",
            "in": "query",
            "schema": {
              "type": "string",
              "nullable": true
            }
          },
          {
            "name": "$search",
            "in": "query",
            "schema": {
              "type": "string",
              "nullable": true
            }
          },
          {
            "name": "$top",
            "in": "query",
            "schema": {
              "type": "integer",
              "format": "int32",
              "default": 1000,
              "nullable": true
            }
          },
          {
            "name": "$skip",
            "in": "query",
            "schema": {
              "type": "integer",
              "format": "int32",
              "default": 0,
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PageResult"
                }
              }
            }
          }
        }
      },
      "post": {
        "tags": [
          "Epr"
        ],
        "summary": "Save earnings payment resource",
        "requestBody": {
          "content": {
            "application/json;odata.metadata=minimal;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=minimal;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=minimal": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=full": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=none": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/xml": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/prs.odatatestxx-odata": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json-patch+json": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/EarningsPayment"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/epr/{id}": {
      "get": {
        "tags": [
          "Epr"
        ],
        "summary": "Gets a single object based on identifer",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "Unique identifier for the object",
            "required": true,
            "schema": {
              "type": "string",
              "description": "Unique identifier for the object",
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/EarningsPayment"
                }
              }
            }
          }
        }
      },
      "delete": {
        "tags": [
          "Epr"
        ],
        "summary": "Delete earnings payment resource",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "schema": {
              "type": "string",
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/epr/new": {
      "get": {
        "tags": [
          "Epr"
        ],
        "summary": "Gets new earnings payment resource",
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/EarningsPayment"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/epr/validate": {
      "post": {
        "tags": [
          "Epr"
        ],
        "summary": "Validate earnings payment resource",
        "requestBody": {
          "content": {
            "application/json;odata.metadata=minimal;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=minimal;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=minimal": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=full": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.metadata=none": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/xml": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/prs.odatatestxx-odata": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/json-patch+json": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/EarningsPayment"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/EarningsPayment"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/epr/{id}/versions": {
      "get": {
        "tags": [
          "Epr"
        ],
        "summary": "Get earnings payment versions data",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "schema": {
              "type": "string",
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": { }
              }
            }
          }
        }
      }
    },
    "/v01/api/epr/{id}/versions/{versionId}": {
      "get": {
        "tags": [
          "Epr"
        ],
        "summary": "Get specific version of earnings payment resource",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "schema": {
              "type": "string",
              "nullable": true
            }
          },
          {
            "name": "versionId",
            "in": "path",
            "required": true,
            "schema": {
              "type": "string",
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/EarningsPayment"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/epr/{id}/schedule": {
      "get": {
        "tags": [
          "Epr"
        ],
        "summary": "Get earnings payment schedule object from queue",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "schema": {
              "type": "string",
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ApiScheduleObject"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/epr/schedule": {
      "post": {
        "tags": [
          "Epr"
        ],
        "summary": "Send earnings payment schedule object to queue",
        "requestBody": {
          "content": {
            "application/json;odata.metadata=minimal;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=minimal;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=minimal": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=full": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=none": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/xml": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/prs.odatatestxx-odata": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json-patch+json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ApiScheduleObject"
                }
              }
            }
          }
        }
      },
      "delete": {
        "tags": [
          "Epr"
        ],
        "summary": "Delete earnings payment schedule object from queue",
        "requestBody": {
          "content": {
            "application/json;odata.metadata=minimal;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=minimal;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=minimal": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=full": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=none": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/xml": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/prs.odatatestxx-odata": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json-patch+json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ApiScheduleObject"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/ir/epr/send": {
      "post": {
        "tags": [
          "Ir"
        ],
        "summary": "Send earnings payment to incomes register",
        "requestBody": {
          "content": {
            "application/json;odata.metadata=minimal;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=minimal;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=minimal": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=full": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=none": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/xml": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/prs.odatatestxx-odata": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json-patch+json": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/AckData"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/ir/epr/cancel": {
      "post": {
        "tags": [
          "Ir"
        ],
        "summary": "Cancel / Invalidate earnings payment from incomes register",
        "requestBody": {
          "content": {
            "application/json;odata.metadata=minimal;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=minimal;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=minimal": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=full": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=none": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/xml": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/prs.odatatestxx-odata": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json-patch+json": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/AckData"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/ir/epr/check/{scheduleaction}": {
      "post": {
        "tags": [
          "Ir"
        ],
        "summary": "Check earnings payment status from incomes register",
        "parameters": [
          {
            "name": "scheduleaction",
            "in": "path",
            "required": true,
            "schema": {
              "$ref": "#/components/schemas/ScheduleAction"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json;odata.metadata=minimal;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=minimal;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=minimal": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=full": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.metadata=none": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/xml": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/prs.odatatestxx-odata": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/json-patch+json": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/ScheduleObject"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/StatusResponse"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/psr": {
      "get": {
        "tags": [
          "Psr"
        ],
        "summary": "OData search supports e.g. calculation id.",
        "parameters": [
          {
            "name": "$filter",
            "in": "query",
            "schema": {
              "type": "string",
              "nullable": true
            }
          },
          {
            "name": "$orderby",
            "in": "query",
            "schema": {
              "type": "string",
              "nullable": true
            }
          },
          {
            "name": "$select",
            "in": "query",
            "schema": {
              "type": "string",
              "nullable": true
            }
          },
          {
            "name": "$search",
            "in": "query",
            "schema": {
              "type": "string",
              "nullable": true
            }
          },
          {
            "name": "$top",
            "in": "query",
            "schema": {
              "type": "integer",
              "format": "int32",
              "default": 1000,
              "nullable": true
            }
          },
          {
            "name": "$skip",
            "in": "query",
            "schema": {
              "type": "integer",
              "format": "int32",
              "default": 0,
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PageResult"
                }
              }
            }
          }
        }
      },
      "post": {
        "tags": [
          "Psr"
        ],
        "summary": "Save earnings payment resource",
        "requestBody": {
          "content": {
            "application/json;odata.metadata=minimal;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=minimal;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=minimal": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=full": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=none": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/xml": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/prs.odatatestxx-odata": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json-patch+json": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PayerSummary"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/psr/{id}": {
      "get": {
        "tags": [
          "Psr"
        ],
        "summary": "Gets a single object based on identifer",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "Unique identifier for the object",
            "required": true,
            "schema": {
              "type": "string",
              "description": "Unique identifier for the object",
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PayerSummary"
                }
              }
            }
          }
        }
      },
      "delete": {
        "tags": [
          "Psr"
        ],
        "summary": "Delete earnings payment resource",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "schema": {
              "type": "string",
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/psr/new": {
      "get": {
        "tags": [
          "Psr"
        ],
        "summary": "Gets new earnings payment resource",
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PayerSummary"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/psr/validate": {
      "post": {
        "tags": [
          "Psr"
        ],
        "summary": "Validate earnings payment resource",
        "requestBody": {
          "content": {
            "application/json;odata.metadata=minimal;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=minimal;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=minimal": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=full": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.metadata=none": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/xml": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/prs.odatatestxx-odata": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/json-patch+json": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/PayerSummary"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PayerSummary"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/psr/{id}/versions": {
      "get": {
        "tags": [
          "Psr"
        ],
        "summary": "Get earnings payment versions data",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "schema": {
              "type": "string",
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": { }
              }
            }
          }
        }
      }
    },
    "/v01/api/psr/{id}/versions/{versionId}": {
      "get": {
        "tags": [
          "Psr"
        ],
        "summary": "Get specific version of earnings payment resource",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "schema": {
              "type": "string",
              "nullable": true
            }
          },
          {
            "name": "versionId",
            "in": "path",
            "required": true,
            "schema": {
              "type": "string",
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PayerSummary"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/psr/{id}/schedule": {
      "get": {
        "tags": [
          "Psr"
        ],
        "summary": "Get earnings payment schedule object from queue",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "schema": {
              "type": "string",
              "nullable": true
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ApiScheduleObject"
                }
              }
            }
          }
        }
      }
    },
    "/v01/api/psr/schedule": {
      "post": {
        "tags": [
          "Psr"
        ],
        "summary": "Send earnings payment schedule object to queue",
        "requestBody": {
          "content": {
            "application/json;odata.metadata=minimal;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=minimal;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=minimal": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=full": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=none": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/xml": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/prs.odatatestxx-odata": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json-patch+json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ApiScheduleObject"
                }
              }
            }
          }
        }
      },
      "delete": {
        "tags": [
          "Psr"
        ],
        "summary": "Delete earnings payment schedule object from queue",
        "requestBody": {
          "content": {
            "application/json;odata.metadata=minimal;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=minimal;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=minimal": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=full;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=full": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=none;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.metadata=none": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.streaming=true": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json;odata.streaming=false": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/xml": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/prs.odatatestxx-odata": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/json-patch+json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/ApiScheduleObject"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Success",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ApiScheduleObject"
                }
              }
            }
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "CalcGroup": {
        "enum": [
          "undefined",
          "baseSalary",
          "salaryAdditions",
          "benefits",
          "expenses",
          "deductions",
          "otherNoPayment",
          "totals",
          "disabled"
        ],
        "type": "string",
        "description": "Type of row in Calculation"
      },
      "IncomeTypeCode": {
        "type": "object",
        "properties": {
          "code": {
            "type": "integer",
            "description": "Income type code",
            "format": "int32"
          },
          "nameEn": {
            "type": "string",
            "description": "Name of income type (english)",
            "nullable": true
          },
          "nameFi": {
            "type": "string",
            "description": "Name of income type (finnish)",
            "nullable": true
          },
          "nameSv": {
            "type": "string",
            "description": "Name of income type (swedish)",
            "nullable": true
          },
          "isNegativeSupported": {
            "type": "boolean",
            "description": "True if supports negative values."
          },
          "pensionInsurance": {
            "type": "boolean",
            "description": "Subject to earnings-related pension insurance contribution"
          },
          "accidentInsurance": {
            "type": "boolean",
            "description": "Subject to accident insurance and occupational disease insurance contribution"
          },
          "unemploymentInsurance": {
            "type": "boolean",
            "description": "Subject to unemployment insurance contribution"
          },
          "healthInsurance": {
            "type": "boolean",
            "description": "Subject to health insurance contribution"
          },
          "insuranceInformationAllowed": {
            "type": "boolean",
            "description": "Type of insurance information allowed"
          },
          "taxDefault": {
            "type": "integer",
            "description": "Defines the default calculation for Widthholding tax: \r\n\r\n- 1 = Widthholding tax for full amount\r\n- 0 = No widthholdign tax\r\n- -1 = Deducts widhtholding tax (Row-specific logic is added, because there is typically a maximum that is applied).\r\n- Other defaults may be added later.",
            "format": "int32"
          },
          "notSupportedError": {
            "type": "string",
            "description": "Error message that describes why the particular income type is not supported.\r\nNull if IsSupported is true",
            "nullable": true
          },
          "description": {
            "type": "string",
            "description": "Description of the income type.\r\nThis is currently only in Finnish.",
            "nullable": true
          },
          "taxAndSidecostsDescr": {
            "type": "string",
            "description": "Texts that describes how the income type is handled in terms of widthholding tax and social security insurances: pension, accident, unemployment and health.\r\nThis is currently only in Finnish.",
            "nullable": true
          },
          "isSupported": {
            "type": "boolean",
            "description": "True if the income type is supported by Salaxy"
          },
          "paymentDefault": {
            "type": "integer",
            "description": "Defines the amount that is routed through customer funds account:\r\n\r\n- 1 The amount is paid to the customer funds account\r\n- 0 The amount is NOT paid through the customer funds account\r\n- -1 The amount is dedcuted from the payment to the customer funds account.",
            "format": "int32"
          },
          "usecases": {
            "type": "array",
            "items": {
              "type": "string"
            },
            "description": "Usecase uris that are relevant to this Income type.",
            "nullable": true
          },
          "calcGrouping": {
            "$ref": "#/components/schemas/CalcGroup"
          }
        },
        "additionalProperties": false,
        "description": "Incomes Register / Vero.fi income types and control rules"
      },
      "OccupationCode": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "description": "Occupation id",
            "nullable": true
          },
          "code": {
            "type": "string",
            "description": "Occupation code",
            "nullable": true
          },
          "labelFi": {
            "type": "string",
            "description": "Label in Finnish",
            "nullable": true
          },
          "labelSv": {
            "type": "string",
            "description": "Label Swedish",
            "nullable": true
          },
          "labelEn": {
            "type": "string",
            "description": "Label in English",
            "nullable": true
          },
          "searchFi": {
            "type": "string",
            "description": "Search string in Finnish",
            "nullable": true,
            "readOnly": true
          },
          "searchSv": {
            "type": "string",
            "description": "Search string in Swedish",
            "nullable": true,
            "readOnly": true
          },
          "searchEn": {
            "type": "string",
            "description": "Search string in English",
            "nullable": true,
            "readOnly": true
          },
          "freeTextFi": {
            "type": "string",
            "description": "Free text in Finnish",
            "nullable": true
          },
          "freeTextSv": {
            "type": "string",
            "description": "Free text in Swedish",
            "nullable": true
          },
          "freeTextEn": {
            "type": "string",
            "description": "Free text in English",
            "nullable": true
          },
          "keyword": {
            "type": "string",
            "description": "Keyword",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Occupation code, translations and properties"
      },
      "CBACode": {
        "type": "object",
        "properties": {
          "code": {
            "type": "integer",
            "description": "Identifier code",
            "format": "int32"
          },
          "labelFi": {
            "type": "string",
            "description": "Label in Finnish",
            "nullable": true
          },
          "labelEn": {
            "type": "string",
            "description": "Label in English",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Applicable collective agreement"
      },
      "PageResult": {
        "type": "object",
        "properties": {
          "nextPageLink": {
            "type": "string",
            "format": "uri",
            "nullable": true
          },
          "count": {
            "type": "integer",
            "format": "int64",
            "nullable": true
          }
        },
        "additionalProperties": false
      },
      "DeliveryDataType": {
        "enum": [
          "earningsPaymentReports",
          "employersSeparateReports",
          "benefitsPaymentReports",
          "recordSubscription",
          "messages",
          "cancellationOfEarningsPaymentReports",
          "cancellationOfEmployersSeparateReports",
          "cancellationOfBenefitsPaymentReports",
          "cancellationOfARecordSubscription",
          "cancellationOfARecordContainingEarningsPaymentReports",
          "cancellationOfARecordContainingEmployersSeparateReports",
          "cancellationOfARecordContainingBenefitsPaymentReports",
          "cancellationOfARecordContainingARecordSubscription"
        ],
        "type": "string",
        "description": "Record type, submitted to Incomes Register (DeliveryDataType)"
      },
      "FaultyControl": {
        "enum": [
          "onlyInvalidItems",
          "entireRecordIsRejected"
        ],
        "type": "string",
        "description": "Rule for processing invalid data (FaultyControl)"
      },
      "IdType": {
        "enum": [
          "businessId",
          "personalIdentificationNumber",
          "vat",
          "giin",
          "taxIdentificationNumber",
          "finnishTradeRegistrationNumber",
          "foreignBusinessRegistrationNumber",
          "foreignPersonalIdentificationNumber",
          "other"
        ],
        "type": "string",
        "description": "Identifier type (IdType)"
      },
      "Id": {
        "type": "object",
        "properties": {
          "type": {
            "$ref": "#/components/schemas/IdType"
          },
          "code": {
            "type": "string",
            "description": "Identifier\r\n\r\nIf the \"Identifier type\" is \"Business ID\", the identifier must exist (according to information from the Business Information System (YTJ)).\r\nIf the \"Identifier type\" is \"Finnish Personal Identification Number\", the identifier must exist (according to information from the Population Information System (VTJ)).",
            "nullable": true
          },
          "countryCode": {
            "type": "string",
            "description": "Country code\r\n\r\nMandatory data if \"Identifier type\" is other than \"Business ID\" or \"Finnish Personal Identification Number\".",
            "nullable": true
          },
          "countryName": {
            "type": "string",
            "description": "Country name\r\n\r\nMandatory data if \"Country code\" is \"99\".",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Employer Identifier"
      },
      "PaymentPeriod": {
        "type": "object",
        "properties": {
          "paymentDate": {
            "type": "string",
            "description": "Date of payment or other report\r\n\r\nThe date must not be earlier than 1 January 2019 or more than 45 days later than the current date.",
            "format": "date"
          },
          "startDate": {
            "type": "string",
            "description": "Start date\r\n\r\nThe date must not be earlier than 1 January 1800.",
            "format": "date"
          },
          "endDate": {
            "type": "string",
            "description": "End date\r\n\r\nThe date must not be earlier than the \"Start date\".",
            "format": "date"
          }
        },
        "additionalProperties": false,
        "description": "Pay Period\r\n\r\nIf the \"Action type\" is \"Replacement report\", the data should match that of the report’s previous version."
      },
      "ResponsibilityCode": {
        "enum": [
          "contentIssues",
          "technicalIssues"
        ],
        "type": "string",
        "description": "Field of responsibility (ResponsibilityCode)"
      },
      "ContactPerson": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string",
            "description": "Name",
            "nullable": true
          },
          "telephone": {
            "type": "string",
            "description": "Telephone number",
            "nullable": true
          },
          "email": {
            "type": "string",
            "description": "E-mail address\r\n\r\nIf the ‘Reimbursement application data’ data group is specified for a paid absence and 1 or 2 as the ‘Payment data type’ even in a single report in a record, you must report at least one ‘Contact person for the record’ for whom the ‘E-mail address’ and ‘Field of responsibility’ data items have been specified.",
            "nullable": true
          },
          "responsibilityCode": {
            "$ref": "#/components/schemas/ResponsibilityCode"
          }
        },
        "additionalProperties": false
      },
      "Language": {
        "enum": [
          "finnish",
          "swedish",
          "english"
        ],
        "type": "string",
        "description": "Contact language (Language)"
      },
      "PayerBasic": {
        "type": "object",
        "properties": {
          "missingId": {
            "type": "boolean",
            "description": "The payer does not have a customer ID\r\n\r\nMandatory data, if no identifiers have been specified in the \"Payer identifiers\" data group.",
            "nullable": true
          },
          "companyName": {
            "type": "string",
            "description": "Company name\r\n\r\nMandatory data in the following cases:\r\n• No identifier where the \"Identifier type\" is \"Business ID\" or \"Finnish Personal Identification Number\" has been specified in the \"Payer identifiers\" data group, and \"Last name\" and \"First name\" have not been specified.\r\n• The value of \"The payer does not have a customer ID\" is \"true\", and \"Last name\" and \"First name\" have not been specified.",
            "nullable": true
          },
          "lastName": {
            "type": "string",
            "description": "Last name\r\n\r\nMandatory data in the following cases:\r\n• No identifier where the \"Identifier type\" is \"Business ID\" or \"Finnish Personal Identification Number\" has been specified in the \"Payer identifiers\" data group, and \"Company name\" has not been specified.\r\n• The value of \"The payer does not have a customer ID\" is \"true\", and \"Company name\" has not been specified.\r\n• The value of \"Payer type\" is \"Temporary employer\", and \"Company name\" has not been specified.",
            "nullable": true
          },
          "firstName": {
            "type": "string",
            "description": "First name\r\n\r\nMandatory data in the following cases:\r\n• No identifier where the \"Identifier type\" is \"Business ID\" or \"Finnish Personal Identification Number\" has been specified in the \"Payer identifiers\" data group, and \"Company name\" has not been specified.\r\n• The value of \"The payer does not have a customer ID\" is \"true\", and \"Company name\" has not been specified.\r\n• The value of \"Payer type\" is \"Temporary employer\", and \"Company name\" has not been specified.",
            "nullable": true
          },
          "birthDate": {
            "type": "string",
            "description": "Date of birth\r\n\r\nMandatory data in the following cases:\r\n• No identifier where the \"Identifier type\" is \"Finnish Personal Identification Number\" has been specified in the \"Payer identifiers\" data group, and \"Last name\" and \"First name\" have been specified.\r\n• The value of \"The payer does not have a customer ID\" is \"true\", and \"Last name\" and \"First name\" have been specified.\r\nThe date must not be later than the current date.\r\nThe date must not be earlier than 1 January 1800",
            "format": "date",
            "nullable": true
          },
          "language": {
            "$ref": "#/components/schemas/Language"
          }
        },
        "additionalProperties": false,
        "description": "Basic payer details"
      },
      "Address": {
        "type": "object",
        "properties": {
          "co": {
            "type": "string",
            "description": "c/o",
            "nullable": true
          },
          "street": {
            "type": "string",
            "description": "Street address\r\n\r\nIf this data group is included, either \"Street address\" or \"P.O. Box\" must be specified.Both must not be entered.",
            "nullable": true
          },
          "poBox": {
            "type": "string",
            "description": "P.O. Box\r\n\r\nIf this data group is included, either \"Street address\" or \"P.O. Box\" must be specified.Both must not be entered.",
            "nullable": true
          },
          "postalCode": {
            "type": "string",
            "description": "Postal code",
            "nullable": true
          },
          "postOffice": {
            "type": "string",
            "description": "City",
            "nullable": true
          },
          "countryCode": {
            "type": "string",
            "description": "Country code\r\n\r\nA two-character code in accordance with the ISO 3166 country codes.\r\nIf the country is unknown, the value \"99\" is entered.",
            "nullable": true
          },
          "countryName": {
            "type": "string",
            "description": "Country name\r\n\r\nMandatory data if \"Country code\" is \"99\"",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Address"
      },
      "PayerSubOrgType": {
        "enum": [
          "kevasSubmitterCodes",
          "payersOwnCodes",
          "governmentAgencyIdentifier"
        ],
        "type": "string",
        "description": "Identifier type of payer's suborganisation"
      },
      "SubOrg": {
        "type": "object",
        "properties": {
          "type": {
            "$ref": "#/components/schemas/PayerSubOrgType"
          },
          "code": {
            "type": "string",
            "description": "Suborganisation identifier",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Payer's suborganisations"
      },
      "PayerType": {
        "enum": [
          "publicSector",
          "household",
          "temporaryEmployer",
          "foreignEmployer",
          "state",
          "unincorporatedStateEnterpriseOrGovernmentalInstitution",
          "specialisedAgency",
          "foreignGroupCompany"
        ],
        "type": "string",
        "description": "Payer type (PayerType)"
      },
      "PayerOther": {
        "type": "object",
        "properties": {
          "payerTypes": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/PayerType"
            },
            "description": "Payer types",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Other payer details"
      },
      "SubstitutePayer": {
        "type": "object",
        "properties": {
          "acts": {
            "type": "boolean",
            "description": "Acts as a substitute payer"
          },
          "employerId": {
            "$ref": "#/components/schemas/Id"
          },
          "employerName": {
            "type": "string",
            "description": "Employer name\r\n\r\nMandatory data if \"Identifier type\" is other than \"Business ID\" or \"Finnish Personal Identification Number\".",
            "nullable": true
          },
          "wageSec": {
            "type": "boolean",
            "description": "Paid as wage security",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Substitute payer\r\n\r\nIf substitute payer data is specified, at least the \"Acts as a substitute payer\" and \"Employer identifier\" data must be specified.\r\nIf the \"Action type\" is \"Replacement report\", the data should match that of the report’s previous version."
      },
      "Payer": {
        "type": "object",
        "properties": {
          "payerIds": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Id"
            },
            "description": "Payer identifiers",
            "nullable": true
          },
          "payerBasic": {
            "$ref": "#/components/schemas/PayerBasic"
          },
          "address": {
            "$ref": "#/components/schemas/Address"
          },
          "subOrgs": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/SubOrg"
            },
            "description": "Payer's suborganisations",
            "nullable": true
          },
          "payerOther": {
            "$ref": "#/components/schemas/PayerOther"
          },
          "substitutePayer": {
            "$ref": "#/components/schemas/SubstitutePayer"
          }
        },
        "additionalProperties": false,
        "description": "Payer details"
      },
      "ActionCode": {
        "enum": [
          "newReport",
          "replacementReport"
        ],
        "type": "string",
        "description": "Action type (ActionCode)"
      },
      "ReportData": {
        "type": "object",
        "properties": {
          "actionCode": {
            "$ref": "#/components/schemas/ActionCode"
          },
          "irReportId": {
            "type": "string",
            "description": "Incomes Register report reference",
            "nullable": true
          },
          "reportId": {
            "type": "string",
            "description": "Report reference",
            "nullable": true
          },
          "reportVersion": {
            "type": "integer",
            "description": "Report version number",
            "format": "int32",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Service data"
      },
      "Gender": {
        "enum": [
          "male",
          "female"
        ],
        "type": "string",
        "description": "Gender"
      },
      "IncomeEarnerBasic": {
        "type": "object",
        "properties": {
          "missingId": {
            "type": "boolean",
            "description": "The income earner does not have a customer ID",
            "nullable": true
          },
          "companyName": {
            "type": "string",
            "description": "Company name",
            "nullable": true
          },
          "lastName": {
            "type": "string",
            "description": "Last name",
            "nullable": true
          },
          "firstName": {
            "type": "string",
            "description": "First name",
            "nullable": true
          },
          "birthDate": {
            "type": "string",
            "description": "Date of birth",
            "format": "date",
            "nullable": true
          },
          "gender": {
            "$ref": "#/components/schemas/Gender"
          }
        },
        "additionalProperties": false,
        "description": "Basic income earner details"
      },
      "AddressType": {
        "enum": [
          "addressInHomeCountry",
          "addressInTheCountryOfWork"
        ],
        "type": "string",
        "description": "Address type (AddressType)"
      },
      "TypedAddress": {
        "type": "object",
        "properties": {
          "addressType": {
            "$ref": "#/components/schemas/AddressType"
          },
          "co": {
            "type": "string",
            "description": "c/o",
            "nullable": true
          },
          "street": {
            "type": "string",
            "description": "Street address\r\n\r\nIf this data group is included, either \"Street address\" or \"P.O. Box\" must be specified.Both must not be entered.",
            "nullable": true
          },
          "poBox": {
            "type": "string",
            "description": "P.O. Box\r\n\r\nIf this data group is included, either \"Street address\" or \"P.O. Box\" must be specified.Both must not be entered.",
            "nullable": true
          },
          "postalCode": {
            "type": "string",
            "description": "Postal code",
            "nullable": true
          },
          "postOffice": {
            "type": "string",
            "description": "City",
            "nullable": true
          },
          "countryCode": {
            "type": "string",
            "description": "Country code\r\n\r\nA two-character code in accordance with the ISO 3166 country codes.\r\nIf the country is unknown, the value \"99\" is entered.\r\n\r\nMandatory data in the following cases:\r\n• No identifier where \"Identifier type\" is \"Business ID\" or \"Finnish Personal Identification Number\" has been specified in the \"Income earner identifiers\" data group.\r\n• The value of \"The income earner does not have a customer ID\" is \"true\".\r\n• The value of \"Non-resident taxpayer\" is \"true\".",
            "nullable": true
          },
          "countryName": {
            "type": "string",
            "description": "Country name\r\n\r\nMandatory data if \"Country code\" is \"99\".",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Income earner's address"
      },
      "EmploymentCode": {
        "enum": [
          "fullTime",
          "partTime",
          "notAvailable"
        ],
        "type": "string",
        "description": "Employment type (EmploymentCode)"
      },
      "TermCode": {
        "enum": [
          "untilFurtherNotice",
          "fixedTerm"
        ],
        "type": "string",
        "description": "Duration of employment (TermCode)"
      },
      "PaymentType": {
        "enum": [
          "monthly",
          "hourly",
          "contractPay"
        ],
        "type": "string",
        "description": "Form of payment"
      },
      "Period": {
        "type": "object",
        "properties": {
          "startDate": {
            "type": "string",
            "description": "Start date \r\n\r\nThe date must not be more than 45 days later than the current date. The date must not be earlier than 1 January 1800.\r\n\r\nIf a paid absence and the ‘Reimbursement application data’ data group for it are specified in the report, with 1 or 2 as the ‘Payment data type’, the ‘Start date’ of the period of employment must be no earlier than the ‘Start date’ of the period of paid absence in question.",
            "format": "date"
          },
          "endDate": {
            "type": "string",
            "description": "End date\r\n\r\nThe date must not be earlier than the \"Start date\".",
            "format": "date",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Time of employment"
      },
      "EmploymentEndingType": {
        "enum": [
          "kevaCodes",
          "bankOfFinlandCodes",
          "incomesRegisterCodes"
        ],
        "type": "string",
        "description": "Reason codes for the termination of employment (EmploymentEndingType)"
      },
      "EmploymentEnding": {
        "type": "object",
        "properties": {
          "type": {
            "$ref": "#/components/schemas/EmploymentEndingType"
          },
          "code": {
            "type": "string",
            "description": "Reason code for termination of employment\r\n\r\nIf \"Keva codes\" are used for the reason codes, the codeset must include the combination(\"Reason code for the termination of employment\", \"Pension provider code\") and it must be valid on the date that is the value of \"Payment date or other report date\". \r\nIf some other reason codeset is used, it must include this data.If the codeset includes a validity period, the value must be valid on the date that is the value of \"Payment date or other report date\" in the specified codeset.",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Reason for the termination of employment\r\n\r\nAn income earner can have several reasons for the termination of employment but only one value in accordance with the “Reason codes for termination of employment” data item."
      },
      "Employment": {
        "type": "object",
        "properties": {
          "employed": {
            "type": "boolean",
            "description": "Employed",
            "nullable": true
          },
          "employmentCode": {
            "$ref": "#/components/schemas/EmploymentCode"
          },
          "termCode": {
            "$ref": "#/components/schemas/TermCode"
          },
          "partTime": {
            "type": "number",
            "description": "Part-time %\r\n\r\nThe value must be a non-negative decimal number with a maximum value of 100.00.",
            "format": "double",
            "nullable": true
          },
          "hoursPerWeek": {
            "type": "number",
            "description": "Regular agreed working hours per week\r\n\r\nThe value must be a non-negative decimal number with a maximum value of 168.00.",
            "format": "double",
            "nullable": true
          },
          "paymentTypes": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/PaymentType"
            },
            "description": "Forms of payment\r\n\r\nThis data group is mandatory if a paid absence and the ‘Reimbursement application data’ data group for it are specified in the report, with 1 or 2 as the ‘Payment data type’.",
            "nullable": true
          },
          "employmentPeriods": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Period"
            },
            "description": "Employment period\r\n\r\nMandatory data group, if \"Pension provider code\" is 20, 24, 25, 29 or 30. This data group is mandatory if a paid absence and the ‘Reimbursement application data’ data group for it are specified in the report, with 1 or 2 as the ‘Payment data type’.",
            "nullable": true
          },
          "employmentEndings": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/EmploymentEnding"
            },
            "description": "Termination of employment\r\n\r\nMandatory data group, if \"Time of employment\" with an \"End date\" has been specified in the report, and the \"Pension provider code\" is 20, 30, 25, 24, 29 or 29.",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Employment relationship data"
      },
      "ProfessionType": {
        "enum": [
          "statisticsFinland",
          "keva",
          "bankOfFinland",
          "trafi"
        ],
        "type": "string",
        "description": "Type of occupational title or classification (ProfessionType)"
      },
      "Profession": {
        "type": "object",
        "properties": {
          "type": {
            "$ref": "#/components/schemas/ProfessionType"
          },
          "code": {
            "type": "string",
            "description": "Occupational class or title identifier",
            "nullable": true
          },
          "title": {
            "type": "string",
            "description": "Title (free text)",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Occupational class or title"
      },
      "EmploymentRegType": {
        "enum": [
          "kevasGroundsForRegistration",
          "bankOfFinlandsGroundsForRegistration"
        ],
        "type": "string",
        "description": "Type of registration grounds (EmploymentRegType)"
      },
      "EmploymentReg": {
        "type": "object",
        "properties": {
          "type": {
            "$ref": "#/components/schemas/EmploymentRegType"
          },
          "code": {
            "type": "string",
            "description": "Registration grounds identifier\r\n\r\nIf \"Type of registration grounds\" is \"Keva's grounds for registration\", the codeset must include the combination(\"Registration grounds identifier\", \"Pension provider code\"), and must be valid on the date that is the value of \"Payment date or other report date\".\r\n\r\nIf \"Type of registration grounds\" is \"Keva's grounds for registration\", and the codeset requires the use of a specific \"Occupational title or classification identifier\" in connection with the specified \"Registration grounds identifier\" value, the codeset must include the combination(\"Registration grounds identifier\", \"Pension provider code\") and must be valid on the date that is the value of \"Payment date or other report date\".\r\n\r\nIf \"Type of registration grounds\" has some other value, it must be included in the specified codeset.If the codeset includes a validity period, the value must be valid on the date that is the value of \"Payment date or other report date\" in the specified codeset",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Grounds for registration\r\n\r\nAn income earner can have several grounds for registration but only one value in accordance with the “Type of registration grounds” data item."
      },
      "PlaceOfBusiness": {
        "type": "object",
        "properties": {
          "code": {
            "type": "string",
            "description": "Place of business code",
            "nullable": true
          },
          "street": {
            "type": "string",
            "description": "Street address",
            "nullable": true
          },
          "postalCode": {
            "type": "string",
            "description": "Postal code",
            "nullable": true
          },
          "postOffice": {
            "type": "string",
            "description": "City",
            "nullable": true
          },
          "countryCode": {
            "type": "string",
            "description": "Country code",
            "nullable": true
          },
          "countryName": {
            "type": "string",
            "description": "Country name",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Place of business"
      },
      "PensionActCode": {
        "enum": [
          "employeesEarningsRelatedPensionInsurance",
          "pensionInsuranceForFarmers",
          "pensionInsuranceForTheSelfEmployed"
        ],
        "type": "string",
        "description": "Earnings-related pension insurance information (PensionActCode)"
      },
      "PensionProvIdCode": {
        "enum": [
          "alandia",
          "etera",
          "finnishCentreForPensions",
          "clergyOfTheOrthodoxChurch",
          "kevaMemberOrganisations",
          "kevaÅlandLandskapsregeringsPensionssystem",
          "kevaChurch",
          "bankOfFinland",
          "kevaKelaEmploymentPensions",
          "kevaState",
          "seafarers",
          "farmers",
          "ilmarinen",
          "elo",
          "varma",
          "veritas",
          "upm",
          "sanoma",
          "sandvik",
          "kontino",
          "yleisradio",
          "abb",
          "lFashionGroup",
          "honeywell",
          "telia",
          "yaraSuomi",
          "orion",
          "valio",
          "op",
          "verso",
          "apteekkien",
          "viabek",
          "reka"
        ],
        "type": "string",
        "description": "Pension provider code (PensionProvIdCode)"
      },
      "PensionInsurance": {
        "type": "object",
        "properties": {
          "pensionActCode": {
            "$ref": "#/components/schemas/PensionActCode"
          },
          "pensionProvIdCode": {
            "$ref": "#/components/schemas/PensionProvIdCode"
          },
          "pensionPolicyNo": {
            "type": "string",
            "description": "Pension policy number",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Earnings-related pension insurance\r\n\r\nThis data group is mandatory, except in the following situations:\r\n\r\na) The report contains no income types that require specifying a value for \"Earnings-related pension insurance information\". This takes account of whether the income type is, by default, income subject to earningsrelated pension insurance(see the document, \"Wages – Codes – Income types\") and also whether the income type in the report is exceptionally subject to earnings-related pension insurance.In the report, the exceptions to the income type's default values are specified using the \"Type of insurance information\" and \"Grounds for insurance contribution\" entries. If \"Type of insurance information\" is \"Subject to earnings-related pension insurance contribution\" and the corresponding \"Grounds for insurance contribution\" value is \"true\"/\"false\", the income type in the report is/is not income subject to earnings-related pension insurance.\r\n\r\nb) \"Acts as a substitute payer\" is \"true\".\r\n\r\nc) \"Type of exception to insurance\" is one of the following:\r\n• No obligation to provide insurance(earnings-related pension, health, unemployment or accident and occupational disease insurance)\r\n• No obligation to provide insurance(earnings-related pension insurance)\r\n• Not subject to Finnish social security (earnings-related pension, health, unemployment or accident and occupational disease insurance)\r\n• Not subject to Finnish social security(earnings-related pension insurance)\r\n\r\nException: Although the data group is not mandatory under items a to c, it must always be included if \"Type of exception to insurance\" is \"Voluntary insurance in Finland(earnings-related pension insurance)\".\r\n\r\nEmployee's earnings-related pension insurance\r\nIf \"Employee's earnings-related pension insurance\" is specified as the value of \"Earnings-related pension insurance information\", values must also be specified for \"Pension provider code\" and \"Pension policy number\".\r\n\r\nPension insurance for farmers(MYEL)\r\n\r\nIf \"Pension insurance for farmers (MYEL)\" is specified as the value of \"Earningsrelated pension insurance information\", values must not be specified for \"Pension provider code\" or \"Pension policy number\".\r\n\r\nPension insurance for the self-employed(YEL)\r\n\r\nIf \"Pension insurance for the self-employed (YEL)\" is specified as the value of \"Earnings-related pension insurance information\", values must not be specified for \"Pension provider code\" or \"Pension policy number\". \r\nIf the \"Action type\" is \"Replacement report\", the data should match that of the report’s previous version."
      },
      "AccidentInsurance": {
        "type": "object",
        "properties": {
          "accInsProvId": {
            "$ref": "#/components/schemas/Id"
          },
          "accInsPolicyNo": {
            "type": "string",
            "description": "Occupational accident insurance policy number",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Occupational accident insurance \r\n\r\nIf the \"Action type\" is \"Replacement report\", the data should match that of the report’s previous version."
      },
      "ExceptionCode": {
        "enum": [
          "noObligationToSocialInsurance",
          "noObligationToHealthInsurance",
          "noObligationToEarningsRelatedPensionInsurance",
          "noObligationToAccidentAndOccupationalDiseaseInsurance",
          "noObligationToUnemploymentInsurance",
          "notSubjectToSocialInsurance",
          "notSubjectToEarningsRelatedPensionInsurance",
          "notSubjectToAccidentAndOccupationalDiseaseInsurance",
          "notSubjectToUnemploymentInsurance",
          "notSubjectToHealthInsurance",
          "voluntaryEarningsRelatedPensionInsurance"
        ],
        "type": "string",
        "description": "Type of exception to insurance (ExceptionCode)"
      },
      "PostedCertCode": {
        "enum": [
          "fromFinlandA1CertificateOrAgreement",
          "toFinlandA1CertificateOrAgreement",
          "nonEUEEACountryOrNonSocialSecurityAgreementCountry"
        ],
        "type": "string"
      },
      "FormCode": {
        "enum": [
          "nT1",
          "nT2",
          "employersReportOnPeriodsOfStayInFinland",
          "employeeLeasingNotice"
        ],
        "type": "string",
        "description": "Form type"
      },
      "InternationalData": {
        "type": "object",
        "properties": {
          "postedCertCode": {
            "$ref": "#/components/schemas/PostedCertCode"
          },
          "nonResident": {
            "type": "boolean",
            "description": "Non-resident taxpayer \r\n\r\nIf income type 412 or 362 is specified in the report, the value of the ‘Nonresident taxpayer’ data item must be ‘true’.",
            "nullable": true
          },
          "nonResidentCountryCode": {
            "type": "string",
            "description": "Country code of country of residence \r\n\r\nMandatory data, if \"Non-resident taxpayer\" is \"true\".",
            "nullable": true
          },
          "nonResidentCountryName": {
            "type": "string",
            "description": "Name of country of residence\r\n\r\nMandatory data if \"Country code\" is \"99\".",
            "nullable": true
          },
          "subToWithhold": {
            "type": "boolean",
            "description": "Income subject to withholding",
            "nullable": true
          },
          "taxTreatyCountryCode": {
            "type": "string",
            "description": "Tax-treaty country code\r\n\r\nMandatory data if the Type of additional income earner data (IncomeEarnerType) = \"Income earner did not stay longer than 183 days in Finland during the Tax-Treaty-defined sojourn period\". This data can only be specified if the Type of additional income earner data is the above-mentioned value.\r\nThe value must be valid on the date specified in the data item \"Payment date or other report date\".",
            "nullable": true
          },
          "workForm": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/FormCode"
            },
            "description": "Forms for work abroad",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "International situations"
      },
      "IncomeEarnerType": {
        "enum": [
          "employedByStateEmploymentFund",
          "jointOwnerWithPayer",
          "partialOwner",
          "keyEmployee",
          "leasedEmployeeLivingAbroad",
          "personWorkingInFrontierDistrict",
          "personWorkingAbroad",
          "athlete",
          "performingArtist",
          "restrictedPeriodInFinland",
          "netOfTaxContract",
          "organization",
          "personWorkingOnAlandFerry",
          "entrepreneurOrFarmerNoPensionRequired"
        ],
        "type": "string",
        "description": "Type of additional income earner informatio"
      },
      "RefPaymentType": {
        "enum": [
          "earningsRelatedPensionProvider"
        ],
        "type": "string",
        "description": "Payment data type (RefPaymentType)"
      },
      "Payment": {
        "type": "object",
        "properties": {
          "paymentType": {
            "$ref": "#/components/schemas/RefPaymentType"
          },
          "paymentRef": {
            "type": "string",
            "description": "Payment's reference number \r\n\r\nMandatory data, if \"Payment data type\" is \"Earnings-related pension provider\".",
            "nullable": true
          },
          "paymentSpecifier": {
            "type": "string",
            "description": "Payment specifier",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Payment data"
      },
      "IncomeEarnerOther": {
        "type": "object",
        "properties": {
          "cbaCode": {
            "type": "integer",
            "description": "Applicable collective agreement\r\n\r\nThe value must be valid on the date specified in the data item \"Payment date or other report date\".",
            "format": "int32",
            "nullable": true
          },
          "incomeEarnerTypes": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/IncomeEarnerType"
            },
            "description": "Additional income earner details",
            "nullable": true
          },
          "payments": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Payment"
            },
            "description": "Payment data",
            "nullable": true
          }
        },
        "additionalProperties": false
      },
      "IncomeEarner": {
        "type": "object",
        "properties": {
          "incomeEarnerIds": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Id"
            },
            "description": "Income earner identifiers\r\n\r\nMandatory data group, if \"The income earner does not have a customer ID\" has not been specified. \r\nThe data group must not be included, if \"The income earner does not have a customer ID\" has been specified.\r\n\r\nIf the \"Action type\" is \"Replacement report\", the data should match that of the report’s previous version.",
            "nullable": true
          },
          "incomeEarnerBasic": {
            "$ref": "#/components/schemas/IncomeEarnerBasic"
          },
          "addresses": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/TypedAddress"
            },
            "description": "Income earner's addresses\r\n\r\nMandatory data group in the following cases:\r\n• No identifiers where \"Identifier type\" is \"Business ID\" or \"Finnish Personal Identification Number\" have been specified in the \"Income earner identifiers\" data group.\r\n• The value of \"The income earner does not have a customer ID\" is \"true\".\r\n• The value of \"Non-resident taxpayer\" is \"true\". In this case, at least the address in home country (\"Address type\" is \"Address in home country\") must be specified for the income earner",
            "nullable": true
          },
          "subOrgs": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/SubOrg"
            },
            "description": "Income earner's suborganisations",
            "nullable": true
          },
          "employment": {
            "$ref": "#/components/schemas/Employment"
          },
          "professions": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Profession"
            },
            "description": "Occupation\r\n\r\nThe \"Occupational class or title\" data group must always be specified for an income earner, with the \"Occupational class or title identifier\" in accordance with \"Statistics Finland's classification of occupations\" included, except in the following situations:\r\n\r\na) The report contains no income types that require specification of th \"Occupational class or title\" data group.This takes account of whether the income type is, by default, income subject to accident and occupational disease insurance (see the document, \"Wages – Codes – Income types\") and whether the income type in the report is exceptionally subject to accident and occupational disease insurance.In the report, the exceptions to the income type's default values are specified using the \"Type of insurance information\" and \"Grounds for insurance contribution\" entries. If \"Type of insurance information\" is \"Subject to accident and occupational disease insurance contribution\" and the corresponding \"Grounds for insurance contribution\" value is \"true\"/\"false\", the income type in the report is/is not income subject to accident and occupational disease insurance.\r\n\r\nb) The value of \"Acts as a substitute payer\" is \"true\".\r\n\r\nc) The report includes a \"Type of exception to insurance\" that is one of the following:\r\n• No obligation to provide insurance (earnings-related pension, health, unemployment or accident and occupational disease insurance)\r\n• No obligation to provide insurance(accident and occupational disease insurance)\r\n• Not subject to Finnish social security(earnings-related pension, health, unemployment or accident and occupational disease insurance\r\n• Not subject to Finnish social security(accident and occupational disease insurance)\r\n\r\nThe \"Occupational class or title\" data group must always be specified for an income earner, with the \"Occupational class or title identifier\" in accordance with \"Trafi's titles\" included, if the \"Pension provider code\" is 34.*\r\nThe \"Occupational class or title\" data group must always be specified for an income earner, with the \"Occupational class or title identifier\" in accordance with \"Keva's titles\" included, if the \"Pension provider code\" is 20, 30, 25, 24 or 29.*\r\nThe \"Occupational class or title\" data group must always be specified for an income earner, with the \"Occupational class or title identifier\" in accordance with \"Bank of Finland's titles\" included, if the \"Pension provider code\" is 27.*\r\n\r\n* Exception: The \"Occupational class or title\" data group does not need to be specified, if \"Type of exception to insurance\" with one of the following values has been specified for the income earner:\r\n• No obligation to provide insurance(earnings-related pension, health, unemployment or accident and occupational disease insurance)\r\n• No obligation to provide insurance(earnings-related pension insurance)\r\n• Not subject to Finnish social security(earnings-related pension, health, unemployment or accident and occupational disease insurance)\r\n• Not subject to Finnish social security(earnings-related pension insurance)",
            "nullable": true
          },
          "employmentRegs": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/EmploymentReg"
            },
            "description": "Employment registration\r\n\r\nThe \"Grounds for registration\" data group must be specified, with the \"Registration grounds identifier\" in accordance with \"Keva's grounds for registration\" included, if the \"Pension provider code\" is 20, 30, 25, 24 or 29.\r\nThe \"Grounds for registration\" data group must be specified, with the \"Registration grounds identifier\" in accordance with \"Bank of Finland's grounds for registration\" included, if the \"Pension provider code\" is 27",
            "nullable": true
          },
          "placeOfBusiness": {
            "$ref": "#/components/schemas/PlaceOfBusiness"
          },
          "pensionInsurance": {
            "$ref": "#/components/schemas/PensionInsurance"
          },
          "accidentInsurance": {
            "$ref": "#/components/schemas/AccidentInsurance"
          },
          "insuranceExceptions": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/ExceptionCode"
            },
            "description": "Insurance exceptions\r\n\r\nIf the Type of exception to insurance = 11 (Voluntary earnings-related pension) has been specified for the income earner, the values 1, 3, 6 or 7 should not be specified for the Type of exception to insurance in the same report.\r\n\r\nIf the \"Action type\" is \"Replacement report\", the data should match that of the report’s previous version.",
            "nullable": true
          },
          "internationalData": {
            "$ref": "#/components/schemas/InternationalData"
          },
          "incomeEarnerOther": {
            "$ref": "#/components/schemas/IncomeEarnerOther"
          }
        },
        "additionalProperties": false,
        "description": "Income earner details"
      },
      "TransactionBasic": {
        "type": "object",
        "properties": {
          "transactionCode": {
            "type": "integer",
            "description": "Income type code \r\n\r\nIf the ‘Income type code’ is 412 or 362, the value of the ‘Non-resident taxpayer’ data item must be ‘true’.\r\nIf the ‘Income type code’ is 334 (Meal benefit) and the value of the ‘Reimbursement for a meal benefit corresponds to taxable value(TaxValue)’ is ‘true’, and none of the income types 301, 302, 317, 330 or 343 have been specified in the report, the income type 407 must not be specified.\r\nIncome type 407 must not be specified if none of the following income types have been specified in the report: 301, 302, 317, 330 or 334.\r\nIncome type 419 must not be specified if none of the following income types have been specified in the report: 101−106, 201−207, 209-227, 230−239, 308, 315, 316, 320, 326, 336, 339, 351, 353−355, 359.",
            "format": "int32"
          },
          "amount": {
            "type": "number",
            "description": "Amount\r\n\r\nIf ‘Income type code’ is 102, 103, 104, 105 or 106, the value must not be greaterthan the amount reported for income type 101.\r\n\r\nThe Wages – Codes – Income types document lists the income types for which anegative Amount can be specified.A negative Amount must not be specified forother income types.\r\n\r\nIf the value of the ‘Reimbursement for a meal benefit corresponds to taxablevalue (TaxValue)’ data item specified for income type 334 is ‘true’, the amountis not mandatory.\r\n\r\nIf ‘Income type code’ is 401 (Compensation collected for car benefit), theamount must not be greater than the amount reported using income type 304(Car benefit).\r\n\r\nIf ‘Income type code’ is 407 (Reimbursement collected for other fringe benefits),the value must not be greater than the amount reported using one of thefollowing income types or the total amount reported using some of thefollowing income types: 301 (Housing benefit), 302 (Interest benefit for ahousing loan), 317 (Other fringe benefit), 330 (Telephone benefit) and 334(Meal benefit).\r\n\r\nIf ‘Income type code’ is 415 (Reimbursement for employer-subsidisedcommuter ticket), the amount must not be higher than the amount reportedusing income type 341 (Employer-subsidised commuter ticket, tax-exemptshare) or income type 342 (Employer-subsidised commuter ticket, taxableshare), or the total amount reported using these income types.\r\n\r\nIf ‘Income type code’ is 419 (Deduction before withholding), the value must notbe greater than the amount reported using one of the following income typesor the total amount reported using some of the following income types:101−106, 201−207, 209-227, 230−239, 308, 315, 316, 320, 326, 336, 339, 351,353−355, 359.",
            "format": "double",
            "nullable": true
          },
          "noMoney": {
            "type": "boolean",
            "description": "Payment other than money",
            "nullable": true
          },
          "oneOff": {
            "type": "boolean",
            "description": "One-off remuneration",
            "nullable": true
          },
          "unjustEnrichment": {
            "type": "boolean",
            "description": "Unjust enrichment \r\n\r\nThe Wages – Codes – Income types document lists the income types for which the ‘Unjust enrichment’ data item must not be specified.",
            "nullable": true
          },
          "recovery": {
            "type": "boolean",
            "description": "Recovery\r\n\r\nThe Wages – Codes – Income types document lists the income types for which the ‘Recovery’ data item must not be specified.",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "General details on the income type"
      },
      "InsuranceCode": {
        "enum": [
          "subjectToSocialInsuranceContributions",
          "subjectToEarningsRelatedPensionInsuranceContribution",
          "subjectToHealthInsuranceContribution",
          "subjectToUnemploymentInsuranceContribution",
          "subjectToAccidentInsuranceAndOccupationalDiseaseInsuranceContribution"
        ],
        "type": "string",
        "description": "Insurance information type (InsuranceCode)"
      },
      "TransactionInclusion": {
        "type": "object",
        "properties": {
          "insuranceCode": {
            "$ref": "#/components/schemas/InsuranceCode"
          },
          "included": {
            "type": "boolean",
            "description": "Grounds for insurance contribution"
          }
        },
        "additionalProperties": false,
        "description": "Insurance information"
      },
      "EarningPeriod": {
        "type": "object",
        "properties": {
          "startDate": {
            "type": "string",
            "description": "Start date\r\n\r\nThe date must not be earlier than 1 January 1800",
            "format": "date"
          },
          "endDate": {
            "type": "string",
            "description": "End date\r\n\r\nThe date must not be earlier than the \"Start date\".",
            "format": "date"
          }
        },
        "additionalProperties": false,
        "description": "Earnings period"
      },
      "WageUnitCode": {
        "enum": [
          "hour",
          "day",
          "week",
          "period"
        ],
        "type": "string",
        "description": "Pay unit (WageUnitCode)"
      },
      "UnitWage": {
        "type": "object",
        "properties": {
          "unitPrice": {
            "type": "number",
            "description": "Unit price\r\n\r\nThe value must be non-negative.",
            "format": "double"
          },
          "unitAmount": {
            "type": "number",
            "description": "Number of units\r\n\r\nThe value must be non-negative.",
            "format": "double"
          },
          "unitCode": {
            "$ref": "#/components/schemas/WageUnitCode"
          }
        },
        "additionalProperties": false,
        "description": "Unit wage"
      },
      "CarBenefitCode": {
        "enum": [
          "limitedCarBenefit",
          "fullCarBenefit"
        ],
        "type": "string",
        "description": "Car benefit type (CarBenefitCode)"
      },
      "AgeGroupCode": {
        "enum": [
          "a",
          "b",
          "c",
          "u"
        ],
        "type": "string",
        "description": "Car age group (AgeGroupCode)"
      },
      "CarBenefit": {
        "type": "object",
        "properties": {
          "carBenefitCode": {
            "$ref": "#/components/schemas/CarBenefitCode"
          },
          "ageGroupCode": {
            "$ref": "#/components/schemas/AgeGroupCode"
          },
          "kilometers": {
            "type": "integer",
            "description": "Odometer reading\r\n\r\nThe value must be greater than or equal to 0",
            "format": "int32",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Car benefit\r\n\r\nMandatory data group, if the income type is \"Car benefit\". This data group may not be used with other income types."
      },
      "MealBenefit": {
        "type": "object",
        "properties": {
          "taxValue": {
            "type": "boolean",
            "description": "Reimbursement for a meal benefit corresponds to taxable value"
          }
        },
        "additionalProperties": false,
        "description": "Meal benefit"
      },
      "BenefitCode": {
        "enum": [
          "accommodationBenefit",
          "telephoneBenefit",
          "mealBenefit",
          "otherBenefits"
        ],
        "type": "string",
        "description": "Type of other fringe benefit (BenefitCode)"
      },
      "WithdrawalPeriod": {
        "type": "object",
        "properties": {
          "startDate": {
            "type": "string",
            "description": "Start date\r\n\r\nThe date must not be earlier than 1 January 1800.",
            "format": "date"
          },
          "endDate": {
            "type": "string",
            "description": "End date\r\n\r\nThe date must not be earlier than the \"Start date\".",
            "format": "date"
          }
        },
        "additionalProperties": false,
        "description": "Withdrawal period"
      },
      "SailorIncome": {
        "type": "object",
        "properties": {
          "sailorIncomeProp": {
            "type": "boolean",
            "description": "Payment is seafarer's income"
          },
          "crossTradeTime": {
            "type": "integer",
            "description": "Time of cross trade\r\n\r\nAllowed valus: 1 - 12",
            "format": "int32",
            "nullable": true
          },
          "withdrawalPeriod": {
            "$ref": "#/components/schemas/WithdrawalPeriod"
          }
        },
        "additionalProperties": false,
        "description": "Seafarer's income"
      },
      "OrigPaymentPeriod": {
        "type": "object",
        "properties": {
          "paymentDate": {
            "type": "string",
            "description": "Payment date\r\n\r\nThe date must not be earlier than 1 January 1800. The date must not be later than that of ‘Payment date or other report date’ on the report.",
            "format": "date",
            "nullable": true
          },
          "startDate": {
            "type": "string",
            "description": "Start date\r\n\r\nThe date must not be earlier than 1 January 1800.",
            "format": "date"
          },
          "endDate": {
            "type": "string",
            "description": "End date\r\n\r\nThe \"End date\" may not be earlier than the \"Start date\".",
            "format": "date"
          }
        },
        "additionalProperties": false,
        "description": "Original payment period"
      },
      "RecoveryData": {
        "type": "object",
        "properties": {
          "recoveryDate": {
            "type": "string",
            "description": "Recovery date\r\n\r\nThe date must not be earlier than 1 January 1800.",
            "format": "date"
          },
          "withhold": {
            "type": "number",
            "description": "Withholding from the recovered amount",
            "format": "double",
            "nullable": true
          },
          "taxAtSource": {
            "type": "number",
            "description": "Tax at source from the recovered amount",
            "format": "double",
            "nullable": true
          },
          "origPaymentPeriods": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/OrigPaymentPeriod"
            },
            "description": "Original payment periods",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Additional details on recovery"
      },
      "AllowanceCode": {
        "enum": [
          "mealAllowance",
          "partialDailyAllowance",
          "fullDailyAllowance",
          "internationalDailyAllowance",
          "taxExemptReimbursementsAbroad"
        ],
        "type": "string",
        "description": "Daily allowance type (AllowanceCode)"
      },
      "KmAllowance": {
        "type": "object",
        "properties": {
          "kilometers": {
            "type": "integer",
            "description": "Number of kilometres",
            "format": "int32"
          }
        },
        "additionalProperties": false,
        "description": "Kilometre allowance"
      },
      "SixMonthRule": {
        "type": "object",
        "properties": {
          "applicable": {
            "type": "boolean",
            "description": "Six-month rule is applicable"
          },
          "countryCode": {
            "type": "string",
            "description": "Country code\r\n\r\nA two-character code in accordance with the ISO 3166 country codes.\r\nIf the country is unknown, the value \"99\" is entered.\r\n\r\nMandatory data, if \"Six-month rule is applicable\" is \"true\".",
            "nullable": true
          },
          "countryName": {
            "type": "string",
            "description": "Country name\r\n\r\nMandatory data if \"Country code\" is \"99\".",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Six-month rule"
      },
      "Transaction": {
        "type": "object",
        "properties": {
          "transactionBasic": {
            "$ref": "#/components/schemas/TransactionBasic"
          },
          "insuranceData": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/TransactionInclusion"
            },
            "description": "Insurance information",
            "nullable": true
          },
          "earningPeriods": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/EarningPeriod"
            },
            "description": "Earnings periods",
            "nullable": true
          },
          "unitWages": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/UnitWage"
            },
            "description": "Unit of the wages",
            "nullable": true
          },
          "carBenefit": {
            "$ref": "#/components/schemas/CarBenefit"
          },
          "mealBenefit": {
            "$ref": "#/components/schemas/MealBenefit"
          },
          "otherBenefit": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/BenefitCode"
            },
            "description": "Other fringe benefit",
            "nullable": true
          },
          "sailorIncome": {
            "$ref": "#/components/schemas/SailorIncome"
          },
          "recoveryData": {
            "$ref": "#/components/schemas/RecoveryData"
          },
          "dailyAllowance": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/AllowanceCode"
            },
            "description": "Daily allowance",
            "nullable": true
          },
          "kmAllowance": {
            "$ref": "#/components/schemas/KmAllowance"
          },
          "sixMonthRule": {
            "$ref": "#/components/schemas/SixMonthRule"
          }
        },
        "additionalProperties": false,
        "description": "Income type details"
      },
      "Representative": {
        "type": "object",
        "properties": {
          "representativeIds": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Id"
            },
            "description": "Representative identifiers",
            "nullable": true
          },
          "name": {
            "type": "string",
            "description": "Representative's name",
            "nullable": true
          },
          "address": {
            "$ref": "#/components/schemas/Address"
          },
          "representativeReports": {
            "type": "boolean",
            "description": "Representative acts as submitter",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Representative"
      },
      "FinServiceRecipient": {
        "type": "object",
        "properties": {
          "finServiceRecipientIds": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Id"
            },
            "description": "Employer identifiers",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Finnish employer\r\n\r\nMandatory data group if a form for work abroad where \"Form type\" is \"Employee leasing notice\" has been included in the report."
      },
      "ForeignLeasedWork": {
        "type": "object",
        "properties": {
          "representative": {
            "$ref": "#/components/schemas/Representative"
          },
          "estAmount": {
            "type": "number",
            "description": "Estimated pay amount\r\n\r\nMandatory data if a form for work abroad where \"Form type\" is \"Employee leasing notice\" has been included in the report.\r\n\r\nThe value must be greater than or equal to 0.",
            "format": "double",
            "nullable": true
          },
          "finServiceRecipient": {
            "$ref": "#/components/schemas/FinServiceRecipient"
          }
        },
        "additionalProperties": false,
        "description": "Details on foreign leased workforce"
      },
      "StayPeriod": {
        "type": "object",
        "properties": {
          "startDate": {
            "type": "string",
            "description": "Start date\r\n\r\nThe date must not be earlier than 1 January 1800.",
            "format": "date"
          },
          "endDate": {
            "type": "string",
            "description": "End date\r\n\r\nThe date must not be earlier than the \"Start date\".",
            "format": "date"
          }
        },
        "additionalProperties": false,
        "description": "Stay period"
      },
      "StayPeriodsInFinland": {
        "type": "object",
        "properties": {
          "noStayPeriods": {
            "type": "boolean",
            "description": "Report contains no stay periods in Finland",
            "nullable": true
          },
          "period": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/StayPeriod"
            },
            "description": "Stay periods",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Stay periods in Finland"
      },
      "WorkPeriodInFinland": {
        "type": "object",
        "properties": {
          "startDate": {
            "type": "string",
            "description": "Start date\r\n\r\nThe date must not be earlier than 1 January 1800.",
            "format": "date",
            "nullable": true
          },
          "endDate": {
            "type": "string",
            "description": "End date\r\n\r\nThe date must not be earlier than the \"Start date\".",
            "format": "date",
            "nullable": true
          },
          "workDays": {
            "type": "integer",
            "description": "Number of workdays in Finland\r\n\r\nThe value must be a non-negative integer.",
            "format": "int32",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Work period in Finland"
      },
      "WorkCountryAddress": {
        "type": "object",
        "properties": {
          "co": {
            "type": "string",
            "description": "c/o",
            "nullable": true
          },
          "street": {
            "type": "string",
            "description": "Street address\r\n\r\nEither \"Street address\" or \"P.O. Box\" must be specified in this data group. Both must not be entered.",
            "nullable": true
          },
          "poBox": {
            "type": "string",
            "description": "P.O. Box\r\n\r\nEither \"Street address\" or \"P.O. Box\" must be specified in this data group. Both must not be entered.",
            "nullable": true
          },
          "postalCode": {
            "type": "string",
            "description": "Postal code",
            "nullable": true
          },
          "postOffice": {
            "type": "string",
            "description": "City",
            "nullable": true
          },
          "countryCode": {
            "type": "string",
            "description": "Country code\r\n\r\nA two-character code in accordance with the ISO 3166 country codes.\r\nIf the country is unknown, the value \"99\" is entered.",
            "nullable": true
          },
          "countryName": {
            "type": "string",
            "description": "Country name\r\n\r\nMandatory data if \"Country code\" is \"99\"",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Address in country of work"
      },
      "ServiceRecipientAddress": {
        "type": "object",
        "properties": {
          "co": {
            "type": "string",
            "description": "c/o",
            "nullable": true
          },
          "street": {
            "type": "string",
            "description": "Street address",
            "nullable": true
          },
          "poBox": {
            "type": "string",
            "description": "P.O. Box",
            "nullable": true
          },
          "postalCode": {
            "type": "string",
            "description": "Postal code",
            "nullable": true
          },
          "postOffice": {
            "type": "string",
            "description": "City",
            "nullable": true
          },
          "countryCode": {
            "type": "string",
            "description": "Country code",
            "nullable": true
          },
          "countryName": {
            "type": "string",
            "description": "Country name",
            "nullable": true
          }
        },
        "additionalProperties": false
      },
      "ServiceRecipient": {
        "type": "object",
        "properties": {
          "serviceRecipientIds": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Id"
            },
            "description": "Principal identifiers",
            "nullable": true
          },
          "name": {
            "type": "string",
            "description": "Principal's name",
            "nullable": true
          },
          "serviceRecipientAddress": {
            "$ref": "#/components/schemas/ServiceRecipientAddress"
          }
        },
        "additionalProperties": false,
        "description": "Principal"
      },
      "RemunerationCode": {
        "enum": [
          "dailyAllowance",
          "accommodationBenefit",
          "carBenefit",
          "otherBenefit"
        ],
        "type": "string",
        "description": "Type of other reimbursement or benefit"
      },
      "WorkCountry": {
        "type": "object",
        "properties": {
          "workCountryCode": {
            "type": "string",
            "description": "Code of country of work \r\n\r\nA two-character code inaccordance with the ISO3166 country codes.If the country isunknown, the value \"99\"is entered.",
            "nullable": true
          },
          "workCountryName": {
            "type": "string",
            "description": "Name of country of work\r\n\r\nMandatory data if \"Country code\" is \"99\".",
            "nullable": true
          },
          "workMunicipalities": {
            "type": "array",
            "items": {
              "type": "string"
            },
            "description": "Districts of work",
            "nullable": true
          },
          "workCountryAddress": {
            "$ref": "#/components/schemas/WorkCountryAddress"
          },
          "serviceRecipient": {
            "$ref": "#/components/schemas/ServiceRecipient"
          },
          "stayPeriodsAbroad": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/StayPeriod"
            },
            "description": "Stay periods in country of work\r\n\r\nThere may be several stay periods in the country of work. Mandatory data group, if a form for work abroad where \"Form type\" is \"NT1\" or \"NT2\" has been included in the report",
            "nullable": true
          },
          "workPeriodsAbroad": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Period"
            },
            "description": "Work periods abroad\r\n\r\nMandatory data group, if a form for work abroad where \"Form type\" is \"NT2\" has been included in the report.",
            "nullable": true
          },
          "taxingRight": {
            "type": "boolean",
            "description": "Country of work has taxing right\r\n\r\nMandatory data if a form for work abroad where \"Form type\" is \"NT1\" or \"NT2\" has been included in the report.",
            "nullable": true
          },
          "burdensResultOfPE": {
            "type": "boolean",
            "description": "Pay burdens the result of the employer’s permanent establishment in the country of work\r\n\r\nMandatory data if a form for work abroad where \"Form type\" is \"NT1\" or \"NT2\" has been included in the report.",
            "nullable": true
          },
          "wagePerMonth": {
            "type": "number",
            "description": "Pay per month\r\n\r\nThis data group may only be used if a form for work abroad, where \"Form type\" is \"NT2\", has been included in the report.",
            "format": "double",
            "nullable": true
          },
          "remunerations": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/RemunerationCode"
            },
            "description": "Other remunerations and benefits\r\n\r\nThis data group may only be used if a form for work abroad, where \"Form type\" is \"NT2\", has been included in the report.",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Work country"
      },
      "UnpaidAbsenceCauseCode": {
        "enum": [
          "illness",
          "partTimeSickLeave",
          "maternityPaternityAndParentalLeave",
          "specialMaternityLeave",
          "rehabilitation",
          "childsIllnessOrACompellingFamilyReason",
          "partTimeChildCareLeave",
          "trainingEducation",
          "jobAlternationLeave",
          "studyLeave",
          "industrialActionOrLockOut",
          "interruptionInWorkProvision",
          "leaveOfAbsence",
          "militaryRefresherTraining",
          "militaryOrNonMilitaryService",
          "layOff",
          "childCare",
          "other"
        ],
        "type": "string",
        "description": "Reason for unpaid absence"
      },
      "UnpaidAbsencePeriod": {
        "type": "object",
        "properties": {
          "startDate": {
            "type": "string",
            "description": "Start date\r\n\r\nThe date must not be earlier than 1 January 1800.",
            "format": "date"
          },
          "endDate": {
            "type": "string",
            "description": "End date\r\n\r\nThe \"End date\" may not be earlier than the \"Start date\".",
            "format": "date",
            "nullable": true
          },
          "absenceDays": {
            "type": "integer",
            "description": "Number of days of absence \r\n\r\nThe value must be a non-negative number.",
            "format": "int32",
            "nullable": true
          },
          "causeCode": {
            "$ref": "#/components/schemas/UnpaidAbsenceCauseCode"
          }
        },
        "additionalProperties": false,
        "description": "Absence period"
      },
      "UnpaidAbsence": {
        "type": "object",
        "properties": {
          "unpaidAbsencePeriods": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/UnpaidAbsencePeriod"
            },
            "description": "Absence periods",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Unpaid absence"
      },
      "PaidAbsenceCauseCode": {
        "enum": [
          "illness",
          "partTimeSickLeave",
          "maternityPaternityAndParentalLeave",
          "specialMaternityLeave",
          "rehabilitation",
          "childsIllnessOrACompellingFamilyReason",
          "partTimeChildCareLeave",
          "trainingEducation",
          "leaveOfAbsence",
          "militaryRefresherTraining",
          "midWeekHoliday",
          "accruedHoliday",
          "occupationalAccident",
          "annualLeave",
          "partTimeAbsenceDueToRehabilitation",
          "other"
        ],
        "type": "string",
        "description": "Reason for paid absence"
      },
      "ReimbPaymentType": {
        "enum": [
          "kelaDailyAllowanceApplication",
          "kelaFamilyLeaveCompensation"
        ],
        "type": "string",
        "description": "Payment data type, compensation application"
      },
      "ReimbPayment": {
        "type": "object",
        "properties": {
          "paymentType": {
            "$ref": "#/components/schemas/ReimbPaymentType"
          },
          "paymentRef": {
            "type": "string",
            "description": "Payment's reference number",
            "nullable": true
          },
          "paymentSpecifier": {
            "type": "string",
            "description": "Payment specifier",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Payment data"
      },
      "PaidAbsencePeriod": {
        "type": "object",
        "properties": {
          "startDate": {
            "type": "string",
            "description": "Start date\r\n\r\nThe date must not be earlier than 1 January 1800.",
            "format": "date"
          },
          "endDate": {
            "type": "string",
            "description": "End date\r\n\r\nThe \"End date\" may not be earlier than the \"Start date”\r\n\r\nMandatory data if 1 or 2 was specified as the ‘Payment data type’ in the ‘Reimbursement application data’ data group.\r\n\r\nThe time period between the ‘Start date’ and ‘End date’ of the absence period must not overlap by a single day with the absence period of another paid absence on the same report (‘Start date’ – ‘End date’).\r\n\r\nThe time period between the ‘Start date’ and ‘End date’ of the absence period must not overlap by a single day with the absence period of an unpaid absence in the same report(‘Start date’ – ‘End date’).",
            "format": "date",
            "nullable": true
          },
          "absenceDays": {
            "type": "integer",
            "description": "Number of days of absence",
            "format": "int32",
            "nullable": true
          },
          "absenceUntil": {
            "type": "string",
            "description": "Absence continues, until when\r\n\r\n\"Absence continues, until when\" may not be earlier than \"Start date\".",
            "format": "date",
            "nullable": true
          },
          "causeCode": {
            "$ref": "#/components/schemas/PaidAbsenceCauseCode"
          },
          "amount": {
            "type": "number",
            "description": "Pay for period of paid absence\r\n\r\nThe value must be a non-negative number.",
            "format": "double"
          },
          "reimbApp": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/ReimbPayment"
            },
            "description": "Reimbursement application data",
            "nullable": true
          },
          "paidAbsencePeriodInfo": {
            "type": "string",
            "description": "Additional reimbursement application data\r\n\r\nYou can enter freeform text as additional information. This information may be entered only when the ‘Reimbursement application data’ data group has been specified in connection with the absence period.In other situations, the additional information must not be specified.",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Absence period"
      },
      "PaidAbsence": {
        "type": "object",
        "properties": {
          "paidAbsencePeriods": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/PaidAbsencePeriod"
            },
            "description": "Absence periods",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Paid absence"
      },
      "Absence": {
        "type": "object",
        "properties": {
          "absenceRepStartDate": {
            "type": "string",
            "description": "Start date of the time period for reporting absences\r\n\r\nThe date must not be earlier than 1 January 1800.",
            "format": "date",
            "nullable": true
          },
          "absenceRepEndDate": {
            "type": "string",
            "description": "End date of the time period for reporting absences\r\n\r\nThe \"End date\" may not be earlier than the \"Start date\".",
            "format": "date",
            "nullable": true
          },
          "unpaidAbsence": {
            "$ref": "#/components/schemas/UnpaidAbsence"
          },
          "paidAbsence": {
            "$ref": "#/components/schemas/PaidAbsence"
          }
        },
        "additionalProperties": false,
        "description": "Absences\r\n\r\nIf the details on the time period for reporting absences are specified in the data group, both the Start date of the time period for reporting absences and the End date of the time period for reporting absences data items must be specified."
      },
      "Report": {
        "type": "object",
        "properties": {
          "reportData": {
            "$ref": "#/components/schemas/ReportData"
          },
          "incomeEarner": {
            "$ref": "#/components/schemas/IncomeEarner"
          },
          "transactions": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Transaction"
            },
            "description": "Income types",
            "nullable": true
          },
          "foreignLeasedWork": {
            "$ref": "#/components/schemas/ForeignLeasedWork"
          },
          "stayPeriodsInFinland": {
            "$ref": "#/components/schemas/StayPeriodsInFinland"
          },
          "workPeriodsInFinland": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/WorkPeriodInFinland"
            },
            "description": "Work periods in Finland",
            "nullable": true
          },
          "workCountries": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/WorkCountry"
            },
            "description": "Countries of work",
            "nullable": true
          },
          "absence": {
            "$ref": "#/components/schemas/Absence"
          }
        },
        "additionalProperties": false,
        "description": "Report data"
      },
      "DeliveryData": {
        "type": "object",
        "properties": {
          "timestamp": {
            "type": "string",
            "description": "Time of record creation",
            "format": "date-time"
          },
          "source": {
            "type": "string",
            "description": "Data source",
            "nullable": true
          },
          "deliveryDataType": {
            "$ref": "#/components/schemas/DeliveryDataType"
          },
          "deliveryId": {
            "type": "string",
            "description": "Record owner's (payer's) record reference\r\n\r\nThe record owner's record reference must uniquely identify all records of a specific record owner (DeliveryDataOwner) containing earnings payment reports (DeliveryDataType=100).",
            "nullable": true
          },
          "faultyControl": {
            "$ref": "#/components/schemas/FaultyControl"
          },
          "productionEnvironment": {
            "type": "boolean",
            "description": "Production environment\r\n\r\nIf the value is \"true\", the data must have been delivered to the Incomes Register's production environment. If the value is \"false\", the data must have been delivered to the Incomes Register's test environment."
          },
          "deliveryDataOwner": {
            "$ref": "#/components/schemas/Id"
          },
          "deliveryDataCreator": {
            "$ref": "#/components/schemas/Id"
          },
          "deliveryDataSender": {
            "$ref": "#/components/schemas/Id"
          },
          "paymentPeriod": {
            "$ref": "#/components/schemas/PaymentPeriod"
          },
          "contactPersons": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/ContactPerson"
            },
            "description": "Contact persons for the record",
            "nullable": true
          },
          "payer": {
            "$ref": "#/components/schemas/Payer"
          },
          "reports": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Report"
            },
            "description": "Reports data",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Record data"
      },
      "ErrorInfo": {
        "type": "object",
        "properties": {
          "errorCode": {
            "type": "string",
            "description": "Error code",
            "nullable": true
          },
          "errorMessage": {
            "type": "string",
            "description": "Error code description",
            "nullable": true
          },
          "errorDetails": {
            "type": "string",
            "description": "Error details",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": "Error information"
      },
      "Item": {
        "type": "object",
        "properties": {
          "itemId": {
            "type": "string",
            "description": "Record submitter's reference\r\n\r\nThis element always appears in the data of an approved item.",
            "nullable": true
          },
          "irItemId": {
            "type": "string",
            "description": "Incomes Register reference \r\n\r\nThis element always appears in the data of an approved item.",
            "nullable": true
          },
          "itemVersion": {
            "type": "integer",
            "description": "Report version number \r\n\r\nThe Incomes Register assigns version number 1 to a new report. The version number of a substitute report is the number of the substituted version + 1.",
            "format": "int32"
          },
          "itemVersionSpecified": {
            "type": "boolean",
            "description": "Item Version specified"
          },
          "itemErrors": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/ErrorInfo"
            },
            "description": "Errors",
            "nullable": true
          }
        },
        "additionalProperties": false,
        "description": ""
      },
      "StatusResponse": {
        "type": "object",
        "properties": {
          "deliveryDataStatus": {
            "type": "integer",
            "description": "Record status",
            "format": "int32"
          },
          "irDeliveryId": {
            "type": "string",
            "description": "Incomes Register record reference\r\n\r\nIf the record has been saved in the Incomes Register, the reference assigned to the record by the Incomes Register is delivered in this element.The Incomes Register record reference uniquely identifies all records submitted to the Incomes Register.\r\nIf the record has not been saved in the Incomes Register, this data is blank.",
            "nullable": true
          },
          "validItems": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Item"
            },
            "description": "Approved items\r\n\r\nThe valid items submitted in the record are returned in this data group.",
            "nullable": true
          },
          "invalidItems": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Item"
            },
            "description": "Rejected items \r\n\r\nThe invalid items submitted in the record and rejected on the basis of itemspecific errors are returned in this data group. If all items in the record are rejected on the basis of record-level errors, the rejected items are not returned in this data group.",
            "nullable": true
          },
          "messageErrors": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/ErrorInfo"
            },
            "description": "Message-level errors\r\n\r\nThis data group is used to return technical and authorisation errors.\r\nIf message-level errors are detected in a record submitted to the Incomes Register, the record is not processed further in the Incomes Register. In addition to message-level errors, the processing feedback cannot therefore contain any other errors(record-level errors, and rejected items and the errors related to them).",
            "nullable": true
          },
          "deliveryErrors": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/ErrorInfo"
            },
            "description": "Record-level errors\r\n\r\nThis data group is used to return errors related to the contents of record-level data.\r\nRecord-level errors apply to all items in the record.If there are record-level errors in the record, all items of the record are invalid and will not be saved in the Incomes Register.In such a case, the invalid items are not separately delivered in the \"Rejected items\" data group.",
            "nullable": true
          },
          "irResponseId": {
            "type": "string",
            "description": "Incomes Register message reference",
            "nullable": true
          },
          "irResponseTimestamp": {
            "type": "string",
            "description": "Processing feedback timestamp",
            "format": "date-time"
          }
        },
        "additionalProperties": false,
        "description": "Processing feedback data"
      },
      "ReportLogEventType": {
        "enum": [
          "undefined",
          "sent",
          "cancelled"
        ],
        "type": "string"
      },
      "DeliveryStatus": {
        "type": "object",
        "properties": {
          "statusResponse": {
            "$ref": "#/components/schemas/StatusResponse"
          },
          "deliveryId": {
            "type": "string",
            "description": "Record owner's record reference",
            "nullable": true
          },
          "action": {
            "$ref": "#/components/schemas/ReportLogEventType"
          }
        },
        "additionalProperties": false,
        "description": "Contains delivery status data."
      },
      "IrDeliveryStatus": {
        "type": "object",
        "properties": {
          "statusResponse": {
            "$ref": "#/components/schemas/StatusResponse"
          },
          "deliveryId": {
            "type": "string",
            "description": "Record owner's record reference",
            "nullable": true
          },
          "action": {
            "$ref": "#/components/schemas/ReportLogEventType"
          }
        },
        "additionalProperties": false,
        "description": "Contains delivery status data."
      },
      "ApiValidationErrorType": {
        "enum": [
          "general",
          "required",
          "invalid",
          "warning"
        ],
        "type": "string"
      },
      "IApiValidationError": {
        "required": [
          "msg",
          "type"
        ],
        "type": "object",
        "properties": {
          "key": {
            "type": "string",
            "nullable": true
          },
          "type": {
            "$ref": "#/components/schemas/ApiValidationErrorType"
          },
          "msg": {
            "type": "string"
          },
          "code": {
            "type": "string",
            "nullable": true
          }
        },
        "additionalProperties": false
      },
      "IApiValidation": {
        "type": "object",
        "properties": {
          "isValid": {
            "type": "boolean",
            "readOnly": true
          },
          "hasAllRequiredFields": {
            "type": "boolean",
            "readOnly": true
          },
          "errors": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/IApiValidationError"
            },
            "nullable": true,
            "readOnly": true
          }
        },
        "additionalProperties": false
      },
      "EarningsPayment": {
        "type": "object",
        "properties": {
          "deliveryData": {
            "$ref": "#/components/schemas/DeliveryData"
          },
          "status": {
            "$ref": "#/components/schemas/DeliveryStatus"
          },
          "irStatus": {
            "$ref": "#/components/schemas/IrDeliveryStatus"
          },
          "info": {
            "type": "object",
            "additionalProperties": { },
            "nullable": true
          },
          "validation": {
            "$ref": "#/components/schemas/IApiValidation"
          },
          "id": {
            "type": "string",
            "description": "Identifier of the object.",
            "nullable": true
          },
          "createdAt": {
            "type": "string",
            "description": "The date when the object was created.",
            "format": "date-time",
            "nullable": true
          },
          "updatedAt": {
            "type": "string",
            "description": "The time when the object was last updated. \r\nTypically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates.",
            "format": "date-time",
            "nullable": true
          },
          "owner": {
            "type": "string",
            "description": "Owner ID for this data",
            "nullable": true
          },
          "isReadOnly": {
            "type": "boolean",
            "description": "Indication that for the currently logged-in account, the data is generally read-only."
          },
          "partner": {
            "type": "string",
            "description": "Primary partner information.",
            "nullable": true
          }
        },
        "additionalProperties": false
      },
      "ScheduleAction": {
        "enum": [
          "send",
          "cancel"
        ],
        "type": "string",
        "description": "Schedule action"
      },
      "ApiScheduleObject": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "description": "Id of the object to schedule",
            "nullable": true
          },
          "startAt": {
            "type": "string",
            "description": "The date when the object was created.",
            "format": "date-time",
            "nullable": true
          },
          "createdAt": {
            "type": "string",
            "description": "The date when the object was created.",
            "format": "date-time",
            "nullable": true
          },
          "updatedAt": {
            "type": "string",
            "description": "The time when the object was last updated. \r\nTypically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates.",
            "format": "date-time",
            "nullable": true
          },
          "owner": {
            "type": "string",
            "description": "Owner ID for this data",
            "nullable": true
          },
          "isReadOnly": {
            "type": "boolean",
            "description": "Indication that for the currently logged-in account, the data is generally read-only."
          },
          "partner": {
            "type": "string",
            "description": "Primary partner information.",
            "nullable": true
          },
          "action": {
            "$ref": "#/components/schemas/ScheduleAction"
          }
        },
        "additionalProperties": false
      },
      "ScheduleItemStatus": {
        "enum": [
          "new",
          "pending",
          "inProcess",
          "done"
        ],
        "type": "string"
      },
      "ScheduleObject": {
        "type": "object",
        "properties": {
          "payload": {
            "type": "string",
            "description": "Payload",
            "nullable": true
          },
          "statusCheckCount": {
            "type": "integer",
            "description": "Status check count when status has been still in process.",
            "format": "int32"
          },
          "lastChecked": {
            "type": "string",
            "description": "Last status checked timestamp.",
            "format": "date-time",
            "nullable": true
          },
          "scheduleItemStatus": {
            "$ref": "#/components/schemas/ScheduleItemStatus"
          },
          "irItemId": {
            "type": "string",
            "description": "Used on Cancel to identify the item.",
            "nullable": true
          },
          "id": {
            "type": "string",
            "description": "Id of the object to schedule",
            "nullable": true
          },
          "startAt": {
            "type": "string",
            "description": "The date when the object was created.",
            "format": "date-time",
            "nullable": true
          },
          "createdAt": {
            "type": "string",
            "description": "The date when the object was created.",
            "format": "date-time",
            "nullable": true
          },
          "updatedAt": {
            "type": "string",
            "description": "The time when the object was last updated. \r\nTypically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates.",
            "format": "date-time",
            "nullable": true
          },
          "owner": {
            "type": "string",
            "description": "Owner ID for this data",
            "nullable": true
          },
          "isReadOnly": {
            "type": "boolean",
            "description": "Indication that for the currently logged-in account, the data is generally read-only."
          },
          "partner": {
            "type": "string",
            "description": "Primary partner information.",
            "nullable": true
          },
          "action": {
            "$ref": "#/components/schemas/ScheduleAction"
          }
        },
        "additionalProperties": false,
        "description": "Schedule Object"
      },
      "AckData": {
        "type": "object",
        "properties": {
          "deliveryDataStatus": {
            "type": "integer",
            "description": "Record status\r\n\r\nThe status of the record after reception in the Incomes Register is delivered in this element.",
            "format": "int32"
          },
          "irDeliveryId": {
            "type": "string",
            "description": "Incomes Register record reference\r\n\r\nIf the record was successfully received by the Incomes Register, the reference assigned to the record by the Incomes Register is delivered in this element.The Incomes Register record reference uniquely identifies all records submitted to the Incomes Register.\r\nIf the reception of the record by the Incomes Register failed, this value is blank.",
            "nullable": true
          },
          "messageErrors": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/ErrorInfo"
            },
            "description": "Message-level errors\r\n\r\nThis data group is used to return technical and authorisation errors.",
            "nullable": true
          },
          "deliveryErrors": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/ErrorInfo"
            },
            "description": "Record-level errors\r\n\r\nThis data group is used to return errors related to the contents of record-level data.",
            "nullable": true
          },
          "irResponseId": {
            "type": "string",
            "description": "Incomes Register message reference",
            "nullable": true
          },
          "irResponseTimestamp": {
            "type": "string",
            "description": "Processing feedback timestamp",
            "format": "date-time"
          }
        },
        "additionalProperties": false,
        "description": " Details of an acknowledgement of receipt\r\n \r\nThe data group contains the data of the acknowledgement of receipt generated by the Incomes Register."
      },
      "PayerSummary": {
        "type": "object",
        "properties": {
          "status": {
            "$ref": "#/components/schemas/DeliveryStatus"
          },
          "irStatus": {
            "$ref": "#/components/schemas/IrDeliveryStatus"
          },
          "info": {
            "type": "object",
            "additionalProperties": { },
            "nullable": true
          },
          "validation": {
            "$ref": "#/components/schemas/IApiValidation"
          },
          "id": {
            "type": "string",
            "description": "Identifier of the object.",
            "nullable": true
          },
          "createdAt": {
            "type": "string",
            "description": "The date when the object was created.",
            "format": "date-time",
            "nullable": true
          },
          "updatedAt": {
            "type": "string",
            "description": "The time when the object was last updated. \r\nTypically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates.",
            "format": "date-time",
            "nullable": true
          },
          "owner": {
            "type": "string",
            "description": "Owner ID for this data",
            "nullable": true
          },
          "isReadOnly": {
            "type": "boolean",
            "description": "Indication that for the currently logged-in account, the data is generally read-only."
          },
          "partner": {
            "type": "string",
            "description": "Primary partner information.",
            "nullable": true
          }
        },
        "additionalProperties": false
      }
    }
  }
};