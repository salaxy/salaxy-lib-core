﻿/// <reference path="../../dist/npm/@salaxy/core/salaxy-core-all.d.ts" />

var ajax = new salaxy.api.AjaxJQuery();
var calcCrudApi = new salaxy.api.Calculations(ajax);

function getAndUpdateCalculation() {
    calcCrudApi.get(
        $('#sample5-id').val(),
        function(data) {
            console.info(data);
            var msg = "Orginal salary: " + data.salary.amount;
            data.salary.amount += 10;
            console.log(data);
            calcCrudApi.createOrUpdate(data, function(resp) {
                msg += " Current salary: " + resp.salary.amount;
                $('#sample5-result').text(msg);
            });
        });
}