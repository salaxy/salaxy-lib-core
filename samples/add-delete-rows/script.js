﻿/// <reference path="../../dist/npm/@salaxy/core/salaxy-core-all.d.ts" />

var sample7Calc = null;

function sample7AddRow() {
    console.info("Sample 7 - Add row");
    salaxyApi.calculator.addRow(sample7Calc,
        "expenses",
        $('#sample7-expenses-msg').val() || "Some expenses",
        $('#sample7-expenses-price').val() || 15,
        1,
        sample7recalculationDone);
}

function sample7DeleteRow() {
    console.info("Sample 7 - Delete row");
    salaxyApi.calculator.deleteRow(sample7Calc, $('#sample7-row-number').val() || 0, sample7recalculationDone);
}

function sample7recalculationDone(data) {
    sample7Calc = data;
    console.info(data);

    $("#sample7_table > tbody > tr").remove();

    var index;

    console.debug(data.rows);

    $('#sample7_table > tbody').append('<tr><td> Salary</td><td class="right">' + data.result.totals.totalGrossSalary.toFixed(2) + '</td></tr>');

    for (index = 0; index < data.rows.length; ++index) {
        $('#sample7_table > tbody').append('<tr><td>' + data.rows[index].message + '</td><td class="right">' + data.rows[index].price.toFixed(2) + '</td></tr>');
    }

    $('#sample7_table > tbody').append('<tr><td>Side costs</td><td class="right">' + data.result.employerCalc.allSideCosts.toFixed(2) + '</td></tr>');
    $('#sample7_table > tbody').append('<tr><td>Benefits</td><td class="right">' + data.result.workerCalc.benefits.toFixed(2) + '</td></tr>');
    $('#sample7_table > tbody').append('<tr class="total"><td>Total payment</td><td class="right">' + data.result.employerCalc.totalPayment.toFixed(2) + '</td></tr>');

    //$('#sample7-result-totalsalary').text(data.result.totals.totalGrossSalary.toFixed(2));
    //$('#sample7-result-sidecosts').text(data.result.employerCalc.allSideCosts.toFixed(2));
    //$('#sample7-result-totalpayment').text(data.result.employerCalc.totalPayment.toFixed(2));
    //$('#sample7-result-benefits').text(data.result.workerCalc.benefits.toFixed(2));
}
