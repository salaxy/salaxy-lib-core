/// <reference path="../../dist/npm/@salaxy/core/salaxy-core-all.d.ts" />
var ajax = new salaxy.api.AjaxJQuery();
var calcApi = new salaxy.api.Calculator(ajax);

function sample2Calculate() {
    calcApi.createNew(emptyCalcObjectReceived);
}

function emptyCalcObjectReceived(data) {
    data.salary.kind = "hourly";
    data.salary.amount = $('#sample2-amount').val();
    data.salary.price = $('#sample2-price').val() || 10;
    calcApi.recalculate(data, recalculationDone);
};

function recalculationDone(data) {
    // NOTE: as-of-writing you would need to implement rounding here for this to be correct.
    // However, we are planning to round to 2 decimals inside API in the near-time future.
    $('#sample2-result-amount').text(data.salary.amount.toFixed(2));
    $('#sample2-result-price').text(data.salary.price.toFixed(2));
    $('#sample2-result-totalsalary').text(data.result.totals.totalGrossSalary.toFixed(2));
    $('#sample2-result-sidecosts').text(data.result.employerCalc.allSideCosts.toFixed(2));
    $('#sample2-result-totalpayment').text(data.result.employerCalc.totalPayment.toFixed(2));
}