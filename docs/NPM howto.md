Release check-list
------------------

Publish NPM package:

- If this is the first time you publish on this workstation, add a adduser:
  - `npm adduser`, UID: salaxy, PWD: check in salaxy-vault, E-mail: contact@salaxy.com
- Update the version numbers
  - `/src/salaxy-lib-core/src/@salaxy/core/package.json`
  - Keep in-sync with NG1-library: If you update NG1, make the two the same version number.
- Make a build
  - `cd /src/salaxy-lib-core`
  - `grunt build`
  - `npm run lint`
- If you are making large changes, test the package at this point. See "Testing the package locally" below
- Publish JS
  - `cd /src/salaxy-lib-core/dist/npm/@salaxy/core`
  - `npm publish --access public`
  - `cd ../../../..`
- Commit + Push to GIT
  - Push must in be in WWW-branch
  - Preferably, both WWW and Master on same level. At least, merge WWW to Master.
  - Add Tag for version number, e.g. "2.10.20"

Testing release from NPM:
  
- `cd /src/foobar`
- `npm uninstall --save @salaxy/core`
- `npm install --save @salaxy/core@latest`

Testing the package locally (when big changes):

- `cd /src/salaxy-lib-core`
- `grunt`
- `cd /src/salaxy-lib-core/dist/npm/@salaxy/core`
- `npm link`
- `cd /src/foobar`
- `npm link @salaxy/core`
