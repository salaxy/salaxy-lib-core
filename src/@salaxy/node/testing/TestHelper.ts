import { Accounts, Calculations, Calculator, Files, OAuth2, OAuthGrantType, OAuthMessage, Payments, Reports, Taxcards, Test, Workers } from "@salaxy/core";

import { AjaxNode } from "../api";
import { TestHelperConfig } from "./TestHelperConfig";

/**
 * Helpers for creating and runing tests in Node (server) environment.
 * Unit tests, System tests and demonstrations written in test format.
 * Tested and designed for Mocha / Chai, but should apply to other frameworks as well.
 */
export class TestHelper {

  /**
   * Ajax instance that is used in this test session.
   */
  public ajax: AjaxNode;

  /** Configuration for the ajax connection */
  public config: TestHelperConfig;

  /**
   * Creates a new TestHelper with server configured, but not authenticated (call authenticate() to do that).
   * Anonymous mode is enough for many unit testing scenarios but not for e.g. CRUD operations.
   * We do not create an authenticated connection in constructor as a best practice.
   *
   * If suite callback context is specified, the timeout is set to config.timeout or 30000 as default.
   * The default number may be changed later without it being a breaking change, so set in config if you need it to be something specific.
   *
   * @param config Configuration for the test helper.
   * @param suiteCallBackContext Callback context is the **this** of 'describe(\"\", function ()' in Mocha.
   * See Mocha.ISuiteCallbackContext for details.
   * NOTE that you cannot use 'describe(\"\", () =>', because that will pass the wrong context (this).
   * @example
   * ```js
   * import { config } from "../config";
   *
   * describe("Basic tests demonstrate the use of tester methods.", function() {
   *             const sxyHelper = new TestHelper(config, this);
   * ```
   */
  constructor(config?: TestHelperConfig, suiteCallBackContext?: any) {
    this.config = config;
    if (suiteCallBackContext) {
      if (!suiteCallBackContext.timeout) {
        throw new Error("SuiteCallBackContext does not have a timeout() method. Are you using 'describe(\"\", () =>' instead of 'describe(\"\", function ()'?");
      }
      suiteCallBackContext.timeout((config || {}).timeout || 30000);
    } else {
      throw new Error("There is no parameter for suiteCallBackContext. Typically, this is because you are calling 'describe('', () =>' insted of 'describe('', function ()'.");
    }
    this.ajax = new AjaxNode();
    this.ajax.serverAddress = this.getServer();
  }

  /**
   * Initializes the authenticated connection to the test server according to config.
   * Before calling this method, the ajax connection is anonymous.
   * Note that in Mocha / Chai, you can attach the method directly to before(), see examples below.
   *
   * @example
   * ```js
   * // Mocha / Chai testing
   * describe("Calculations basic CRUD operations.", function() {
   *             const testHelper = new TestHelper(config, this);
   *             before("Authenticate", testHelper.authenticate);
   *```
   *
   * @example
   * ```js
   * const testHelper = new TestHelper(config);
   * testHelper.authenticate().then(() => {
   *             // Some other initialization code here.
   * });
   * ```
   */
  public authenticate = (): Promise<OAuthMessage> => {
    const oauth = new OAuth2(this.ajax);
    if (!this.config) {
      throw new Error("No config set before calling authanticate().");
    }
    if (!this.config.username || !this.config.password) {
      throw new Error("Authenticate called without setting a username or password.");
    }
    const request: OAuthMessage = {
      grant_type: OAuthGrantType.Password,
      username: this.config.username,
      password: this.config.password,
    };
    this.ajax.serverAddress = this.getServer();
    return oauth.token(request).then((oauthResponse) => {
      this.ajax.setCurrentToken(oauthResponse.access_token);
      return oauthResponse;
    });
  }

  /** Shortcut to get the Salaxy OpenApi wrappers with the initialized ajax. */
  public get api(): {
    /** Test API */
    test: Test,
    /** Accounts API */
    accounts: Accounts,
    /** Workers CRUD API */
    workers: Workers,
    /** Calculator */
    calculator: Calculator,
    /** Calculations CRUD API */
    calculations: Calculations,
    /** Taxcards API */
    taxcards: Taxcards,
    /** Reports API */
    reports: Reports,
    /** Payments API */
    payments: Payments,
    /** Files API */
    files: Files,
  } {
    if (!this.ajax) {
      throw new Error("Ajax has not been initialized!");
    }
    return {
      test: new Test(this.ajax),
      accounts: new Accounts(this.ajax),
      workers: new Workers(this.ajax),
      calculator: new Calculator(this.ajax),
      calculations: new Calculations(this.ajax),
      taxcards: new Taxcards(this.ajax),
      reports: new Reports(this.ajax),
      payments: new Payments(this.ajax),
      files: new Files(this.ajax),
    };
  }

  private getServer() {
    if (this.config && this.config.serverUrl) {
      return this.config.serverUrl;
    }
    switch ((this.config || {}).server) {
      case "localhost":
        return "http://localhost:82";
      case "prod":
        return "https://secure.salaxy.com";
      case "auto":
      case "test":
      default:
        return "https://test-api.salaxy.com";
    }
  }
}
