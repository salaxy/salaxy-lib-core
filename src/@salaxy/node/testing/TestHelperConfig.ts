
/**
 * Configuration for the test helper:
 * Which server to contact, the credentials etc.
 */
export interface TestHelperConfig {

  /**
   * Server to connect to.
   * Default / recommended is "auto", which currently points to test,
   * but may later be directed to a separte unit test environment.
   */
  server?: "auto" | "test" | "prod" | "localhost";

  /**
   * Possibility to set the server URL and port explicitly.
   * This setting overrides the server property, but is typically null: Used only in special situations.
   */
  serverUrl?: string;

  /** Username that is used to authenticate the connection. */
  username?: string;

  /** Password that is used to authenticate the connection. */
  password?: string;

  /**
   * Test timeout in milliseconds. Used for calling Mocha.ISuiteCallbackContext.timeout() if provided.
   * The default is currently 30000, but it may be changed without it being a breaking change, so configure yout own, if you need a specific value.
   */
  timeout?: number;

}
