import { Ajax, Configs } from "@salaxy/core";
import { AxiosRequestConfig, AxiosResponse} from "axios";
import axios from "axios";

/**
 * Provides wrapper methods for communicating with the Palkkaus.fi API
 * The request-promise access to the server methods: GET, POST and DELETE
 * with different return types and authentication / error events.
 * This is implementation is the default for server-side running of the Salaxy library code.
 */
export class AjaxNode implements Ajax {

  /**
   * By default credentials are not used in http-calls.
   * Enable credentials with this flag (force disabled when the token is set).
   */
  public useCredentials: boolean;

  /**
   * The server address - root of the server. This is settable field.
   * Will probably be changed to a configuration object in the final version.
   */
  public serverAddress = "https://test-api.salaxy.com";

  private token: string;

    /**
     * Creates a new instance of AjaxNode
     */
    constructor() {
      const config = Configs.current;
      if (config) {
        // apiServer
        if (config.apiServer) {
          this.serverAddress = config.apiServer;
        }

        // useCredentials
        if (config.useCredentials != null) {
          this.useCredentials = config.useCredentials;
        }
      }
  }

  /** Gets the Server address that is used as bases to the HTML queries. E.g. 'https://test-api.salaxy.com' */
  public getServerAddress(): string {
    return this.serverAddress;
  }

  /** Gets the API address with version information. E.g. 'https://test-api.salaxy.com/v02/api' */
  public getApiAddress(): string {
    return this.serverAddress + "/v02/api";
  }

  /**
   * Gets a JSON-message from server using the API
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   * and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   *
   * @returns A Promise with result. Standard Promise rejection to be used for error handling.
   */
  public getJSON(method: string): Promise<any> {
    const token = this.getCurrentToken();

    const options: AxiosRequestConfig = {
      headers: {},
      responseType: "json",
      method: "GET",
      url: this.getUrl(method),
      withCredentials : (token) ? false : this.useCredentials,
    };

    if (token) {
      const authHeaderKey = "Authorization";
      options.headers[authHeaderKey] = "Bearer " + token;
    }
    const stackTrace = this.getStackTrace();
    return axios.request(options)
      .then( (response: AxiosResponse<any>) => {
        return response.data;
      }).catch(this.axiosCatch(stackTrace));
  }

  /**
   * Gets a HTML-message from server using the API
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   * and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   *
   * @returns A Promise with result html. Standard Promise rejection to be used for error handling.
   */
  public getHTML(method: string): Promise<string> {
    const token = this.getCurrentToken();

    const options: AxiosRequestConfig = {
      headers: {},
      method: "GET",
      responseType: "text",
      url: this.getUrl(method),
      withCredentials : (token) ? false : this.useCredentials,
    };

    if (token) {
      const authHeaderKey = "Authorization";
      options.headers[authHeaderKey] = "Bearer " + token;
    }
    const stackTrace = this.getStackTrace();
    return axios.request(options)
      .then( (value: AxiosResponse<any>) => {
        return value.data;
      })
      .catch(this.axiosCatch(stackTrace));
  }

  /**
   * POSTS data to server and receives back a JSON-message.
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   * and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   * @param data - The data that is posted to the server.
   *
   * @returns A Promise with result. Standard Promise rejection to be used for error handling.
   */
  public postJSON(method: string, data: any): Promise<any> {
    const token = this.getCurrentToken();

    const options: AxiosRequestConfig = {
      data,
      headers: {},
      responseType: "json",
      method: "POST",
      url: this.getUrl(method),
      withCredentials : (token) ? false : this.useCredentials,
    };

    if (token) {
      const authHeaderKey = "Authorization";
      options.headers[authHeaderKey] = "Bearer " + token;
    }
    const stackTrace = this.getStackTrace();
    return axios.request(options)
      .then((value: AxiosResponse<any>) => {
        return value.data;
      })
      .catch(this.axiosCatch(stackTrace));
  }

  /**
   * POSTS data to server and receives back HTML.
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   * and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   * @param data - The data that is posted to the server.
   *
   * @returns A Promise with result. Standard Promise rejection to be used for error handling.
   */
  public postHTML(method: string, data: any): Promise<string> {
    const token = this.getCurrentToken();

    const options: AxiosRequestConfig = {
      data,
      headers: { "Content-Type": "text/plain" },
      method: "POST",
      responseType: "text",
      url: this.getUrl(method),
      withCredentials : (token) ? false : this.useCredentials,
    };
    if (token) {
      const authHeaderKey = "Authorization";
      options.headers[authHeaderKey] = "Bearer " + token;
    }
    const stackTrace = this.getStackTrace();
    return axios.request(options)
      .then( (value: AxiosResponse<any>) => {
        return value.data;
      })
      .catch(this.axiosCatch(stackTrace));
  }

  /**
   * Sends a DELETE-message to server using the API
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   * and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   *
   * @returns A Promise with result. Standard Promise rejection to be used for error handling.
   */
  public remove(method: string): Promise<any> {
    const token = this.getCurrentToken();

    const options: AxiosRequestConfig = {
      headers: {},
      responseType: "json",
      method: "DELETE",
      url: this.getUrl(method),
      withCredentials : (token) ? false : this.useCredentials,
    };

    if (token) {
      const authHeaderKey = "Authorization";
      options.headers[authHeaderKey] = "Bearer " + token;
    }
    const stackTrace = this.getStackTrace();
    return axios.request(options)
      .then( (value: AxiosResponse<any>) => {
        return value.data;
      })
      .catch(this.axiosCatch(stackTrace));
  }

  /**
   * Returns current JWT token.
   */
  public getCurrentToken(): string {
    return this.token;
  }

  /**
   * Sets token.
   * @param token - JWT token.
   */
  public setCurrentToken(token: string): void {
    this.token = token;
  }

  /**
   * Fixes stack trace in Axios
   * Fixes this issue: https://github.com/axios/axios/issues/2387
   * Taken from: https://github.com/rafaelalmeidatk/TIL/issues/4
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public axiosCatch = (stackTrace: any) => (error: any) => {
    // TODO: We should go through all the errors from the server so we could simplify this.
    let errorMessage: string = error.message;
    if (error.response) {
      errorMessage = `Server error (${error.response.status as string})`;
      if (error.response.data) {
        if (error.response.data.allErrors && error.response.data.allErrors.length) {
          errorMessage += ": " + (error.response.data.allErrors[0] as string);
        } else if (error.response.data.message) {
          errorMessage += ": " + (error.response.data.message as string);
        }
        if (error.response.data.stack) {
          errorMessage += "\nServer stack:\n" + (error.response.data.stack.replace(/\\r\\n/g, "\n") as string);
          if (errorMessage.indexOf("--- End of stack trace from previous location where exception was thrown ---")) {
            errorMessage = errorMessage.substr(0, errorMessage.indexOf("--- End of stack trace from previous location where exception was thrown ---"));
          }
        } else {
          const responseBody = JSON.stringify(error.response.data, null, 2);
          errorMessage += `\nResponse body:\n${responseBody}`;
        }
      }
    }
    error.message = errorMessage;
    error.stack = stackTrace;
    throw error;
  }

  /**
   * Gets a new stacktrace at the current location.
   * Fixes this issue: https://github.com/axios/axios/issues/2387
   * Taken from: https://github.com/rafaelalmeidatk/TIL/issues/4
   */
  public getStackTrace(): string {
    const { stack } = new Error();
    let split = stack.split("\n");
    // Remove the above "new Error" line from the stack trace
    if (split[1].includes("at getStackTrace")) {
      split = [split[0], ...split.splice(2)];
    }
    return split.join("\n");
  }

  /** If missing, append the API server address to the given url method string */
  private getUrl(method: string): string {
    if (!method || method.trim() === "") {
      return null;
    }
    if (method.toLowerCase().startsWith("http")) {
      return method;
    }
    if (method.toLowerCase().startsWith("/v")) {
      return this.getServerAddress() + method;
    }
    return this.getApiAddress() + method;
  }
}
