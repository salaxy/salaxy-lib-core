import {
  IrRow,
} from "@salaxy/core";

/** Row data for worker and employer calculation tables. */
export interface ICalculationTableRowDataInput {

  /** Ir row. */
  irRow?: IrRow;

  /** Full total for the row. For example the original calculated version. */
  fullTotal?: number;

  /** Returns true if there is no enough money to pay the calculated amount. */
  isTotalReduced?: boolean;

  /** Row further information. */
  comment?: string;

}
