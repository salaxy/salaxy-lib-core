import { ReportType } from "@salaxy/core";

import {CalculationReportHeader} from "./CalculationReportHeader";
import {CalculationReportSimpleRow} from "./CalculationReportSimpleRow";
import {CalculationReportText} from "./CalculationReportText";
import {ICalculationReportRowGroup} from "./ICalculationReportRowGroup";

/** Contains calculation data structured for easy rendering in reports. */
export interface CalculationReport {
  /** Calculation report type. */
  reportType?: ReportType | null;
  /** Report header section. */
  header?: CalculationReportHeader | null;
  /** Cumulative data for the report. */
  cumulative?: CalculationReportSimpleRow[] | null;
  /** Report row groups. */
  rowGroups?: ICalculationReportRowGroup[] | null;
  /** Validation messages for the report. Contains for example deduction warnings. */
  validationMessages?: CalculationReportText[] | null;
  /** Returns true if the report has validation messages. */
  readonly hasValidationMessages?: boolean | null;
  /** Additional notes for the report. */
  notes?: CalculationReportText[] | null;
  /** Returns true if the report has notes. */
  readonly hasNotes?: boolean | null;
}
