import { DateRange } from "@salaxy/core";

import {CalculationReportEmployer} from "./CalculationReportEmployer";
import {CalculationReportWorker} from "./CalculationReportWorker";

/** Report header information for the report. Contains employer and worker details as well as further info about the work. */
export interface CalculationReportHeader {
  /** Employer name and contact details. */
  employer?: CalculationReportEmployer | null;
  /** Worker name and contact details. */
  worker?: CalculationReportWorker | null;
  /** Start date of the work. */
  workStartDate?: string | null;
  /** End date of the work. */
  workEndDate?: string | null;
  /** Employment period: Start and potential end date of the employment relation. */
  employmentPeriod?: DateRange | null;
  /** Description of the work for reporting purposes. */
  workDescription?: string | null;
  /** The day when the total amount was paid by Employer to Palkkaus.fi */
  paidAt?: string | null;
  /** The estimated date for the salary in worker. */
  salaryDate?: string | null;
  /** The actual date for the salary in worker. */
  requestedSalaryDate?: string | null;
  /** Number of days may affect calculation of different payments calculated based on a Framework agreement. */
  numberOfDays?: number | null;
  /** If true, the worker has not given any tax card, 60% will be taken. */
  workerHasNoTaxcard?: boolean | null;
  /** If true, the employer is household and no withholding tax is deducted (less than 1500€/year/Worker). */
  noWithholdingHousehold?: boolean | null;
  /** Calculated tax percent. Please note this is a number between 0-1. */
  taxPercent?: number | null;
  /** Tax percent in the tax card. */
  taxcardTaxPercent?: number | null;
  /** Additional tax percent in the tax card. */
  taxcardTaxPercent2?: number | null;
  /** Income limit for the taxcard */
  taxcardIncomeLimit?: number | null;
  /** The cumulative income on the taxcard - including this calculation.
   * Note that with shared taxcards, this may include income from other employers.
   */
  taxcardCumulativeIncome?: number | null;
  /** Salary slip message */
  salarySlipMessage?: string | null;
}
