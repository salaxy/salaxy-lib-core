/** Generic text object for the report. Contains heading, text and optional interpolation parameters. */
export interface CalculationReportText {
  /** Heading for the text. */
  heading?: string | null;
  /** The actual content for the text. */
  text?: string | null;
  /** Parameters to be interpolated in the text. */
  parameters?: { [key: string]: any; } | null;
}
