import {
  ReportType,
} from "@salaxy/core";

import {CalculationReport} from "./CalculationReport";
import {CalculationReportHeader} from "./CalculationReportHeader";
import {CalculationReportText} from "./CalculationReportText";
import {ICalculationReportRowGroup} from "./ICalculationReportRowGroup";

/** CalculationReport implementation. */
export class CalculationReportImpl implements CalculationReport {

  /** Report header section. */
  public header: CalculationReportHeader = {};

  /** Report row groups. */
  public rowGroups: ICalculationReportRowGroup[] = new Array<ICalculationReportRowGroup>();

  /** Validation messages for the report. Contains for example deduction warnings. */
  public validationMessages: CalculationReportText[] = new Array<CalculationReportText>();

  /** Returns true if the report has validation messages. */
  public get hasValidationMessages(): boolean { return this.validationMessages.length > 0; }

  /** Additional notes for the report. */
  public notes: CalculationReportText[] = new Array<CalculationReportText>();

  /** Returns true if the report has notes. */
  public get hasNotes(): boolean { return this.notes.length > 0; }

  /**
   * Creates a new report.
   * @param reportType Report type for the report.
   */
  constructor(public reportType: ReportType) { }
}
