/** Interface for report row. */
export interface ICalculationReportRow {

  /** Level of the row in the row hierarchy. */
  level?: number | null;

  /** Sum of sub row totals. */
  subTotal?: number | null;
}
