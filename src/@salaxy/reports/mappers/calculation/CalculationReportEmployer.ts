import {Avatar, Contact} from "@salaxy/core";

/** Employer name and contact details for reports. */
export interface CalculationReportEmployer {
  /** Finnish Business Identifier (Y-tunnus) for the company. */
  businessId?: string | null;
  /** Employer avatar. */
  avatar?: Avatar | null;
  /** Employer contact information. */
  contact?: Contact | null;
}
