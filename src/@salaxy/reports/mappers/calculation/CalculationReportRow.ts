import { ICalculationReportRow } from "./ICalculationReportRow";
import { ICalculationReportRowData } from "./ICalculationReportRowData";

/** Extension to IR row for reporting purposes. */
export class CalculationReportRow<T extends ICalculationReportRowData> implements ICalculationReportRow {

  /** Contains sub rows. */
  public subRows: Array<CalculationReportRow<T>>;

  private _mainRow: CalculationReportRow<T> = null;

  /**
   * Creates a new report row based on given row data. *
   *
   * @param data The data for the row.
   */
  constructor(public data: T) {
  }

  /** Level of the row in the row hierarchy. */

  public get level(): number {
    if (!this.mainRow) {
      return 0;
    }
    return this.mainRow.level + 1;
  }

  /** Returns true if level is 0. */
  public get isLevel0(): boolean { return this.level === 0; }

  /** Returns true if level is 1. */
  public get isLevel1(): boolean { return this.level === 1; }

  /** Returns true if level is 2. */
  public get isLevel2(): boolean { return this.level === 2; }

  /** Returns true if level is 3. */
  public get isLevel3(): boolean { return this.level === 3; }

  /** Returns true if level is 4. */
  public get isLevel4(): boolean { return this.level === 4; }

  /** Boolean to indicate if this row has sub rows */
  public get hasSubRows(): boolean { return this.subRows != null && this.subRows.length > 0; }

  /** Sum of sub row totals. */
  public get subTotal(): number {
    if (!this.subRows) {
      return this.data.total;
    }
    return this.subRows.reduce((a, b) => a + b.subTotal, 0);
  }

  /** Parent row for this row. */
  public get mainRow(): CalculationReportRow<T> { return this._mainRow; }

  /** Parent row for this row. */
  public set mainRow(value: CalculationReportRow<T>) {

    this._mainRow = value;
    if (this._mainRow == null) {
      return;
    }
    if (this._mainRow.subRows == null) {
      this._mainRow.subRows = new Array<CalculationReportRow<T>>();
    }
    this._mainRow.subRows.push(this);
  }
}
