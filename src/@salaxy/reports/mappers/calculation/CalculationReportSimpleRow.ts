/** A simple row for misc. tables in calculation. */
export interface CalculationReportSimpleRow {
  /**
   * Technical name / key for the cumulative field.
   */
  key?: string | null;
  /**
   * Label for the row.
   */
  label?: string | null;
  /**
   * Total for the row. This may later be extended with count and/or price.
   */
  total?: number | null;
  /**
   * Additional data for the row.
   */
  data?: { [key: string]: any; } | null;
}
