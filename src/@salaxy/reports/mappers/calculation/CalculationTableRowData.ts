import {
  IrRow,
  Unit,
} from "@salaxy/core";

import { ICalculationReportRowData } from "./ICalculationReportRowData";
import { ICalculationTableRowDataInput } from "./ICalculationTableRowDataInput";

/** Row data for worker and employer calculation tables. */
export class CalculationTableRowData implements ICalculationReportRowData {

  /** Ir row. */
  public irRow: IrRow = {};

  /** Full total for the row. For example the original calculated version. */
  public fullTotal?: number;

  /** Returns true if there is no enough money to pay the calculated amount. */
  public isTotalReduced?: boolean;
  private _comment: string = null;

  /** Constructor for creating row. */
  constructor(data: ICalculationTableRowDataInput) {
    if (!data) {
      return;
    }
    this.fullTotal = data.fullTotal;
    this.comment = data.comment;
    this.irRow = data.irRow || {};
    this.isTotalReduced = data.isTotalReduced;
  }

  /** Total amount. */
  public get total(): number { return this.irRow.total || 0; }

  /** Total amount. */
  public set total(value: number) { this.irRow.total = value; }

  /** Returns true if the unit is percentage */
  public get isUnitPercent(): boolean { return this.irRow.unit === Unit.Percent; }

  /** Row further information. */
  public get comment(): string {
    if (this._comment) {
      return this._comment;
    }

    if (this.irRow.data != null && this.irRow.data.sxySalarySlipRowComment) {
      return this.irRow.data.sxySalarySlipRowComment;
    }
    return null;
  }

  /** Row further information. */
  public set comment(value: string) { this._comment = value; }

}
