/** Interface for report row group. */
export interface ICalculationReportRowGroup {
  /** Group total. */
  total?: number | null;
}
