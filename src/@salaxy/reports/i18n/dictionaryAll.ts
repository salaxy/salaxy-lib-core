import { CoreDictionaryLang } from "@salaxy/core";
import * as en from "./en/reports.json";
import * as fi from "./fi/reports.json";
import * as sv from "./sv/reports.json";
import * as sx from "./sx/reports.json";

export const dictionaryAll = {
  en: en as any,
  fi: fi as any,
  sv: sv as any,
  sx: sx as any,
}