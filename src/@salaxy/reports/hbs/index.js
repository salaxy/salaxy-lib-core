var glob = ('undefined' === typeof window) ? global : window,

Handlebars = glob.Handlebars || require('handlebars');

this["salaxy"] = this["salaxy"] || {};
this["salaxy"]["reports"] = this["salaxy"]["reports"] || {};
this["salaxy"]["reports"]["templates"] = this["salaxy"]["reports"]["templates"] || {};

this["salaxy"]["reports"]["templates"]["accountingReport"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"page"),depth0,{"name":"page","fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"accountingReportHead"),depth0,{"name":"accountingReportHead","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"accountingTable"),depth0,{"name":"accountingTable","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"document"),depth0,{"name":"document","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n";
},"usePartial":true,"useData":true});

this["salaxy"]["reports"]["templates"]["employerReportV2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"page"),depth0,{"name":"page","fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "<div style=\"page-break-before: always\"></div>\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"page"),depth0,{"name":"page","fn":container.program(4, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"employerReportHeadV2"),depth0,{"name":"employerReportHeadV2","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"employerCalculationTablesV2"),depth0,{"name":"employerCalculationTablesV2","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n<h3>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.moreInfoOnNextPage",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":8,"column":4},"end":{"line":8,"column":61}}}))
    + "</h3>\r\n<p>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.text.moreInfoOnNextPage",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":9,"column":3},"end":{"line":9,"column":57}}}))
    + "</p>\r\n\r\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"employerReportHeadV2"),depth0,{"name":"employerReportHeadV2","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n<h2 class=\"salaxy-content-subheader\">"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.heading.salaryAdditionalInfo",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":17,"column":37},"end":{"line":17,"column":96}}}))
    + "</h2>\r\n<p>&nbsp;</p>\r\n\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"employerCalculationAdditionalInfoV2"),depth0,{"name":"employerCalculationAdditionalInfoV2","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"document"),depth0,{"name":"document","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["salaxy"]["reports"]["templates"]["employerReportV2Page1"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"page"),depth0,{"name":"page","fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"employerReportHeadV2"),depth0,{"name":"employerReportHeadV2","data":data,"indent":"      ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + ((stack1 = container.invokePartial(lookupProperty(partials,"employerCalculationTablesV2"),depth0,{"name":"employerCalculationTablesV2","data":data,"indent":"      ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"document"),depth0,{"name":"document","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["salaxy"]["reports"]["templates"]["accountingReportHead"] = Handlebars.template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, alias4=container.lambda, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<p>&nbsp;</p> \r\n<table>\r\n    <tr>\r\n        <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"Nimi",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":4,"column":12},"end":{"line":4,"column":24}}}))
    + ":</td>\r\n        <td><strong>"
    + alias3(alias4(((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"employer") : stack1)) != null ? lookupProperty(stack1,"firstName") : stack1), depth0))
    + " "
    + alias3(alias4(((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"employer") : stack1)) != null ? lookupProperty(stack1,"lastName") : stack1), depth0))
    + "</strong></td>\r\n    </tr>\r\n    <tr>\r\n        <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"Y-tunnus",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":8,"column":12},"end":{"line":8,"column":28}}}))
    + ":</td>\r\n        <td>"
    + alias3(alias4(((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"businessId") : stack1), depth0))
    + "</td>\r\n    </tr>\r\n    <tr>\r\n        <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"Sähköposti",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":12,"column":12},"end":{"line":12,"column":30}}}))
    + ":</td>\r\n        <td>"
    + alias3(alias4(((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"contact") : stack1)) != null ? lookupProperty(stack1,"email") : stack1), depth0))
    + "</td>\r\n    </tr>\r\n</table>\r\n<p>&nbsp;</p>";
},"useData":true});

this["salaxy"]["reports"]["templates"]["accountingTable"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "          <tr>\r\n              <td>"
    + alias4(((helper = (helper = lookupProperty(helpers,"accountNumber") || (depth0 != null ? lookupProperty(depth0,"accountNumber") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"accountNumber","hash":{},"data":data,"loc":{"start":{"line":12,"column":18},"end":{"line":12,"column":36}}}) : helper)))
    + "</td>\r\n              <td>"
    + alias4(((helper = (helper = lookupProperty(helpers,"accountName") || (depth0 != null ? lookupProperty(depth0,"accountName") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"accountName","hash":{},"data":data,"loc":{"start":{"line":13,"column":18},"end":{"line":13,"column":33}}}) : helper)))
    + "</td>\r\n              <td class=\"right\">"
    + alias4((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"saldo") : depth0),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":14,"column":32},"end":{"line":14,"column":44}}}))
    + "</td>\r\n          </tr>\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <table class=\"table table-striped table-condensed\">\r\n      <thead style=\"display: table-header-group;\">\r\n          <tr>\r\n              <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"Ehdotustili",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":4,"column":18},"end":{"line":4,"column":38}}}))
    + "</th>\r\n              <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"Selite",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":5,"column":18},"end":{"line":5,"column":33}}}))
    + "</th>\r\n              <th class=\"right\">"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"Summa",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":6,"column":32},"end":{"line":6,"column":46}}}))
    + "</th>\r\n          </tr>\r\n      </thead>\r\n      <tbody>\r\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"ledgerAccounts") : stack1),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":10,"column":10},"end":{"line":16,"column":19}}})) != null ? stack1 : "")
    + "\r\n          <tr>\r\n              <td colspan=\"3\">&nbsp;</td>\r\n          </tr>\r\n          <tr>\r\n              <td></td>\r\n              <td><b>Lisätiedot</b></td>\r\n              <td class=\"right\"></td>\r\n          </tr>\r\n          <tr>\r\n              <td></td>\r\n              <td>Ennakonpidätykset</td>\r\n              <td class=\"right\">"
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"summary") : stack1)) != null ? lookupProperty(stack1,"workerCalc") : stack1)) != null ? lookupProperty(stack1,"tax") : stack1),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":29,"column":32},"end":{"line":29,"column":68}}}))
    + "</td>\r\n          </tr>\r\n          <tr>\r\n              <td></td>\r\n              <td>Maksettu työntekijöille</td>\r\n              <td class=\"right\">"
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"summary") : stack1)) != null ? lookupProperty(stack1,"workerCalc") : stack1)) != null ? lookupProperty(stack1,"totalWorkerPayment") : stack1),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":34,"column":32},"end":{"line":34,"column":83}}}))
    + "</td>\r\n          </tr>\r\n      </tbody>\r\n  </table>\r\n";
},"useData":true});

this["salaxy"]["reports"]["templates"]["contractPartiesContactDetailsV2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <div>\r\n        <div class=\"uppercase\">"
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"worker") : stack1)) != null ? lookupProperty(stack1,"avatar") : stack1)) != null ? lookupProperty(stack1,"sortableName") : stack1), depth0))
    + "</div>\r\n        "
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"worker") : stack1)) != null ? lookupProperty(stack1,"contact") : stack1)) != null ? lookupProperty(stack1,"street") : stack1), depth0))
    + "<br />\r\n        "
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"worker") : stack1)) != null ? lookupProperty(stack1,"contact") : stack1)) != null ? lookupProperty(stack1,"postalCode") : stack1), depth0))
    + " "
    + alias2(alias1(((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"worker") : stack1)) != null ? lookupProperty(stack1,"contact") : stack1)) != null ? lookupProperty(stack1,"city") : stack1), depth0))
    + "\r\n    </div>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <p>"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.text.noWorkerDetails",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":8,"column":7},"end":{"line":8,"column":58}}}))
    + "</p>\r\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.escapeExpression, alias2=container.lambda, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <div class=\"margin-top\">\r\n        <b>"
    + alias1((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.heading.employer",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":13,"column":11},"end":{"line":13,"column":58}}}))
    + ":</b>\r\n        <div class=\"uppercase\">"
    + alias1(alias2(((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"employer") : stack1)) != null ? lookupProperty(stack1,"avatar") : stack1)) != null ? lookupProperty(stack1,"sortableName") : stack1), depth0))
    + "</div>\r\n        "
    + alias1(alias2(((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"employer") : stack1)) != null ? lookupProperty(stack1,"contact") : stack1)) != null ? lookupProperty(stack1,"street") : stack1), depth0))
    + "<br />\r\n        "
    + alias1(alias2(((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"employer") : stack1)) != null ? lookupProperty(stack1,"contact") : stack1)) != null ? lookupProperty(stack1,"postalCode") : stack1), depth0))
    + " "
    + alias1(alias2(((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"employer") : stack1)) != null ? lookupProperty(stack1,"contact") : stack1)) != null ? lookupProperty(stack1,"city") : stack1), depth0))
    + "\r\n    </div>\r\n";
},"7":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <p>"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.text.noEmployerDetails",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":19,"column":7},"end":{"line":19,"column":60}}}))
    + "</p>\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"worker") : stack1)) != null ? lookupProperty(stack1,"avatar") : stack1)) != null ? lookupProperty(stack1,"firstName") : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":9,"column":7}}})) != null ? stack1 : "")
    + "<br/>\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"employer") : stack1)) != null ? lookupProperty(stack1,"avatar") : stack1)) != null ? lookupProperty(stack1,"firstName") : stack1),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data,"loc":{"start":{"line":11,"column":0},"end":{"line":20,"column":7}}})) != null ? stack1 : "");
},"useData":true});

this["salaxy"]["reports"]["templates"]["document"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<link type=\"text/css\" rel=\"stylesheet\" href=\""
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"baseUrl") : stack1), depth0))
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"branding") : stack1)) != null ? lookupProperty(stack1,"css") : stack1), depth0))
    + "\" />\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<base href=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"baseUrl") : stack1), depth0))
    + "\" />\r\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "-pdf";
},"7":function(container,depth0,helpers,partials,data) {
    return "-preview";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"branding") : stack1)) != null ? lookupProperty(stack1,"hasCss") : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":3,"column":7}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"isModePdf") : stack1),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":4,"column":0},"end":{"line":6,"column":7}}})) != null ? stack1 : "")
    + "<div class=\"salaxy-rpt salaxy-component\">\r\n    <div class=\"document"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"isModePdf") : stack1),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data,"loc":{"start":{"line":8,"column":24},"end":{"line":8,"column":79}}})) != null ? stack1 : "")
    + "\">\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"@partial-block"),depth0,{"name":"@partial-block","data":data,"indent":"        ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "    </div>\r\n</div>\r\n";
},"usePartial":true,"useData":true});

this["salaxy"]["reports"]["templates"]["employerCalculationAdditionalInfoV2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"notes") : stack1),{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":4,"column":0},"end":{"line":7,"column":9}}})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h3>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"heading") : depth0),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":5,"column":4},"end":{"line":5,"column":18}}}))
    + "</h3>\r\n<p>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"text") : depth0),(depth0 != null ? lookupProperty(depth0,"parameters") : depth0),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":6,"column":3},"end":{"line":6,"column":25}}}))
    + "</p>\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"workerDeductionWarningsV2"),depth0,{"name":"workerDeductionWarningsV2","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"hasNotes") : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":3,"column":0},"end":{"line":8,"column":7}}})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["salaxy"]["reports"]["templates"]["employerCalculationTablesV2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"hasRows") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":2,"column":0},"end":{"line":67,"column":7}}})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "\r\n  <table class=\"salaxy-accounting-report-table report-table columns-four\">\r\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"isHidden") : depth0),{"name":"unless","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":5,"column":4},"end":{"line":55,"column":17}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"isTotals") : depth0),{"name":"if","hash":{},"fn":container.program(18, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":56,"column":6},"end":{"line":65,"column":13}}})) != null ? stack1 : "")
    + "  </table>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <thead style=\"display: table-header-group;\">\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"isTotals") : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data,"loc":{"start":{"line":7,"column":10},"end":{"line":28,"column":17}}})) != null ? stack1 : "")
    + "      </thead>\r\n      <tbody>\r\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"rows") : depth0),{"name":"each","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":31,"column":12},"end":{"line":45,"column":21}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"isTotals") : depth0),{"name":"unless","hash":{},"fn":container.program(16, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":46,"column":12},"end":{"line":53,"column":23}}})) != null ? stack1 : "")
    + "      </tbody>\r\n";
},"4":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "          <tr>\r\n              <th><h3>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"groupingTerm") : depth0),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":9,"column":22},"end":{"line":9,"column":40}}}))
    + "</h3></th>\r\n              <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.percentOf",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":10,"column":18},"end":{"line":10,"column":66}}}))
    + "</th>\r\n              <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.fromSum",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":11,"column":18},"end":{"line":11,"column":64}}}))
    + "</th>\r\n              <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.euros",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":12,"column":18},"end":{"line":12,"column":62}}}))
    + "</th>\r\n          </tr>\r\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"isUndefined") : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(7, data, 0),"data":data,"loc":{"start":{"line":14,"column":10},"end":{"line":28,"column":10}}})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "          <tr>\r\n              <th><h3>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"groupingTerm") : depth0),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":23,"column":22},"end":{"line":23,"column":40}}}))
    + "</h3></th>\r\n              <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.amount",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":24,"column":18},"end":{"line":24,"column":63}}}))
    + "</th>\r\n              <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.aPrice",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":25,"column":18},"end":{"line":25,"column":63}}}))
    + "</th>\r\n              <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.euros",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":26,"column":18},"end":{"line":26,"column":62}}}))
    + "</th>\r\n          </tr>\r\n          ";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <tr>\r\n                <td>\r\n                    "
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"irRow") : stack1)) != null ? lookupProperty(stack1,"message") : stack1),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":34,"column":20},"end":{"line":34,"column":45}}}))
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"comment") : stack1),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":34,"column":45},"end":{"line":35,"column":90}}})) != null ? stack1 : "")
    + "\r\n                </td>\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"isUnitPercent") : stack1),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.program(14, data, 0),"data":data,"loc":{"start":{"line":37,"column":16},"end":{"line":41,"column":23}}})) != null ? stack1 : "")
    + "                <td>"
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"irRow") : stack1)) != null ? lookupProperty(stack1,"price") : stack1),{"name":"c","hash":{"nullChar":" "},"data":data,"loc":{"start":{"line":42,"column":20},"end":{"line":42,"column":55}}}))
    + "</td>\r\n                <td>"
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"subTotal") : depth0),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":43,"column":20},"end":{"line":43,"column":34}}}))
    + "</td>\r\n            </tr>\r\n";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "\r\n                    <span style=\"font-size: 0.8em;\">&nbsp;"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"comment") : stack1),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":35,"column":58},"end":{"line":35,"column":76}}}))
    + "</span>";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                <td>"
    + container.escapeExpression((lookupProperty(helpers,"p")||(depth0 && lookupProperty(depth0,"p"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"irRow") : stack1)) != null ? lookupProperty(stack1,"count") : stack1),{"name":"p","hash":{"nullChar":" "},"data":data,"loc":{"start":{"line":38,"column":20},"end":{"line":38,"column":55}}}))
    + "</td>\r\n";
},"14":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                <td>"
    + container.escapeExpression(container.lambda(((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"irRow") : stack1)) != null ? lookupProperty(stack1,"count") : stack1), depth0))
    + "</td>\r\n";
},"16":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <tr class=\"total\">\r\n                <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"totalTerm") : depth0),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":48,"column":20},"end":{"line":48,"column":36}}}))
    + "</td>\r\n                <td></td>\r\n                <td></td>\r\n                <td>"
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"total") : depth0),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":51,"column":20},"end":{"line":51,"column":32}}}))
    + "</td>\r\n            </tr>\r\n";
},"18":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"hasTotalValue") : depth0),{"name":"if","hash":{},"fn":container.program(19, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":57,"column":8},"end":{"line":64,"column":15}}})) != null ? stack1 : "");
},"19":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <tfoot>\r\n          <tr class=\"total\">\r\n              <th colspan=\"3\">"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.palkkausFee",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":60,"column":30},"end":{"line":60,"column":80}}}))
    + "</th>\r\n              <th>"
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"total") : depth0),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":61,"column":18},"end":{"line":61,"column":30}}}))
    + "</th>\r\n          </tr>\r\n      </tfoot>\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"rowGroups") : stack1),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":68,"column":9}}})) != null ? stack1 : "");
},"useData":true});

this["salaxy"]["reports"]["templates"]["employerContractDetailsV2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return container.escapeExpression((lookupProperty(helpers,"d")||(depth0 && lookupProperty(depth0,"d"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"paidAt") : stack1),{"name":"d","hash":{},"data":data,"loc":{"start":{"line":16,"column":41},"end":{"line":16,"column":68}}}));
},"3":function(container,depth0,helpers,partials,data) {
    return "-";
},"5":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.heading.requestedSalaryDate",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":19,"column":54},"end":{"line":19,"column":112}}}));
},"7":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.heading.salaryDate",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":19,"column":120},"end":{"line":19,"column":169}}}));
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return container.escapeExpression((lookupProperty(helpers,"d")||(depth0 && lookupProperty(depth0,"d"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"salaryDate") : stack1),{"name":"d","hash":{},"data":data,"loc":{"start":{"line":20,"column":45},"end":{"line":20,"column":76}}}));
},"11":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <span>"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.text.noTaxcard",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":36,"column":18},"end":{"line":36,"column":63}}}))
    + "</span>\r\n";
},"13":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <span>"
    + container.escapeExpression((lookupProperty(helpers,"p")||(depth0 && lookupProperty(depth0,"p"))||container.hooks.helperMissing).call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"taxPercent") : stack1),{"name":"p","hash":{},"data":data,"loc":{"start":{"line":38,"column":18},"end":{"line":38,"column":49}}}))
    + "</span>\r\n            "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"taxcardTaxPercent") : stack1),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":39,"column":12},"end":{"line":39,"column":172}}})) != null ? stack1 : "")
    + "\r\n            "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"taxcardTaxPercent2") : stack1),{"name":"if","hash":{},"fn":container.program(16, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":40,"column":12},"end":{"line":40,"column":175}}})) != null ? stack1 : "")
    + "\r\n";
},"14":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<br /><span>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.taxcardTaxPercent",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":39,"column":64},"end":{"line":39,"column":119}}}))
    + " "
    + alias3((lookupProperty(helpers,"p")||(depth0 && lookupProperty(depth0,"p"))||alias2).call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"taxcardTaxPercent") : stack1),{"name":"p","hash":{},"data":data,"loc":{"start":{"line":39,"column":120},"end":{"line":39,"column":158}}}))
    + "</span>";
},"16":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<br /><span>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.taxcardTaxPercent2",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":40,"column":65},"end":{"line":40,"column":121}}}))
    + " "
    + alias3((lookupProperty(helpers,"p")||(depth0 && lookupProperty(depth0,"p"))||alias2).call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"taxcardTaxPercent2") : stack1),{"name":"p","hash":{},"data":data,"loc":{"start":{"line":40,"column":122},"end":{"line":40,"column":161}}}))
    + "</span>";
},"18":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <tr>\r\n        <td>"
    + alias1((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.heading.workDescription",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":46,"column":12},"end":{"line":46,"column":66}}}))
    + ":</td>\r\n        <td>"
    + alias1(container.lambda(((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"workDescription") : stack1), depth0))
    + "</td>\r\n    </tr>\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, alias4=container.lambda, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<table class=\"report-table-compact\">\r\n    <tr>\r\n        <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.worker",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":3,"column":12},"end":{"line":3,"column":57}}}))
    + ":</td>\r\n        <td>"
    + alias3(alias4(((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"worker") : stack1)) != null ? lookupProperty(stack1,"avatar") : stack1)) != null ? lookupProperty(stack1,"sortableName") : stack1), depth0))
    + "</td>\r\n    </tr>\r\n    <tr>\r\n        <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.socialSecurityNumber",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":7,"column":12},"end":{"line":7,"column":71}}}))
    + ":</td>\r\n        <td>"
    + alias3(alias4(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"worker") : stack1)) != null ? lookupProperty(stack1,"socialSecurityNumberValid") : stack1), depth0))
    + "</td>\r\n    </tr>\r\n    <tr>\r\n        <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.bankAccountNumber",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":11,"column":12},"end":{"line":11,"column":68}}}))
    + ":</td>\r\n        <td>"
    + alias3(alias4(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"worker") : stack1)) != null ? lookupProperty(stack1,"ibanNumber") : stack1), depth0))
    + "</td>\r\n    </tr>\r\n    <tr>\r\n        <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.paidAt",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":15,"column":12},"end":{"line":15,"column":57}}}))
    + ":</td>\r\n        <td>"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"paidAt") : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":16,"column":12},"end":{"line":16,"column":84}}})) != null ? stack1 : "")
    + "</td>\r\n    </tr>\r\n    <tr>\r\n        <td>"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"requestedSalaryDate") : stack1),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data,"loc":{"start":{"line":19,"column":12},"end":{"line":19,"column":176}}})) != null ? stack1 : "")
    + ":</td>\r\n        <td>"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"salaryDate") : stack1),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":20,"column":12},"end":{"line":20,"column":92}}})) != null ? stack1 : "")
    + "</td>\r\n    </tr>\r\n    <tr>\r\n        <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.workPeriod",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":23,"column":12},"end":{"line":23,"column":61}}}))
    + ":</td>\r\n        <td>"
    + alias3((lookupProperty(helpers,"d")||(depth0 && lookupProperty(depth0,"d"))||alias2).call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"workStartDate") : stack1),{"name":"d","hash":{"end":((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"workEndDate") : stack1)},"data":data,"loc":{"start":{"line":24,"column":12},"end":{"line":24,"column":76}}}))
    + "</td>\r\n    </tr>\r\n    <tr>\r\n        <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.numberOfWorkDays",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":27,"column":12},"end":{"line":27,"column":67}}}))
    + ":</td>\r\n        <td>"
    + alias3(alias4(((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"numberOfDays") : stack1), depth0))
    + "</td>\r\n    </tr>\r\n    <tr>\r\n        <td>\r\n            "
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.withholdingTax",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":32,"column":12},"end":{"line":32,"column":65}}}))
    + ":\r\n        </td>\r\n        <td>\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"workerHasNoTaxcard") : stack1),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.program(13, data, 0),"data":data,"loc":{"start":{"line":35,"column":12},"end":{"line":41,"column":20}}})) != null ? stack1 : "")
    + "        </td>\r\n    </tr>\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"workDescription") : stack1),{"name":"if","hash":{},"fn":container.program(18, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":44,"column":4},"end":{"line":49,"column":11}}})) != null ? stack1 : "")
    + "</table>";
},"useData":true});

this["salaxy"]["reports"]["templates"]["employerReportHeadV2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.escapeExpression, alias2=container.lambda, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <div>\r\n                <b>"
    + alias1((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.heading.employer",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":9,"column":19},"end":{"line":9,"column":66}}}))
    + "</b>\r\n                <div class=\"uppercase\">"
    + alias1(alias2(((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"employer") : stack1)) != null ? lookupProperty(stack1,"avatar") : stack1)) != null ? lookupProperty(stack1,"sortableName") : stack1), depth0))
    + "</div>\r\n                "
    + alias1(alias2(((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"employer") : stack1)) != null ? lookupProperty(stack1,"contact") : stack1)) != null ? lookupProperty(stack1,"street") : stack1), depth0))
    + "<br />\r\n                "
    + alias1(alias2(((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"employer") : stack1)) != null ? lookupProperty(stack1,"contact") : stack1)) != null ? lookupProperty(stack1,"postalCode") : stack1), depth0))
    + " "
    + alias1(alias2(((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"employer") : stack1)) != null ? lookupProperty(stack1,"contact") : stack1)) != null ? lookupProperty(stack1,"city") : stack1), depth0))
    + "\r\n            </div>\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <p>"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.text.noEmployerDetails",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":15,"column":15},"end":{"line":15,"column":68}}}))
    + "</p>\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<table>\r\n    <colgroup>\r\n        <col width=\"40%\" /><col width=\"60%\" />\r\n    </colgroup>\r\n    <tr>\r\n        <td>\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"employer") : stack1)) != null ? lookupProperty(stack1,"avatar") : stack1)) != null ? lookupProperty(stack1,"firstName") : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":7,"column":12},"end":{"line":16,"column":19}}})) != null ? stack1 : "")
    + "        </td>\r\n        <td>\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"employerContractDetailsV2"),depth0,{"name":"employerContractDetailsV2","data":data,"indent":"            ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "        </td>\r\n    </tr>\r\n</table>";
},"usePartial":true,"useData":true});

this["salaxy"]["reports"]["templates"]["page"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "style=\"height:auto;min-height:"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"isLayoutA4Portrait") : stack1),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data,"loc":{"start":{"line":1,"column":109},"end":{"line":1,"column":171}}})) != null ? stack1 : "")
    + "\"";
},"2":function(container,depth0,helpers,partials,data) {
    return "277mm";
},"4":function(container,depth0,helpers,partials,data) {
    return "190mm";
},"6":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <div class=\"watermark\">"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.document.preview",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":3,"column":27},"end":{"line":3,"column":66}}}))
    + "</div>\r\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <header class=\"header\" style=\"border-bottom: "
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"branding") : stack1)) != null ? lookupProperty(stack1,"borderStyle") : stack1), depth0))
    + "\">\r\n        <table>\r\n            <tr>\r\n                <td width=\"40%\">\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"branding") : stack1)) != null ? lookupProperty(stack1,"cdnLogoUrl") : stack1),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.program(11, data, 0),"data":data,"loc":{"start":{"line":10,"column":20},"end":{"line":14,"column":27}}})) != null ? stack1 : "")
    + "                </td>\r\n                <td width=\"60%\">\r\n                    <h1 class=\"report-header-txt uppercase bold-txt\">"
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"title") : stack1), depth0))
    + "</h1>\r\n                    <p class=\"report-header-txt uppercase bold-txt\">"
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"subTitle") : stack1), depth0))
    + "</p>\r\n                </td>\r\n            </tr>\r\n        </table>\r\n    </header>\r\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                    <img class=\"logo\" src=\""
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"branding") : stack1)) != null ? lookupProperty(stack1,"cdnLogoUrl") : stack1), depth0))
    + "\" style=\""
    + alias2(alias1(((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"branding") : stack1)) != null ? lookupProperty(stack1,"logoStyle") : stack1), depth0))
    + "\" alt=\"Logo\" />\r\n";
},"11":function(container,depth0,helpers,partials,data) {
    return "                    <div class=\"logo reportbrandlogo\"></div>\r\n";
},"13":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <footer class=\"footer\" style=\"border-top: "
    + alias1(container.lambda(((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"branding") : stack1)) != null ? lookupProperty(stack1,"borderStyle") : stack1), depth0))
    + "\">"
    + alias1((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.document.footerInfo.html",{"name":"t","hash":{"date":((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"date") : stack1)},"data":data,"loc":{"start":{"line":28,"column":83},"end":{"line":28,"column":152}}}))
    + "</footer>\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"document "
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"layout") : stack1), depth0))
    + "\" "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"isModeHtmlPage") : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":46},"end":{"line":1,"column":179}}})) != null ? stack1 : "")
    + ">\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"isPreview") : stack1),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":2,"column":4},"end":{"line":4,"column":11}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"isModeHtmlPage") : stack1),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":5,"column":4},"end":{"line":23,"column":11}}})) != null ? stack1 : "")
    + "    <div class=\"document-body\">\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"@partial-block"),depth0,{"name":"@partial-block","data":data,"indent":"        ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "    </div>\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"isModeHtmlPage") : stack1),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":27,"column":4},"end":{"line":29,"column":11}}})) != null ? stack1 : "")
    + "</div>\r\n        ";
},"usePartial":true,"useData":true});

this["salaxy"]["reports"]["templates"]["partial"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<link href=\""
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"baseUrl") : stack1), depth0))
    + "/Content/css/salaxy-lib-ng1/salaxy-rpt.css?v=20190506\" type=\"text/css\" rel=\"stylesheet\" />\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"reportTask") : depth0)) != null ? lookupProperty(stack1,"isPartial") : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":3,"column":7}}})) != null ? stack1 : "");
},"useData":true});

this["salaxy"]["reports"]["templates"]["paymentReportHeadV2"] = Handlebars.template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<table>\r\n    <colgroup>\r\n        <col width=\"40%\" /><col width=\"60%\" />\r\n    </colgroup>\r\n    <tr>\r\n        <td>\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"contractPartiesContactDetailsV2"),depth0,{"name":"contractPartiesContactDetailsV2","data":data,"indent":"            ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "        </td>\r\n        <td>\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"employerContractDetailsV2"),depth0,{"name":"employerContractDetailsV2","data":data,"indent":"            ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "        </td>\r\n    </tr>\r\n</table>\r\n";
},"usePartial":true,"useData":true});

this["salaxy"]["reports"]["templates"]["paymentTablesV2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"hasRows") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":2,"column":0},"end":{"line":55,"column":7}}})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "\r\n  <table class=\"salaxy-accounting-report-table report-table columns-six\">\r\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"isHidden") : depth0),{"name":"unless","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":5,"column":4},"end":{"line":42,"column":17}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"isTotals") : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":43,"column":6},"end":{"line":52,"column":13}}})) != null ? stack1 : "")
    + "  </table>\r\n\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <thead style=\"display: table-header-group;\">\r\n          <tr>\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"params") : depth0)) != null ? lookupProperty(stack1,"hideColumnHeaders") : stack1),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data,"loc":{"start":{"line":8,"column":14},"end":{"line":18,"column":21}}})) != null ? stack1 : "")
    + "          </tr>\r\n      </thead>\r\n      <tbody>\r\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"rows") : depth0),{"name":"each","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":22,"column":10},"end":{"line":34,"column":19}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"isTotals") : depth0),{"name":"unless","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":35,"column":10},"end":{"line":40,"column":21}}})) != null ? stack1 : "")
    + "      </tbody>\r\n";
},"4":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "              <th colspan=\"5\"><h3>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"groupingTerm") : depth0),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":9,"column":34},"end":{"line":9,"column":52}}}))
    + "</h3></th>\r\n              <th style=\"text-align:right\">"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.euros",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":10,"column":43},"end":{"line":10,"column":87}}}))
    + "</th>\r\n";
},"6":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "              <th><h3>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"groupingTerm") : depth0),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":12,"column":22},"end":{"line":12,"column":40}}}))
    + "</h3></th>\r\n              <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.receiver",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":13,"column":18},"end":{"line":13,"column":65}}}))
    + "</th>\r\n              <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.fromSum",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":14,"column":18},"end":{"line":14,"column":64}}}))
    + "</th>\r\n              <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.fromWorker",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":15,"column":18},"end":{"line":15,"column":67}}}))
    + "</th>\r\n              <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.fromEmployer",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":16,"column":18},"end":{"line":16,"column":69}}}))
    + "</th>\r\n              <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.euros",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":17,"column":18},"end":{"line":17,"column":62}}}))
    + "</th>\r\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "          <tr>\r\n              <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"label") : stack1),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":24,"column":18},"end":{"line":24,"column":35}}}))
    + "</td>\r\n              <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"receiver") : stack1),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":25,"column":18},"end":{"line":25,"column":38}}}))
    + "</td>\r\n              <td>"
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"baseAmount") : stack1),{"name":"c","hash":{"nullChar":" "},"data":data,"loc":{"start":{"line":26,"column":18},"end":{"line":26,"column":52}}}))
    + "</td>\r\n              <td>"
    + alias3((lookupProperty(helpers,"p")||(depth0 && lookupProperty(depth0,"p"))||alias2).call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"workerPercent") : stack1),{"name":"p","hash":{"nullChar":" "},"data":data,"loc":{"start":{"line":27,"column":18},"end":{"line":27,"column":55}}}))
    + "</td>\r\n              <td>"
    + alias3((lookupProperty(helpers,"p")||(depth0 && lookupProperty(depth0,"p"))||alias2).call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"employerPercent") : stack1),{"name":"p","hash":{"nullChar":" "},"data":data,"loc":{"start":{"line":28,"column":18},"end":{"line":28,"column":57}}}))
    + "</td>\r\n              <td>"
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"subTotal") : depth0),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":29,"column":18},"end":{"line":29,"column":33}}}))
    + "</td>\r\n          </tr>\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"comment") : stack1),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":31,"column":10},"end":{"line":33,"column":17}}})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "          <tr><td colspan=\"6\" style=\"padding-left: 10px;font-size: 0.8em\">"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"comment") : stack1),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":32,"column":74},"end":{"line":32,"column":92}}}))
    + "</td></tr>\r\n";
},"11":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "          <tr class=\"total\">\r\n              <td colspan=\"5\">"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"totalTerm") : depth0),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":37,"column":30},"end":{"line":37,"column":46}}}))
    + "</td>\r\n              <td style=\"text-align:right\">"
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"total") : depth0),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":38,"column":43},"end":{"line":38,"column":55}}}))
    + "</td>\r\n          </tr>\r\n";
},"13":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"hasTotalValue") : depth0),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":44,"column":8},"end":{"line":51,"column":15}}})) != null ? stack1 : "");
},"14":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <tfoot>\r\n          <tr class=\"total\">\r\n              <th colspan=\"3\">"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.palkkausFee",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":47,"column":30},"end":{"line":47,"column":80}}}))
    + "</th>\r\n              <th colspan=\"3\" style=\"text-align:right\">"
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"total") : depth0),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":48,"column":55},"end":{"line":48,"column":67}}}))
    + "</th>\r\n          </tr>\r\n      </tfoot>\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"rowGroups") : stack1),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":56,"column":9}}})) != null ? stack1 : "");
},"useData":true});

this["salaxy"]["reports"]["templates"]["workerCalculationAdditionalInfoV2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"notes") : stack1),{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":4,"column":0},"end":{"line":7,"column":9}}})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h3>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"heading") : depth0),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":5,"column":4},"end":{"line":5,"column":18}}}))
    + "</h3>\r\n<p>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"text") : depth0),(depth0 != null ? lookupProperty(depth0,"parameters") : depth0),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":6,"column":3},"end":{"line":6,"column":25}}}))
    + "</p>\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"workerDeductionWarningsV2"),depth0,{"name":"workerDeductionWarningsV2","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"hasNotes") : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":3,"column":0},"end":{"line":8,"column":7}}})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["salaxy"]["reports"]["templates"]["workerCalculationTablesV2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"hasRows") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":4,"column":0},"end":{"line":83,"column":7}}})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "\r\n<table class=\"salaxy-accounting-report-table report-table columns-four\">\r\n    <thead style=\"display: table-header-group;\">\r\n     \r\n        <tr>\r\n            <th><h3>"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(alias1,(depth0 != null ? lookupProperty(depth0,"groupingTerm") : depth0),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":10,"column":20},"end":{"line":10,"column":38}}}))
    + "</h3></th>\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"isDeductions") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0, blockParams, depths),"inverse":container.program(5, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":11,"column":12},"end":{"line":23,"column":19}}})) != null ? stack1 : "")
    + "        </tr>\r\n    </thead>\r\n    <tbody>\r\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"rows") : depth0),{"name":"each","hash":{},"fn":container.program(10, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":27,"column":8},"end":{"line":61,"column":17}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"isTotals") : depth0),{"name":"unless","hash":{},"fn":container.program(28, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":62,"column":8},"end":{"line":69,"column":19}}})) != null ? stack1 : "")
    + "    </tbody>\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"isTotals") : depth0),{"name":"if","hash":{},"fn":container.program(30, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":71,"column":4},"end":{"line":80,"column":11}}})) != null ? stack1 : "")
    + "</table>\r\n\r\n";
},"3":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.percentOf",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":12,"column":16},"end":{"line":12,"column":64}}}))
    + "</th>\r\n            <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.fromSum",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":13,"column":16},"end":{"line":13,"column":62}}}))
    + "</th>\r\n            <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.euros",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":14,"column":16},"end":{"line":14,"column":60}}}))
    + "</th>\r\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"isTotals") : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.program(8, data, 0),"data":data,"loc":{"start":{"line":15,"column":12},"end":{"line":23,"column":12}}})) != null ? stack1 : "");
},"6":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <th></th>\r\n            <th></th>\r\n            <th>"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.heading.euros",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":18,"column":16},"end":{"line":18,"column":60}}}))
    + "</th>\r\n";
},"8":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.amount",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":20,"column":16},"end":{"line":20,"column":61}}}))
    + "</th>\r\n            <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.aPrice",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":21,"column":16},"end":{"line":21,"column":61}}}))
    + "</th>\r\n            <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.euros",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":22,"column":16},"end":{"line":22,"column":60}}}))
    + "</th>\r\n            ";
},"10":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <tr>\r\n            <td>"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"irRow") : stack1)) != null ? lookupProperty(stack1,"message") : stack1),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":29,"column":16},"end":{"line":29,"column":41}}}))
    + "</td>\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depths[1] != null ? lookupProperty(depths[1],"isDeductions") : depths[1]),{"name":"if","hash":{},"fn":container.program(11, data, 0, blockParams, depths),"inverse":container.program(16, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":30,"column":12},"end":{"line":48,"column":19}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"isTotalReduced") : stack1),{"name":"if","hash":{},"fn":container.program(22, data, 0, blockParams, depths),"inverse":container.program(24, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":49,"column":12},"end":{"line":56,"column":19}}})) != null ? stack1 : "")
    + "        </tr>\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"comment") : stack1),{"name":"if","hash":{},"fn":container.program(26, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":58,"column":8},"end":{"line":60,"column":15}}})) != null ? stack1 : "");
},"11":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"isUnitPercent") : stack1),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.program(14, data, 0),"data":data,"loc":{"start":{"line":31,"column":14},"end":{"line":37,"column":21}}})) != null ? stack1 : "");
},"12":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "              <td>"
    + alias3((lookupProperty(helpers,"p")||(depth0 && lookupProperty(depth0,"p"))||alias2).call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"irRow") : stack1)) != null ? lookupProperty(stack1,"count") : stack1),{"name":"p","hash":{},"data":data,"loc":{"start":{"line":32,"column":18},"end":{"line":32,"column":41}}}))
    + "</td>\r\n              <td>"
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"irRow") : stack1)) != null ? lookupProperty(stack1,"price") : stack1),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":33,"column":18},"end":{"line":33,"column":41}}}))
    + "</td>\r\n";
},"14":function(container,depth0,helpers,partials,data) {
    return "              <td></td>\r\n              <td></td>\r\n";
},"16":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depths[1] != null ? lookupProperty(depths[1],"isTotals") : depths[1]),{"name":"if","hash":{},"fn":container.program(14, data, 0, blockParams, depths),"inverse":container.program(17, data, 0, blockParams, depths),"data":data,"loc":{"start":{"line":38,"column":12},"end":{"line":48,"column":12}}})) != null ? stack1 : "");
},"17":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"isUnitPercent") : stack1),{"name":"if","hash":{},"fn":container.program(18, data, 0),"inverse":container.program(20, data, 0),"data":data,"loc":{"start":{"line":42,"column":14},"end":{"line":46,"column":23}}})) != null ? stack1 : "")
    + "              <td>"
    + container.escapeExpression((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||container.hooks.helperMissing).call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"irRow") : stack1)) != null ? lookupProperty(stack1,"price") : stack1),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":47,"column":18},"end":{"line":47,"column":40}}}))
    + "</td>\r\n            ";
},"18":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                <td>"
    + container.escapeExpression((lookupProperty(helpers,"p")||(depth0 && lookupProperty(depth0,"p"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"irRow") : stack1)) != null ? lookupProperty(stack1,"count") : stack1),{"name":"p","hash":{"nullChar":" "},"data":data,"loc":{"start":{"line":43,"column":20},"end":{"line":43,"column":55}}}))
    + "</td>\r\n";
},"20":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                <td>"
    + container.escapeExpression(container.lambda(((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"irRow") : stack1)) != null ? lookupProperty(stack1,"count") : stack1), depth0))
    + "</td>\r\n";
},"22":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <td>\r\n                <span style=\"text-decoration: line-through\">"
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"fullTotal") : stack1),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":51,"column":60},"end":{"line":51,"column":81}}}))
    + "</span><br />\r\n                "
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"irRow") : stack1)) != null ? lookupProperty(stack1,"total") : stack1),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":52,"column":16},"end":{"line":52,"column":39}}}))
    + "\r\n            </td>\r\n";
},"24":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <td>"
    + container.escapeExpression((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"irRow") : stack1)) != null ? lookupProperty(stack1,"total") : stack1),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":55,"column":16},"end":{"line":55,"column":39}}}))
    + "</td>\r\n";
},"26":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <tr colspan=\"4\"><td style=\"padding-left: 10px;font-size: 0.8em\">"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"comment") : stack1),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":59,"column":72},"end":{"line":59,"column":90}}}))
    + "</td></tr>\r\n";
},"28":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <tr class=\"total\">\r\n            <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"totalTerm") : depth0),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":64,"column":16},"end":{"line":64,"column":32}}}))
    + "</td>\r\n            <td></td>\r\n            <td></td>\r\n            <td>"
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"total") : depth0),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":67,"column":16},"end":{"line":67,"column":28}}}))
    + "</td>\r\n        </tr>\r\n";
},"30":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <tfoot>\r\n        <tr class=\"total\">\r\n            <th>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.toBePaid",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":74,"column":16},"end":{"line":74,"column":63}}}))
    + "</th>\r\n            <th></th>\r\n            <th></th>\r\n            <th>"
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"total") : depth0),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":77,"column":16},"end":{"line":77,"column":28}}}))
    + "</th>\r\n        </tr>\r\n    </tfoot>\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "\r\n<div class=\"additional-message pre-line\">"
    + container.escapeExpression(container.lambda(((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"salarySlipMessage") : stack1), depth0))
    + "</div>\r\n"
    + ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"rowGroups") : stack1),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":3,"column":0},"end":{"line":84,"column":9}}})) != null ? stack1 : "");
},"useData":true,"useDepths":true});

this["salaxy"]["reports"]["templates"]["workerContractDetailsV2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <tr>\r\n        <td>"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"employmentPeriod") : stack1)) != null ? lookupProperty(stack1,"end") : stack1),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data,"loc":{"start":{"line":8,"column":12},"end":{"line":8,"column":196}}})) != null ? stack1 : "")
    + ":</td>\r\n        <td>"
    + container.escapeExpression((lookupProperty(helpers,"d")||(depth0 && lookupProperty(depth0,"d"))||container.hooks.helperMissing).call(alias1,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"employmentPeriod") : stack1)) != null ? lookupProperty(stack1,"start") : stack1),{"name":"d","hash":{},"data":data,"loc":{"start":{"line":9,"column":12},"end":{"line":9,"column":55}}}))
    + " "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"employmentPeriod") : stack1)) != null ? lookupProperty(stack1,"end") : stack1),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":9,"column":56},"end":{"line":9,"column":150}}})) != null ? stack1 : "")
    + "</td>\r\n    </tr>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.heading.employmentPeriodStartAndEnd",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":8,"column":55},"end":{"line":8,"column":121}}}));
},"4":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.heading.employmentPeriodStart",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":8,"column":129},"end":{"line":8,"column":189}}}));
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return " - "
    + container.escapeExpression((lookupProperty(helpers,"d")||(depth0 && lookupProperty(depth0,"d"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"employmentPeriod") : stack1)) != null ? lookupProperty(stack1,"end") : stack1),{"name":"d","hash":{},"data":data,"loc":{"start":{"line":9,"column":102},"end":{"line":9,"column":143}}}));
},"8":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.heading.requestedSalaryDate",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":21,"column":54},"end":{"line":21,"column":112}}}));
},"10":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.heading.salaryDate",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":21,"column":120},"end":{"line":21,"column":169}}}));
},"12":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return container.escapeExpression((lookupProperty(helpers,"d")||(depth0 && lookupProperty(depth0,"d"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"salaryDate") : stack1),{"name":"d","hash":{},"data":data,"loc":{"start":{"line":22,"column":45},"end":{"line":22,"column":76}}}));
},"14":function(container,depth0,helpers,partials,data) {
    return "-";
},"16":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <tr>\r\n        <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.withholdingTax",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":34,"column":12},"end":{"line":34,"column":65}}}))
    + ":</td>\r\n        <td><span>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.text.noTaxcard",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":35,"column":18},"end":{"line":35,"column":63}}}))
    + "</span></td>\r\n    </tr>\r\n";
},"18":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"noWithholdingHousehold") : stack1),{"name":"if","hash":{},"fn":container.program(19, data, 0),"inverse":container.program(21, data, 0),"data":data,"loc":{"start":{"line":38,"column":8},"end":{"line":60,"column":15}}})) != null ? stack1 : "");
},"19":function(container,depth0,helpers,partials,data) {
    var alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <tr>\r\n            <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.withholdingTax",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":40,"column":16},"end":{"line":40,"column":69}}}))
    + ":</td>\r\n            <td><span>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.text.noWithholdingHousehold",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":41,"column":22},"end":{"line":41,"column":80}}}))
    + "</span></td>\r\n        </tr>\r\n";
},"21":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <tr>\r\n            <td>"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(alias1,"SALAXY.REPORTS.salary.text.percents",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":45,"column":16},"end":{"line":45,"column":60}}}))
    + ":</td>\r\n            <td>\r\n                "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"taxcardTaxPercent") : stack1),{"name":"if","hash":{},"fn":container.program(22, data, 0),"inverse":container.program(24, data, 0),"data":data,"loc":{"start":{"line":47,"column":16},"end":{"line":47,"column":136}}})) != null ? stack1 : "")
    + "\r\n                "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"taxcardTaxPercent2") : stack1),{"name":"if","hash":{},"fn":container.program(26, data, 0),"inverse":container.program(28, data, 0),"data":data,"loc":{"start":{"line":48,"column":16},"end":{"line":48,"column":144}}})) != null ? stack1 : "")
    + "\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td>\r\n                "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"taxcardCumulativeIncome") : stack1),{"name":"if","hash":{},"fn":container.program(30, data, 0),"inverse":container.program(32, data, 0),"data":data,"loc":{"start":{"line":53,"column":16},"end":{"line":53,"column":225}}})) != null ? stack1 : "")
    + "\r\n            </td>\r\n            <td>\r\n                "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"taxcardIncomeLimit") : stack1),{"name":"if","hash":{},"fn":container.program(34, data, 0),"inverse":container.program(24, data, 0),"data":data,"loc":{"start":{"line":56,"column":16},"end":{"line":56,"column":138}}})) != null ? stack1 : "")
    + "\r\n                "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"taxcardCumulativeIncome") : stack1),{"name":"if","hash":{},"fn":container.program(36, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":57,"column":16},"end":{"line":57,"column":129}}})) != null ? stack1 : "")
    + "\r\n            </td>\r\n        </tr>\r\n";
},"22":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<span>"
    + container.escapeExpression((lookupProperty(helpers,"p")||(depth0 && lookupProperty(depth0,"p"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"taxcardTaxPercent") : stack1),{"name":"p","hash":{},"data":data,"loc":{"start":{"line":47,"column":62},"end":{"line":47,"column":100}}}))
    + "</span>";
},"24":function(container,depth0,helpers,partials,data) {
    return "<span>-</span>";
},"26":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<span> ("
    + container.escapeExpression((lookupProperty(helpers,"p")||(depth0 && lookupProperty(depth0,"p"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"taxcardTaxPercent2") : stack1),{"name":"p","hash":{},"data":data,"loc":{"start":{"line":48,"column":65},"end":{"line":48,"column":104}}}))
    + ")</span>";
},"28":function(container,depth0,helpers,partials,data) {
    return "<span> (-)</span>";
},"30":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<span>"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.text.taxcardCumulativeIncome",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":53,"column":68},"end":{"line":53,"column":127}}}))
    + ":</span>";
},"32":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<span>"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.text.taxcardNoCumulativeIncome",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":53,"column":149},"end":{"line":53,"column":210}}}))
    + ":</span>";
},"34":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<span>"
    + container.escapeExpression((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"taxcardIncomeLimit") : stack1),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":56,"column":63},"end":{"line":56,"column":102}}}))
    + "</span>";
},"36":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<span> ("
    + container.escapeExpression((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"taxcardCumulativeIncome") : stack1),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":57,"column":70},"end":{"line":57,"column":114}}}))
    + ")</span>";
},"38":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <tr>\r\n        <td>"
    + alias1((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.heading.workDescription",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":64,"column":12},"end":{"line":64,"column":66}}}))
    + ":</td>\r\n        <td>"
    + alias1(container.lambda(((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"workDescription") : stack1), depth0))
    + "</td>\r\n    </tr>\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, alias4=container.lambda, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<table class=\"report-table-compact\">\r\n    <tr>\r\n        <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.worker",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":3,"column":12},"end":{"line":3,"column":57}}}))
    + ":</td>\r\n        <td>"
    + alias3(alias4(((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"worker") : stack1)) != null ? lookupProperty(stack1,"avatar") : stack1)) != null ? lookupProperty(stack1,"sortableName") : stack1), depth0))
    + "</td>\r\n    </tr>\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"employmentPeriod") : stack1)) != null ? lookupProperty(stack1,"start") : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":6,"column":4},"end":{"line":11,"column":11}}})) != null ? stack1 : "")
    + "    <tr>\r\n        <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.socialSecurityNumber",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":13,"column":12},"end":{"line":13,"column":71}}}))
    + ":</td>\r\n        <td>"
    + alias3(alias4(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"worker") : stack1)) != null ? lookupProperty(stack1,"socialSecurityNumberValid") : stack1), depth0))
    + "</td>\r\n    </tr>\r\n    <tr>\r\n        <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.bankAccountNumber",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":17,"column":12},"end":{"line":17,"column":68}}}))
    + ":</td>\r\n        <td>"
    + alias3(alias4(((stack1 = ((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"worker") : stack1)) != null ? lookupProperty(stack1,"ibanNumber") : stack1), depth0))
    + "</td>\r\n    </tr>\r\n    <tr>\r\n        <td>"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"requestedSalaryDate") : stack1),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.program(10, data, 0),"data":data,"loc":{"start":{"line":21,"column":12},"end":{"line":21,"column":176}}})) != null ? stack1 : "")
    + ":</td>\r\n        <td>"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"salaryDate") : stack1),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.program(14, data, 0),"data":data,"loc":{"start":{"line":22,"column":12},"end":{"line":22,"column":92}}})) != null ? stack1 : "")
    + "</td>\r\n    </tr>\r\n    <tr>\r\n        <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.workPeriod",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":25,"column":12},"end":{"line":25,"column":61}}}))
    + ":</td>\r\n        <td>"
    + alias3((lookupProperty(helpers,"d")||(depth0 && lookupProperty(depth0,"d"))||alias2).call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"workStartDate") : stack1),{"name":"d","hash":{"end":((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"workEndDate") : stack1)},"data":data,"loc":{"start":{"line":26,"column":12},"end":{"line":26,"column":76}}}))
    + "</td>\r\n    </tr>\r\n    <tr>\r\n        <td>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.numberOfWorkDays",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":29,"column":12},"end":{"line":29,"column":67}}}))
    + ":</td>\r\n        <td>"
    + alias3(alias4(((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"numberOfDays") : stack1), depth0))
    + "</td>\r\n    </tr>\r\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"workerHasNoTaxcard") : stack1),{"name":"if","hash":{},"fn":container.program(16, data, 0),"inverse":container.program(18, data, 0),"data":data,"loc":{"start":{"line":32,"column":4},"end":{"line":61,"column":11}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = ((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"header") : stack1)) != null ? lookupProperty(stack1,"workDescription") : stack1),{"name":"if","hash":{},"fn":container.program(38, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":62,"column":4},"end":{"line":67,"column":11}}})) != null ? stack1 : "")
    + "</table>";
},"useData":true});

this["salaxy"]["reports"]["templates"]["workerCumulativeTables"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h2 class=\"salaxy-content-subheader\">Kumulatiiviset tiedot</h2>\r\n<p>&nbsp;</p>\r\n<table>\r\n    <tr>\r\n        <th colspan=\"2\">\r\n            <h3>Vuoden alusta</h3>\r\n        </th>\r\n    </tr>\r\n    <tr>\r\n        <td style=\"width: 50%\">\r\n            <table>\r\n                "
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"cumulative") : stack1),{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":13,"column":16},"end":{"line":18,"column":39}}})) != null ? stack1 : "")
    + "\r\n            </table>\r\n        </td>\r\n        <td style=\"width: 50%\">\r\n            <table>\r\n                "
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"cumulative") : stack1),{"name":"each","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":23,"column":16},"end":{"line":28,"column":39}}})) != null ? stack1 : "")
    + "\r\n            </table>\r\n        </td>\r\n    </tr>\r\n    <tr>\r\n        <th colspan=\"2\">\r\n            <p>&nbsp;</p>\r\n            <h3>Edellinen vuosi</h3>\r\n        </th>\r\n    </tr>\r\n    <tr>\r\n        <td style=\"width: 50%\">\r\n            <table>\r\n                "
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"cumulative") : stack1),{"name":"each","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":41,"column":16},"end":{"line":46,"column":39}}})) != null ? stack1 : "")
    + "\r\n            </table>\r\n        </td>\r\n        <td style=\"width: 50%\">\r\n            <table>\r\n                "
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"cumulative") : stack1),{"name":"each","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":51,"column":16},"end":{"line":56,"column":39}}})) != null ? stack1 : "")
    + "\r\n            </table>\r\n        </td>\r\n    </tr>\r\n</table>\r\n<p>&nbsp;</p>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"current") : stack1),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":13,"column":43},"end":{"line":18,"column":30}}})) != null ? stack1 : "");
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"left") : stack1),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":13,"column":63},"end":{"line":18,"column":23}}})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "\r\n                <tr>\r\n                    <th>"
    + alias3(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":15,"column":24},"end":{"line":15,"column":35}}}) : helper)))
    + "</th>\r\n                    <td class=\"right\">"
    + alias3((lookupProperty(helpers,"c")||(depth0 && lookupProperty(depth0,"c"))||alias2).call(alias1,(depth0 != null ? lookupProperty(depth0,"total") : depth0),{"name":"c","hash":{},"data":data,"loc":{"start":{"line":16,"column":38},"end":{"line":16,"column":50}}}))
    + "</td>\r\n                </tr>\r\n                ";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"current") : stack1),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":23,"column":43},"end":{"line":28,"column":30}}})) != null ? stack1 : "");
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"right") : stack1),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":23,"column":63},"end":{"line":28,"column":23}}})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"previous") : stack1),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":41,"column":43},"end":{"line":46,"column":30}}})) != null ? stack1 : "");
},"11":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"data") : depth0)) != null ? lookupProperty(stack1,"previous") : stack1),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":51,"column":43},"end":{"line":56,"column":30}}})) != null ? stack1 : "");
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"cumulative") : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":62,"column":7}}})) != null ? stack1 : "");
},"useData":true});

this["salaxy"]["reports"]["templates"]["workerDeductionWarningsV2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h3>"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(alias1,"SALAXY.REPORTS.salary.heading.insufficientSalary",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":2,"column":4},"end":{"line":2,"column":61}}}))
    + "</h3>\r\n<ul>\r\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"validationMessages") : stack1),{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":4,"column":0},"end":{"line":8,"column":9}}})) != null ? stack1 : "")
    + "</ul>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    var lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <li>\r\n        "
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"text") : depth0),(depth0 != null ? lookupProperty(depth0,"parameters") : depth0),{"name":"t","hash":{},"data":data,"loc":{"start":{"line":6,"column":8},"end":{"line":6,"column":30}}}))
    + "\r\n    </li>\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? lookupProperty(depth0,"report") : depth0)) != null ? lookupProperty(stack1,"hasValidationMessages") : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":10,"column":7}}})) != null ? stack1 : "")
    + "\r\n";
},"useData":true});

this["salaxy"]["reports"]["templates"]["workerReportHeadV2"] = Handlebars.template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<table>\r\n    <colgroup>\r\n        <col width=\"40%\" /><col width=\"60%\" />\r\n    </colgroup>\r\n    <tr>\r\n        <td>\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"contractPartiesContactDetailsV2"),depth0,{"name":"contractPartiesContactDetailsV2","data":data,"indent":"            ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "        </td>\r\n        <td>\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"workerContractDetailsV2"),depth0,{"name":"workerContractDetailsV2","data":data,"indent":"            ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "        </td>\r\n    </tr>\r\n</table>\r\n";
},"usePartial":true,"useData":true});

this["salaxy"]["reports"]["templates"]["paymentReportV2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"page"),depth0,{"name":"page","fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"paymentReportHeadV2"),depth0,{"name":"paymentReportHeadV2","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"paymentTablesV2"),depth0,{"name":"paymentTablesV2","data":data,"indent":"    ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"document"),depth0,{"name":"document","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

this["salaxy"]["reports"]["templates"]["salarySlipV2"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"page"),depth0,{"name":"page","fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n<div style=\"page-break-before: always\"></div>\r\n\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"page"),depth0,{"name":"page","fn":container.program(4, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"workerReportHeadV2"),depth0,{"name":"workerReportHeadV2","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"workerCalculationTablesV2"),depth0,{"name":"workerCalculationTablesV2","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n<h3>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.heading.moreInfoOnNextPage",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":8,"column":4},"end":{"line":8,"column":61}}}))
    + "</h3>\r\n<p>"
    + alias3((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||alias2).call(alias1,"SALAXY.REPORTS.salary.text.moreInfoOnNextPage",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":9,"column":3},"end":{"line":9,"column":57}}}))
    + "</p>\r\n\r\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"workerReportHeadV2"),depth0,{"name":"workerReportHeadV2","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"workerCumulativeTables"),depth0,{"name":"workerCumulativeTables","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n<h2 class=\"salaxy-content-subheader\">"
    + container.escapeExpression((lookupProperty(helpers,"t")||(depth0 && lookupProperty(depth0,"t"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),"SALAXY.REPORTS.salary.heading.salaryAdditionalInfo",{"name":"t","hash":{},"data":data,"loc":{"start":{"line":21,"column":37},"end":{"line":21,"column":96}}}))
    + "</h2>\r\n<p>&nbsp;</p>\r\n\r\n"
    + ((stack1 = container.invokePartial(lookupProperty(partials,"workerCalculationAdditionalInfoV2"),depth0,{"name":"workerCalculationAdditionalInfoV2","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"document"),depth0,{"name":"document","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\r\n";
},"usePartial":true,"useData":true});

this["salaxy"]["reports"]["templates"]["salarySlipV2Page1"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"page"),depth0,{"name":"page","fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"workerReportHeadV2"),depth0,{"name":"workerReportHeadV2","data":data,"indent":"      ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + ((stack1 = container.invokePartial(lookupProperty(partials,"workerCalculationTablesV2"),depth0,{"name":"workerCalculationTablesV2","data":data,"indent":"      ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = container.invokePartial(lookupProperty(partials,"document"),depth0,{"name":"document","fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});

if (typeof exports === 'object' && exports) {module.exports = this["salaxy"]["reports"]["templates"];}