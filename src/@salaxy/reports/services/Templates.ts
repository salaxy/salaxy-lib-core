import * as Handlebars from "handlebars";

import { Dates, Numeric, Objects, Translations } from "@salaxy/core";

import * as templates from "../hbs/index.js";
import { dictionaryAll } from "../i18n/dictionaryAll";

/* eslint-disable-next-line */
declare var global;
/* eslint-disable-next-line */
declare var window;
/* eslint-disable-next-line */
declare var require;

/**
 * Provides helper methods for Handlebars templating.
 */
export class Templates {

  /**
   * For NG-dependency injection
   * @ignore
   */
  public static $inject = ["Ng1Translations"];

  private handlebars: typeof Handlebars =  Object.assign(((("undefined" === typeof window ? global : window).Handlebars || require("handlebars")) as typeof Handlebars).noConflict(), {templates: {}});

  private translate: any = null;

  /**
   * Creates a new instance of the service.
   * @param translate - Translations service.
   */
  constructor(translate: any = null) {
    this.translate = translate;
    if (this.translate == null) {
      this.translate = Translations;
    }
      // If not injected, use Translations from salaxy/core
    this.translate.addDictionaries(dictionaryAll);
    this.translate.setLanguage("fi");

    this.registerTemplates();
    this.registerPartials();
    this.registerHelpers();
  }

  /**
   * Gets the HTML using the Handlebars template.
   * @param templateName Name of the template to render.
   * @param data Data for the template.
   */
  public getHtml(templateName: string, data?: any): any {
    if (!this.handlebars.templates[templateName]) {
      console.debug("Registered templates", Object.getOwnPropertyNames(this.handlebars.templates));
      throw new Error(`Cannot find report template: ${templateName || "null"}. See debug for list of registered templates.`);
    }
    return this.handlebars.templates[templateName](data,  { allowProtoPropertiesByDefault: true});
  }

  /** Returns current handlebars */
  public getHandlebars(): any {
    return this.handlebars;
  }

  /**
   * Register templates as partials.
   */
  private registerTemplates() {
     for (const template in templates) {
      if (Objects.has(templates, template)) {
        this.handlebars.templates[template] = templates[template];
      }
     }
  }

  /**
   * Register templates as partials.
   */
  private registerPartials() {
    for (const name in this.handlebars.templates) {
      if (Objects.has(this.handlebars.templates, name)) {
        this.handlebars.registerPartial(name, this.handlebars.templates[name]);
      }
    }
  }

  /**
   * Register helpers.
   */
  private registerHelpers() {

    const stringEndsWith = (s, suffix) => s && s.indexOf(suffix, s.length - suffix.length) !== -1;

    const getParameters = (options: any) => {
      if (options && options.hash) {
        return options.hash;
      }
      return options;
    };

    // Translate
    this.handlebars.registerHelper("t", (key: string, options: any) => {
      const params = getParameters(options);
      const result = this.translate.get(key, params);
      return stringEndsWith(key, ".html") ? new this.handlebars.SafeString(result) : result;
    });

    // Date with optional end date (outputs only start date if given end date is empty)
    this.handlebars.registerHelper("d", (start: string | number, options: any) => {
      const params = getParameters(options);
      return (params && params.end) ? Dates.getFormattedRange(start, params.end) : start ? Dates.getFormattedDate(start) : "-";
    });

    // Round and format decimals with optional "dec" param (default 2)
    this.handlebars.registerHelper("r", (value: number, options: any) => {
      const params = getParameters(options);
      return value ? (params && params.dec) ? Numeric.formatNumber(value, params.dec) : Numeric.formatNumber(value) : ( (params && params.nullChar) ? params.nullChar : "-");
    });

    // Percent with round and format decimals with optional "dec" param (default 2)
    this.handlebars.registerHelper("p", (value: string | number, options: any) => {
      const params = getParameters(options);
      return value ? (params && params.dec) ?  Numeric.formatPercent(Numeric.parseNumber(value) * 100.0 , params.dec) :  Numeric.formatPercent(Numeric.parseNumber(value) * 100.0) : ( (params && params.nullChar) ? params.nullChar : "-");
    });

    // Amount as currency price i.e. 122,20€
    this.handlebars.registerHelper("c", (value: number, options: any) => {
      const params = getParameters(options);
      return value ? Numeric.formatPrice(value) : ( (params && params.nullChar) ? params.nullChar : "-");
    });
  }
}
