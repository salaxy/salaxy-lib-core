import { AccountingReportTableType, Calculation, CalculationRowType, CalcWorktime, HolidayYear, SalaryKind, UnionPaymentType, WorkerAbsences, YearlyChangingNumbers } from "../model";
import { Numeric, Years } from "../util";
import { Ajax } from "./infra";

/**
 * Calculator is the main module for creating and modifying salary calculations.
 * The module can be used completely anonymously without any authentication.
 */
export class Calculator {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  constructor(private ajax: Ajax) {}

  /**
   * OBSOLETE: use CalculatorLogic.getBlank() instead.
   * @deprecated Factory method to create a new calculation on server-side.
   * Typically, you do not need to call server-side factory methods in JavaScript.
   * Instead, you can use client-side factory methods under logic. This saves you one round-trip to server.
   * @ignore
   */
  public createNew(): Promise<Calculation> {
    return this.ajax.getJSON("/calculator/new");
  }

  /**
   * Creates a new calucation with simple salary
   *
   * @param kind The type of the salary.
   * @param [amount=1] The amount of typically hours (could be months etc.). Defaults to 1 if left as null.
   * @param price The price of one unit of amount.
   * @param message Message text for end user - used in salary calculation
   * @param calc The calculation object that should be updated. This is optional - often null, but can be set if you are updating an existing calculation.
   *
   * @returns A Promise with the recalculated Calculation.
   */
  public calculateSalary(kind: SalaryKind, amount: number, price: number, message: string, calc = null): Promise<Calculation> {
    const method = `/calculator/salary?kind=${kind}&amount=${Numeric.parseNumber(amount)}&price=${Numeric.parseNumber(price)}&message=${message}`;
    return this.ajax.postJSON(method, calc);
  }

  /**
   * Sets a household deduction to a calculation
   *
   * @param calc The calculation object to which household deduction is added
   * @param setHouseHoldDeductionTo Boolean to which household
   *
   * @returns A Promise with the recalculated Calculation.
   */
  public setTaxDeduction(calc: Calculation, setHouseHoldDeductionTo: boolean): Promise<Calculation> {
    const method = `/calculator/taxdeduction?setHouseHoldDeduction=${setHouseHoldDeductionTo}`;
    return this.ajax.postJSON(method, calc);
  }

  /**
   * Takes in a calculation - typically with modified parameters - and recalculates it.
   *
   * @param calc The salary calculation that should be recalculated
   *
   * @returns A Promise with the recalculated Calculation.
   */
  public recalculate(calc: Calculation): Promise<Calculation> {
    return this.ajax.postJSON("/calculator/recalculate", calc);
  }

  /**
   * Recalculates a calculation taking into account holiday year and absences from the worktime property.
   * This is an anonymous method that can be used for unit testing: Does not read / write from database.
   * @param calc The salary calculation that should be recalculated.
   * Worktime property should contian the bases for the recalculation.
   *
   * @returns A Promise with the recalculated Calculation.
   */
  public recalculateWorktime(calc: Calculation): Promise<Calculation> {
    return this.ajax.postJSON("/calculator/recalculate-worktime", calc);
  }

  /**
   * Gets the worktime data for Period recalculation from raw calc, holidays andabsences data.
   * This is an anonymous method that can be used for unit testing: Does not read / write from database.
   *
   * @param calc The salary calculation that should be recalculated.
   * @param holidayYear Holiday year with holidays that should be added as salary rows. If not provided, holidays are not calculated.
   * @param absences Absences that should be deducted from monthly salary. If not provided, absences are not calculated.
   *
   * @returns A Promise with Worktime data.
   */
  public getWorktimeData(calc: Calculation, holidayYear?: HolidayYear, absences?: WorkerAbsences): Promise<CalcWorktime> {
    return this.ajax.postJSON("/calculator/worktime", {
      calc,
      holidayYear,
      absences,
    });
  }

  /**
   * Sets a unionPayment to a calculation
   *
   * @param calc The calculation object to which union payment is added
   * @param paymentType Type of union payment, options are 'notSelected', 'raksaNormal', 'raksaUnemploymentOnly' and 'other'
   * @param unionPaymentPercent How many percents the union payment is from the salary
   *
   * @returns A Promise with the recalculated Calculation.
   */
  public setUnionPayment(calc: Calculation, paymentType: UnionPaymentType, unionPaymentPercent: number): Promise<Calculation> {
    const method = `/calculator/setunionpayment?paymentType=${paymentType}&unionPaymentPercent=${Numeric.parseNumber(unionPaymentPercent)}`;
    return this.ajax.postJSON(method, calc);
  }

  /**
   * Adds a calculation row: An expenses, benefits or other such row and returns the recalculated calculation with full data.
   *
   * @param calc The salary calculation that should be modified.
   * @param rowType Type of the calculation row that should be added.
   * @param message Message text for end user - used in salary calculation
   * @param price Price for the row (e.g. €/h, €/km, €/month. Default is 0.
   * @param count Count of the items - default is 1.
   *
   * @returns A Promise with the recalculated Calculation.
   */
  public addRow(calc: Calculation, rowType: CalculationRowType, message: string, price: number, count: number): Promise<Calculation> {
    const method = `/calculator/addrow?rowType=${rowType}&message=${message}&price=${Numeric.parseNumber(price)}&count=${Numeric.parseNumber(count)}`;
    return this.ajax.postJSON(method, calc);
  }

  /**
   * Deletes a calculation row: Expenses, benefits etc. item and returns the recalculated calculation with full data.
   *
   * @param calc The salary calculation that should be modified.
   * @param rowIndex Zero based row index of the row that should be deleted.
   *
   * @returns A Promise with the recalculated Calculation.
   */
  public deleteRow(calc: Calculation, rowIndex: number): Promise<Calculation> {
    return this.ajax.postJSON(`/calculator/deleterow?rowIndex=${rowIndex}`, calc);
  }

  /**
   * Gets salary calculation parameters that are changing yearly.
   * The method is designed for the end-of-year and as such it only supports 2 years:
   * the current / previous year (from Jan to Nov) OR current / next year (in approx. December).
   *
   * @param forYear Date for which the numbers are fetched.
   *
   * @returns The yearly changing numbers if year supported. Error if year not supported.
   */
  public getYearlyChangingNumbers(forYear: Date | string): YearlyChangingNumbers {
    return Years.getYearlyChangingNumbers(forYear);
  }

  /**
   * Experimental: Returns a new Finvoice object for the given calculations.
   * @param calculations Calculations for the Finvoice.
   * @param tableType Accounting table type for the generated Finvoice.
   * @ignore
   */
  public getFinvoice(calculations: Calculation[], tableType: AccountingReportTableType  = AccountingReportTableType.Classic): Promise<any> {
    const method = `/calculator/finvoice/new?tableType=${encodeURIComponent("" + tableType)}`;
    return this.ajax.postJSON(method, calculations);
  }
}
