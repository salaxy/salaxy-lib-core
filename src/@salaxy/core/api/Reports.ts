import { AccountingData, AccountingPeriodClosingOption, AccountingReportTable, AccountingReportTableType, Calculation, CalculationStatus, PeriodDateKind, PeriodType, Report, ReportType } from "../model";
import { Dates } from "../util";
import { Ajax } from "./infra";

/**
 * Report types that are available for calculations as stand-alone (typically pop-up dialog)
 */
export type calcReportType = "salarySlip" | "employerReport" | "paymentReport";

/**
 * Report partials that are available as HTML for developers to create their own
 * reporting views
 */
export type reportPartial = "employer" | "employerPage2" | "worker" | "workerPage2" | "payments";

/**
 * Provides access to HTML reports on calculations
 */
export class Reports {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  constructor(private ajax: Ajax) { }

  /**
   * Gets a list of generated HTML/PDF/JSON reports by type.
   *
   * @param reportType - Type of reports to fetch.
   *
   * @returns A Promise with result data (array of reports).
   */
  public getReportsByType(reportType: ReportType): Promise<Report[]> {
    return this.ajax.getJSON("/reports/type/" + reportType);
  }

  /**
   * Gets a single generated HTML/PDF/JSON report by type and id.
   *
   * @param reportType - Type of the report to fetch.
   * @param id - Id of the report to fetch.
   * @param wait - Optional flag to indicate whether to wait for the report to be generated.
   *
   * @returns A Promise with result data (report).
   */
  public getReport(reportType: ReportType, id: string, wait: boolean): Promise<Report> {
    const method = `/reports/type/${reportType}/encodeURIComponent(id)?wait=${wait}`;
    return this.ajax.getJSON(method);
  }

  /**
   * Gets the monthly / quarterly/ yearly accounting data for the current account.
   *
   * @param refDate Reference date for the period. Please note that even if the date is not the first day of the given period, the entire period is returned.
   * @param target Accounting target name : contains  default ruleset and other parameters for creating accounting entries.
   * @param periodType Month, quarter, year or a custom period. The custom period requires endDate. Default value is the month.
   * @param endDate End date for the period. Required only for the custom period.
   * @param periodDateKind Period date type: paid at date or salary date.
   * @param minStatus Minimum status level for the calculations to include in the data. The default level is PaymentSucceeded.
   * @param ruleSet Optional additional ruleset for the target to use for creating the accounting entries.
   * @returns A Promise with result data (Raw data for accounting purposes).
   */
  public getAccountingDataForPeriod(refDate: string, target?: string, periodType?: PeriodType, endDate?: string, periodDateKind?: PeriodDateKind, minStatus?: CalculationStatus, ruleSet?: string): Promise<AccountingData> {
    refDate = Dates.asDate(refDate) || Dates.getToday();
    const prmsArr = [];
    if (target) {
      prmsArr.push(`target=${encodeURIComponent(target)}`);
    }
    if (ruleSet) {
      prmsArr.push(`ruleSet=${encodeURIComponent(ruleSet)}`);
    }
    if (periodType) {
      prmsArr.push(`periodType=${periodType}`);
    }
    if (endDate) {
      prmsArr.push(`endDate=${endDate}`);
    }
    if (minStatus) {
      prmsArr.push(`minStatus=${minStatus}`);
    }
    if (periodDateKind) {
      prmsArr.push(`periodDateKind=${periodDateKind}`);
    }
    const prms = prmsArr.join("&");

    return this.ajax.getJSON(`/v03-rc/api/reports/accounting/${refDate}${prms ? "?" + prms : ""}`);
  }

  /**
   * Gets the accounting data based on given set of calculations.
   *
   * @param calculationIds - Calculations that are the bases for the report.
   * @param target  - Accounting target name : contains  default ruleset and other parameters for creating accounting entries.
   * @param ruleSet - Optional additional ruleset for the target to use for creating the accounting entries.
   * @returns A Promise with result data (Raw data for accounting purposes).
   */
  public getAccountingDataForCalculationIds(calculationIds: string[], target?: string, ruleSet?: string): Promise<AccountingData> {
    const ids = calculationIds.map((x) => `calculationIds=${x}`).join("&");
    const prmsArr = [];
    if (target) {
      prmsArr.push(`target=${encodeURIComponent(target)}`);
    }
    if (ruleSet) {
      prmsArr.push(`ruleSet=${encodeURIComponent(ruleSet)}`);
    }
    const prms = prmsArr.join("&");
    return this.ajax.getJSON(`/v03-rc/api/reports/accounting?${ids}${prms ? "&" + prms : ""}`);
  }

  /**
   * Gets the accounting data based on given report id.
   *
   * @param id - Saved report id containing accounting data.
   * @param target  - Accounting target name : contains  default ruleset and other parameters for creating accounting entries.
   * @param ruleSet - Optional additional ruleset for the target to use for creating the accounting entries.
   * @returns A Promise with result data (Raw data for accounting purposes).
   */
  public getAccountingData(id: string, target?: string, ruleSet?: string): Promise<AccountingData> {
    const prmsArr = [];
    if (target) {
      prmsArr.push(`target=${encodeURIComponent(target)}`);
    }
    if (ruleSet) {
      prmsArr.push(`ruleSet=${encodeURIComponent(ruleSet)}`);
    }
    const prms = prmsArr.join("&");
    return this.ajax.getJSON(`/v03-rc/api/reports/accounting/${id}${prms ? "?" + prms : ""}`);
  }

  /**
   * Gets the accounting report based on given set of calculations.
   * This method can be used anonymously.
   *
   * @param calculations - Calculations that are the bases for the report.
   * @param target  - Accounting target name : contains  default ruleset and other parameters for creating accounting entries.
   * @param ruleSet - Optional additional ruleset for the target to use for creating the accounting entries.
   * @returns A Promise with result data (Raw data for accounting purposes).
   */
  public getAccountingDataForCalculations(calculations: Calculation[], target?: string, ruleSet?: string): Promise<AccountingData> {
    const prmsArr = [];
    if (target) {
      prmsArr.push(`target=${encodeURIComponent(target)}`);
    }
    if (ruleSet) {
      prmsArr.push(`ruleSet=${encodeURIComponent(ruleSet)}`);
    }
    const prms = prmsArr.join("&");
    return this.ajax.postJSON(`/v03-rc/api/reports/accounting${prms ? "?" + prms : ""}`, calculations);
  }

  /**
   * Closes the accounting period for the given period, and generates a report for the period.
   * @param refDate - Period reference date.
   * @param option - Closing option. Currently only default -option supported.
   * @returns Report id for the period.
   */
  public closeAccountingPeriod(refDate: string, option?: AccountingPeriodClosingOption): Promise<string> {
    return this.ajax.postJSON(`/v03-rc/api/reports/accounting/${refDate}/close${option ? "?option=" + option : ""}`, null);
  }

  /**
   * Gets an HTML report based on Calculation ID.
   *
   * @param reportType - Type of the report to fetch. See the HtmlReportType enumeration for possible values.
   * @param calculationId - GUID for the calculation
   *
   * @returns A Promise with result HTML.
   */
  public getReportHtmlById(reportType: reportPartial, calculationId: string): Promise<string> {
    let method: string;
    if (!reportType) {
      return;
    }
    switch (reportType.toLowerCase()) {
      case "employer":
        method = "/reportpartials/employerCalculation/" + calculationId;
        break;
      case "employerpage2":
        method = "/reportpartials/employerCalculationAdditionalInfo/" + calculationId;
        break;
      case "worker":
        method = "/reportpartials/workerCalculation/" + calculationId;
        break;
      case "workerpage2":
        method = "/reportpartials/workerCalculationAdditionalInfo/" + calculationId;
        break;
      case "payments":
        method = "/reportpartials/paymentCalculation/" + calculationId;
        break;
      default:
        throw new Error("Unknown report partial type: " + reportType);
    }
    return this.ajax.getHTML(this.ajax.getServerAddress() + method);
  }

  /**
   * Gets the monthly / quarterly/ yearly accounting data for the current account.
   *
   * @param refDate - Reference date for the period. Please note that even if the date is not the first day of the given period, the entire period is returned.
   * @param tableType  - Accounting table type.
   * @param periodType - Month, quarter, year or a custom period. The custom period requires endDate. Default value is the month.
   * @param endDate - End date for the period. Required only for the custom period.
   * @returns A Promise with result data (Raw data for accounting purposes).
   */
  public getAccountingReportTableForPeriod(
    refDate: string,
    tableType: AccountingReportTableType = AccountingReportTableType.Classic,
    periodType: PeriodType = PeriodType.Month,
    endDate: string = null,
    ): Promise<AccountingReportTable> {
    refDate = Dates.asDate(refDate) || Dates.getToday();
    let method = "/reports/data/accounting/" + tableType + "/" + encodeURIComponent(refDate) + "?";
    method += "periodType=" + encodeURIComponent("" + periodType);
    if (endDate) {
      method += "&endDate=" + encodeURIComponent(endDate);
    }
    return this.ajax.getJSON(method);
  }

  /**
   * Experimental: Gets the accounting report based on given set of calculations.
   *
   * @param calculationIds - Calculations that are the bases for the report.
   * @param tableType  - Accounting table type.
   * @returns Account report based on the calculations.
   * @ignore
   */
  public getAccountingReportTableForCalculationIds(calculationIds: string[], tableType: AccountingReportTableType  = AccountingReportTableType.Classic): Promise<AccountingReportTable> {
    let method = "/reports/data/accounting/" + tableType + "?";
    calculationIds.forEach((x) => { method += "calculationIds=" + encodeURIComponent("" + x) + "&"; });
    method = method.replace(/[?&]$/, "");
    return this.ajax.getJSON(method);
  }

  /**
   * Experimental: Gets the accounting report based on given set of calculations.
   *
   * @param calculations - Calculations that are the bases for the report.
   * @param tableType  - Accounting table type.
   * @returns Account report based on the calculations.
   * @ignore
   */
  public getAccountingReportTableForCalculations(calculations: Calculation[], tableType: AccountingReportTableType  = AccountingReportTableType.Classic): Promise<AccountingReportTable> {
    const method = "/reports/data/accounting/" + tableType;
    return this.ajax.postJSON(method, calculations);
  }
}
