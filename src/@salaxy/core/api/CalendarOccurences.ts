import { CalendarOccurence } from "../model";
import { Ajax } from "./infra/Ajax";
import { CrudApiBase } from "./infra/CrudApiBase";

/**
 * Provides CRUD access for Calendar occurences.
 * NOTE: Occurences are mainly created and updated through Calendar Events,
 * but limited modifications and delete (exception to recurrence rules) is allowed.
 */
export class CalendarOccurences extends CrudApiBase<CalendarOccurence> {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/calendar/occurences";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
  public getBlank(): CalendarOccurence {
    return {
      categories: [],
    };
  }
}
