import { Ajax } from "./Ajax";

/**
 * Base class for APIs that return only one item (not a list as in CRUD).
 */
export abstract class SingleItemApiBase<T> {

  /**
   * Deriving classes should define the baseURL for the method. E.g. "/v03-rc/api/settings/accounting"
   * Ajax implementations will add the beginning.
   */
  protected abstract baseUrl: string;

  /** Constructor creates a new CRUD api with given Ajax-implementation */
  constructor(protected ajax: Ajax) {}

  /**
   * Gets a single settings object.
   * @returns A Promise with settings object.
   */
  public get(): Promise<T> {
    return this.ajax.getJSON(this.baseUrl);
  }

  /**
   * Saves the settings to the storage.
   * @param settings - The settings that is saved.
   * @returns A Promise with settings object.
   */
  public save(settings: T): Promise<T> {
    return this.ajax.postJSON(`${this.baseUrl}/`, settings);
  }
}
