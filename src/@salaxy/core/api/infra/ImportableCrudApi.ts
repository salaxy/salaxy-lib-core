import {DataRow, Mapper} from "../../logic";
import { ApiListItem, ApiValidation} from "../../model";
import { CrudApiBase } from "./CrudApiBase";

/**
 * Interface for APIs which implement import/export functionality.
 */
export interface ImportableCrudApi<TObject> extends CrudApiBase<TObject> {

   /** Available object mappers for import/export */
   getObjectMappers(): Array<Mapper<TObject, DataRow>> | null;

   /** Available list item mappers for import/export */
   getListItemMappers(): Array<Mapper<ApiListItem, DataRow>> | null;

  /**
   * Validates the item using a server side call.
   *
   * @param itemToValidate - The item that is validated.
   * @returns A Promise with result data containing validation property.
   */
  validate(itemToValidate: TObject): Promise<TObject>;

  /**
   * Returns the validation object for the validated object.
   * @param validatedItem The validated object with a validation result.
   */
  getValidationResult(validatedItem: TObject): ApiValidation;
}
