import { VersionInfo, WorkflowData, WorkflowEvent } from "../../model";
import { OData, ODataResult, ODataQueryOptions } from "../../util";
import { Ajax } from "./Ajax";

/** Base class for CRUD services. */
export abstract class CrudApiBase<T> {

  /**
   * Deriving classes should define the baseURl for the method. E.g. "/calculations"
   * Ajax implementations will add the beginning (e.g. "https://test-api.salaxy.com/api/v02")
   */
  protected abstract baseUrl: string;

  /** Constructor creates a new CRUD api with given Ajax-implementation */
  constructor(protected ajax: Ajax) { }

  /**
   * Client-side (synchronous) method for getting a new blank CRUD item as bases for UI binding.
   * The basic idea is that all the child object properties should be non-null to avoid null reference exceptions.
   * In special cases, when null is significant it may be used: e.g. Calculation.result is null untill the recalculation has been done.
   * Strings, dates and booleans may be null (false if bool), but typically just undefined.
   */
  public abstract getBlank(): T;

  /**
   * Makes an OData query to the service's main OData method.
   * @param query The options for the query: Filter, search sort etc.
   * This is either a strongly typed object or query string that is added directly.
   */
  public getOData(query: ODataQueryOptions | string): Promise<ODataResult<any>> {
    // TODO: Make result strongly typed: A second Generic parameter of the class.
    const baseUrl = this.getODataUrl();
    return OData.getOData<any>(baseUrl, query, this.ajax);
  }

  /**
   * Makes an OData query to the the given service URL.
   * @param baseUrl The service URL that is used, e.g. "/v03-rc/api/taxcards/current".
   * @param query The options for the query: Filter, search sort etc.
   * This is either a strongly typed object or query string that is added directly.
   * @deprecated You should use the OData.getOData() static util method instead.
   */
  public getODataBase(baseUrl: string, query: ODataQueryOptions | string): Promise<ODataResult<any>> {
    // TODO: Check that it is not used and remove.
    return OData.getOData<any>(baseUrl, query, this.ajax);
  }

  /**
   * Gets all the items of a given type.
   * @returns A Promise with result data array.
   */
  public getAll(): Promise<T[]> {
    return this.ajax.getJSON(this.baseUrl);
  }

  /**
   * Gets a single item based on identier
   * @param id - Unique identifier for the object
   * @returns A Promise with result data.
   */
  public getSingle(id: string): Promise<T> {
    return this.ajax.getJSON(`${this.baseUrl}/${id}`);
  }

  /**
   * Deletes an single item from the sotrage
   * @param id - Unique identifier for the object
   * @returns A Promise with result data "Object deleted".
   */
  public delete(id: string): Promise<string> {
    return this.ajax.remove(`${this.baseUrl}/${id}`);
  }

  /**
   * Saves an item to the storage.
   * If id is null, this is add/insert. If id exists, this is update.
   * @param itemToSave - The item that is updated/inserted.
   * @returns A Promise with result data as saved to the storage (contains id, createAt, owner etc.).
   */
  public save(itemToSave: T): Promise<T> {
    return this.ajax.postJSON(`${this.baseUrl}/`, itemToSave);
  }

  /** Gets the OData URL for v03 compatible services */
  public getODataUrl(): string {
    if (this.baseUrl.startsWith("/v03")) {
      // e.g. "/v03-rc/api/messages"
      return this.baseUrl;
    }
    // Backporting the OData features to v02 version API's
    switch (this.baseUrl) {
      case "/accounts/workers":
        // This API now really returns employment relations, not workers. Will probably not change this in v03 scope
        return "/v03-rc/api/accounts/workers";
      case "/calculations":
        return "/v03-rc/api/calculations/all";
      case "/taxcards/owned":
        return "/v03-rc/api/taxcards";
      case "/payments":
      case "/accounts/certificate": // TODO: Account based - probably implement
      case "/accounts/credential":
      case "/accounts/authorizedAccount":
        throw new Error("getODataUrl() not implemented for: " + this.baseUrl);
      case "/absences":
      case "/holidays/all":
        throw new Error("getODataUrl() is only implemented in Version 03 for: " + this.baseUrl);
      case "/yearEnd/feedback":
        throw new Error("Year end is depricated and OData querying does not make sense for YearEnd in any case (because there is only on per year).");
    }
    throw new Error(`Service ${this.baseUrl} does not support OData interface.`);
  }

  /** Returns the base url for this api. */
  public getBaseUrl(): string {
    return this.baseUrl;
  }

  /**
   * Adds/updates the workflow event for the item.
   *
   * @param item - Item for which to set the workflow event.
   * @param wfEvent - Event to add or update.
   * @returns Workflow data for the item.
   */
  public saveWorkflowEvent(item: T, wfEvent: WorkflowEvent): Promise<WorkflowData> {
    const id = (item as any as { /** required id */ id: string }).id;
    return this.ajax.postJSON(`${this.baseUrl}/${id}/workflows`, wfEvent);
  }

  /**
   * Deletes the worklflow item or items from the item.
   * @param item - Item for which to delete workflow events.
   * @param wfIdOrType - Event id or type.
   * @returns A Promise with result data "Object deleted".
   */
  public deleteWorkflowEvent(item: T, wfIdOrType: string): Promise<string> {
    const id = (item as any as { /** required id */ id: string }).id;
    return this.ajax.remove(`${this.baseUrl}/${id}/workflows/${wfIdOrType}`);
  }

  /**
   * Lists versions for the object.
   * @param id - Unique identifier for the object
   * @returns A Promise with result data.
   */
  public getVersions(id: string): Promise<VersionInfo[]> {
    return this.ajax.getJSON(`${this.baseUrl}/${id}/versions`);
  }

  /**
   * Get the given version for the object.
   * @param id - Unique identifier for the object
   * @param versionId - Version identifier
   * @returns A Promise with result data.
   */
  public getVersion(id: string, versionId: string): Promise<T> {
    return this.ajax.getJSON(`${this.baseUrl}/${id}/versions/${versionId}`);
  }
}
