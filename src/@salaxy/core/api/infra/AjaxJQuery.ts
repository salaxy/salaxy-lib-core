import { Configs } from "../../logic/Configs";
import { Cookies } from "../../util/Cookies";
import { Ajax } from "./Ajax";

/**
 * Provides wrapper methods for communicating with the Palkkaus.fi API.
 * The raw Ajax-access to the server methods: GET, POST and DELETE
 * with different return types and authentication / error events.
 * This is the JQuery based ajax implementation.
 */
export class AjaxJQuery implements Ajax {

  /**
   * By default (true) the token is set to salaxy-token -cookie.
   * Disable cookies with this flag.
   */
  public useCookie = true;

  /**
   * By default credentials are not used in http-calls.
   * Enable credentials with this flag.
   */
  public useCredentials = false;

  /**
   * The server address - root of the server. This is settable field.
   * Will probably be changed to a configuration object in the final version.
   */
  public serverAddress = "https://test-api.salaxy.com";

  private token: string;

  /**
   * Creates a new instance of AjaxJQuery
   */
  constructor() {
    const config = Configs.current;
    if (config) {
      // apiServer
      if (config.apiServer) {
        this.serverAddress = config.apiServer;
      }

      // useCredentials
      if (config.useCredentials != null) {
        this.useCredentials = config.useCredentials;
      }

      // useCookie
      if (config.useCookie != null) {
        this.useCookie = config.useCookie;
      }
    }
  }

  /** Gets the API address with version information. E.g. 'https://test-api.salaxy.com/v02/api' */
  public getApiAddress(): string {
    return this.serverAddress + "/v02/api";
  }

  /** Gets the Server address that is used as bases to the HTML queries. E.g. 'https://test-api.salaxy.com' */
  public getServerAddress(): string {
    return this.serverAddress;
  }

  /**
   * Gets a JSON-message from server using the API
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   * and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   *
   * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
   */
  public getJSON(method: string): Promise<any> {

    const token = this.getCurrentToken();
    return this.getJQuery().ajax({
      dataType: "json",
      url: this.getUrl(method),
      xhrFields: {
        withCredentials: (token) ? false : this.useCredentials,
      },
      beforeSend: (request) => {
        if (token) {
          request.setRequestHeader("Authorization", "Bearer " + token);
        }
      },
    });
  }

  /**
   * Gets a HTML-message from server using the API
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   * and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   *
   * @returns A Promise with result html. Standard Promise rejection to be used for error handling.
   */
  public getHTML(method: string): Promise<string> {
    const token = this.getCurrentToken();
    return this.getJQuery().ajax({
      dataType: "html",
      url: this.getUrl(method),
      xhrFields: {
        withCredentials: (token) ? false : this.useCredentials,
      },
      beforeSend: (request) => {
        if (token) {
          request.setRequestHeader("Authorization", "Bearer " + token);
        }
      },
    });
  }

  /**
   * POSTS data to server and receives back a JSON-message.
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   * and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   * @param data - The data that is posted to the server.
   *
   * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
   */
  public postJSON(method: string, data: any): Promise<any> {
    const token = this.getCurrentToken();
    return this.getJQuery().ajax({
      type: "POST",
      url: this.getUrl(method),
      data,
      dataType: "json",
      xhrFields: {
        withCredentials: (token) ? false : this.useCredentials,
      },
      beforeSend: (request) => {
        if (token) {
          request.setRequestHeader("Authorization", "Bearer " + token);
        }
      },
    });
  }

  /**
   * POSTS data to server and receives back HTML.
   *
   * @param method - The API method is the url starting from api version, e.g. '/v02/api'. E.g. '/calculator/new'
   * @param data - The data that is posted to the server.
   *
   * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
   */
  public postHTML(method: string, data: any): Promise<string> {
    const token = this.getCurrentToken();
    return this.getJQuery().ajax({
      type: "POST",
      url: this.getUrl(method),
      data,
      dataType: "html",
      xhrFields: {
        withCredentials: (token) ? false : this.useCredentials,
      },
      beforeSend: (request) => {
        if (token) {
          request.setRequestHeader("Authorization", "Bearer " + token);
        }
      },
    });
  }

  /**
   * Sends a DELETE-message to server using the API
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   * and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   *
   * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
   */
  public remove(method: string): Promise<any> {
    const token = this.getCurrentToken();
    return this.getJQuery().ajax({
      type: "DELETE",
      url: this.getUrl(method),
      xhrFields: {
        withCredentials: (token) ? false : this.useCredentials,
      },
      beforeSend: (request) => {
        if (token) {
          request.setRequestHeader("Authorization", "Bearer " + token);
        }
      },
    });
  }

  /**
   * Gets the current token.
   * Will check the salaxy-token cookie if the token is persisted there
   */
  public getCurrentToken(): string {
    if (!this.token && this.useCookie) {
      this.token = new Cookies().get("salaxy-token") || "";
    }
    return this.token;
  }

  /**
   * Sets the current token. The token is set to cookie called "salaxy-token" or
   * if the HTML page is running from local computer, it is set to local storage.
   *
   * @param token - the authentication token to store.
   */
  public setCurrentToken(token: string): void {
    if (this.useCookie) {
      new Cookies().setCookie("salaxy-token", token || "");
    }
    this.token = token;
  }

  /**
   * Implements the OAuth2 "Resource Owner Password Credentials Grant" flow (RFC6749 4.3).
   * This method is not typically used in production web sites - use Implicit Grant instead for client side JavScript (SPAs).
   * However, it is very useful in development, testing, trusted helpers and server-side scenarios.
   */
  public resourceOwnerLogin(username: string, password: string): void {
    return this.getJQuery().ajax({
      type: "POST",
      url: this.getServerAddress() + "/oauth2/token",
      data: {
        grant_type: "password",
        username,
        password,
      },
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      success: (data, textStatus, request) => {
        this.setCurrentToken(data.access_token);
      },
      dataType: "json",
    });
  }

  private getJQuery() {
    const jQuery = Configs.global.jQuery;
    if (jQuery) {
      return jQuery;
    } else {
      throw new Error("ERROR: No jQuery. AjaxJQuery requires jQuery to be referenced.");
    }
  }

  /** If missing, append the API server address to the given url method string */
  private getUrl(method: string): string {
    if (!method || method.trim() === "") {
      return null;
    }
    if (method.toLowerCase().startsWith("http")) {
      return method;
    }
    if (method.toLowerCase().startsWith("/v")) {
      return this.getServerAddress() + method;
    }
    return this.getApiAddress() + method;
  }
}
