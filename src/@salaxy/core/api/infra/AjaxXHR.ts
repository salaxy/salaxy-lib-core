import { Configs } from "../../logic/Configs";
import { Cookies } from "../../util/Cookies";
import { Ajax } from "./Ajax";

/**
 * Reference to XDomainRequest for IE8/9.
 * @ignore
 */
declare let XDomainRequest;

/**
 * Provides wrapper methods for communicating with the Palkkaus.fi API.
 * The raw Ajax-access to the server methods: GET, POST and DELETE
 * with different return types and authentication / error events.
 * This is the XMLHttpRequest based Ajax implementation.
 */
export class AjaxXHR implements Ajax {

  /**
   * By default (true) the token is set to salaxy-token -cookie.
   * Disable cookies with this flag.
   */
  public useCookie = true;

  /**
   * By default credentials are not used in http-calls.
   * Enable credentials with this flag (force disabled when the token is set).
   */
  public useCredentials: boolean;

  /**
   * The server address - root of the server. This is settable field.
   * Will probably be changed to a configuration object in the final version.
   */
  public serverAddress = "https://test-api.salaxy.com";

  private token: string;

  /**
   * Creates a new instance of AjaxXHR
   */
  constructor() {

    const config = Configs.current;
    if (config) {
      // apiServer
      if (config.apiServer) {
        this.serverAddress = config.apiServer;
      }

      // useCredentials
      if (config.useCredentials != null) {
        this.useCredentials = config.useCredentials;
      }

      // useCookie
      if (config.useCookie != null) {
        this.useCookie = config.useCookie;
      }
    }
  }

  /** Gets the API address with version information. E.g. 'https://test-api.salaxy.com/v02/api' */
  public getApiAddress(): string {
    return this.serverAddress + "/v02/api";
  }

  /** Gets the Server address that is used as bases to the HTML queries. E.g. 'https://test-api.salaxy.com' */
  public getServerAddress(): string {
    return this.serverAddress;
  }

  /**
   * Gets a JSON-message from server using the API
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   * and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   *
   * @returns A Promise with result data. Standard Promise rejection to be used for error handling.
   */
  public getJSON(method: string): Promise<any> {

    return new Promise((resolve, reject) => {

      const xhr = this.createCORSRequest("GET", this.getUrl(method));
      if (!xhr) {
        return reject(new Error("CORS not supported!"));
      }

      xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
      xhr.setRequestHeader("Accept", "application/json; charset=UTF-8");
      // xhr.setRequestHeader("Cache-Control", "max-age=0");

      const token = this.getCurrentToken();
      if (token) {
        xhr.setRequestHeader("Authorization", "Bearer " + token);
      }
      xhr.withCredentials = (token) ? false : this.useCredentials;

      xhr.onreadystatechange = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
          if (xhr.status === 200) {
            try {
              const response = JSON.parse(xhr.responseText);
              return resolve(response);
            } catch (error) {
              return reject(error);
            }
          } else {
            return reject(new Error("There was a problem with the request."));
          }
        }
      };
      xhr.onerror = () => {
        return reject(new Error("There was a problem with the request."));
      };

      xhr.send();
    });
  }

  /**
   * Gets a HTML-message from server using the API
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   * and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   *
   * @returns A Promise with result html. Standard Promise rejection to be used for error handling.
   */
  public getHTML(method: string): Promise<string> {

    return new Promise((resolve, reject) => {

      const xhr = this.createCORSRequest("GET", this.getUrl(method));
      if (!xhr) {
        return reject(new Error("CORS not supported!"));
      }

      xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
      xhr.setRequestHeader("Accept", "application/json; charset=UTF-8");
      // xhr.setRequestHeader("Cache-Control", "max-age=0");

      const token = this.getCurrentToken();
      if (token) {
        xhr.setRequestHeader("Authorization", "Bearer " + token);
      }
      xhr.withCredentials = (token) ? false : this.useCredentials;

      xhr.onreadystatechange = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
          if (xhr.status === 200) {
            return resolve(xhr.responseText);
          } else {
            return reject(new Error("There was a problem with the request."));
          }
        }
      };
      xhr.onerror = () => {
        return reject(new Error("There was a problem with the request."));
      };

      xhr.send();
    });
  }

  /**
   * POSTS data to server and receives back a JSON-message.
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   * and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   * @param data - The data that is posted to the server.
   *
   * @returns A Promise with result. Standard Promise rejection to be used for error handling.
   */
  public postJSON(method: string, data: any): Promise<any> {

    return new Promise((resolve, reject) => {

      const xhr = this.createCORSRequest("POST", this.getUrl(method));
      if (!xhr) {
        return reject(new Error("CORS not supported!"));
      }

      xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
      xhr.setRequestHeader("Accept", "application/json; charset=UTF-8");
      // xhr.setRequestHeader("Cache-Control", "max-age=0");

      const token = this.getCurrentToken();
      if (token) {
        xhr.setRequestHeader("Authorization", "Bearer " + token);
      }
      xhr.withCredentials = (token) ? false : this.useCredentials;

      xhr.onreadystatechange = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
          if (xhr.status === 200) {
            try {
              const response = JSON.parse(xhr.responseText);
              return resolve(response);
            } catch (error) {
              return reject(error);
            }
          } else {
            return reject(new Error("There was a problem with the request."));
          }
        }
      };
      xhr.onerror = () => {
        return reject(new Error("There was a problem with the request."));
      };

      const params = (!data || typeof data === "string") ? data
        : Object.keys(data).map((k) => encodeURIComponent(k) + "=" + encodeURIComponent(data[k])).join("&");

      xhr.send(params);
    });
  }

  /**
   * POSTS data to server and receives back HTML.
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   * and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   * @param data - The data that is posted to the server.
   *
   * @returns A Promise with result. Standard Promise rejection to be used for error handling.
   */
  public postHTML(method: string, data: any): Promise<string> {

    return new Promise((resolve, reject) => {

      const xhr = this.createCORSRequest("POST", this.getUrl(method));
      if (!xhr) {
        return reject(new Error("CORS not supported!"));
      }

      xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
      xhr.setRequestHeader("Accept", "application/json; charset=UTF-8");
      // xhr.setRequestHeader("Cache-Control", "max-age=0");

      const token = this.getCurrentToken();
      if (token) {
        xhr.setRequestHeader("Authorization", "Bearer " + token);
      }
      xhr.withCredentials = (token) ? false : this.useCredentials;

      xhr.onreadystatechange = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
          if (xhr.status === 200) {
            return resolve(xhr.responseText);
          } else {
            return reject(new Error("There was a problem with the request."));
          }
        }
      };
      xhr.onerror = () => {
        return reject(new Error("There was a problem with the request."));
      };

      const params = (!data || typeof data === "string") ? data
        : Object.keys(data).map((k) => encodeURIComponent(k) + "=" + encodeURIComponent(data[k])).join("&");

      xhr.send(params);
    });
  }

  /**
   * Sends a DELETE-message to server using the API
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   * and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   *
   * @returns A Promise with result. Standard Promise rejection to be used for error handling.
   */
  public remove(method: string): Promise<any> {
    return new Promise((resolve, reject) => {

      const xhr = this.createCORSRequest("DELETE", this.getUrl(method));
      if (!xhr) {
        return reject(new Error("CORS not supported!"));
      }

      xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
      xhr.setRequestHeader("Accept", "application/json; charset=UTF-8");
      // xhr.setRequestHeader("Cache-Control", "max-age=0");

      const token = this.getCurrentToken();
      if (token) {
        xhr.setRequestHeader("Authorization", "Bearer " + token);
      }
      xhr.withCredentials = (token) ? false : this.useCredentials;

      xhr.onreadystatechange = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
          if (xhr.status === 200) {
            try {
              const response = JSON.parse(xhr.responseText);
              return resolve(response);
            } catch (error) {
              return reject(error);
            }
          } else {
            return reject(new Error("There was a problem with the request."));
          }
        }
      };
      xhr.onerror = () => {
        return reject(new Error("There was a problem with the request."));
      };

      xhr.send();
    });
  }

  /**
   * Gets the current token.
   * Will check the salaxy-token cookie if the token is persisted there
   */
  public getCurrentToken(): string {
    if (!this.token && this.useCookie) {
      this.token = new Cookies().get("salaxy-token") || "";
    }
    return this.token;
  }

  /**
   * Sets the current token. The token is set to cookie called "salaxy-token" or
   * if the HTML page is running from local computer, it is set to local storage.
   *
   * @param token - the authentication token to store.
   */
  public setCurrentToken(token: string): void {
    if (this.useCookie) {
      new Cookies().setCookie("salaxy-token", token || "");
    }
    this.token = token;
  }

  /** If missing, append the API server address to the given url method string */
  private getUrl(method: string): string {
    if (!method || method.trim() === "") {
      return null;
    }
    if (method.toLowerCase().startsWith("http")) {
      return method;
    }
    if (method.toLowerCase().startsWith("/v")) {
      return this.getServerAddress() + method;
    }
    return this.getApiAddress() + method;
  }

  private createCORSRequest(method, url) {
    let xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      // XHR for Chrome/Firefox/Opera/Safari.
      xhr.open(method, url, true);
    } else if (typeof XDomainRequest !== "undefined") {
      // XDomainRequest for IE8/9.
      xhr = new XDomainRequest();
      xhr.open(method, url);
    } else {
      // CORS not supported.
      xhr = null;
    }
    return xhr;
  }
}
