import { OAuthMessage } from "../../model";
import { Ajax } from "./Ajax";

/**
 * Methods for oauth2 communication.
 */
export class OAuth2 {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  constructor(private ajax: Ajax) {}

  /**
   * Token endpoint.
   *
   * @param msg - OAuth 2.0 authorization request.
   *
   * @returns A Promise with OAuthMessage.
   */
  public token(msg: OAuthMessage): Promise<OAuthMessage> {
    return this.ajax.postJSON(this.ajax.getServerAddress() + "/oauth2/token", msg);
  }

  /**
   * Gets a federation message for the currently signed-in account as OpenID Connect UserInfo Response
   * (http://openid.net/specs/openid-connect-core-1_0.html#UserInfo).
   * This message can be used by trusted authentication providers (mainly Banks) to federate authentication and authorization information to Salaxy.
   * In such a flow this service end-point provides an example that could be used in testing.
   *
   * @returns A Promise with federation message {OpenIdUserInfoSupportedClaims}.
   */
  public getFederationMessage(): Promise<any> {
    return this.ajax.getJSON(this.ajax.getServerAddress() + "/oauth2/federationMessage");
  }
}
