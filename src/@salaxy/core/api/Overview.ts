import { ApiListItem, WelcomeDataWorker } from "../model";
import { OData, ODataQueryOptions, ODataResult } from "../util";
import { Ajax } from "./infra";

/**
 * Methods for testing the API.
 */
export class Overview {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  constructor(private ajax: Ajax) {}

  /**
   * Makes an OData query over all objects within the current account.
   * @param query The options for the query: Filter, search sort etc.
   * This is either a strongly typed object or query string that is added directly.
   */
  public getAllData(query: ODataQueryOptions | string): Promise<ODataResult<ApiListItem>> {
    return OData.getOData<ApiListItem>("/v03-rc/api/overview/all", query, this.ajax);
  }

  /**
   * Gets the welcome page data for a Worker account.
   */
  public getWelcomeDataWorker(): Promise<WelcomeDataWorker> {
    return this.ajax.getJSON("/v03-rc/api/overview/welcome-worker");
  }

}
