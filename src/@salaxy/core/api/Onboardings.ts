import { Onboarding } from "../model";
import { Ajax } from "./infra";

/**
 * Provides functionality for Onboarding:
 * The process where the user becomes a user of Salaxy service and an account is created for him / her / the company.
 */
export class Onboardings {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  constructor(private ajax: Ajax) {}

  /**
   * Gets a single Onboarding object based on identier
   *
   * @param id - Unique identifier for the object
   *
   * @returns A Promise with result data (Onboarding).
   */
  public getOnboarding(id: string): Promise<Onboarding> {
    return this.ajax.getJSON("/onboarding/" + encodeURIComponent("" + id));
  }

  /**
   * Gets a single Onboarding object for authorizing account
   *
   * @param accountId - Account id of the authorizing account.
   *
   * @returns A Promise with result data (Onboarding).
   */
  public getOnboardingForAccount(accountId: string): Promise<Onboarding> {
    return this.ajax.getJSON("/onboarding/account/" + encodeURIComponent("" + accountId));
  }

  /**
   * Creates or updates an Onboarding.
   *
   * @param onboarding - The Onboarding object that is updated.
   *
   * @returns A Promise with result data (saved Onboarding).
   */
  public saveOnboarding(onboarding: Onboarding): Promise<Onboarding> {
    return this.ajax.postJSON("/onboarding", onboarding);
  }

  /**
   * Deletes an onboarding object from the storage
   *
   * @param id - Unique identifier for the object
   *
   * @returns A Promise with result data "Object deleted".
   */
  public deleteOnboarding(id: string): Promise<string> {
    return this.ajax.remove("/onboarding/" + encodeURIComponent("" + id));
  }

  /**
   * Sends a new pin code to the telephone number to be verified in onboarding.
   *
   * @param onboarding - The Onboarding object which contains the telephone number.
   *
   * @returns A Promise with result data (true if sending succeeded).
   */
  public sendSmsVerificationPin(onboarding: Onboarding): Promise<boolean> {
    return this.ajax.postJSON("/onboarding/verification/sms", onboarding);
  }

  /**
   * Commits onboarding and creates/updates the account.
   *
   * @param onboarding - The Onboarding object that is to be committed..
   *
   * @returns A Promise with result data (committed Onboarding).
   */
  public commitOnboarding(onboarding: Onboarding): Promise<Onboarding> {
    return this.ajax.postJSON("/onboarding/commit", onboarding);
  }

  /**
   * Returns the url to start the onboarding for current account (server side).
   * @param id - Optional id for the existing onboarding object.
   */
  public getLaunchUrl(id: string = null): string {
    return `${this.ajax.getServerAddress()}/onboarding/launch${id ? "/" + id : ""}?access_token=${encodeURIComponent("" + this.ajax.getCurrentToken())}`;
  }
}
