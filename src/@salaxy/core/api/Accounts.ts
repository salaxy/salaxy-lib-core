import {
  CompanyAccount,
  MessageType,
  PersonAccount,
} from "../model";
import { Ajax } from "./infra";

/**
 * Methods for updating account information
 */
export class Accounts {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  constructor(private ajax: Ajax) {}

  /**
   * Gets the current company account if the current account is a company.
   * NOTE: Typically the account data is fetched in Session check => You do not need to fetch it separately.
   *
   * @returns A Promise with result data (Company account or null).
   */
  public getCompany(): Promise<CompanyAccount> {
    return this.ajax.getJSON("/accounts/company");
  }

  /**
   * Gets the current Person account:
   * Either the current account or the Primary contact person account if the current account is a company.
   * NOTE: Typically the current account data is fetched in Session check => You do not need to fetch it separately.
   * => Only use this to fetch the Primary contact person account if the current account is a company.
   *
   * @returns A Promise with result data (Personal account).
   */
  public getPerson(): Promise<PersonAccount> {
    return this.ajax.getJSON("/accounts/person");
  }

  /**
   * Saves the avatar and contact for the current Person account:
   * Either the current account or the Primary contact person account if the current account is a company.
   *
   * @returns A Promise with result data (Personal account).
   */
  public savePerson(personAccount: PersonAccount): Promise<PersonAccount> {
    return this.ajax.postJSON("/accounts/person", personAccount);
  }

  /**
   * Returns the url where to post the avatar file
   */
  public getAvatarUploadUrl(): string {
    return `${this.ajax.getApiAddress()}/accounts/avatar`;
  }

  /**
   * Switches the current web site usage role.
   *
   * @param role - household or worker. You cannot change to company at the moment using this method.
   *
   * @returns A Promise with result data (new role).
   */
  public switchRole(role: "worker" | "household"): Promise<"worker"|"household"> {
    return this.ajax.postJSON("/accounts/switchRole?role=" + role, {});
  }

  /**
   * Sends a verification.
   *
   * @param type - Verification type.
   * @param address - Verification address.
   *
   * @returns A Promise with result data (pin code in test environments).
   */
  public sendVerification(type: MessageType, address: string): Promise<string> {
    return this.ajax.postJSON("/accounts/verification/" + type, JSON.stringify(address));
  }

  /**
   * Confirms a verification.
   *
   * @param type - Verification type.
   * @param pin - Verification pin code.
   *
   * @returns A Promise with result data (true/false).
   */
  public confirmVerification(type: MessageType, pin: string): Promise<boolean> {
    return this.ajax.postJSON("/accounts/verification/" + type + "/confirm", JSON.stringify(pin));
  }
}
