import { AccountBase } from "../model";
import { Ajax } from "./infra";

/**
 * Provides read-only access to the current user session
 */
export class Session {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  constructor(private ajax: Ajax) {}

  /**
   * Checks whether the current browser session is authorized.
   *
   * @returns A Promise with result data (boolean).
   */
  public isAuthorized(): Promise<boolean> {
    return this.ajax.getJSON("/session/isAuthorized");
  }

  /**
   * Gets the current session information or null if there is no active session.
   * UserSession data is the Authorization object that provides information about the current session. Null, if there is no session.
   *
   * @returns A Promise with result data (UserSession).
   */
  public getSession(): Promise<AccountBase> {
    return this.ajax.getJSON("/session/current");
  }

  /**
   * Returns server address
   */
  public getServerAddress(): string {
    return this.ajax.getServerAddress();
  }
}
