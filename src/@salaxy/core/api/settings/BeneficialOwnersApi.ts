import { BeneficialOwners } from "../../model";
import { Ajax, SingleItemApiBase } from "../infra";

/**
 * Provides CRUD access for beneficiary owners
 */
export class BeneficialOwnersApi extends SingleItemApiBase<BeneficialOwners> {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/beneficial-owners";

  constructor(ajax: Ajax) {
    super(ajax);
  }
}
