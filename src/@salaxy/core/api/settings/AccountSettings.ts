import { CompanyAccountSettings } from "../../model";
import { Ajax, SingleItemApiBase } from "../infra";

/**
 * Provides read / update functionality for main account settings (employer/company)
 */
export class AccountSettings extends SingleItemApiBase<CompanyAccountSettings> {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/settings/company";

  constructor(ajax: Ajax) {
    super(ajax);
  }

}
