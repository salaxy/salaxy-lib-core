import { CalculationsDataRow, CalculatorLogic, DataRow, Map, Mapper, Mappers } from "../../logic";
import { ApiListItem, ApiValidation, Calculation, CalculationSharing, CalcWorktime, EmploymentRelation, irepr, SharingUriType } from "../../model";
import { Calculator } from "../Calculator";
import { Ajax, CrudApiBase, ImportableCrudApi } from "../infra";

/**
 * Provides CRUD access for authenticated user to access a his/her own calculations
 */
export class Calculations extends CrudApiBase<Calculation> implements ImportableCrudApi<Calculation> {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. (2) */
  protected baseUrl = "/calculations";

  private calculator: Calculator;

  constructor(ajax: Ajax) {
    super(ajax);
    this.calculator = new Calculator(ajax);
  }

  /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
  public getBlank(): Calculation {
    return CalculatorLogic.getBlank();
  }

  /**
   * Takes in a calculation - typically with modified parameters - and recalculates it.
   * @param calc The salary calculation that should be recalculated
   * @returns A Promise with the recalculated Calculation.
   */
  public recalculate(calc: Calculation): Promise<Calculation> {
    return this.calculator.recalculate(calc);
  }

  /**
   * Recalculates a calculation taking into account holiday year and absences from the worktime property.
   * This is an anonymous method that can be used for unit testing: Does not read / write from database.
   * @param calc The salary calculation that should be recalculated.
   * Worktime property should contian the bases for the recalculation.
   *
   * @returns A Promise with the recalculated Calculation.
   */
  public recalculateWorktime(calc: Calculation): Promise<Calculation> {
    return this.calculator.recalculateWorktime(calc);
  }

  /**
   * Gets the worktime data for Period recalculation based on given calculation
   * but fetching absences and holidays from the database.
   *
   * @param calc The salary calculation that should be recalculated.
   * It may potentially have changes to compared to the one in database or the calc may not have even been saved.
   * The calculation MUST have emloymentId and period set for calculation to work properly.
   * @returns A Promise with Worktime data.
   */
  public getWorktimeData(calc: Calculation): Promise<CalcWorktime> {
    return this.ajax.postJSON(`${this.baseUrl}/worktime`, calc);
  }

  /**
   * Gets the Calculation objects for the given ids.
   * @param ids Identifiers for the Calculation objects.
   * @returns A Promise with result data array.
   */
  public getMulti(ids: string[]): Promise<Calculation[]> {
    if (ids == null || ids.length === 0) {
      return Promise.resolve([]);
    }
    let method = this.baseUrl + "/set?";
    ids.forEach((x) => { method += "ids=" + encodeURIComponent("" + x) + "&"; });
    method = method.replace(/[?&]$/, "");
    return this.ajax.getJSON(method);
  }

  /**
   * Gets the Calculations for a specific employment relation.
   * @param employmentId Identifier for the Employment relation.
   * @returns A Promise with Calculations.
   */
  public getForEmployment(employmentId: string): Promise<Calculation[]> {
    return this.ajax.getJSON(`${this.baseUrl}/employment/${employmentId}`);
  }

  /**
   * Gets the Calculations for the given employment relations.
   * @param employmentIds Identifiers for the Employment relations.
   * @returns A Promise with Calculations.
   */
  public getForEmployments(employmentIds: string[]): Promise<Calculation[]> {
    if (employmentIds == null || employmentIds.length === 0) {
      return Promise.resolve([]);
    }
    let method = `${this.baseUrl}/employment/set?`;
    employmentIds.forEach((x) => { method += "employmentIds=" + encodeURIComponent("" + x) + "&"; });
    method = method.replace(/[?&]$/, "");
    return this.ajax.getJSON(method);
  }

  /**
   * Updates a calculation from employment relation by deleting all the rows and re-setting the employment.
   * @param calc Calculation to update.
   * @param save If true, saves the calculation. If false, only recalculates. Returns the result in any case.
   * @param updateRows If true, updates also the default salary rows from employment data to calculation.
   * False updates only other employment data IBAN, phone, E-mail, pension insurance code message etc.
   * @returns The updated and saved or recalculated calculation
   */
  public updateFromEmployment(calc: Calculation, save: boolean, updateRows: boolean): Promise<Calculation> {
    return this.ajax.postJSON(`${this.baseUrl}/update-from-employment?save=${save}&updateRows=${updateRows}`, calc);
  }

  /**
   * Updates (saves) the employment related information from Calculation to Employment relation.
   * Currently only updates rows, OccupationCode and WorkDescription.
   * @param calc Calculation to update.
   * @returns The updated and saves employment relation
   */
  public updateToEmployment(calc: Calculation): Promise<EmploymentRelation> {
    return this.ajax.postJSON(`${this.baseUrl}/update-to-employment`, calc);
  }

  /**
   * Shares the calculation.
   * @param id Id for the calculation to share.
   * @param type Type of the sharing action or link.
   * @param employerId Id of the employer to share the calculation with.
   * @param to Email address of the employer.
   * @param cc Cc address for the email.
   * @param message Message to the employer.
   * @returns Sharing object of the calculation.
   */
  public share(id: string, type: SharingUriType = SharingUriType.Undefined, employerId: string = null, to: string = null, cc: string = null, message: string = null): Promise<CalculationSharing> {
    let method = `${this.baseUrl}/sharing/${id}?type=${encodeURIComponent(type)}${employerId ? "&employerId=" + encodeURIComponent(employerId) : ""}`;
    method += `${to ? "&to=" + encodeURIComponent(to) : ""}${cc ? "&cc=" + encodeURIComponent(cc) : ""}${message ? "&message=" + encodeURIComponent(message) : ""}`;
    return this.ajax.postJSON(method, null);
  }

  /**
   * Notifies the employer from the shared calculation.
   * @param id Id of the shared calculation.
   * @param to Email address of the employer.
   * @param cc Cc address for the email.
   * @param message Message to the employer.
   * @returns True, if the send action was successful.
   */
  public notifySharing(id: string, to: string = null, cc: string = null, message: string = null): Promise<boolean> {
    let method = `${this.baseUrl}/sharing/${id}/notification?`;
    method += `${to ? "&to=" + encodeURIComponent(to) : ""}${cc ? "&cc=" + encodeURIComponent(cc) : ""}${message ? "&message=" + encodeURIComponent(message) : ""}`;
    return this.ajax.postJSON(method, null);
  }

  /**
   * Cancels the invoice or payment processing for the given calculation.
   * Please note that this is possible only if the channel supports the cancel action.
   * @param id Calculation for which the cancellation is requested.
   * @returns Calculation which have been cancelled.
   */
  public cancel(id: string): Promise<Calculation> {
    const url = `${this.baseUrl}/cancel/${id}`;
    return this.ajax.getJSON(url);
  }

  /** Available object mappers for import/export */
  public getObjectMappers(): Array<Mapper<Calculation, DataRow>> {
    const map: Map<Calculation, CalculationsDataRow> = {
      keys: ["id", "personalId", "salaryDate"],
      id: "id",
      status: "workflow.status",
      personalId: "worker.paymentData.socialSecurityNumber",
      occupationCode: "info.occupationCode",
      periodStart: "info.workStartDate",
      periodEnd: "info.workEndDate",
      workDescription: "info.workDescription",
      salarySlipMessage: "info.salarySlipMessage",
      projectNumber: "info.projectNumber",
      costCenter: "info.costCenter",
      salaryDate: "workflow.salaryDate",
      fixedTaxAmount: "worker.tax.fixedTaxAmount",
      rows: {
        "rows": {
          rowType: "rowType",
          message: "message",
          count: "count",
          price: "price",
        },
      },
    };
    const defaultMapper = new Mapper<Calculation, CalculationsDataRow>(map, new Calculations(null).getBlank());
    defaultMapper.rowType = "CalculationsDataRow";
    defaultMapper.description = "Vakioaineisto";
    return [defaultMapper as any];
  }

  /** Available list item mappers for import/export */
  public getListItemMappers(): Array<Mapper<ApiListItem, DataRow>> {

    const paths = [
      "status",
      "otherPartyInfo.avatar.displayName",
      "otherPartyInfo.officialId",
      "data.workStartDate",
      "data.workEndDate",
      "data.shortText",
      "salaryDate",
      "data.workerPayment",
      "data.taxPayment",
      "grossSalary"];
    const defaultMapper = Mappers.pathMapper(paths);
    defaultMapper.rowType = "ApiListItem";
    defaultMapper.description = "Esikatselu";
    defaultMapper.isListItemMapper = true;
    return [defaultMapper];
  }

  /**
   * Validates the item using a server side call.
   *
   * @param itemToValidate - The item that is validated.
   * @returns A Promise with result data containing validation property.
   */
  public validate(itemToValidate: Calculation): Promise<Calculation> {
    return Promise.resolve(itemToValidate);
  }

  /**
   * Returns the validation object for the validated object.
   * @param validatedItem The validated object with a validation result.
   */
  public getValidationResult(validatedItem: Calculation): ApiValidation { // eslint-disable-line @typescript-eslint/no-unused-vars
    return { isValid: true};
  }

  /**
   * Saves the IR -related data of the calculation.
   * This operation can be done even if the calculation otherwise is in read only state.
   * @param calculation Calculation object to save.
   */
  public saveIr(calculation: Calculation): Promise<Calculation> {
    return this.ajax.postJSON(`${this.baseUrl}/ir`, calculation);
  }

  /**
   * Converts the given calculation to an earnings payment report.
   * @param calculation - Calculation object.
   * @returns A Promise with earnings payment report data.
   */
  public convertToEpr(calculation: Calculation): Promise<irepr.EarningsPayment> {
    return this.ajax.postJSON(`${this.baseUrl}/epr`, calculation);
  }

  /**
   * Returns the earnings payment report for the given calculation.
   * @param id Id of the calculation for the earnings payment report.
   * @returns Earnings payment report for the calculation.
   */
  public getEpr(id: string): Promise<irepr.EarningsPayment> {
    return this.ajax.getJSON(`${this.baseUrl}/epr/${id}`);
  }
}
