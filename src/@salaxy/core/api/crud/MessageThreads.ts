import { Message, MessageFrom, MessageThread } from "../../model";
import { Ajax, CrudApiBase } from "../infra";

/**
 * Provides CRUD access for Message threads.
 */
export class MessageThreads extends CrudApiBase<MessageThread> {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/messages";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /** Gets a new blank message thread. */
  public getBlank(): MessageThread {
    return {
      messages: [],
    };
  }

  /**
   * Adds a new message with text.
   * @param id Identifier of the messages thread.
   * @param message The message to add.
   */
  public addMessage(id: string, message: Message): Promise<MessageThread> {
    const method = `${this.baseUrl}/${id}/messages`;
    return this.ajax.postJSON(method, message);
  }

  /**
   * Gets the upload URL for attachment files of a message thread.
   * This URL is compatible with the HTML forms standard multipart/form-data upload.
   * @param id Identifier of the messages thread.
   * @returns The URL for multipart/form-data upload
   */
  public getUploadUrl(id: string): string {
    return `${this.ajax.getServerAddress()}${this.baseUrl}/${id}/files`;
  }

  /**
   * Marks all messages as read by a given party
   * @param id Identifier of the thread
   * @param readBy The party for which the messages are marked as read.
   * @returns The entire thread with the messages marked as read.
   */
  public markAsRead(id: string, readBy: MessageFrom): Promise<MessageThread> {
    const method = `${this.baseUrl}/${id}/read-by/${readBy}`;
    return this.ajax.postJSON(method, null);
  }

  /**
   * Sends an email notification to the owner of the message thread.
   * @param id Identifier of the thread
   * @param subject Subject of the notification.
   * @param body Body of the notification.
   * @returns A boolean indicating if the email was sent successfully.
   */
  public sendNotification(id: string, subject: string, body: string): Promise<MessageThread> {
    const method = `${this.baseUrl}/${id}/notifications?subject=${encodeURIComponent(subject)}`;
    return this.ajax.postJSON(method, JSON.stringify(body));
  }
}
