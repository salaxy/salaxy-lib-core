import { Invoice, InvoiceStatus, InvoiceStatusNotification, PaymentChannel } from "../../model";
import { DatelyObject, Dates, ODataQueryOptions, ODataResult } from "../../util";
import { Ajax, CrudApiBase } from "../infra";
import { Invoices } from "./Invoices";

/**
 * Payment channels are methods that hanlde salary payments:
 * Paytrail / Palkkaus.fi Customer funds account handling, Finvoice, Accounting software, Banks Web Services etc.
 * This controller is for those channels to fetch Invoices and other data as well as to update payment feedback back to Palkkaus.fi.
 * The services can only be used by registered payment channels: Ask us if you wish to register.
 */
export class PaymentChannelApi extends CrudApiBase<Invoice> {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL is set by the constructor because it is using the paymentChannel as part of URL. */
  protected baseUrl: string = null;

  /** Creates a new API class for a given PaymentChannel */
  constructor(private paymentChannel: PaymentChannel, ajax: Ajax) {
    super(ajax);
    this.baseUrl = `/v03-rc/api/payment-channel/${paymentChannel}`;
  }

  /**
   * NOTE: Direct Save is currently not supported.
   * Use createInvoices and updateStatus instead.
   * This method is only for compatibility with CrudApiBase and in expectation of future needs (non-salary invoices).
   * @param itemToSave - The item that is updated/inserted.
   * @throws Not supported
   */
  public save(itemToSave: Invoice): Promise<Invoice> { // eslint-disable-line @typescript-eslint/no-unused-vars
    throw new Error("Not supported.");
  }

  /**
   * Client-side (synchronous) method for getting a new blank item as bases for UI binding.
   * NOTE: At the moment, this method is pretty useless as you cannot save invoice items:
   * You need to create them using createInvoice() method based on a calculation.
   */
  public getBlank(): Invoice {
    const invoice = new Invoices(this.ajax).getBlank();
    invoice.header.channel = this.paymentChannel;
    return invoice;
  }

  /**
   * Gets invoices with their full payload - including PDF content - filtering by a status.
   * NOTE: This is designed mainly for long-running batch processes for reading unread Invoices.
   * They can read the items in one read. For browser process we suggest OData query (with list item)
   * and then fetching items one-by-one.
   * This is for more http-queries, but each taking a shorter time so you can use )
   * @param status Status filter for the query. Default is unread.
   * @param start Optional start date for the period to query the invoices. If not set, defaults to last 14 days.
   * @param end Optional end date for the period to query the invoices. If not set, today is used.
   * @returns Invoices which have been created in the Invoices queue.
   */
  public getInvoicesByStatus(status: InvoiceStatus = InvoiceStatus.Unread, start: DatelyObject = null, end: DatelyObject = null): Promise<Invoice[]> {
    if (this.paymentChannel) {
      start = Dates.asDate(start);
      end = Dates.asDate(end);
      const url = `${this.baseUrl}/status/${status}?start=${start || "null"}&end=${end || "null"}`;
      return this.ajax.getJSON(url);
    } else {
      return Promise.resolve<Invoice[]>([]);
    }
  }

  /**
   * Makes an OData query to the service's main OData method.
   * @param query The options for the query: Filter, search sort etc.
   * This is either a strongly typed object or query string that is added directly.
   */
  public getOData(query: ODataQueryOptions | string): Promise<ODataResult<any>> {
    if (this.paymentChannel) {
      return super.getOData(query);
    }
    return Promise.resolve<ODataResult<any>>({items: [], count: 0});
  }

  /**
   * Gets all the items of a given type.
   * @throws Not supported
   */
  public getAll(): Promise<Invoice[]> {
    throw new Error("Not supported.");
  }

  /**
   * Gets a single item based on identier
   * @param id - Unique identifier for the object
   * @returns A Promise with result data.
   */
  public getSingle(id: string): Promise<Invoice> {
    if (this.paymentChannel) {
      return super.getSingle(id);
    }
    return Promise.resolve<Invoice>(null);
  }

  /**
   * Updates the payment statuses of invoices.
   * @param data Array of invoices to update.
   */
  public updateStatus(data: InvoiceStatusNotification[]): Promise<Invoice> {
    const url = `${this.baseUrl}/update-status`;
    return this.ajax.postJSON(url, data);
  }

  /**
   * Returns content (PDF) url for an invoice.
   * @param id The id of the invoice.
   * @param inline If true, returns the content as inline (disposition) in browser.
   * If false, the content is returned as an attachment (download).
   */
  public getContentUrl(id: string, inline = false): string {
    return `${this.ajax.getServerAddress()}${this.baseUrl}/${id}/content?inline=${inline}`
      + `&access_token=${encodeURIComponent(this.ajax.getCurrentToken())}`;
  }
}
