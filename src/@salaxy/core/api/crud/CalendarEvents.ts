import { CalendarEvent } from "../../model";
import { Dates } from "../../util";
import { Ajax } from "../infra/Ajax";
import { CrudApiBase } from "../infra/CrudApiBase";

/**
 * Provides CRUD access for Calendar events and other calendar methods.
 */
export class CalendarEvents extends CrudApiBase<CalendarEvent> {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/calendar/events";

  /** URL for top-level (non calendar event) methods. */
  private calendarBaseUrl = "/v03-rc/api/calendar"

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
  public getBlank(): CalendarEvent {
    return {
      event: {
        isAllDay: true, // By default, we have all-day events of one day.
        start: Dates.getToday(),
        end: Dates.getToday(),
        duration: "1",
        actions: [],
        attachments: [],
        attendees: [],
        categories: [],
        recurrenceRules: [],
      },
      bo: {},
      workflowData: {
        events: [],
      }
    };
  }

  /**
   * Recalculates the occurence info based on the event.
   * This is an anonymous method that does not commit anything to database.
   * @param event Calendar event to recalculate.
   * @returns A Promise with result data: The calendar event with occurenceInfo recalculated.
   */
   public recalculate(event: CalendarEvent): Promise<CalendarEvent> {
    return this.ajax.postJSON(`${this.baseUrl}/recalculate`, event);
  }
}
