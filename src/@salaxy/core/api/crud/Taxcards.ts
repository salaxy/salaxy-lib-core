import { DataRow, Map, Mapper, Mappers,  TaxCard2019Logic, TaxcardsDataRow } from "../../logic";
import { ApiListItem, ApiValidation, SharedTaxcardExistsResult, Taxcard, TaxcardApprovalMethod, TaxcardCalcDiffCheck, TaxcardListItem, TaxcardResult, WorkerCurrentTaxcards } from "../../model";
import { DatelyObject, Dates, Numeric, OData, ODataQueryOptions, ODataResult } from "../../util";
import { Ajax, CrudApiBase, ImportableCrudApi } from "../infra";

/**
 * Provides CRUD functionality as well as business logic for storing and using Finnish Taxcards and calculating Withholding tax.
 */
export class Taxcards extends CrudApiBase<Taxcard> implements ImportableCrudApi<Taxcard> {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/taxcards";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /**
   * Sets the auto-taxcard functionality on for the given personal ID.
   * Auto-taxcard fetches the widtholding data automatically from the tax authorities API every time the salary is paid.
   * NOTE: In current implementation, the income type is always Salary (00024: Palkkaa).
   * @param personalId The Personal ID for the person to whom to set the taxcard as auto.
   * @param firstSalaryDate First salary date if known: If this is close enought to the actual salary date we do not need to refetch the taxcard from the authorities.
   * @returns The saved new automatic taxcard.
   * If null, the tax authorities do not have a record for this personal id: It must be handled manually (cannot be set as shared).
   */
  public setAuto(personalId: string, firstSalaryDate: DatelyObject = null): Promise<Taxcard> {
    firstSalaryDate = Dates.asDate(firstSalaryDate);
    return this.ajax.postJSON(`${this.baseUrl}/auto-set?personalId=${personalId}&salaryDate=${firstSalaryDate}`, null);
  }

  /**
   * Checks automatic taxcards from the tax authorities. If the taxcard has been checked within 2 days, does nothing.
   * If there are no changes, updates the LastChecked date only. If there are changes, creates a new taxcard container and returns it instead.
   * @param taxcardIds List of identifiers of taxcards that should be checked.
   * @param salaryDate Salary date for which the taxcard is checked.
   * @param forceUpdate If true, skips the 2 day rule and always checks from the tax athorities API.
   * @returns The refreshed taxcards.
   */
  public refreshAuto(taxcardIds: string, salaryDate: DatelyObject = null, forceUpdate = false) : Promise<Taxcard> {
    salaryDate = Dates.asDate(salaryDate);
    return this.ajax.postJSON(`${this.baseUrl}/auto-refresh?&taxcardIds=${taxcardIds || ""}&salaryDate=${salaryDate || ""}&forceUpdate=${forceUpdate}`, null);
  }

  /**
   * Client-side (synchronous) method for getting a new blank item as bases for UI binding.
   * You need to set the Personal ID to bind the taxcard to an account.
   */
  public getBlank(): Taxcard {
    return TaxCard2019Logic.getBlank(null);
  }

  /**
   * Saves a taxcard to the storage.
   * Removes empty incomeLog entries because they may be added by data binding in UI.
   * Entries are currently considered empty if both id and income are falsy: Typically income should be there,
   * but taxable income may be zero on a calculation (entry with id) and it makes sense to keep the entry still.
   * @param itemToSave - The item that is updated/inserted.
   * @returns A Promise with result data as saved to the storage (contains id, createAt, owner etc.).
   */
  public save(itemToSave: Taxcard): Promise<Taxcard> {
    itemToSave.incomeLog = itemToSave.incomeLog.filter((x) => x.id || x.income);
    return super.save(itemToSave);
  }

  /**
   * Commits the current DIFF to the saved item.
   * Diff is the difference of income log saved in the taxcard to real paid calculations in the storage..
   * It is returned in each Save / GetSingle.
   * Modifications to non-modified items are also saved, but modifications to diff items have no effect: Diff is fetched from server.
   * Removes empty incomeLog entries because they may be added by data binding in UI (see Save for details).
   * @param itemToSave - The item that is updated/inserted with Diff saved.
   * @returns A Promise with result data as saved to the storage (contains id, createAt, owner etc.).
   */
  public commitDiff(itemToSave: Taxcard): Promise<Taxcard> {
    itemToSave.incomeLog = itemToSave.incomeLog.filter((x) => x.id || x.income);
    return this.ajax.postJSON(`${this.baseUrl}?calcDiffCheck=${TaxcardCalcDiffCheck.CommitDiff}`, itemToSave);
  }

  /**
   * Sets the approval for a shared taxcard.
   * @param uri - Shared taxcard is owned by Worker, so you need to pass a URI which has the owner as part of the URI.
   * @param method Approval / Reject method.
   * @returns Employer owned copy of the taxcard after approval / reject.
   */
  public approveShared(uri: string, method: TaxcardApprovalMethod): Promise<Taxcard> {
    return this.ajax.postJSON(`${this.baseUrl}/approve/${method}?sharedUri=${encodeURIComponent("" + uri)}`, {});
  }

  /**
   * Sets the approval for the Owned copy of the taxcard.
   * @param id Id of the local copy owned by Employer / current account (Statuses SharedWaiting, SharedRejected or SharedRejectedWithoutOpen).
   * @param method Approval / Reject method.
   * @returns Employer owned copy with the new approval status and other changes.
   */
  public approveOwned(id: string, method: TaxcardApprovalMethod): Promise<Taxcard> {
    return this.ajax.postJSON(`${this.baseUrl}/approve/${method}?ownedId=${encodeURIComponent("" + id)}`, {});
  }

  /**
   * Calculates the widhholding tax based on a given tax card.
   *
   * @param id - Identifier of the tax card.
   * @param income - New salary or other income, for which the widthholding tax is calculated.
   *
   * @returns A Promise with result data (TaxCardResult).
   */
  public calculateWithId(id: string, income: number): Promise<TaxcardResult> {
    const method = `${this.baseUrl}/calculate/${encodeURIComponent("" + id)}?income=${Numeric.parseNumber(income)}`;
    return this.ajax.getJSON(method);
  }

  /**
   * Calculates the widhholding tax based on a given tax card
   *
   * @param income - New salary or other income, for which the widthholding tax is calculated.
   * @param taxCard - Tax card based on which the widthholding tax is calculated. This method will not connect to database in any way so the taxcard may or may not be stored.
   *
   * @returns A Promise with result data (TaxCardResult).
   */
  public calculate(income: number, taxCard: Taxcard): Promise<TaxcardResult> {
    const method = `${this.baseUrl}/calculate?income=${Numeric.parseNumber(income)}`;
    return this.ajax.postJSON(method, taxCard);
  }

  /**
   * Gets all the current taxcards for the current Worker account:
   * The cards that the account has uploaded as well as the cards created by all Employers.
   * This method can only be run for a personal account (not for a Company account).
   * @returns The cards that the account has uploaded as well as the cards created by all Employers.
   * Only current cards (not expired) are provided.
   */
  public getMyTaxcards(): Promise<WorkerCurrentTaxcards> {
    const method = `${this.baseUrl}/my`;
    return this.ajax.getJSON(method);
  }

  /**
   * Gets all the current taxcards for an employment relation:
   * All the cards by the Employer, but also the latest card uploaded by the Worker herself.
   * @param employmentId The employment id for which to search the taxcards.
   * @param paymentDate Payment date for which the validity (current card in use) is calculated. If null, Next workday is used.
   * This may be changed to next default payment date without that being a breaking change.
   * @returns The cards created by the Employer and also the latest card uploaded by the Worker herself.
   * Only current cards (not expired) are provided.
   */
  public getEmploymentTaxcards(employmentId: string, paymentDate: DatelyObject): Promise<WorkerCurrentTaxcards> {
    let method = `${this.baseUrl}/employment/${employmentId}`;
    const date = Dates.asDate(paymentDate);
    if (date) {
      method += "?paymentDate=" + date;
    }
    return this.ajax.getJSON(method);
  }

  /**
   * For an employment relationship that is not yet created, allows checking if there is a shared taxcard.
   * Returns true if there is a shared taxcard available for the given Personal ID.
   * NOTE: There are unspecified and potentially changing restrictions on how many times this interface can be called to remove the risk of Phishing.
   * If such restriciton hits, the IsError property will be true. In this case, you should currently create the employment relation and fetch the taxcard info after that.
   * We may later introduce a "Are you robot" type of interface to go around this (if necessary).
   * @param personalId Personal ID of the Worker that will be added.
   * @returns Anonymous information of the shared taxcard if found. Check exists-property.
   */
  public getSharedTaxcardExists(personalId: string): Promise<SharedTaxcardExistsResult> {
    const method = `${this.baseUrl}/shared-exists/${personalId}`;
    return this.ajax.getJSON(method);
  }

  /**
   * Returns the url where to post the tax card file using form post ('multipart/form-data').
   */
  public getUploadUrl(): string {
    return `${this.ajax.getServerAddress()}${this.baseUrl}/file`;
  }

  /**
   * Returns the url where to get the tax card file with authentication token.
   */
  public getDownloadUrl(taxCard: Taxcard): string {
    if (taxCard && taxCard.card && taxCard.card.fileUri) {
      return `${this.ajax.getServerAddress()}${this.baseUrl}/file/${taxCard.id}?access_token=${this.ajax.getCurrentToken()}`;
    }
    return null;
  }

  /**
   * Returns the url where to get the tax card preview image with authentication token.
   * @param taxcard Either an API object or list item for which the preview is shown.
   * For Workers, supports Employer owned taxcards by passing owner ID in addition to just ID.
   */
  public getPreviewUrl(taxcard: Taxcard | TaxcardListItem | ApiListItem): string {
    if (((taxcard as Taxcard).card || {} as any).previewUri || ((taxcard as ApiListItem).data || {} as any).previewUri) {
      return `${this.ajax.getServerAddress()}${this.baseUrl}/preview/${taxcard.id}@${taxcard.owner}?access_token=${this.ajax.getCurrentToken()}`;
    }
    return null;
  }

  /**
   * Gets a list of all non-terminated employments of the employer and resolves their current taxcards.
   * The current taxcards may be for the current year or for next year if the current date is either December or January.
   * @param query The options for the query: Filter, search sort etc.
   * This is either a strongly typed object or query string that is added directly.
   * @param paymentDate Payment date for which the validity (current card in use) is calculated. If null, Next workday is used.
   * This may be changed to next default payment date without that being a breaking change.
   */
  public getCurrentCards(query: ODataQueryOptions | string, paymentDate: DatelyObject = null ): Promise<ODataResult<any>> {
    const date = Dates.asDate(paymentDate);
    const url = "/v03-rc/api/taxcards/current" + (date ? ("/" + date) : "");
    return OData.getOData<any>(url, query, this.ajax);
  }

  /** Available object mappers for import/export */
  public getObjectMappers(): Array<Mapper<Taxcard, DataRow>> {
    const map: Map<Taxcard, TaxcardsDataRow> = {
      id: "id",
      personalId: "card.personalId",
      forYear: "card.forYear",
      start: "card.validity.start",
      end: "card.validity.end",
      taxPercent: "card.taxPercent",
      incomeLimit: "card.incomeLimit",
      taxPercent2: "card.taxPercent2",
      kind: "card.kind",
    };
    const defaultMapper = new Mapper<Taxcard, TaxcardsDataRow>(map, new Taxcards(null).getBlank());
    defaultMapper.rowType = "TaxcardsDataRow";
    defaultMapper.description = "Vakioaineisto";
    return [defaultMapper as any];
  }

  /** Available list item mappers for import/export */
  public getListItemMappers(): Array<Mapper<ApiListItem, DataRow>> {

    const paths = [
      "otherPartyInfo.avatar.displayName",
      "otherPartyInfo.officialId",
      "data.forYear",
      "data.start",
      "data.end",
      "data.taxPercent",
      "data.incomeLimit",
      "data.taxPercent2",
      "data.kind" ];
    const defaultMapper = Mappers.pathMapper(paths);
    defaultMapper.rowType = "ApiListItem";
    defaultMapper.description = "Esikatselu";
    defaultMapper.isListItemMapper = true;
    return [defaultMapper];
  }

 /**
  * Validates the item using a server side call.
  *
  * @param itemToValidate - The item that is validated.
  * @returns A Promise with result data containing validation property.
  */
 public validate(itemToValidate: Taxcard): Promise<Taxcard> {
   return Promise.resolve(itemToValidate);
 }

 /**
  * Returns the validation object for the validated object.
  * @param validatedItem The validated object with a validation result.
  */
 public getValidationResult(validatedItem: Taxcard): ApiValidation { // eslint-disable-line @typescript-eslint/no-unused-vars
    return { isValid: true};
 }

}
