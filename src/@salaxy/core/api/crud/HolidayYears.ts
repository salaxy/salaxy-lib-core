import { HolidaysLogic } from "../../logic";
import { HolidaySpecification, HolidaySpecificationForYear, HolidayYear } from "../../model";
import { Ajax, CrudApiBase } from "../infra";

/**
 * Provides CRUD access for Holiday Years as a raw list and
 * more typically as holiday years relate to an employment relation.
 */
export class HolidayYears extends CrudApiBase<HolidayYear> {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/holidays";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
  public getBlank(): HolidayYear {
    return HolidaysLogic.getBlank();
  }

  /**
   * Gets all the Holiday Year objects of specified year.
   * @param year The holiday year to filter
   * @returns A Promise with result data: The full holiday year objects.
   */
   public getForYear(year: number): Promise<HolidayYear[]> {
    return this.ajax.getJSON(`${this.baseUrl}/year/${year.toFixed()}`);
  }

  /**
   * Gets a default holiday specification based on employment relation information.
   * This is presented to the user before calling initForEmployment().
   * @param employmentId The employment relation ID from which the defaults are fetched.
   * @returns Holiday specification that can be used for inititalization.
   */
  public getDefaultSpecForEmployment(employmentId: string): Promise<HolidaySpecificationForYear>  {
    const url = `${this.baseUrl}/employment/${employmentId}/default-spec`;
    return this.ajax.getJSON(url);
  }

  /**
   * Initializes (or re-creates) the holiday years for an employment relation.
   * @param employmentId Identifier of the Employment relation that stores the holiday specification.
   * @param holidaySpec The holiday specification tht should be used for inititalization. The spec is also stored to the Employment relation.
   * @returns Holiday years for this employment relation - as initialized.
   */
  public initForEmployment(employmentId: string, holidaySpec: HolidaySpecification): Promise<HolidayYear[]>  {
    const url = `${this.baseUrl}/employment/${employmentId}`;
    return this.ajax.postJSON(url, holidaySpec);
  }

  /**
   * Gets the Holiday Years for a specific employment relation.
   * @param employmentId Identifier for the Employment relation.
   * @returns A Promise with Holiday Years.
   */
  public getForEmployment(employmentId: string): Promise<HolidayYear[]> {
    return this.ajax.getJSON(`${this.baseUrl}/employment/${employmentId}`);
  }

  /**
   * Gets the Holiday Years for the given employment relations.
   * @param employmentIds Identifiers for the Employment relations.
   * @returns A Promise with Holiday Years.
   */
  public getForEmployments(employmentIds: string[]): Promise<HolidayYear[]> {
    if (employmentIds == null || employmentIds.length === 0) {
      return Promise.resolve([]);
    }
    let method = "${this.baseUrl}/employment/set?";
    employmentIds.forEach((x) => { method += "employmentIds=" + encodeURIComponent("" + x) + "&"; });
    method = method.replace(/[?&]$/, "");
    return this.ajax.getJSON(method);
  }
}
