import { DataRow, Map, Mapper, Mappers,  WorkerLogic, WorkersDataRow } from "../../logic";
import { ApiListItem, ApiValidation, WorkerAccount } from "../../model";
import { Ajax, CrudApiBase, ImportableCrudApi } from "../infra";

/**
 * Provides CRUD access to Workers (employees) created by the employer
 * or to whom the employer has paid salaries.
 */
export class Workers extends CrudApiBase<WorkerAccount> implements ImportableCrudApi<WorkerAccount> {
  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/accounts/workers";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
  public getBlank(): WorkerAccount {
    return WorkerLogic.getBlank();
  }

  /** Saves the Worker account ot the storage */
  public save(itemToSave: WorkerAccount): Promise<WorkerAccount> {
    return super.save(itemToSave);
  }

  /** Available object mappers for import/export */
  public getObjectMappers(): Array<Mapper<WorkerAccount, DataRow>> {
    const map: Map<WorkerAccount, WorkersDataRow> = {
      id: "id",
      firstName: "avatar.firstName",
      lastName: "avatar.lastName",
      email: "contact.email",
      telephone: "contact.telephone",
      ibanNumber: "ibanNumber",
      officialId: "officialPersonId",
    };
    const defaultMapper = new Mapper<WorkerAccount, WorkersDataRow>(map, new Workers(null).getBlank());
    defaultMapper.rowType = "WorkersDataRow";
    defaultMapper.description = "Vakioaineisto";
    return [defaultMapper as any];
  }

  /** Available list item mappers for import/export */
  public getListItemMappers(): Array<Mapper<ApiListItem, DataRow>> {

    const paths = [
      "otherPartyInfo.avatar.firstName",
      "otherPartyInfo.avatar.lastName",
      "otherPartyInfo.email",
      "otherPartyInfo.telephone",
      "otherPartyInfo.ibanNumber",
      "otherPartyInfo.officialId" ];
    const defaultMapper = Mappers.pathMapper(paths);
    defaultMapper.rowType = "ApiListItem";
    defaultMapper.description = "Esikatselu";
    defaultMapper.isListItemMapper = true;
    return [defaultMapper];
  }

 /**
  * Validates the item using a server side call.
  *
  * @param itemToValidate - The item that is validated.
  * @returns A Promise with result data containing validation property.
  */
 public validate(itemToValidate: WorkerAccount): Promise<WorkerAccount> {
    return Promise.resolve(itemToValidate);
 }

 /**
  * Returns the validation object for the validated object.
  * @param validatedItem The validated object with a validation result.
  */
 public getValidationResult(validatedItem: WorkerAccount): ApiValidation {
    return validatedItem.validation || { isValid: true};
 }
}
