import { EmploymentContract } from "../../model";
import { Ajax, CrudApiBase } from "../infra";

/**
 * Provides CRUD access to employment contracts.
 * NOTE: Currently, this API is read-only.
 */
export class EmploymentContracts extends CrudApiBase<EmploymentContract> {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/employment-contracts";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /**
   * Client-side (synchronous) method for getting a new blank item as bases for UI binding.
   * NOTE: Currently, this API is read-only => Method is not supported.
   */
  public getBlank(): any {
    throw new Error("Not supported.");
  }
}
