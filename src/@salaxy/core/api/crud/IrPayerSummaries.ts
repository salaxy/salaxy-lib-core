import { irpsr } from "../../model";
import { Ajax, CrudApiBase } from "../infra";

/**
 * Provides CRUD access for authenticated user to access a his/her own Earnings Payments objects
 */
export class IrPayerSummaries extends CrudApiBase<irpsr.PayerSummary> {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v01/api/psr";

  constructor(ajax: Ajax) {
    super(ajax);
    this.baseUrl = `${ajax.getServerAddress()}/v01/api/psr`;
  }

  /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
  public getBlank(): irpsr.PayerSummary {
    return {
      deliveryData: null
    };
  }

  /** Get odata base url */
  public getODataUrl(): any {
    return this.baseUrl;
  }

/** Validate */
  public validate (earningsPayment : irpsr.PayerSummary): Promise<irpsr.PayerSummary>  {
    return this.ajax.postJSON(`${this.baseUrl}/validate`, earningsPayment);
  }

  /** Send schedule item to Incomes register queue */
  public sendSchedule (aso: irpsr.IrApiScheduleObject): Promise<irpsr.IrApiScheduleObject>  {
    return this.ajax.postJSON(`${this.baseUrl}/schedule`, aso);
  }

  /** Get Service Bus queue item */
  public getIrQueueItem (id: string): Promise<irpsr.IrApiScheduleObject> {
    return this.ajax.getJSON(`${this.baseUrl}/${id}/schedule`);
  }

  /** Remove Service Bus queue item */
  public removeIrQueueItem (id: string): Promise<any> {
    return this.ajax.remove(`${this.baseUrl}/${id}/schedule`);
  }
}
