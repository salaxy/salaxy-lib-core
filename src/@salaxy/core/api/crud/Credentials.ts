import { SessionUserCredential } from "../../model";
import { Ajax, CrudApiBase } from "../infra";

/**
 * Provides CRUD access for authenticated user to access all credentials which can access this account.
 */
export class Credentials extends CrudApiBase<SessionUserCredential> {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/accounts/credential";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /**
   * Returns an empty credential without any values.
   */
  public getBlank(): SessionUserCredential {
    return {};
  }

  /**
   * Returns the url where to post the avatar file
   */
  public getAvatarUploadUrl(credentialId: string): string {

    return this.ajax.getApiAddress()
    +  this.baseUrl
    + (credentialId ? `/${credentialId}` : "" )
    + "/avatar";
  }

}
