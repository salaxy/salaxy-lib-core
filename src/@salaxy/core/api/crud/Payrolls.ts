import { Calculation, Payment, PaymentMethod, PayrollDetails } from "../../model";
import { Ajax, CrudApiBase } from "../infra";

/**
 * Provides CRUD access for authenticated user to access a his/her own PayrollDetails objects
 */
export class Payrolls extends CrudApiBase<PayrollDetails> {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/payroll";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
  public getBlank(): PayrollDetails {
    return {
      input: {
        period : {},
        calculations: [],
      },
      info: {},
      calcs: [],
      validations: [],
      workflowData: {},
    };
  }

  /** Because of historical reasons, Payrolls v03 has a non-standard endpoint for OData methods. */
  public getODataUrl(): string {
    return this.baseUrl + "/odata";
  }

  /**
   * Validates a PayrollDetails object for payment.
   * @param payrollToValidate PayrollDetails that should be validated.
   * @returns The PayrollDetails object with Calculations added to the Result object and Validated.
   */
  public validate(payrollToValidate: PayrollDetails): Promise<PayrollDetails> {
    return this.ajax.postJSON(`${this.baseUrl}/validate`, payrollToValidate);
  }

  /**
   * Starts the payment process for the payroll.
   * Creates and saves the calculations as well as the new payment object.
   * Updates the status of calculations and payroll to paymentStarted.
   * @param payroll PayrollDetails which is going to be paid.
   * @param paymentMethod - Payment method.
   * @param successUrl - Return address after successful payment
   * @param cancelUrl - Return address after cancelled payment
   * @param sendEmails - Boolean indicating whether to send emails in the workflow
   * @returns The PayrollDetails object with payment id.
   */
  public startPayment(payroll: PayrollDetails, paymentMethod: PaymentMethod, successUrl: string, cancelUrl: string, sendEmails: boolean): Promise<Payment> {
    let method = `${this.baseUrl}/payment?`;
    method += "paymentMethod=" +  encodeURIComponent("" + paymentMethod) + "&";
    method += "successUrl=" + encodeURIComponent("" + successUrl) + "&";
    method += "cancelUrl=" + encodeURIComponent("" + cancelUrl) + "&";
    method += "sendEmails=" + encodeURIComponent(sendEmails);
    return this.ajax.postJSON(method, payroll);
  }

  /**
   * Saves changes to to a Calculation that is part of a Payroll and returns the Paryoll validation data.
   * @param calc The calculation object that is updated.
   * @param payrollId Payroll ID where calculation belongs to.
   * Note, that the current implementation does not assure that the calculation is within a Payroll.
   * The ID is simply used for assuring corrrect data in Calculation.
   * @returns The Saved Payroll object with Calculations added to the Result object and Validated.
   */
  public calculationSave(calc: Calculation, payrollId: string): Promise<Calculation> {
    const method = `${this.baseUrl}/${payrollId}/calcs`;
    return this.ajax.postJSON(method, calc);
  }

  /**
   * Adds existing calculations to the the payroll and saves the payroll object.
   * If calculation is read-only, a copy is made. If it is draft, the calculation is moved.
   * @param payroll The Payroll object to update and save.
   * @param calculationIds Array of calculation id's that are copied / moved to the Payroll list.
   * @returns The Saved Payroll object with Calculations added to the Result object and Validated.
   */
  public addCalculations(payroll: PayrollDetails, calculationIds: string[]): Promise<PayrollDetails> {
    let method = `${this.baseUrl}/add-calc?`;
    method += "ids=" +  calculationIds.join(",");
    return this.ajax.postJSON(method, payroll);
  }

  /**
   * Remove calculations from the payroll and saves the payroll object.
   * The calculations are completely deleted.
   * This can only be done if the calculations and the payroll have not been paid yet.
   * @param payroll The Payroll  object.
   * @param calculationIds Array of calculation id's that are deleted.
   * @returns The Saved Payroll object with Calculations removed from the Result object and Validated.
   */
  public deleteCalculations(payroll: PayrollDetails, calculationIds: string[]): Promise<PayrollDetails> {
    let method = `${this.baseUrl}/delete-calc?`;
    method += "ids=" +  calculationIds.join(",");
    return this.ajax.postJSON(method, payroll);
  }

  /**
   * Remove calculations from the payroll and saves the payroll object.
   * The calculations are unlinked and they become individual calculations in the draft list (and can then be processed individually).
   * This can only be done if the calculations and the payroll have not been paid yet.
   * @param payroll The Payroll  object.
   * @param calculationIds Array of calculation id's that are unlinked from the Payroll list.
   * @returns The Saved Payroll object with Calculations removed from the Result object and Validated.
   */
  public unlinkCalculations(payroll: PayrollDetails, calculationIds: string[]): Promise<PayrollDetails> {
    let method = `${this.baseUrl}/unlink-calc?`;
    method += "ids=" +  calculationIds.join(",");
    return this.ajax.postJSON(method, payroll);
  }

  /**
   * Adds new calculations to the the payroll and saves the payroll object.
   * The calculations are added based on employment relation ID's.
   * @param payroll The Payroll object to update and save.
   * @param employmentIds Array of employment relation id's for which the a new calculation is added to the Payroll list.
   * @returns The Saved Payroll object with Calculations added to the Result object and Validated.
   */
  public addEmployments(payroll: PayrollDetails, employmentIds: string[]): Promise<PayrollDetails> {
    let method = `${this.baseUrl}/add-employment?`;
    method += "ids=" +  employmentIds.join(",");
    return this.ajax.postJSON(method, payroll);
  }

  /**
   * Returns the url for starting the payment process for the payroll.
   * @param payrollId Payroll which is going to be paid.
   * @param paymentMethod - Payment method.
   * @param successUrl - Return address after successful payment
   * @param cancelUrl - Return address after cancelled payment
   * @param sendEmails - Boolean indicating whether to send emails in the workflow
   * @returns The url to the Paytrail payment page.
   */
  public getPaytrailPaymentUrl(payrollId: string, paymentMethod: PaymentMethod, successUrl: string, cancelUrl: string, sendEmails: boolean): string {
    let url = this.ajax.getServerAddress() + "/SalaryPayment/PayPayroll?";
    url += "payrollId=" +  encodeURIComponent("" + payrollId) + "&";
    url += "successUrl=" + encodeURIComponent("" + successUrl) + "&";
    url += "cancelUrl=" + encodeURIComponent("" + cancelUrl) + "&";
    url += "sendEmails=" + encodeURIComponent(sendEmails) + "&";
    url += "paymentMethod=" +  encodeURIComponent("" + paymentMethod) + "&";
    url += "access_token=" +  encodeURIComponent("" + this.ajax.getCurrentToken());
    return url;
  }
}
