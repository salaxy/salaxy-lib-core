import { AccountInIndex, PrimaryPartner } from "../../model";
import { Ajax, CrudApiBase } from "../infra";

/**
 * Provides CRUD access for accounting settings
 */
export class PrimaryPartners extends CrudApiBase<PrimaryPartner> {

 /**
  * For NG1-dependency injection
  * @ignore
  */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/settings/primary-partners";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /**
   * Makes a search to YTJ.
   * @param search Search text: company name or business id.
   */
  public searchYtj(search: string): Promise<AccountInIndex[]> {
    return this.ajax.getJSON(`/v03-rc/api/settings/ytj-companies?search=${encodeURIComponent(search)}`);
  }

  /**
   * Client-side (synchronous) method for getting a new blank CRUD item as bases for UI binding.
   * The basic idea is that all the child object properties should be non-null to avoid null reference exceptions.
   * In special cases, when null is significant it may be used: e.g. Calculation.result is null untill the recalculation has been done.
   * Strings, dates and booleans may be null (false if bool), but typically just undefined.
   */
  public getBlank(): PrimaryPartner {
    throw new Error("Not supported.");
  }

  /**
   * Gets all the items of a given type.
   * @throws Not supported
   */
  public getAll(): Promise<PrimaryPartner[]> {
    throw new Error("Not supported.");
  }

  /**
   * Deletes an single item from the sotrage
   * @param id - Unique identifier for the object
   * @throws Not supported
   */
  public delete(id: string): Promise<string> { // eslint-disable-line @typescript-eslint/no-unused-vars
    throw new Error("Not supported.");
  }

  /**
   * Saves an item to the storage.
   * If id is null, this is add/insert. If id exists, this is update.
   * @param itemToSave - The item that is updated/inserted.
   * @throws Not supported
   */
  public save(itemToSave: PrimaryPartner): Promise<PrimaryPartner> { // eslint-disable-line @typescript-eslint/no-unused-vars
    throw new Error("Not supported.");
  }
}
