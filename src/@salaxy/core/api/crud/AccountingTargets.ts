import { AccountingTarget } from "../../model";
import { Ajax, CrudApiBase } from "../infra";

/**
 * Provides CRUD access for Accounting Targets
 */
export class AccountingTargets extends CrudApiBase<AccountingTarget> {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/accounting-target";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
  public getBlank(): AccountingTarget {
    return {
      info: {
        availableMethods: [],
        target: {
          avatar: {},
        },
        data: {},
      },
      currentAccount: {
        availableMethods: [],
        data: {},
      },
      ruleSet: {
        accounts: [],
        rows: [],
        targetCoA: [],
      },
      workflowData: {
        events: [],
      },
    };
  }

  /**
   * IN TEST ONLY: Regenerates the base data for the current account.
   * This method will be removed after development has ended.
   */
  public testRecreateBaseData(): Promise<string> {
    return this.ajax.postJSON(`${this.baseUrl}/regenerate`, {}) as Promise<string>;
  }
}
