import { Ajax, CrudApiBase } from "../infra";
import { OData, ODataQueryOptions, ODataResult } from "../../util";

/**
 * Provides CRUD access to employment relations.
 */
export class Employments extends CrudApiBase<any> { // TODO  -> Employment

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/employments";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
  public getBlank(): any {
    throw new Error("Not supported.");
  }

  /**
   * Returns a list of all employing relations of existing employers for the current account.
   */
  public getODataForEmployingRelations(options: ODataQueryOptions): Promise<ODataResult<any>> {
    const fullPath = OData.getUrl(`${this.baseUrl}/employing`, options);
    return this.ajax.getJSON(fullPath).then((data) => {
      return OData.getODataResult<any>(data);
    });
  }
}
