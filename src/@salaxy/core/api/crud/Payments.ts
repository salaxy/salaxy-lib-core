import { AccountingReportTableType, Payment, PaymentMethod, PaymentStatus } from "../../model";
import { Ajax, CrudApiBase } from "../infra";
/**
 * Methods for payment creation and processing.
 */
export class Payments extends CrudApiBase<Payment> {

  /**
   * For NG1-dependency injection
   * @ignore
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/payments";

  constructor(ajax: Ajax) {
    super(ajax);
  }

 /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
 public getBlank(): Payment {
  return {
    payer: { },
    recipient: { },
    businessObjects: [],
    request: {},
  };
}

  /**
   * List payments for the current user.
   *
   * @param status - Payment status filter. If null or empty, returns all statuses.
   *
   * @returns A Promise with result data (list of payments).
   */
  public getPayments(status: PaymentStatus[]): Promise<Payment[]> {
    if (!status) {
      return this.getAll();
    }
    let method = "/payments?";
    if (status) {
      status.forEach((item) => { method += "status=" + encodeURIComponent("" + item) + "&"; });
    }
    method = method.replace(/[?&]$/, "");
    return this.ajax.getJSON(method);
  }

  /**
   * Gets a single payment object based on identifier.
   *
   * @param id - Unique identifier for the payment.
   *
   * @returns A Promise with result data (Payment).
   */
  public getPayment(id: string): Promise<Payment> {
    return this.getSingle(id);
  }

  /**
   * Deletes a payment.
   *
   * @param id - Unique identifier for the payment.
   *
   * @returns A Promise with result data "Object deleted".
   */
  public deletePayment(id: string): Promise<string> {
    return this.delete(id);
  }

  /**
   * OBSOLETE
   * @deprecated Creates a new SalaryGrossPayment (startPayment will replace this method).
   * @ignore
   *
   * @param calculationIds - Array of calculation ids.
   * @param successUrl - Return address after successful payment
   * @param cancelUrl - Return address after cancelled payment
   * @param sendEmails - Boolean indicating whether to send emails in the workflow
   *
   * @returns A Promise with result data (created Payment).
   */
  public createSalaryGrossPayment(calculationIds: string[], successUrl: string, cancelUrl: string, sendEmails: boolean): Promise<Payment> {
    let method = "/payments/salaryGrossPayment?";
    calculationIds.forEach((item) => { method += "calculationIds=" + encodeURIComponent("" + item) + "&"; });
    method += "successUrl=" + encodeURIComponent("" + successUrl) + "&";
    method += "cancelUrl=" + encodeURIComponent("" + cancelUrl) + "&";
    method += "sendEmails=" + encodeURIComponent(sendEmails);
    return this.ajax.postJSON(method, calculationIds);
  }

  /**
   * Creates/updates the SalaryGrossPayment.
   * If a payment for the same calculation ids exist, it will be updated and no new payment
   * is created.
   *
   * @param calculationIds - Array of calculation ids.
   * @param paymentMethod - Payment method.
   * @param successUrl - Return address after successful payment
   * @param cancelUrl - Return address after cancelled payment
   * @param sendEmails - Boolean indicating whether to send emails in the workflow
   *
   * @returns A Promise with result data (created Payment).
   */
  public startPayment(calculationIds: string[], paymentMethod: PaymentMethod, successUrl: string, cancelUrl: string, sendEmails: boolean): Promise<Payment> {
    let method = "/payments?";
    calculationIds.forEach((item) => { method += "calculationIds=" + encodeURIComponent("" + item) + "&"; });
    method += "paymentMethod=" +  encodeURIComponent("" + paymentMethod) + "&";
    method += "successUrl=" + encodeURIComponent("" + successUrl) + "&";
    method += "cancelUrl=" + encodeURIComponent("" + cancelUrl) + "&";
    method += "sendEmails=" + encodeURIComponent(sendEmails);
    return this.ajax.postJSON(method, {});
  }

  /**
   * Experimental: Gets a new finvoice object for the given payment.
   * @param id - The payment id which the finvoice will be returned for.
   * @param tableType - Accounting table type for the generated Finvoice.
   * @ignore
   */
  public getFinvoice(id: string, tableType: AccountingReportTableType  = AccountingReportTableType.Classic): Promise<any> {
    let method = `${this.baseUrl}/${id}/finvoice?`;
    method += "tableType=" + encodeURIComponent("" + tableType);
    return this.ajax.postJSON(method, "");
  }

  /**
   * Experimental: Sends the finvoice object to operator.
   * @param finvoice - Finvoice for the delivery.
   *
   * @returns Finvoice which was sent.
   * @ignore
   */
  public sendFinvoice(finvoice: any): Promise<any> {
    const method = `${this.baseUrl}/finvoice`;
    return this.ajax.postJSON(method, finvoice);
  }

  /**
   * Returns the url for starting the payment process for the payroll.
   * @param calculationIds - Array of calculation ids.
   * @param paymentMethod - Payment method.
   * @param successUrl - Return address after successful payment
   * @param cancelUrl - Return address after cancelled payment
   * @param sendEmails - Boolean indicating whether to send emails in the workflow
   * @returns The url to the Paytrail payment page.
   */
  public getPaytrailPaymentUrl(calculationIds: string[], paymentMethod: PaymentMethod, successUrl: string, cancelUrl: string, sendEmails: boolean): string {
    let url = this.ajax.getServerAddress() + "/SalaryPayment/PayCalculations?";
    calculationIds.forEach((item) => { url += "calculationIds=" + encodeURIComponent("" + item) + "&"; });
    url += "successUrl=" + encodeURIComponent("" + successUrl) + "&";
    url += "cancelUrl=" + encodeURIComponent("" + cancelUrl) + "&";
    url += "sendEmails=" + encodeURIComponent(sendEmails) + "&";
    url += "paymentMethod=" +  encodeURIComponent("" + paymentMethod) + "&";
    url += "access_token=" +  encodeURIComponent("" + this.ajax.getCurrentToken());
    return url;
  }

}
