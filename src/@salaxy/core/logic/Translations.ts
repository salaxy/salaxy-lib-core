import { dictionaryAll } from "../i18n/dictionaryAll";
import { Objects } from "../util/Objects";
import { CoreDictionaryLang } from "./model";

/**
 * Provides universal i18n methods:
 * The core JavaScript code uses this to determine the UI texts and potentially
 * formatting. The frameworks on top of core (Angular, NG1) may have their own
 * frameworks and/or they may integrate to this core i18n.
 */
export class Translations {

  /**
   * Translates given key to the current language.
   * @param key - Key for the text.
   * @param interpolateParams - Variables for the interpolation.
   * @returns The translated value in current language or if none is found the key.
   */
  public static get(key: string, interpolateParams?: any): string {

    const template = this.getWithDefault(key, null);
    if (!template) {
      return key;
    }

    // interpolate
    const regex = /\{\{\s*(\S+)\s*\}\}/g;

    // dotted paths not supported yet.
    // array references not supported yet.
    return template.replace(regex, (_unused, varName) => {
      if (!interpolateParams) {
        return "";
      }
      return interpolateParams[varName] || "";
    });
  }

  /**
   * Translates given key to the current language with default value.
   * @param key - Key for the text.
   * @param defaultValue - The default value that is returned if the key is not found. Null is the default.
   * @returns The translated value in current language or if none is found the key.
   */
  public static getWithDefault(key: string, defaultValue = null): string {
    if (!key) {
      return null;
    }
    // TODO: We could check here that the value is actually a string and not a dictionary.
    // But some functionality may already be relying that the function returns also dictionaries.
    return this.getAny(key) || defaultValue;
  }

  /**
   * Gets any object (string or dictionary) with the given property path.
   * @param key Key for the text.
   * @returns The object if found using the key, null if none is found.
   */
  public static getAny(key: string): any {
    if (!key) {
      return null;
    }
    const subKeys = key.split(".");
    let value = this.getCurrentDictionary()[this.lang];
    for (const subKey of subKeys) {
      if (Objects.has(value, subKey)) {
        value = value[subKey];
      } else {
        return null;
      }
    }
    return value;
  }

  /**
   * Adds a dictionary.
   * @param lang - ISO language code for the dictionary to be added: fi, en or sv.
   * @param translations - A translations object (json).
   */
  public static addDictionary(lang: string, translations: any): void {
    const newDictionary = {};
    newDictionary[lang] = translations;
    this.dictionary = this.mergeDeep(this.dictionary, newDictionary);
  }

  /**
   * Add all dictionaries from a single language object. This object must have language codes as first level keys.
   * @param translations - A translations object (json).
   */
  public static addDictionaries(translations: any): void {
    for (const key in translations) {
      if (Objects.has(translations, key)) {
        this.addDictionary(key, translations[key]);
      }
    }
  }

  /**
   * Sets the Salaxy user interface language.
   * @param lang - One of the supported languages: "fi", "en" or "sv".
   */
  public static setLanguage(lang: string): void {
    Translations.lang = lang;
  }

  /**
   * Gets the current user interface language
   * @returns One of the supported languages: "fi", "en" or "sv".
   */
  public static getLanguage(): string {
    return Translations.lang;
  }

  /** Gets the current core dictionary (all languages). */
  public static getCurrentDictionary(): {
    /** English */
    en: CoreDictionaryLang,
    /** Finnish */
    fi: CoreDictionaryLang,
    /** Finnish */
    sv: CoreDictionaryLang,
    /** The technical language: Finnish or English from developers. */
    sx: CoreDictionaryLang,
  } {
    return Translations.dictionary;
  }

  /** Current language. */
  private static lang = "fi";

  /** Assign the dictionary to static property that may be updated by the environment. */
  private static dictionary = dictionaryAll;

  /**
   * Merges objects.
   * @param objects - Objects to merge.
   */
  private static mergeDeep(...objects: any[]) {
    const isObject = (obj: any) => obj && typeof obj === "object";

    return objects.reduce((result, current) => {
      Object.keys(current).forEach((key) => {
        const currentVal = current[key];
        const resultVal = result[key];

        if (Array.isArray(resultVal) && Array.isArray(currentVal)) {
          result[key] = resultVal.concat(...currentVal);
        } else if (isObject(resultVal) && isObject(currentVal)) {
          result[key] = this.mergeDeep(resultVal, currentVal);
        } else {
          result[key] = currentVal;
        }
      });
      return result;
    }, {});
  }
}
