import { PaymentChannel } from "../model";
import { DatelyObject, Dates } from "../util";

/**
 * Provides business logic for calculating salary date.
 */
export class SalaryDateLogic {

  /**
   * Returns true, if the given date is a valid Salary date for given gross salary payment date.
   *
   * @param salaryDate - Salary date (the date when the salary is in worker).
   * @param iban - Bank account number of the worker.
   * @param paidAt - Date for the gross salary payment.
   * @param isRequestedSalaryDate - Boolean indicating if the salary date is the requested salary date. The default is true.
   * @param channel - Payment channel.
   * @returns - True if the given dates are not conflicting (salary date is possible for given paid at date).
   */
  public static isValidSalaryDate(salaryDate: DatelyObject, iban: string = null, paidAt: any = null, isRequestedSalaryDate: any = null, channel: PaymentChannel = null): boolean {
    isRequestedSalaryDate = isRequestedSalaryDate === null ? true : isRequestedSalaryDate;
    const calculatedSalaryDate = this.CalculateSalaryDate(iban, paidAt, isRequestedSalaryDate, channel);
    return Dates.isWorkday(salaryDate) && Dates.getWorkdays(calculatedSalaryDate, salaryDate).length  >= 1;
  }

  /**
   * Calculates the estimated salary date (the date when the salary is in worker) for the given gross salary payment date.
   *
   * @param iban  - The iban number of the worker.
   * @param paidAt - Date for the salary gross payment.
   * @param isRequestedSalaryDateSet - Flag to indicate, if the requested salary date is set.
   * @param channel - Payment channel.
   */
  public static CalculateSalaryDate(iban: string = null, paidAt: any = null, isRequestedSalaryDateSet: any = null, channel: PaymentChannel = null): string {

    let requiredTime = 0;
    let latePaymentDelay = 0;
    let paidAtMoment = Dates.getMoment(paidAt);

    if (!paidAtMoment) {
      const d = new Date();
      // time shift needed for backend compatibility
      const unshiftedPaidAtMoment = Dates.getMoment(d);
      paidAtMoment = unshiftedPaidAtMoment.clone().add(-1 * unshiftedPaidAtMoment.utcOffset(), "m");

      if (Dates.isWorkday(paidAtMoment)) {
        if (paidAtMoment.date() === unshiftedPaidAtMoment.date()) {
          if (unshiftedPaidAtMoment.hours() >= 16) {
            latePaymentDelay = this.daysLatePaymentDelay;
          }
        } else {
          latePaymentDelay = this.daysLatePaymentDelay;
        }
      }

    } else {
      if (Dates.isWorkday(paidAtMoment)) {
        if (paidAtMoment.date() === paidAtMoment.local().date()) {
          if (paidAtMoment.local().hours() >= 16) {
            latePaymentDelay = this.daysLatePaymentDelay;
          }
        } else {
          latePaymentDelay = this.daysLatePaymentDelay;
        }
      }
    }
    const paidAtWorkday = this.getWorkday(paidAtMoment);
    const paymentChannel: PaymentChannel = channel || PaymentChannel.Undefined;
    const hasPaymentChannel = !(paymentChannel === PaymentChannel.Undefined ||
       paymentChannel === PaymentChannel.PalkkausCfaReference ||
       paymentChannel === PaymentChannel.PalkkausCfaFinvoice ||
      paymentChannel === PaymentChannel.PalkkausCfaPaytrail ||
      paymentChannel === PaymentChannel.HolviCfa ||
      paymentChannel === PaymentChannel.TalenomCfa);

    if (!hasPaymentChannel) {

      requiredTime += this.daysCustomerServiceProcessing;
      if (isRequestedSalaryDateSet) {
        requiredTime += 2;
      } else {
        requiredTime += latePaymentDelay;
        const smallBankDelay = this.isBigBank(iban) ? 0 : this.daysNonBigBankPaymentDelay;
        requiredTime += smallBankDelay;
      }
    } else {
      requiredTime += latePaymentDelay + 1;
    }

    const salaryDate = Dates.addWorkdays(paidAtWorkday, requiredTime);

    return Dates.asDate(salaryDate);
  }

  /**
   * Required days to process the incoming payment to worker net payment.
   */
  private static daysCustomerServiceProcessing = 1;

  /**
   * Payment delay if the bank is not a big bank.
   */
  private static daysNonBigBankPaymentDelay = 1;

  /**
   * Payment delay if the  payment takes place outside bank hours.
   */
  private static daysLatePaymentDelay = 1;

  private static getWorkday(startDate: DatelyObject): string {
    const newMoment = Dates.getMoment(Dates.asDate(startDate));
    const temp = true;
    while (temp) {
      if (Dates.isWorkday(newMoment)) {
        return Dates.asDate(newMoment);
      }
      newMoment.add(1, "days");
    }
  }

  private static isBigBank(iban: string): boolean {
    if (iban && iban.length > 6) {
      const id = iban.substr(4, 3);
      if (id.startsWith("1") ||
        id.startsWith("2") ||
        id.startsWith("34") ||
        id.startsWith("8") ||
        id.startsWith("5")) {
        return true;
      }

      return false;
      /*
              1nn = Nordea Bank(Nordea)
              2nn = Nordea Bank(Nordea)
              31n = Handelsbanken(SHB)
              33n = Skandinaviska Enskilda Banken(SEB)
              34n = Danske Bank
              36n = S - Bank
              37n = DnB NOR Bank(DnB NOR)
              38n = Swedbank
              39n = S - Bank
              4nn = Savings Banks(Sp), local co-operative banks and Aktia Bank
              5nn = Co - operative banks(Op) and OKO Bank
              6nn = Bank of Åland
              713 = Citibank
              8nn = Danske Bank
        */
    }
  }
}
