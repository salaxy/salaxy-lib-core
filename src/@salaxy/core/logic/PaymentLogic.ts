
/**
 * Business logic relating to payments.
 */
export class PaymentLogic {

  /**
   * OBSOLETE
   * @deprecated This is not any more updated and should not be used.
   */
  public static getEInvoiceIntermediators(): any {
    const result = {
      "HELSFIHH": "Aktia - HELSFIHH",
      "003723327487": "Apix Messaging Oy - 003723327487",
      "BAWCFI22": "Basware Oyj - BAWCFI22",
      "003703575029": "CGI - 003703575029",
      "DABAFIHH": "Danske Bank - DABAFIHH",
      "DNBAFIHX": "DNB - DNBAFIHX",
      "HANDFIHH": "Handelsbanken - HANDFIHH",
      "885790000000418": "HighJump AS - 885790000000418",
      "INEXCHANGE": "InExchange Factorum AB - INEXCHANGE",
      "EXPSYS": "Lexmark Expert Systems AB - EXPSYS",
      "003708599126": "Liaison Technologies Oy - 003708599126",
      "003721291126": "Maventa - 003721291126",
      "003726044706": "Netbox Finland Oy - 003726044706",
      "NDEAFIHH": "Nordea Pankki - NDEAFIHH",
      "E204503": "OpusCapita Solutions Oy - E204503",
      "OKOYFIHH": "Osuuspankit ja Pohjola Pankki - OKOYFIHH",
      "PAGERO": "Pagero - PAGERO",
      "PALETTE": "Palette Software - PALETTE",
      "POPFFI22": "POP Pankki  - POPFFI22",
      "003710948874": "Posti Messaging Oy - 003710948874",
      "003701150617": "PostNord Strålfors Oy - 003701150617",
      "003714377140": "Ropo Capital Oy - 003714377140",
      "SBANFIHH": "S-Pankki - SBANFIHH",
      "TAPIFI22": "S-Pankki (ent. LähiTapiola Pankin tunnus toistaiseksi käytössä) - TAPIFI22",
      "ITELFIHH": "Säästöpankit - ITELFIHH",
      "003701011385": "Tieto Oyj - 003701011385",
      "885060259470028": "Tradeshift - 885060259470028",
      "AABAFI22": "Ålandsbanken - AABAFI22",
    };
    return result;
  }

}
