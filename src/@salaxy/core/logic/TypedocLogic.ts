import { ClassDoc, ClassMemberDoc, DeclarationReflection, EnumDoc, RootDeclarationReflection, TypeDocType } from "./model/docs";

/**
 * Logic functions related to Typedoc parsing and interpretation.
 */
export class TypedocLogic {

  /**
   * Finds an interface based on the type name.
   * @param docs Array of classes and interfaces from which to find an interface.
   * @param typeName The type name of the interface to look for. Search is case-insensitive.
   */
  public static findInterface(docs: ClassDoc[], typeName: string): ClassDoc {
    typeName = typeName.toLowerCase().trim();
    if (!typeName) {
      return null;
    }
    return docs.find((x) => x.kind === "interface" && x.name.toLowerCase() === typeName);
  }

  /**
   * Finds an enum based on the type name.
   * @param allEnums All enumerations from which to find the enum by name.
   * @param name The enum name to look for. Search is case-insensitive.
   */
  public static findEnum(allEnums: EnumDoc[], name: string): EnumDoc {
    name = name.toLowerCase().trim();
    if (!name) {
      return null;
    }
    return allEnums.find((x) => x.name.toLowerCase() === name);
  }

  /**
   * Gets a property from an interface supports single property names or longer property paths.
   * @param type The root type from which the property is found.
   * @param path Property path starting with the root type.
   * @param interfaces The interfaces that is used in finding types.
   * Typically, this is the full colelction of all classes and interfaces.
   */
  public static getProperty(type: ClassDoc, path: string, interfaces: ClassDoc[]): ClassMemberDoc {
    if (!type || !path?.trim()) {
      return null;
    }
    const pathArr = path.split(".");
    let member: ClassMemberDoc = null;
    while (pathArr.length) {
      const propName = pathArr.shift().trim();
      member = type.members.find((x) => x.name.toLowerCase() === propName.toLowerCase());
      if (pathArr.length) {
        const nextTypeName = this.getTypeAsText(member?.type)?.replace("?", "");
        if (!nextTypeName) {
          return null;
        }
        type = this.findInterface(interfaces, nextTypeName);
      }
    }
    return member === undefined ? null : member;
  }



  /**
   * Gets a typedoc type formatted as text.
   * @param type Type as typedoc object model.
   */
  public static getTypeAsText(type: any): string {
    if (type == null) {
      return null;
    }
    switch (type.type) {
      case "intrinsic":
        return `${type.name as string}`;
      case "union":
        if (type.types.length === 2 && type.types[1].name === "null") {
          return TypedocLogic.getTypeAsText(type.types[0]) + "?";
        }
        return type.types.map((innerType) => TypedocLogic.getTypeAsText(innerType)).join(" | ");
      case "array":
        return TypedocLogic.getTypeAsText(type.elementType) + "[]";
      case "stringLiteral":
        return `"${type.value as string}"`;
      case "typeParameter":
        return `<${type.name as string}>`;
      case "reference":
        return type.name as string
          + (type.typeArguments ? "<" + (type.typeArguments.map((arg) => TypedocLogic.getTypeAsText(arg)).join(", ") as string) + ">" : "");
      case "reflection":
        return "Reflection not supported in quick docs.";
      default:
        return `TODO: ${JSON.stringify(type)}`;
    }
  }

  /**
   * Gets a typedoc type formatted as HTML.
   * @param type Type as typedoc object model.
   * @param typeLinker Method that resolves a Typedoc type to link in the current environment.
   */
  public static getTypeHtml(type: TypeDocType, typeLinker: (typedocType: any) => string): string {
    if (type == null) {
      return null;
    }
    switch (type.type) {
      case "intrinsic":
        return `<i>${type.name}</i>`;
      case "union":
        if (type.types.length === 2 && type.types[1].name === "null") {
          return TypedocLogic.getTypeHtml(type.types[0], typeLinker) + "?";
        }
        return type.types.map((innerType) => TypedocLogic.getTypeHtml(innerType, typeLinker)).join(" | ");
      case "array":
        return TypedocLogic.getTypeHtml(type.elementType, typeLinker) + "[]";
      case "stringLiteral":
        return `"${type.value}"`;
      case "typeParameter":
        return `&lt;${type.name}&gt;`;
      case "reference":{
        const typeLink = typeLinker && typeLinker(type);
        let refHtml = typeLink ? `<a href="${typeLink}">${type.name}</a>` : type.name;
        if (type.typeArguments) {
          refHtml += "&lt;" + type.typeArguments.map((arg) => TypedocLogic.getTypeHtml(arg, typeLinker)).join(", ") + "&gt;";
        }
        return refHtml;}
      case "reflection":
        return "<i>Reflection type not implemented in quick docs. See source for details.</i>";
      default:
        return `<span>${type.type}: <code>${JSON.stringify(type)}</code></span>`;
    }
  }

  /** Gets the enumerations for the given library from raw TypeDoc json. */
  public static getEnums(typedoc: any, lib: string): EnumDoc[] {
    const enums: EnumDoc[] = typedoc.children
      .filter((x) => x.kindString === "Enumeration")
      .map((typedocEnum) => {
        const enumResult = TypedocLogic.getRootDeclarationReflection("enum", lib, typedocEnum) as EnumDoc;
        enumResult.names = (typedocEnum.children || []).map((td) => td.name);
        enumResult.values = (typedocEnum.children || []).map((td) => td.defaultValue?.replace("\"", "").replace("\"", ""));
        return enumResult;
      });
    return enums;
  }

  /** Gets the classes and interfaces strongly typed from raw TypeDoc json */
  public static getClasses(typedoc: any, lib: string): ClassDoc[] {
    const classes: ClassDoc[] = typedoc.children
      .filter((x) => x.kindString === "Class" || x.kindString === "Interface")
      .map((typedocClass) => {
        const classResult = this.getRootDeclarationReflection("class", lib, typedocClass) as ClassDoc;
        classResult.extendedBy = typedocClass.extendedBy || [];
        classResult.extends = typedocClass.extendedTypes || [];
        classResult.implementedBy = typedocClass.implementedBy || [];
        classResult.implements = typedocClass.implementedTypes || [];
        if (classResult.rawKind === "Interface") {
          classResult.kind = "interface";
        } else if (classResult.implements.find((x) => x.name === "IController")) {
          classResult.kind = "controllerBase";
        }
        // Registered components and controllers will be set to data later.
        classResult.group = this.getGroup(lib, classResult.source);
        classResult.members = (typedocClass.children || []).filter((x) => x.flags.isPublic || x.flags.isExported)
          .map((tdMember) => {
            const result: ClassMemberDoc = {
              type: tdMember.type,
              grouping: "Other",
              parameters: [],
            };
            if (tdMember.kindString === "Method" && tdMember.signatures[0]) {
              tdMember.signatures[0].sources = tdMember.sources;
              this.setDeclrProperties(result, tdMember.signatures[0]);
              result.rawKind = "Method";
            } else {
              this.setDeclrProperties(result, tdMember);
            }
            switch (result.rawKind) {
              case "Accessor":
                // Alternative way:
                // result.type = (td.getSignature || td.setSignature)[0].type;
                result.type = (tdMember.setSignature && tdMember.setSignature[0]) ? tdMember.setSignature[0].parameters[0].type : tdMember.getSignature[0].type;
                // Add read-only info here if necessary
                result.grouping = "Property";
                break;
              case "Property":
                result.grouping = "Property";
                break;
              case "Method":
                result.grouping = "Method";
                if (tdMember.signatures[0]) {
                  if (tdMember.signatures.length > 1) {
                    result.shortText += "\n\nNOTE: method has more than one signature. See source for all signatures.";
                  }
                  const signature = tdMember.signatures[0];
                  result.type = signature.type;
                  if (signature.parameters) {
                    result.parameters = signature.parameters.map((paramSrc) => {
                      const paramResult: ClassMemberDoc = {
                        grouping: "Property",
                        type: paramSrc.type,
                      };
                      this.setDeclrProperties(paramResult, paramSrc);
                      return paramResult;
                    });
                  }
                } else {
                  result.shortText += "\nERROR: No signature was found!";
                }
                break;
            }
            return result;
          });
        return classResult;
      });
    return classes;
  }

  /**
   * Gets the common ROOT declaration reflection types from the Typedoc JSON: class, interface and enum
   * and their sub-kinds in AngularJS: controller, controllerBase and component.
   */
  public static getRootDeclarationReflection(kind: string, lib: string, typedocSource: any): RootDeclarationReflection {
    const result =  {
      id: typedocSource.id,
      kind,
      lib,
    } as RootDeclarationReflection;
    if (typedocSource.sources && typedocSource.sources[0]) {
      result.source = typedocSource.sources[0].fileName;
    }
    result.group = this.getGroup(lib, result.source);
    this.setDeclrProperties(result, typedocSource);

    result.exampleUrls = typedocSource.comment?.tags?.filter((t) => t.tag === "exampleurl").map((e) => {
      const url = e.text.trim() as string;
      return url.toLowerCase() === "true" ? `/ng1/examples/${result.group}/${result.name}.html` : url.trim();
    }) || [];
    result.screenShots = typedocSource.comment?.tags?.filter((t) => t.tag === "screenshot").map((e) => {
      const url = e.text.trim() as string;
      return url.toLowerCase() === "true" ? `/ng1/screen-shots/${result.group}/${result.name}.png` : url.trim();
    }) || [];
    return result;
  }

  /**
   * Sets the DeclarationReflection properties from source JSON
   * @param target Target data structure
   * @param tdSource The source object from raw typedoc JSON source.
   */
  public static setDeclrProperties(target: DeclarationReflection, tdSource: any): void {
    if (!tdSource) {
      return;
    }
    target.name = tdSource.name;
    target.rawKind = tdSource.kindString;
    if (tdSource.comment) {
      if (tdSource.comment.shortText) {
        target.shortText = tdSource.comment.shortText;
        target.descr = (tdSource.comment.shortText as string) + (tdSource.comment.text ? "\n\n" + (tdSource.comment.text as string) : "");
      } else {
        target.shortText = tdSource.comment.text || "Error: No description in comments.";
        target.descr = tdSource.comment.text || "Error: No description in comments.";
      }
      if (tdSource.comment.returns) {
        target.descr += "\n\n**Returns**: " + (tdSource.comment.returns as string);
      }
      if (tdSource.comment.tags) {
        const examples = tdSource.comment.tags.filter((t) => t.tag === "example");
        if (examples.length > 0) {
          target.example = examples.map((e) => e.text.trim() as string).join("\n\n------------\n\n");
        }
      }
    }
  }

  /** Gets the grouping based on library and source file */
  public static getGroup(lib: string, source: string): string {
    // HACK: Probably make private.
    if (!source) {
      return "err:no-source";
    }
    const splitSrc = source.split("/");
    switch (lib) {
      case "@salaxy/core":
        if (splitSrc[splitSrc.length - 1] === "") {
          return "v01.ts";
        }
        return splitSrc[0];
      case "@salaxy/ng1":
        // For some reason, this changed to: "salaxy-lib-ng1/src/ts/controllers/calc/CalcIrRowsController.ts"
        if (splitSrc.length > 5 && (splitSrc[3] === "components" || splitSrc[3] === "controllers")) {
          return splitSrc[4];
        }
        if (splitSrc.length > 4) {
          return splitSrc[3];
        }
        if (splitSrc.length > 2 && (splitSrc[0] === "components" || splitSrc[0] === "controllers")) {
          return splitSrc[1];
        }
        if (splitSrc.length > 2 && (splitSrc[0] === "components" || splitSrc[0] === "controllers")) {
          return splitSrc[1];
        }
        return splitSrc[0];
    }
  }

  /** For an array of types, gets an array type strings  */
  private static getTypeStrings(types: any[]): string[] {
    if (!types) {
      return [];
    }
    return types.map((t) => TypedocLogic.getTypeAsText(t));
  }
}
