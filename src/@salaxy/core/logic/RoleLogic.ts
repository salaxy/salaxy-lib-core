import { AccountBase, CompanyAccount, LegalEntityType, PersonAccount, Role, UserSession, WebSiteUserRole } from "../model";
import { AppStatus, SystemRole } from "./model";

/**
 * Provides business logic for role membership rules.
 */
export class RoleLogic {

    /**
     * Checks whether the user is in a given role
     * @param session - Session of the current account.
     * @param appStatus - Additional information about the session and application status.
     * @param role - One of the known roles or role from server.
     * You can also use exclamation mark for negative (e.g.)
     * @returns Boolean whether user is in the given role
     */
    public static isInRole(session: UserSession, appStatus: AppStatus, role: SystemRole | Role | string): boolean {
      if (!role) {
        return true;
      }
      role = role.trim();
      const isNegative = role.startsWith("!");
      if (isNegative) {
        role = role.substr(1);
      }
      role = (role || "any").trim();
      role = role.substr(0, 1).toLowerCase() + role.substr(1); // Tolerate Capital first letter

      let primaryRole: SystemRole.Worker | SystemRole.Company | SystemRole.Household = null;
      const currentAccount = session ? session.currentAccount as AccountBase : null;
      if (currentAccount) {
        if (currentAccount.entityType === LegalEntityType.Company) {
          primaryRole = SystemRole.Company; // Company is clear
        } else {
          const personAccount = currentAccount as PersonAccount;
          if (personAccount.lastLoginAs === WebSiteUserRole.Worker || (!personAccount.isEmployer && personAccount.isWorker)) {
            primaryRole = SystemRole.Worker; // LastLoginAs is clear, but we cannot rely on isWorker directly as some users are both isEmployer=true ans isWorker=true.
          } else {
            primaryRole = SystemRole.Household; // Default to household if both and no lastLogin.
          }
        }
      }

      const checkRole = (role): boolean => {
        switch (role) {
          // No session required
          case SystemRole.Unknown:
            return appStatus.sessionCheckInProgress;
          case SystemRole.Anon:
            return !appStatus.sessionCheckInProgress && (!session || !session.isAuthorized);
          case SystemRole.Init:
            return appStatus.sessionCheckInProgress;
          case SystemRole.Hidden:
            return false;
          case SystemRole.Test:
            return !!appStatus.isTestData;
          case SystemRole.Prod:
            return !appStatus.isTestData;
          case SystemRole.Any:
          case "*":
              return true;
          // Session required (may be checked from primaryRole or currentAccount shortcuts)
          case SystemRole.Auth:
            return session && session.isAuthorized;
          case SystemRole.Worker:
            return primaryRole === SystemRole.Worker;
          case SystemRole.Employer:
            return primaryRole === SystemRole.Company || primaryRole === SystemRole.Household;
          case SystemRole.Household:
            return primaryRole === SystemRole.Household;
          case SystemRole.Company:
            return primaryRole === SystemRole.Company;
          case SystemRole.Person:
            return primaryRole === SystemRole.Worker || primaryRole === SystemRole.Household;
          case SystemRole.NonVerified:
            return currentAccount && !currentAccount.isVerified;
          case SystemRole.FiTm:
          case SystemRole.FiOy:
          case SystemRole.FiRy:
            return primaryRole === SystemRole.Company && (session.currentAccount as CompanyAccount).companyType === role;
          case SystemRole.PartnerMessaging:
            return session?.settings?.partner?.serviceModel?.features?.messaging;
          case SystemRole.PartnerService:
            return appStatus.isTestData &&  session?.settings?.partner?.serviceModel?.features?.forms;
          default:
            return (currentAccount && session.currentAccount.identity.roles.indexOf(role) >= 0)
              || appStatus.clientRoles.indexOf(role) >= 0
              ;
        }
      };
      return isNegative ? !checkRole(role) : checkRole(role);
    }

    /**
     * Returns true if the current account is a member of all of the given roles.
     * Returns true if the roles is null or an empty array or and an empty string.
     * @param session - Session of the current account.
     * @param appStatus - Additional information about the session and application status.
     * @param roles - Array of role values.
     */
    public static isInAllRoles(session: UserSession, appStatus: AppStatus, roles: Array<SystemRole | Role | string>): boolean {
      if (!roles || roles.length === 0) {
        return true;
      }
      return roles.every((x) => this.isInRole(session, appStatus, x));
    }

    /**
     * Returns true if the current account is a member of some of the given roles.
     * Returns true if the roles is null or an empty array or an empty string.
     * @param session Session of the current account.
     * @param appStatus State of the current application (i.e. login, session check etc.)
     * @param roles Array of role values.
     */
    public static isInSomeRole(session: UserSession, appStatus: AppStatus, roles: Array<SystemRole | Role | string>): boolean {
      if (!roles || roles.length === 0) {
        return true;
      }
      return roles.some((x) => this.isInRole(session, appStatus, x));
    }
}
