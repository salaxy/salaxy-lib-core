import { ImportableCrudApi } from "../../api";
import { ApiValidation, ApiValidationErrorType } from "../../model";
import { Dates } from "../../util";
import { DataRow } from "./DataRow";
import { ImportBatchInfo } from "./ImportBatchInfo";
import { ImportResult } from "./ImportResult";
import { Mapper } from "./Mapper";
import { TableFormat } from "./TableFormat";
import { Tables } from "./Tables";

/**
 * Logic for the batch import of data from sequence files.
 * TObject is a hierarchical Salaxy object and the TRow is a row in table/dataset.
 *
 */
export class ImportBatch<TObject, TRow extends DataRow> implements ImportBatchInfo {

  /** Storage key for  summary object of all batches. */
  public static readonly BATCH_INFO_KEY = "salaxy-batch-info";
  /** Storage key prefix for a single batch. */
  public static readonly BATCH_KEY_PREFIX = "salaxy-batch";

  /** Unique id for the batch. */
  public id: string;
  /** User friendly name for the batch. */
  public name: string;
  /** Date and time when the batch was created. */
  public createdAt: string;
  /** Date and time when the batch was last updated. */
  public updatedAt: string;
  /**
   * Current phase of the batch:
   *
   * - ```new``` A new batch. Contains no data.
   * - ```read``` The data file (e.g. csv) has been read from file or clipboard.
   * - ```mapped``` The data has been mapped to a supported tabular format.
   * - ```validated``` The data has been validated.
   * - ```imported``` The data has been imported to backend.
   * - ```rejected``` The batch has been rejected manually.
   */
  public phase: "new" | "read" | "mapped" | "validated" | "imported" | "rejected";

  /**
   * Current status of the current phase:
   *
   * - ```info``` Typically the default status for the phase which has not yet been done.
   * - ```success``` The current phase has been successfully finished.
   * - ```warning``` The current phase has been finished with warnings.
   * - ```error``` The current phase contains errors.
   */
  public status: "info" | "success" | "warning" | "error";

  /** Raw file data for importing. E.g. csv or tab formatted file. */
  public rawData: string;

  /** Parsed tabular data but not mapped */
  public unmappedData: ImportResult<DataRow>;

  /** Mapping from unmapped data to actual data. */
  public sourceTargetMap: any;

  /** Result data of the current operation (map, validate, import). */
  public data: TRow[];

  /**
   * Creates a new batch for the data import.
   * @param storage Storage for storing data for the batch.
   * @param batchId Id for the batch. If null, a new batch id will be generated.
   * @param mapper Mapper object for table to objects transformations.
   * @param api api object for validting and posting data to API.
   */
  constructor(
    private storage: Storage,
    batchId: string,
    private mapper: Mapper<TObject, TRow>,
    private api: ImportableCrudApi<TObject>,
  ) {
    this.id = batchId;
  }

  /** Chages the mapper for the batch. */
  public changeMapper(mapper: Mapper<TObject, TRow>): void {
    this.mapper = mapper;
  }

  /**
   * Reloads the data from the storage for existing batch.
   */
  public open(): Promise<ImportBatch<TObject, TRow>> {
    if (this.storage && this.id) {
      const batches = JSON.parse(this.storage.getItem(ImportBatch.BATCH_INFO_KEY) || "[]") as ImportBatchInfo[];
      const batchInfo = batches.find((x) => x.id === this.id);
      if (batchInfo) {
        this.name = batchInfo.name;
        this.createdAt = batchInfo.createdAt;
        this.updatedAt = batchInfo.updatedAt;
        this.phase = batchInfo.phase;
        this.status = batchInfo.status;
        this.rawData = JSON.parse(this.storage.getItem(`${ImportBatch.BATCH_KEY_PREFIX}-${this.id}-raw`) || "null");
        this.unmappedData = JSON.parse(this.storage.getItem(`${ImportBatch.BATCH_KEY_PREFIX}-${this.id}-unmapped`) || "null");
        this.data = JSON.parse(this.storage.getItem(`${ImportBatch.BATCH_KEY_PREFIX}-${this.id}-data`) || "null");
      }
    }
    return Promise.resolve(this);
  }

  /** Saves the current batch data to storage. */
  public save(phase: "new" | "read" | "mapped" | "validated" | "imported" | "rejected" = null, status: "info" | "success" | "warning" | "error" = null): Promise<ImportBatch<TObject, TRow>> {
    const timeStamp = Dates.asMoment(new Date()).toISOString(false);
    this.id = this.id || this.newGuid();
    this.phase = phase || this.phase || "new";
    this.status = status || this.status || "info";
    this.createdAt = this.createdAt || timeStamp;
    this.updatedAt = timeStamp;
    if (this.storage) {
      const batches = JSON.parse(this.storage.getItem(ImportBatch.BATCH_INFO_KEY) || "[]") as ImportBatchInfo[];
      let batchInfo = batches.find((x) => x.id === this.id);
      if (!batchInfo) {
        batchInfo = {
          id: this.id,
        };
        batches.push(batchInfo);
      }
      batchInfo.name = this.name;
      batchInfo.createdAt = this.createdAt;
      batchInfo.updatedAt = this.updatedAt;
      batchInfo.phase = this.phase;
      batchInfo.status = this.status;

      this.storage.setItem(ImportBatch.BATCH_INFO_KEY, JSON.stringify(batches));
      this.storage.setItem(`${ImportBatch.BATCH_KEY_PREFIX}-${this.id}-raw`, JSON.stringify(this.rawData || null));
      this.storage.setItem(`${ImportBatch.BATCH_KEY_PREFIX}-${this.id}-unmapped`, JSON.stringify(this.unmappedData || null));
      this.storage.setItem(`${ImportBatch.BATCH_KEY_PREFIX}-${this.id}-data`, JSON.stringify(this.data || null));
    }
    return Promise.resolve(this);
  }

  /**
   * Read data into batch from the file content.
   * @param rawData File content, e.g. csv or tab separated data.
   * @param tableFormat Table format (csv or tab separated).
   * @param localeId If 'fi', the number and date formats are parsed using Finnish formats.
   */
  public read(rawData: string = null, tableFormat: TableFormat = TableFormat.Csv, localeId: "fi" | "en" = "fi"): Promise<ImportBatch<TObject, TRow>> {
    this.rawData = rawData || this.rawData;
    this.unmappedData = Tables.import(this.rawData, tableFormat, localeId);
    this.sourceTargetMap = this.getSourceTargetMap(this.unmappedData.data);
    const status =
      this.unmappedData.errors.length ? "error" :
        !this.unmappedData.data.length || this.unmappedData.ignoredLines ? "warning" : "success";
    return this.save("read", status);
  }

  /**
   * Map the read data to the supported tabular format.
   * @param sourceTargetMap Object containing source headers as keys with target values.
   */
  public map(sourceTargetMap: any = null): Promise<ImportBatch<TObject, TRow>> {
    if (!this.mapper) {
      return Promise.resolve(this);
    }
    this.sourceTargetMap = sourceTargetMap || this.sourceTargetMap;
    const targetSourceMap = {} as any;
    for (const key of this.mapper.keys) {
      const sourceProperty = Object.getOwnPropertyNames(this.sourceTargetMap).find( (p) => this.sourceTargetMap[p] === key);
      if (sourceProperty) {
        targetSourceMap[key] = sourceProperty;
      }
    }
    const targetProperties = Object.getOwnPropertyNames(targetSourceMap);
    this.data = this.unmappedData.data.map((unmappedItem) => {
      const dataItem = {} as TRow;
      for (const targetProperty of targetProperties) {
          const prop = targetProperty as keyof TRow;
          dataItem[prop ] = unmappedItem[targetSourceMap[targetProperty]];
      }
      return dataItem;
    });

    return this.save("mapped", "success");
  }

  /** Returns true if the import data is valid */
  public get isDataValid(): boolean {
    return this.data && this.data.every( (x) => x.validation && x.validation.isValid);
  }

   /** Returns true if the import data contains validation errors */
   public get hasDataValidationErrors(): boolean {
    return this.data && this.data.some( (x) => x.validation && x.validation.errors && !!x.validation.errors.length);
  }

  /** Clears validation from the rows */
  public clearValidation(): void {
    for (const row of this.data) {
      row.validation = null;
    }
  }

  /**
   * Validates the batch data.
   * @param notify Function for notifying progress to caller.
   */
  public validate(notify: (message: string, index?: number, count?: number, result?: ApiValidation) => void = null): Promise<ImportBatch<TObject, TRow>> {

    const tryValidate = (obj: TObject): Promise<ApiValidation> => {
      try {
        return this.api.validate(obj).then((apiResult) => {
          return this.api.getValidationResult(apiResult);
        });
      } catch (error) {
        return Promise.resolve({
          isValid: false,
          errors: [{
            key: null,
            type: ApiValidationErrorType.General,
            code: "Server error",
            msg: JSON.stringify(error),
          }],
        });
      }
    };

    this.clearValidation();
    let status: any = "success";
    const objects = this.mapper.fromRows(this.data);
    let index = 0;
    const next = (): Promise<void> => {
      if (index === objects.length) {
        return Promise.resolve();
      }
      const obj = objects[index];
      return tryValidate(obj)
        .then((validationResult) => {
          if (notify) {
            notify("validate", index, objects.length, validationResult);
          }
          this.updateValidation(index, objects, validationResult);
          if (!validationResult.isValid) {
            status = "error";
          }
          index++;
          return next();
        });
    };

    return next().then(() => {
      return this.save("validated", status);
    });
  }

  /**
   * Imports the batch data.
   * @param notify Function for notifying progress to caller.
   */
  public import(notify: (message: string, index?: number, count?: number, result?: ApiValidation) => void = null): Promise<ImportBatch<TObject, TRow>> {

    const trySave = (obj: TObject): Promise<
      {
        /** saved object */
        object: TObject,
        /** error */
        validation: ApiValidation,
      }> => {
      try {
        return this.api.save(obj).then((apiResult) => {
          return { object: apiResult, validation: { isValid: true } };
        });
      } catch (error) {
        return Promise.resolve({
          object: obj,
          validation: {
            isValid: false,
            errors: [{
              key: null,
              type: ApiValidationErrorType.General,
              code: "Server error",
              msg: JSON.stringify(error),
            }],
          },
        });
      }
    };

    this.clearValidation();
    let status: any = "success";
    const objects = this.mapper.fromRows(this.data);
    let index = 0;
    const next = (): Promise<void> => {
      if (index === objects.length) {
        return Promise.resolve();
      }
      const obj = objects[index];
      return trySave(obj)
        .then((tryResult) => {
          if (notify) {
            notify("import", index, objects.length, tryResult.validation);
          }
          if (tryResult.validation.isValid) {
            this.updateSave(index, objects, tryResult.object);
          }
          this.updateValidation(index, objects, tryResult.validation);

          if (!tryResult.validation.isValid) {
            status = "error";
          }
          index++;
          return next();
        });
    };
    return next().then(() => {
      return this.save("imported", status);
    });
  }

  private newGuid(): string {
    let dt = new Date().getTime();
    const uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (c) => {
      /* eslint-disable-next-line no-bitwise */
      const r = (dt + Math.random() * 16) % 16 | 0;
      dt = Math.floor(dt / 16);
      /* eslint-disable-next-line no-bitwise */
      return (c === "x" ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }

  private updateValidation(objectIndex: number, objects: TObject[], validation: ApiValidation): void {
    const objectRowCount = this.mapper.fromObjects([objects[objectIndex]]).length;
    const previousObjectsCount = this.mapper.fromObjects(objects.slice(0, objectIndex)).length;
    for (let i = previousObjectsCount; i < previousObjectsCount + objectRowCount; i++) {
      this.data[i].validation = validation;
    }
  }

  private updateSave(objectIndex: number, objects: TObject[], object: TObject): void {
    objects[objectIndex] = object;
  }

  /**
   * Returns source target map from sample data.
   * @param unmappedRows Rows to map
   */
  private getSourceTargetMap(unmappedRows: DataRow[]): any {
    const sourceTargetMap = {};
    const sampleRow = unmappedRows && unmappedRows.length ? unmappedRows[0] : {};
    const sourceProperties = Object.getOwnPropertyNames(sampleRow);
    for (const sourceProperty of sourceProperties ) {
      const targetKey = this.mapper ? this.mapper.keys.find( (k) => k === sourceProperty) : null;
      if (targetKey) {
        sourceTargetMap[sourceProperty] = targetKey;
      } else {
        sourceTargetMap[sourceProperty] = null;
      }
    }
    return sourceTargetMap;
  }
}
