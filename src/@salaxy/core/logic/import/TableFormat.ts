/** File format for a table. */
export enum TableFormat {
  /** Tab separated table. */
  Txt = "txt",
  /** Comma separated table. */
  Csv = "csv",
  /** Excel formatted table. */
  Xlsx = "xlsx",
}
