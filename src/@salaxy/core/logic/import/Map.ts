import { DataRow } from "./DataRow";
import { MapSelector } from "./MapSelector";

/**
 * Map between a hierarchical Salaxy object and a row in a table/dataset.
 */
export interface Map<TObject, TRow extends DataRow> {

  [key: string]: string | Array<keyof TRow> | MapSelector<TObject, TRow> | Map<TObject, TRow>;

}
