import { ApiValidation } from "../../model";

/** Interface for data row for exports and imports */
export interface DataRow {

    /** Row specific validation. */
    validation?: ApiValidation;

    /** The actual data as key value pairs. */
    [key: string]: any;
}
