import { ApiValidationErrorType } from "../../model";
import { Dates, Numeric } from "../../util";
import { DataRow } from "./DataRow";
import { ImportResult } from "./ImportResult";
import { TableFormat } from "./TableFormat";

/** Utility class for processing tables. Eg. exporting and importing table formatted data. */
export class Tables {

  /**
   * Exports table data as in defined format. The default is csv formatted table.
   * @param rows  Rows as an array of objects with properties specified as headers.
   * @param format File format for a table export.
   * @param localeId Locale for data export. Defaults to Finnish (fi). Affects  separator (; vs ,), formatting of date (D.M.YYYY vs YYYY-MM-DD) and numeric fields (, vs . as decimal separator).
   */
  public static export<TRow extends DataRow>(rows: TRow[], format: TableFormat = TableFormat.Csv, localeId: "fi" | "en" = "fi"): any {
    if (!rows || rows.length === 0) {
      return null;
    }

    const data = this.exportAsArrays(rows);
    switch (format) {
      case TableFormat.Csv:
        return this.objectToSeparated(data, localeId, (localeId === "fi" ? ";" : ","));
      case TableFormat.Txt:
        return this.objectToSeparated(data, localeId, "\t");
      default:
        throw new Error(`Table format ${format} not supported.`);
    }
  }

  /**
   * Exports data as an array of rows, each of which consists of an array of cell values.
   * @param rows Array of row objects.
   */
  public static exportAsArrays<TRow extends DataRow>(rows: TRow[]): any[][] {
    const data = [];
    const headers = Object.getOwnPropertyNames(rows[0]).filter((x) => x !== "validation");
    data.push(headers);
    for (const row of rows) {
      const rowData = [];
      for (const header of headers) {
        rowData.push(row[header]);
      }
      data.push(rowData);
    }
    return data;
  }

  /**
   * Imports table from the given data and format.
   * @param data Table data.
   * @param format Format for the table data.
   * @param localeId Locale for data import. Defaults to Finnish (fi). Affects formatting of date (D.M.YYYY vs YYYY-MM-DD) and numeric fields (, vs . as decimal separator).
   */
  public static import<TRow extends DataRow>(data: any, format: TableFormat = TableFormat.Csv, localeId: "fi" | "en" = "fi"): ImportResult<TRow> {
    switch (format) {
      case TableFormat.Csv:
        return this.objectFromSeparated(data, localeId, (localeId === "fi" ? ";" : ","));
      case TableFormat.Txt:
        return this.objectFromSeparated(data, localeId, "\t");
      default:
        throw new Error(`Table format ${format} not supported.`);
    }
  }

  private static objectToSeparated(data: any[], localeId: "fi" | "en", separator: string) {
    const toRow = this.toRow(localeId, separator);
    const rows = data.map(toRow);
    return (separator !== "\t" ? "\uFEFF" : "") + rows.join("\r\n");
  }

  private static toRow(localeId: "fi" | "en", separator: string, addQuotes = true) {
    const toField = this.toField(localeId, addQuotes);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    return (row: any[], index: number, array: any[]) => {
      return row.map(toField).join(separator);
    };
  }

  private static toField(localeId: "fi" | "en", addQuotes = true) {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    return (value: any, index: number, array: any[]) => {
      if (value == null) {
        return "";
      }
      if (typeof (value) === "number") {
        if (localeId === "fi") {
          return value.toString().replace(".", ",");
        }
        return value;
      }
      if (typeof (value) === "boolean") {
        return (value ? "TRUE" : "FALSE");
      }
      if (typeof (value) === "string") {
        // try read date, always in ISO
        if (Dates.isValidDateTime(value, true, "YYYY-MM-DD")) {
          if (localeId === "fi") {
            return Dates.format(value, "D.M.YYYY");
          } else {
            return Dates.format(value, "YYYY-MM-DD");
          }
        }
        if (addQuotes) {
          return '"' + value.replace(/"/g, '""') + '"';
        }
        return value;
      }
      if (typeof (value) === "object") {
        if (value instanceof Date) {
          if (localeId === "fi") {
            return Dates.format(value, "D.M.YYYY");
          } else {
            return Dates.format(value, "YYYY-MM-DD");
          }
        } else {
          if (addQuotes) {
            return '"' + JSON.stringify(value).replace(/"/g, '""') + '"';
          }
          return JSON.stringify(value);
        }
      }
      return JSON.stringify(value);
    };
  }

  private static objectFromSeparated<TRow extends DataRow>(data: string, localeId: "fi" | "en", separator: string): ImportResult<TRow> {
    const result: ImportResult<TRow> = {
      ignoredLines: 0,
      data: [],
      headers: [],
      errors: [],
    };
    if (!data) {
      return result;
    }

    const rows = data.split(/\r\n|\n/);
    if (rows.length === 0) {
      return result;
    }

    const headerRow = rows[0].split(separator);
    result.headers = headerRow.map(this.fromHeaderField());

    if (result.headers.some((x) => !x)) {
      result.errors.push({
        key: null,
        type: ApiValidationErrorType.General,
        code: "InvalidHeader",
        msg: `Some header field contains no value. This is not allowed.`,
      });
      return result;
    }

    if (rows.length === 1) {
      return result;
    }
    const dataRows = rows.slice(1);
    const fromRow = this.fromRow(result, localeId, separator);
    const rawData = dataRows.map(fromRow);
    result.data = rawData.filter((x) => x != null);
    result.ignoredLines = rawData.filter((x) => x === null).length;
    result.data.filter((x) => x.validation && x.validation.errors).forEach((x) => {
      result.errors.push(...x.validation.errors);
    });

    return result;
  }

  private static fromRow<TRow extends DataRow>(result: ImportResult<TRow>, localeId: "fi" | "en", separator: string) {

    const fromField = this.fromField(localeId);

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    return (row: string, index: number, array: any[]): TRow => {
      const rowData = row.split(separator);
      let rowObject = {} as TRow;

      if (rowData.length !== result.headers.length) {
        if (rowData.length === 1 && rowData[0] === "") {
          rowObject = null;
        } else {
          rowObject.validation = rowObject.validation || { isValid: false, errors: [] };
          rowObject.validation.errors.push({
            key: null,
            type: ApiValidationErrorType.General,
            code: "InvalidColAmount",
            msg: `PARSE ERROR: Row ${index} has ${rowData.length} columns whereas the header row has ${result.headers.length}.`,
          });
        }
      } else {
        const typedRowData = rowData.map(fromField);

        if (typedRowData.every((x) => !x)) {
          rowObject = null;
        } else {
          for (let i = 0; i < result.headers.length; i++) {
            rowObject[result.headers[i]] = typedRowData[i];
          }
        }
      }
      return rowObject;
    };
  }

  private static fromHeaderField() {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    return (value: string, index: number, array: any[]): string => {
      if (!value) {
        return value;
      }
      // trim
      value = value.trim();
      // remove double quotes
      value = value.replace(/""/g, '"');
      // remove leading and trailing quotes
      value = value.replace(/^"+/, "");
      value = value.replace(/"+$/, "");

      return value;
    };
  }

  private static fromField(localeId: "fi" | "en") {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    return (value: string, index: number, array: any[]): any => {
      if (!value) {
        return null;
      }
      // trim
      value = value.trim();
      // remove double quotes
      value = value.replace(/""/g, '"');
      // remove leading and trailing quotes
      value = value.replace(/^"+/, "");
      value = value.replace(/"+$/, "");

      // try booleans
      if (value.toLowerCase() === "true") {
        return true;
      }
      if (value.toLowerCase() === "false") {
        return false;
      }
      // try date
      const dateFormat = localeId === "fi" ? "D.M.YYYY" : "YYYY-MM-DD";
      if (Dates.isValidDateTime(value, true, dateFormat)) {
        return Dates.parseDate(value, dateFormat);
      }
      // try number
      const sNumber = localeId === "fi" ? value.replace(",", ".") : value;
      if (Numeric.isNumber(sNumber)) {
        return Numeric.parseNumber(sNumber);
      }

      // let it be string
      return value;
    };
  }
}
