import { ImportableCrudApi } from "../../api";
import { DataRow } from "./DataRow";
import { ImportBatch } from "./ImportBatch";
import { ImportBatchInfo } from "./ImportBatchInfo";
import { Mapper } from "./Mapper";

/** Utility class for listing and deleting batch imports */
export class ImportBatches {
    /**
     * Returns all batches from the storage.
     * @param storage Storage for storing data for the batch.
     */
    public static getAll(storage: Storage): Promise<ImportBatchInfo[]> {
      const batches = JSON.parse( (storage.getItem(ImportBatch.BATCH_INFO_KEY) || "[]"))  as ImportBatchInfo[];
      return Promise.resolve(batches);
    }

    /**
     * Deletes one batch from the storage.
     * @param storage Storage for storing data for the batch.
     * @param batchId Id for the batch to remove.
     */
    public static delete(storage: Storage, batchId: string): Promise<void> {
      if (!storage || !batchId) {
        return Promise.resolve();
      }
      return this.getAll(storage).then( (all) => {
        if (all.some( (x) => x.id === batchId)) {
          const batches = all.filter( (x) => x.id !== batchId );
          storage.setItem(ImportBatch.BATCH_INFO_KEY, JSON.stringify(batches));
          storage.removeItem(`${ImportBatch.BATCH_KEY_PREFIX}-${batchId}-raw`);
          storage.removeItem(`${ImportBatch.BATCH_KEY_PREFIX}-${batchId}-unmapped`);
          storage.removeItem(`${ImportBatch.BATCH_KEY_PREFIX}-${batchId}-data`);
        }
      });
    }

    /**
     * Opens an existing batch or creates a new batch.
     * @param storage Storage for storing data for the batch.
     * @param batchId Id for the batch. If null, a new batch id will be generated.
     * @param mapper Mapper object for table to objects transformations.
     * @param api api object for validting and posting data to API.
     */
    public static open<TObject, TRow extends DataRow>(
      storage: Storage,
      batchId: string,
      mapper: Mapper<TObject, TRow>,
      api: ImportableCrudApi<TObject>,
      ): Promise<ImportBatch<TObject, TRow>> {
        const batch = new ImportBatch(storage, batchId, mapper, api);
        return batch.open();
    }
}
