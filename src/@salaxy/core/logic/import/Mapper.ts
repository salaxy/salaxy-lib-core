import { Objects } from "../../util";
import { DataRow } from "./DataRow";
import { Map } from "./Map";
import { MapSelector } from "./MapSelector";

/**
 * Logic for table mappers to/from Salaxy core model.
 * TObject is a hierarchical Salaxy object and the TRow is a row in table/dataset.
 */
export class Mapper<TObject, TRow extends DataRow> {

  /** Type for the mapper data row */
  public rowType: string = null;

  /** Description for the mapper. */
  public description: string = null;

  /** If true, this is a list item mapper. */
  public isListItemMapper = false;

  /** Keys for the mapper */
  private _keys: Array<keyof TRow> = null;

  /**
   * Creates a new mapper object for mapping objects between hierarchial and tabular format.
   * @param map Map containing selectors or paths for fields.
   * @param defaultObject Default object when mapping data from tabular data to hierarchial object. This is optional.
   */
  constructor(private map: Map<TObject, TRow>, private defaultObject: TObject = null) {
  }

  /** Returns the keys of the current map. */
  public get keys(): Array<keyof TRow> {
    if (!this._keys) {
      const result = [];
      for (const key in this.map) {
        if (Objects.has(this.map, key)) {
          const selector = this.map[key];
          if (this.isString(selector)) {
            //  path
            result.push(key);
          } else if (this.isMapSelector(selector)) {
            // MapSelector
            result.push(key);
          } else if (this.isSubMap(selector)) {
            const subMap = selector[Object.keys(selector)[0]] as Map<any, TRow>;
            const subMapper = new Mapper(subMap, null);
            result.push(...subMapper.keys);
          } else if (this.isKeysArray(selector)) {
            continue;
          }
        }
      }
      this._keys = result;
    }
    return this._keys;
  }

  /**
   * Maps from objects to data rows (typically export).
   * @param objects The objects to map to new rows.
   */
  public fromObjects(objects: TObject[]): TRow[] {
    const rows: TRow[] = [];
    if (!objects || !objects.length) {
      return rows;
    }
    for (const object of objects) {
      const row = {} as TRow;
      let subRows: TRow[] = null;
      for (const key in this.map) {
        if (Objects.has(this.map, key)) {
          const selector = this.map[key];
          const propKey = key as keyof TRow;
          if (this.isString(selector)) {
            // path
            row[propKey] = Objects.getProperty(object, selector);
          } else if (this.isMapSelector(selector)) {
            // MapSelector
            if (selector.fromObject) {
              selector.fromObject(object, row);
            } else {
              // selector path
              row[propKey] = Objects.getProperty(object, selector.path);
            }
          } else if (this.isSubMap(selector)) {
            const items = Objects.getProperty(object, Object.keys(selector)[0]) || [];
            const subMap = selector[Object.keys(selector)[0]] as Map<any, TRow>;
            const subMapper = new Mapper(subMap, null);
            subRows = subMapper.fromObjects(items);
            if (!subRows.length) {
              for (const subKey in subMap) {
                if (Objects.has(subMap, subKey)) {
                  const subPropKey = subKey as keyof TRow;
                  row[subPropKey] = null;
                }
              }
            }
          } else if (this.isKeysArray(selector)) {
            continue;
          }
        }
      }
      if (subRows && subRows.length) {
        for (const subRow of subRows) {
          const newRow = Objects.copy(row);
          for (const subKey in subRow) {
            if (Objects.has(subRow, subKey)) {
              const subPropKey = subKey as keyof TRow;
              newRow[subPropKey] = subRow[subPropKey];
            }
          }
          rows.push(newRow);
        }
      } else {
        rows.push(row);
      }
    }
    return rows;
  }

  /**
   * Maps rows to objects (typically import)
   * @param rows The rows to map to new objects.
   */
  public fromRows(rows: TRow[]): TObject[] {

    const objects: TObject[] = [];
    if (!rows || !rows.length) {
      return objects;
    }

    let rowGroups: TRow[][] = null;

    const groupBy = (all: TRow[], keys: Array<keyof TRow>): TRow[][] => {
      const objectArray = all.reduce((groups, item) => {
        const itemId = keys.map((k) => item[k]).join(".") || null;
        let group = groups.find((x) => x.id === itemId);
        if (!group) {
          group = { id: itemId, rows: [] };
          groups.push(group);
        }
        group.rows.push(item);
        return groups;
      }, []);
      return objectArray.map((x) => x.rows);
    };

    const keysKey = Object.keys(this.map).find((x) => this.isKeysArray(this.map[x]));
    const keys = keysKey ? this.map[keysKey] as Array<keyof TRow> : null;

    if (keys) {
      rowGroups = groupBy(rows, keys);
    } else {
      rowGroups = rows.map((x) => [x]);
    }

    for (const rowGroup of rowGroups) {
      const object = this.defaultObject ? Objects.copy(this.defaultObject) : {} as TObject;
      objects.push(object);

      const row = rowGroup[0];

      for (const key in this.map) {
        if (Objects.has(this.map, key)) {
          const propKey = key as keyof TRow;
          const selector = this.map[key];
          if (this.isString(selector)) {
            //  path
            Objects.setProperty(object, selector, row[propKey]);
          } else if (this.isMapSelector(selector)) {
            // MapSelector
            if (selector.fromRow) {
              selector.fromRow(row, object);
            } else {
              // selector path
              Objects.setProperty(object, selector.path, row[propKey]);
            }
          } else if (this.isSubMap(selector)) {
            const subMap = selector[Object.keys(selector)[0]] as Map<any, TRow>;
            const subMapper = new Mapper(subMap, null);
            const items = subMapper.fromRows(rowGroup);
            Objects.setProperty(object, Object.keys(selector)[0], items);
          } else if (this.isKeysArray(selector)) {
            continue;
          }
        }
      }
    }
    return objects;
  }

  private isString(selector: string | Array<keyof TRow> | MapSelector<TObject, TRow> | Map<TObject, TRow>): selector is string {
    return typeof (selector) === "string";
  }

  private isKeysArray(selector: string | Array<keyof TRow> | MapSelector<TObject, TRow> | Map<TObject, TRow>): selector is Array<keyof TRow> {
    return Array.isArray(selector);
  }

  private isMapSelector(selector: string | Array<keyof TRow> | MapSelector<TObject, TRow> | Map<TObject, TRow>): selector is MapSelector<TObject, TRow> {
    return typeof (selector) === "object" && (
      !!((selector || {}) as MapSelector<TObject, TRow>).fromObject ||
      !!((selector || {}) as MapSelector<TObject, TRow>).fromRow ||
      !!((selector || {}) as MapSelector<TObject, TRow>).path);
  }

  private isSubMap(selector: string | Array<keyof TRow> | MapSelector<TObject, TRow> | Map<TObject, TRow>): selector is Map<TObject, TRow> {
    return typeof (selector) === "object" &&
      !this.isMapSelector(selector) &&
      Object.keys(selector).length === 1 &&
      typeof (selector[Object.keys(selector)[0]]) === "object";
  }
}
