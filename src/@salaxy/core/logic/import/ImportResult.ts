import { ApiValidationError } from "../../model";
import { DataRow } from "./DataRow";

/** Result for importing data (CSV, Excel copy-paste etc.) */
export interface ImportResult<TRow extends DataRow> {

  /** Number of ignored lines (empty line or rem/annotated) in the import. */
  ignoredLines: number;

  /** Headers for the import: These are also the properties for the JSON */
  headers: Array<keyof TRow>;

  /** Result data is an array of objects with properties specified as headers */
  data: TRow[];

  /** Errors as validation errors. */
  errors: ApiValidationError[];

}
