import { CalculationRowType, CalculationStatus } from "../../../model";
import {DataRow} from "../DataRow";

/**
 * Data row for calculations data. This is typically used for imports and exports.
 */
export interface CalculationsDataRow extends DataRow  {

  /** Id for the calculation */
  id?: string | null;

  /** Read only calculation status */
  status?: CalculationStatus | null;

  /** The personal ID for the worker. */
  personalId: string;

  /** Occupation classification, used at least for Accident insurance purposes, but may be used for other reporting.
   * For Finnish Salaries use the Statistics Finland
   * "Classification of Occupations 2010" (TK10): https://www.stat.fi/meta/luokitukset/ammatti/017-2018-05-15/index_en.html
   */
  occupationCode?: string | null;

  /** Start date of the work */
  periodStart?: string | null;

  /** End date of the work */
  periodEnd?: string | null;

  /** Description of the work for reporting purposes. Max 32 chars. */
  workDescription?: string | null;

  /** Message which will be displayed in the payslip. */
  salarySlipMessage?: string | null;

  /** Project number for reporting purposes. */
  projectNumber?: string | null;

  /** Cost center for reporting purposes. */
  costCenter?: string | null;

  /** The requested date for the SalaryDate from the employer. */
  salaryDate?: string | null;

  /** Tax if calculated */
  fixedTaxAmount?: number | null;

  /** Logical type of the row */
  rowType: CalculationRowType | null;

  /** Description text of the row that is shown in reports. If null, will be set according to type. */
  message?: string | null;

  /** Count for the row - default is one */
  count?: number | null;

  /** Price for the row */
  price?: number | null;
}
