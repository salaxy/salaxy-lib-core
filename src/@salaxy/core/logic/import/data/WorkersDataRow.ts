import {DataRow} from "../DataRow";

/**
 * Data row for employments data. This is typically used for imports and exports.
 */
export interface WorkersDataRow extends DataRow  {

  /** Id for the worker */
  id?: string | null;

  /** First name */
  firstName?: string | null;

  /** Last name */
  lastName?: string | null;

  /** Email address. */
  email?: string | null;

  /** Telephone number. */
  telephone?: string | null;

  /** Bank account number (IBAN) */
  ibanNumber?: string | null;

  /** Official id of the person or company. */
  officialId?: string | null;

}
