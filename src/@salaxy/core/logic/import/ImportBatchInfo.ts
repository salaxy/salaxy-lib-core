/**
 * Contains the basic details and the current status of the batch phase.
 * Typically this data is created and updated during the import session.
 */
export interface ImportBatchInfo {
  /** Unique id for the batch. */
  id: string;
  /** User friendly name for the batch. */
  name?: string;
  /** Date and time when the batch was created. */
  createdAt?: string;
  /** Date and time when the batch was last updated. */
  updatedAt?: string;
  /**
   * Current phase of the batch:
   *
   * - ```new``` A new batch. Contains no data.
   * - ```read``` The data file (e.g. csv) has been read from file or clipboard.
   * - ```mapped``` The data has been mapped to a supported tabular format.
   * - ```validated``` The data has been validated.
   * - ```imported``` The data has been imported to backend.
   * - ```rejected``` The batch has been rejected manually.
   */
  phase?: "new" | "read" | "mapped" | "validated" | "imported" | "rejected";

  /**
   * Current status of the current phase:
   *
   * - ```info``` Typically the default status for the phase which has not yet been done.
   * - ```success``` The current phase has been successfully finished.
   * - ```warning``` The current phase has been finished with warnings.
   * - ```error``` The current phase contains errors.
   */
  status?: "info" | "success" | "warning" | "error";
}
