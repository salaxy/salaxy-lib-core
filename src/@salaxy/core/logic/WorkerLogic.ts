import { AgeRange, Avatar, AvatarPictureType, CalculationRowType, CalculationRowUnit, EmploymentRelationType, LegalEntityType, PensionCalculation, UserDefinedRow, WorkerAccount, WorkerListItem } from "../model";
import { Dates, Iban } from "../util";
import { EnumerationsLogic } from "./EnumerationsLogic";

/**
 * Provides business logic related to Worker accounts: Workers as Employers see them.
 */
export class WorkerLogic {

  /**
   * Application level single instance of the WorkerLogic.getSampleWorkers() methods.
   * These sample workers can be used and modified in the demo application.
   */
  public static sampleWorkersSingleton = WorkerLogic.getSampleWorkers();

  /**
   * Gets a new blank worker account with default values, suitable for UI binding.
   */
  public static getBlank(): WorkerAccount {
    return {
      avatar: {
        pictureType: AvatarPictureType.Icon,
      },
      employment: {
        irIncomeEarnerTypes: [],
        holidays: {},
        usecase: {
          data: {},
        },
        work: {
          salaryDefaults: [],
        },
      },
      contact: {},
      ageRange: AgeRange.Age18_52,
      entityType: LegalEntityType.PersonCreatedByEmployer,
      id: null,
      createdAt: Dates.getToday(),
      updatedAt: Dates.getToday(),
      isReadOnly: null,
    };
  }

  /**
   * Creates a new avatar object based on firstName and lastName.
   * This simulates how server will eventually create an avatar object for a worker.
   * @param firstName First name of the person.
   * @param lastName Last name of the person.
   * @param color Avatar color. If not set, this will later be random generated (in this implementation, a static grayish color).
   */
  public static createAvatar(firstName: string, lastName: string, color?: string): Avatar {
    firstName = WorkerLogic.capitalizeFirstLetter((firstName || "").trim()) || " ";
    lastName = WorkerLogic.capitalizeFirstLetter((lastName || "").trim()) || " ";
    color = color || "rgb(21, 83, 93)";
    return {
      color, // TODO: Consider random generation, gray or something.
      displayName: (firstName + " " + lastName).trim() || "?",
      firstName,
      entityType: LegalEntityType.PersonCreatedByEmployer,
      initials: (firstName.substr(0, 1) + lastName.substr(0, 1)) || "?",
      lastName,
      pictureType: AvatarPictureType.Icon,
      sortableName: ((lastName + ", " + firstName).replace(/,/g, "").trim()) || "?",
    };
  }

  /**
   * Capitalizes the first character of a string.
   * The method is null-safe: Returns null if null, empty if empty.
   * @param text Text for which the first letter is capitalized.
   */
  public static capitalizeFirstLetter(text: string): string {
    if (!text) {
      return text;
    }
    return text.charAt(0).toUpperCase() + text.slice(1);
  }

  /**
   * Gets a description text for a WorkerAccount that may be either
   * IWorkerListItem or WorkerAccount.
   * @param worker Worker account to describe. Either account or list item.
   */
  public static getDescription(worker: WorkerAccount | WorkerListItem): string {

    const emptyDescription = "-";

    const describe = (w: WorkerAccount) => {
      if (w.employment && w.employment.type && w.employment.type !== EmploymentRelationType.Undefined) {
        let descr = EnumerationsLogic.getEnumLabel("EmploymentRelationType", w.employment.type);
        switch ( w.employment.type ) {
          case EmploymentRelationType.Entrepreneur:
            if (w.employment.pensionCalculation === PensionCalculation.Entrepreneur) {
              descr += " (YEL)";
            } else if (w.employment.pensionCalculation === PensionCalculation.PartialOwner) {
              descr += " (TyEL, osaomistaja)";
            } else if (w.employment.pensionCalculation === PensionCalculation.SmallEntrepreneur) {
              descr += " (YEL, ei vakuuttamisvelvollisuutta)";
            } else if (w.employment.pensionCalculation === PensionCalculation.Employee) {
              descr += " (TyEL)";
            }
            break;
          case EmploymentRelationType.Farmer:
            if (w.employment.pensionCalculation === PensionCalculation.Farmer) {
              descr += " (MYEL)";
            } else if (w.employment.pensionCalculation === PensionCalculation.SmallFarmer) {
              descr += " (MYEL, ei vakuuttamisvelvollisuutta)";
            } else if (w.employment.pensionCalculation === PensionCalculation.Employee) {
              descr += " (TyEL)";
            }
            break;
        }
        return descr;
      }
      return w.contact.email || emptyDescription;
    };

    if (!worker) {
      return emptyDescription;
    }

    if ((worker as WorkerAccount).employment) {
      return describe(worker as WorkerAccount);
    }
    const workerListItem = worker as WorkerListItem;
    if (!workerListItem.otherPartyInfo) {
      return emptyDescription;
    }
    return describe(
      {
        avatar: workerListItem.otherPartyInfo.avatar,
        contact: {
          email: workerListItem.otherPartyInfo.email,
          telephone: workerListItem.otherPartyInfo.telephone,
        },
        employment: {
          type: workerListItem.data.type as any,
          pensionCalculation: workerListItem.data.pensionCalculation as any,
        },
        entityType: workerListItem.otherPartyInfo.avatar.entityType,
        ibanNumber: workerListItem.otherPartyInfo.ibanNumber,
        id: workerListItem.otherId,
        officialPersonId: workerListItem.otherPartyInfo.officialId,
      } as WorkerAccount);
  }

  /** Sample workers for anonymous demo user interfaces. */
  public static getSampleWorkers(): WorkerAccount[] {
    return [
      {
        avatar: {
          entityType: LegalEntityType.PersonCreatedByEmployer,
          firstName: "Erkki",
          lastName: "Esimerkki",
          displayName: "Erkki Esimerkki",
          sortableName: "Esimerkki, Erkki",
          pictureType: AvatarPictureType.Icon,
          color: "lime",
          initials: "EE",
          url: null,
          description: "Esimerkkityöntekijä, oletus: 18-52 vuotta, veroprosentti 23%.",
          id: "example-default",
        },
        contact: {
          email: "erkki.esimerkki@palkkaus.fi",
          telephone: "09 425 50603",
        },
        id: "example-default",
      },
      {
        avatar: {
          entityType: LegalEntityType.PersonCreatedByEmployer,
          firstName: "Laura",
          lastName: "Lukiolainen",
          displayName: "Laura Lukiolainen",
          sortableName: "Lukiolainen, Laura",
          pictureType: AvatarPictureType.Icon,
          color: "pink",
          initials: "LL",
          url: null,
          description: "Esimerkki nuoresta työntekijästä: 17 vuotias",
          id: "example-17",
        },
        contact: {
          email: "laura.lukiolainen@palkkaus.fi",
          telephone: "09 425 50603",
        },
        id: "example-17",
      },
      {
        avatar: {
          entityType: LegalEntityType.PersonCreatedByEmployer,
          firstName: "Tanja",
          lastName: "Eläkeläinen",
          displayName: "Tanja Eläkeläinen",
          sortableName: "Eläkeläinen, Tanja",
          pictureType: AvatarPictureType.Icon,
          color: "blue",
          initials: "TE",
          url: null,
          description: "Esimerkki eläkeläisestä: 65-67 vuotias",
          id: "example-pensioner",
        },
        contact: {
          email: "tanja.elakelainen@palkkaus.fi",
          telephone: "09 425 50603",
        },
        id: "example-pensioner",
      },
    ];
  }

  /**
   * Sets default values for the worker based on employment type.
   * @param worker - Worker to which default values are set.
   */
  public static setEmploymentDefaultValues(worker: WorkerAccount): void {
    if (!worker) {
      return;
    }
    worker.employment.work.description = null;
    worker.employment.work.occupationCode = null;

    switch (worker.employment.type as any) {
      case EmploymentRelationType.MonthlySalary:
      case EmploymentRelationType.HourlySalary:
      case EmploymentRelationType.Salary:
        (worker.employment as any).pensionCalculation = PensionCalculation.Employee;
        break;
      case EmploymentRelationType.EmployedByStateEmploymentFund:
        (worker.employment as any).pensionCalculation = PensionCalculation.Employee;
        break;
      case EmploymentRelationType.PerformingArtist:
        (worker.employment as any).pensionCalculation = PensionCalculation.Employee;
        break;
      case EmploymentRelationType.KeyEmployee:
        (worker.employment as any).pensionCalculation = PensionCalculation.Employee;
        break;
      case EmploymentRelationType.Entrepreneur:
        (worker.employment as any).pensionCalculation = PensionCalculation.Entrepreneur;
        break;
      case EmploymentRelationType.Farmer:
        (worker.employment as any).pensionCalculation = PensionCalculation.Farmer;
        break;
      case EmploymentRelationType.Compensation:
        (worker.employment as any).pensionCalculation = PensionCalculation.Compensation;
        break;
      case EmploymentRelationType.BoardMember:
        (worker.employment as any).pensionCalculation = PensionCalculation.BoardRemuneration;
        worker.employment.work.description = "Hallituksen jäsen";
        break;
      case EmploymentRelationType.Athlete:
        (worker.employment as any).pensionCalculation = PensionCalculation.Athlete;
        worker.employment.work.occupationCode = "34210";
        worker.employment.work.description = "Urheilija";
        break;
    }

    worker.employment.work.salaryDefaults = [];
    const defaultRow: UserDefinedRow = {
      rowIndex: 0,
      rowType: null,
      count: null,
      price: null,
      message: null,
    };
    worker.employment.work.salaryDefaults.push(defaultRow);

    defaultRow.message = null;
    switch (worker.employment.type) {
      case EmploymentRelationType.MonthlySalary:
        defaultRow.rowType = CalculationRowType.MonthlySalary;
        defaultRow.count = 1;
        defaultRow.price = 0;
        defaultRow.unit = null;
        break;
      case EmploymentRelationType.HourlySalary:
        defaultRow.rowType = CalculationRowType.HourlySalary;
        defaultRow.count = 1;
        defaultRow.price = 0;
        defaultRow.unit = CalculationRowUnit.Hours;
        break;
      case EmploymentRelationType.Salary:
        defaultRow.rowType = CalculationRowType.Salary;
        defaultRow.count = 1;
        defaultRow.price = 0;
        defaultRow.unit = null;
        break;
      case EmploymentRelationType.EmployedByStateEmploymentFund:
        defaultRow.rowType = CalculationRowType.HourlySalary;
        defaultRow.count = 1;
        defaultRow.price = 0;
        defaultRow.unit = CalculationRowUnit.Hours;
        break;
      case EmploymentRelationType.PerformingArtist:
        defaultRow.rowType = CalculationRowType.Salary;
        defaultRow.count = 1;
        defaultRow.price = 0;
        defaultRow.unit = null;
        break;
      case EmploymentRelationType.KeyEmployee:
        defaultRow.rowType = CalculationRowType.MonthlySalary;
        defaultRow.count = 1;
        defaultRow.price = 0;
        defaultRow.unit = null;
        break;
      case EmploymentRelationType.Entrepreneur:
      case EmploymentRelationType.Farmer:
        break;
      case EmploymentRelationType.Compensation:
        defaultRow.rowType = CalculationRowType.Compensation;
        defaultRow.count = 1;
        defaultRow.price = 0;
        defaultRow.unit = null;
        break;
      case EmploymentRelationType.BoardMember:
        defaultRow.rowType = CalculationRowType.Board;
        defaultRow.message = "Hallituspalkkio";
        defaultRow.count = 1;
        defaultRow.price = 0;
        defaultRow.unit = null;
        break;
      case EmploymentRelationType.Athlete:
        defaultRow.rowType = CalculationRowType.Salary;
        defaultRow.message = "Urheilijan palkka";
        defaultRow.count = 1;
        defaultRow.price = 0;
        defaultRow.unit = null;
        break;
    }
  }

  /**
   * Returns options object for PensionCalculation selection by employment relation type.
   * @param employmentRelationType Employment relation type of the worker.
   */
  public static getPensionCalculationOptions(employmentRelationType: EmploymentRelationType): Array<{
    /** Value of the calculation */
    value: any,
    /** Text */
    text: string,
    /** Title */
    title?: string
  }> {
    const types: PensionCalculation[] = [];
    switch (employmentRelationType as any) {
      case EmploymentRelationType.HourlySalary:
      case EmploymentRelationType.MonthlySalary:
      case EmploymentRelationType.Salary:
      case undefined:
      case null:
        types.push(PensionCalculation.Employee);
        break;
      case "employedByStateEmploymentFund": // EmploymentRelationType.EmployedByStateEmploymentFund
        types.push(PensionCalculation.Employee);
        break;
      case "performingArtist": // EmploymentRelationType.PerformingArtist
        types.push(...[PensionCalculation.Employee, PensionCalculation.Compensation]);
        break;
      case "keyEmployee": // EmploymentRelationType.KeyEmployee
        types.push(PensionCalculation.Employee);
        break;
      case EmploymentRelationType.Entrepreneur:
        types.push(...[PensionCalculation.Entrepreneur, PensionCalculation.SmallEntrepreneur, PensionCalculation.PartialOwner, PensionCalculation.Employee]);
        break;
      case EmploymentRelationType.Farmer:
        types.push(...[PensionCalculation.Farmer, PensionCalculation.SmallFarmer, PensionCalculation.Employee]);
        break;
      case EmploymentRelationType.Compensation:
        types.push(PensionCalculation.Compensation);
        break;
      case EmploymentRelationType.BoardMember:
        types.push(PensionCalculation.BoardRemuneration);
        break;
      case EmploymentRelationType.Athlete:
        types.push(PensionCalculation.Athlete);
        break;
    }
    return types.map( (type) => {
      const label = EnumerationsLogic.getEnumLabel("PensionCalculation", type);
      const descr = EnumerationsLogic.getEnumDescr("PensionCalculation", type);

      return {value: type, text: label , title: descr && descr !== type ? descr : null };
   });
  }

  /**
   * Populates given user with test data.
   * Sets values only if the firstName and lastName have been given.
   * @param worker - Worker to populate with test data.
   */
  public static populateWorkerWithTestData(worker: WorkerAccount): void {
    if (!worker || !worker.avatar || !worker.avatar.firstName || !worker.avatar.lastName) {
      return;
    }
    // email
    worker.contact.email = this.cleanupEmailPart(worker.avatar.firstName) + "." + this.cleanupEmailPart(worker.avatar.lastName) + "@palkkaus.fi";
    // phone
    worker.contact.telephone = "040 " + (1000000 + Math.round((Math.random() * 8999999))).toString();
    // hetu
    const birthYear = (new Date().getFullYear()) - (20 + Math.round(Math.random() * 40));
    const birthDate = new Date(new Date(birthYear, 0, 1).getTime() + Math.round(Math.random() * (new Date(birthYear + 1, 11, 31).getTime() - new Date(birthYear, 0, 1).getTime())));
    const separator = birthYear >= 2000 ? "A" : "-";
    const personNumber = 900 + Math.round(Math.random() * 99);
    const checksumLetters = "0123456789ABCDEFHJKLMNPRSTUVWXY";
    const sDate = Dates.format(birthDate, "DDMMYY");
    const idChecksum = checksumLetters[parseInt(sDate + personNumber.toString(), 10) % 31];
    worker.officialPersonId = sDate + separator + personNumber.toString() + idChecksum;

    // iban
    const bank = (100000 + Math.round((Math.random() * 99999)));
    const account = (100000 + Math.round((Math.random() * 99999)));
    const bankAccount = `${bank}00${account}`;
    const ibanValue = bankAccount + "15" + "18" + "00";

    const mod97 = (value: string) => {
      let checksum: any = value.slice(0, 2);
      for (let offset = 2; offset < value.length; offset += 7) {
        const fragment = String(checksum) + value.substring(offset, offset + 7);
        checksum = parseInt(fragment, 10) % 97;
      }
      return checksum;
    };
    const remainder = 98 - mod97(ibanValue);
    const ibanChecksum = remainder < 10 ? "0" + remainder.toString() : remainder.toString();
    worker.ibanNumber = Iban.formatIban("FI" + ibanChecksum + bankAccount);
  }

  private static cleanupEmailPart(name: string): string {
    if (!name) {
      return name;
    }
    // to lower case
    name = name.toLowerCase();
    // remove accents
    name = name.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    // leave only letters
    name = name.replace(/[^a-z]+/g, "");
    return name;
  }
}
