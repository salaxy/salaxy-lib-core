import {
  Avatar, Calculation, CalculationRowType, CalculationRowUnit,
  CalculationStatus, FrameworkAgreement,
  LegalEntityType, ResultRow, SalaryKind, UserDefinedRow,
} from "../model";
import { CalculationRowCategories } from "./model";

/**
 * Provides business logic related to building a salary calculator.
 */
export class CalculatorLogic {

  /**
   * Gets a UI cache object that is used for storing UI properties
   * to the calculation until a roundtrip to server (technically calc.$ui).
   * When a round-trip to server occurs (typically recalculate or save),
   * then the property is set to null => empty object.
   */
  public static getUiCache(calc: Calculation): any {
    if (!calc) {
      return null;
    }
    (calc as any).$ui = (calc as any).$ui || {};
    return (calc as any).$ui;
  }

  /**
   * Gets the first row of the calculation assuring that it exists
   * AND that it is a known Salary row or Unknown.
   * This should be the main row of the calculation that replaces the Salary object.
   * @param calc Calculation from which the row is fetched.
   * @param defaultType Default type for the new item if new item needs to be added.
   */
  public static getSalaryRow(calc: Calculation, defaultType: CalculationRowType = CalculationRowType.Unknown): UserDefinedRow {
    if (!calc) {
      return null;
    }
    const allowedTypes = [
      CalculationRowType.Unknown,
      CalculationRowType.Compensation,
      CalculationRowType.Salary,
      CalculationRowType.HourlySalary,
      CalculationRowType.MonthlySalary,
      CalculationRowType.TotalEmployerPayment,
      CalculationRowType.TotalWorkerPayment,
    ];
    this.moveSalaryToRows(calc);
    if (!calc.rows.find((x) => allowedTypes.indexOf(x.rowType) >= 0)) {
      calc.rows.unshift({
        rowType: defaultType,
      });
    }
    return calc.rows.find((x) => allowedTypes.indexOf(x.rowType) >= 0);
  }

  /**
   * Gets the first row of the calculation assuring that it exists.
   * This should be the main row of the calculation that replaces the Salary object.
   * @param calc Calculation from which the row is fetched.
   */
  public static getFirstRow(calc: Calculation): UserDefinedRow {
    // TODO: Consider generalizing this as the basis of getSalaryRow().
    if (!calc) {
      return null;
    }
    this.moveSalaryToRows(calc);
    if (calc.rows.length === 0) {
      calc.rows.push({
        rowType: CalculationRowType.Unknown,
      });
    }
    return calc.rows[0];
  }

  /**
   * Gets a new blank object with default values, suitable for UI binding.
   * This is a synchronous method - does not go to the server.
   */
  public static getBlank(): Calculation {
    const blank: Calculation = {
      employer: {
        avatar: {
          entityType: LegalEntityType.Undefined,
          initials: "?",
        },
        isSelf: true,
      },
      framework: {
        type: FrameworkAgreement.NotDefined,
      },
      info: {},
      rows: [],
      salary: {
        kind: null,
        price: null,
      },
      worker: {
        avatar: {
          entityType: LegalEntityType.Person,
          initials: "?",
        },
        paymentData: {},
        tax: {},
      },
      workflow: {
        status: CalculationStatus.Draft,
      },
      result: {
        irRows: [],
      },
    };
    return blank;
  }

  /**
   * Returns true if calculation totals should be shown:
   * If there are any such rows that the calculation result makes sense.
   */
  public static shouldShowTotals(calc: Calculation): boolean {
    if (!calc || !calc.result || !calc.result.totals) {
      return false;
    }
    return calc.result.totals.total > 0;
  }

  /**
   * Moves the salary defined in Salary object to rows collection.
   * This is needed in old calculations for backward compatibility until moving to v03.
   * NOTE: In v03 this will be done server-side: The Salary will be removed completely.
   * @param calc Calculation to modify
   */
  public static moveSalaryToRows(calc: Calculation): void {
    const salary = calc.salary;
    if (salary && salary.price > 0) {
      let rowType = CalculationRowType.Salary;
      switch (salary.kind) {
        case SalaryKind.Compensation:
        case SalaryKind.HourlySalary:
        case SalaryKind.MonthlySalary:
          // The same underlying enum string
          rowType = salary.kind as any;
          break;
        case SalaryKind.FixedSalary: // One exception
          rowType = CalculationRowType.Salary;
          break;
        case SalaryKind.TotalEmployerPayment:
        case SalaryKind.TotalWorkerPayment:
        case SalaryKind.Undefined:
        default:
          // Should never occur => Default to Unknown, which is probably an error.
          rowType = CalculationRowType.Unknown;
          break;
      }
      if (rowType !== CalculationRowType.Unknown) {
        const newRow = {
          count: salary.amount,
          message: salary.message,
          price: salary.price,
          rowIndex: -1,
          rowType,
          unit: rowType === CalculationRowType.HourlySalary ? CalculationRowUnit.Hours : CalculationRowUnit.Count,
        };
        calc.rows.push(newRow);
        salary.price = 0;
        salary.amount = 0;
        salary.kind = SalaryKind.Undefined;
      }
    }
  }

  /**
   * Gets the Avatar of other party in a calculation:
   * Employer or Worker
   */
  public static getOtherParty(calc: Calculation, currentAvatar: Avatar): Avatar {
    if (currentAvatar) {
      const id = currentAvatar.id;
      if (calc.worker && calc.worker.accountId !== id) {
        return calc.worker.avatar;
      } else if (calc.employer && calc.employer.accountId !== id) {
        return calc.employer.avatar;
      }
    }
    return {
      id: null,
      displayName: "?",
      sortableName: "?",
      initials: "?",
      firstName: "n",
      lastName: "n",
      entityType: LegalEntityType.Undefined,
    };
  }

  /**
   * Gets a total sum for a row category from calculation results.
   * @param calc - Calculation object
   * @param category - Row category (collection of row types)
   */
  public static getTotalForCategory(calc: Calculation, category: "baseSalary" | "salaryAdditions" | "salary" | "benefits" | "expenses" | "deductions" | "holidays" | "expensesSection"): number {
    let total = 0.0;
    this.getResultRowsByCategory(calc, category).forEach((row) => total += row.total);
    return total;
  }

  /**
   * Gets a total sum for a row category from calculation results.
   * @param calc - Calculation object
   * @param types - Array of row types that are included in the result
   */
  public static getTotalForRowsByType(calc: Calculation, ...types: CalculationRowType[]): number {
    let total = 0.0;
    this.getResultRowsByType(calc, ...types).forEach((row) => total += row.total);
    return total;
  }

  /**
   * Filter the result rows of a calculation based on the type
   * @param calc - Calculation object
   * @param types - Array of row types that are included in the result
   */
  public static getResultRowsByType(calc: Calculation, ...types: CalculationRowType[]): ResultRow[] {
    return (!calc.result || !calc.result.rows) ? [] : calc.result.rows.filter((value) => types.indexOf(value.rowType) !== -1);
  }

  /**
   * Filter the result rows of a calculation based on a row category
   * @param calc - Calculation object
   * @param category - Row category (collection of row types)
   */
  public static getResultRowsByCategory(calc: Calculation, category: "baseSalary" | "salaryAdditions" | "salary" | "benefits" | "expenses" | "deductions" | "holidays" | "expensesSection"): ResultRow[] {
    const types = CalculationRowCategories[category];
    return this.getResultRowsByType(calc, ...types);
  }
}
