/**
 * Environment specific configuration for Salaxy API's and JavaScript in general
 */
export interface Config {
      /** The base address of the Salaxy http API server, e.g. https://secure.salaxy.com */
      apiServer?: string | null;
      /** The base address of the Palkkaus web site, e.g. https://www.palkkaus.fi */
      wwwServer?: string | null;
      /** A flag indicating if the current configuration is for test environment only. If true, the site is for testing only. */
      isTestData?: boolean | null;
      /** A flag indicating if the site sets a Salaxy cookie which persists over web sessions. If true, the cookie will be created. */
      useCookie?: boolean | null;
      /** A flag indicating if the API site uses credentials. If true, the credentials will be used. */
      useCredentials?: boolean | null;

      /** InstrumentationKey for Application Insight */
      instrumentationKey?: string | null;
      /** Partner id for site. This is required for example for onboarding with partner specific style. */
      partnerSite?: string | null;
      /** Pro server site address. e.g. https://pro.palkkaus.fi */
      proServer?: string | null;
}
