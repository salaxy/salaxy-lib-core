import {
  calcReportType,
} from "../api";
import {
  ReportType,
} from "../model";

import { Translations } from "./Translations";

/**
 * Framework independendent business logic methods and helpers for Reporting.
 */
export class ReportsLogic {

  /**
   * Gets a link URL for a yearly report. This is a relative link from the servr root (e.g. "/ReportHtml/YearEndReportJson?year=2017").
   * Typically the environment specific implementation needs to figure out the server address and token.
   * @param type - Type of the report must be one of the yearly reports
   * @param year - Year for the report
   * @param token - Token for authentication. Adds "access_token" to the query string.
   * This is typically required from the environment specific implementation,
   * but in exceptional cases you may use some other authentication mechanism and thus leave this null.
   * @param id - Worker ID for those reports that are specific to one Worker.
   * @param id2 - Second Worker ID for those reports that have two Workers in one report
   */
  public static getYearlyReportUrl(type: ReportType, year: number, token?: string, id?: string, id2?: string): string {
    let url: string;
    switch (type) {
      case ReportType.YearEndReport:
        url = `/ReportHtml/YearEndReportJson?year=${year}`;
        break;
      case ReportType.YearlyDetails:
      case ReportType.Insurance:
      case ReportType.HouseholdDeduction:
        url = `/ReportHtml/${type}?year=${year}`;
        break;
      case ReportType.YearlyWorkerSummary:
        url = `/ReportHtml/${type}/${id}?year=${year}`;
        break;
      case ReportType.Unemployment:
        url = `/ReportPdf/${type}?year=${year}`;
        break;
      case ReportType.TaxYearly7801:
        url = `/ReportPdf/${type}/${id}?year=${year}`;
        break;
      case ReportType.TaxHouseholdDeduction14B:
      case ReportType.TaxHouseholdDeduction14BSpouseA:
      case ReportType.TaxHouseholdDeduction14BSpouseB:
        url = `/ReportPdf/${type}/${id}?id2=${id2 || ""}&year=${year}`;
        break;
      default:
        throw new Error("Not a supported year end report: " + type);
    }
    if (token) {
      url = url + "&access_token=" + token;
    }
    return url;
  }

  /**
   * Gets a URL for a calculation report. This is a relative link from the servr root (e.g. "/ReportHtml/SalarySlip/CALC_ID_HERE").
   * Typically the environment specific implementation needs to figure out the server address and token.
   * @param reportType - Type of report
   * @param calcId - Identifier of the calculation. This method requires that the calculation has been saved.
   * @param token - Token for authentication. Adds "access_token" to the query string.
   * This is typically required from the environment specific implementation,
   * but in exceptional cases you may use some other authentication mechanism and thus leave this null.
   * @param cumulative If true, fetches the report as cumulative.
   * Currently only supported by salarySlip
   */
  public static getCalcReportUrl(reportType: calcReportType, calcId: string, token?: string, cumulative = false): string {
    if (!calcId) {
      throw new Error("Calculation ID is required by getCalcReportUrl");
    }
    switch (reportType) {
      case "salarySlip":
      case "paymentReport":
      case "employerReport": {
        let url = `/ReportHtml/${reportType}/${calcId}?cumulative=${cumulative}`;
        if (token) {
          url += "&access_token=" + token;
        }
        return url;
      }
      default:
        throw new Error("Unknown type in getCalcReportUrl: " + (reportType as string));
    }
  }

  /**
   * Gets a URL for a calculation pdf file. This is a relative link from the servr root (e.g. "/ReportPdf/SalarySlip/CALC_ID_HERE").
   * Typically the environment specific implementation needs to figure out the server address and token.
   * @param reportType - Type of report
   * @param calcId - Identifier of the calculation. This method requires that the calculation has been saved.
   * @param inline - If true, the Content-Disposition header is returned with inline parameter.
   * @param token - Token for authentication. Adds "access_token" to the query string.
   * This is typically required from the environment specific implementation,
   * but in exceptional cases you may use some other authentication mechanism and thus leave this null.
   */
  public static getCalcPdfUrl(reportType: calcReportType, calcId: string, inline = false, token?: string): string {
    if (!calcId) {
      throw new Error("Calculation ID is required by getCalcPdfUrl");
    }
    switch (reportType) {
      case "salarySlip":
      case "paymentReport":
      case "employerReport": {
        let url = `/ReportPdf/${reportType}/${calcId}?inline=${inline ? "true" : "false"}`;
        if (token) {
          url += "&access_token=" + token;
        }
        return url;
      }
      default:
        throw new Error("Unknown type in getCalcPdfUrl: " + (reportType as string));
    }
  }

  /** Gets metadata about the supported report types */
  public static getReportTypes(): any {
    // TODO: Merge this with salaxy.moveToCore.enumerations in palkkaus-web-worker project. Type the structure.
    const reportTypes = [
      {
        id: "undefined",
        title: "Valitse...",
        description: "Ei valintaa tai tuntematon tyyppi. ",
        roles: "",
        category: "tech",
      },
      {
        id: "example",
        title: "Esimerkki",
        description: "Esimerkki raportti kehityskäyttöön. Ei dataa, ei parametrejä.",
        roles: "",
        category: "tech",
      },
      {
        id: "monthlyDetails",
        title: "Monthly details",
        description: "The most detailed of the employer monthly reports.",
        roles: "employer",
        category: "monthly",
      },
      {
        id: "taxMonthly4001",
        title: "Monthly tax report (4001)",
        description: "Monthly tax report",
        roles: "employer",
        category: "monthly",
      },
      {
        id: "monthlyPension",
        title: "Monthly pension report",
        description: "Monthly pension report",
        roles: "employer",
        category: "monthly",
      },
      {
        id: "monthlyLiikekirjuri",
        title: "Accounting report (Liikekirjuri)",
        description: "'Liikekirjuri' - a historical monthly report that was used for 2016 and 2017. Will be replaced by Rapko.",
        roles: "company",
        category: "monthly",
      },
      {
        id: "monthlyRapko",
        title: "Accounting report (Rapko)",
        description: "Rapko / Raportointikoodisto is the Finnish standard for accounting.",
        roles: "company",
        category: "monthly",
      },
      {
        id: "yearlyDetails",
        title: "Yearly details",
        description: "Detailed list for employer - this is a general report.",
        roles: "employer",
        category: "yearly",
      },
      {
        id: "yearEndReport",
        title: "Year-end report",
        description: "The year-end report with customer feedback",
        roles: "employer",
        category: "yearly",
      },
      {
        id: "yearlyWorkerSummary",
        title: "Yearly worker summary",
        description: "Detailed report - similar to salary slip - from employer to worker. This report must be given by employer according to Finnish law.",
        roles: "employer",
        category: "yearly",
      },
      {
        id: "taxYearly7801",
        title: "Yearly tax report (7801)",
        description: "Form 7801: Yearly report for Finnish tax authorities",
        roles: "employer",
        category: "yearly",
      },
      {
        id: "unemployment",
        title: "Unemployment insurance report",
        description: "Report for Finnish Unemployment insurance TVR",
        roles: "employer",
        category: "yearly",
      },
      {
        id: "insurance",
        title: "Accident insurance report",
        description: "Report for mandatory accident insurance company",
        roles: "employer",
        category: "yearly",
      },
      {
        id: "householdDeduction",
        title: "Household deduction yearly report",
        description: "General (internal) household deduction yearly report.",
        roles: "household",
        category: "yearly",
      },
      {
        id: "taxHouseholdDeduction14B",
        title: "Household deduction report to authorities (14B)",
        description: "Form 14B: Household deduction report for Finnish tax authorities. All deductible salaries included.",
        roles: "household",
        category: "yearly",
      },
      {
        id: "taxHouseholdDeduction14BSpouseA",
        title: "Household deduction 14B, self only",
        description: "Form 14B: Household deduction report for Finnish tax authorities. Contains only deductible salaries which are attributed to self.",
        roles: "household",
        category: "yearly",
      },
      {
        id: "taxHouseholdDeduction14BSpouseB",
        title: "Household deduction 14B, spouse only",
        description: "Form 14B: Household deduction report for Finnish tax authorities. Contains only deductible salaries which are split to spouse of customer.",
        roles: "household",
        category: "yearly",
      },
      {
        id: "salarySlipCopy",
        title: "Salary slip (worker copy)",
        description: "Copy of the salary slip for the worker.",
        roles: "worker",
        category: "calc",
      },
      {
        id: "salarySlipPaid",
        title: "Salary slip (paid by employer)",
        description: "Salary slip paid by the employer",
        roles: "employer",
        category: "calc",
      },
      {
        id: "employerReport",
        title: "Employer report of salary",
        description: "Report of a single calculation from the Employer point-of-view",
        roles: "employer",
        category: "calc",
      },
      {
        id: "paymentReport",
        title: "Payment report of salary",
        description: "Report of a single calculation from the payments / society point-of-view.",
        roles: "employer",
        category: "calc",
      },
      {
        id: "employmentContract",
        title: "Employment contract",
        description: "Employment contract",
        roles: "all",
        category: "contract",
      },
    ];
    for (const reportType of reportTypes) {
      reportType.title = Translations.get("SALAXY.ENUM.ReportType." + reportType.id + ".label");
      reportType.description = Translations.get("SALAXY.ENUM.ReportType." + reportType.id + ".description");
    }
    return reportTypes;
  }
}
