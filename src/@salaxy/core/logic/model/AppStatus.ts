/**
 * Supports the UserSession object by providing additional information about the status
 * of fetching the session, data, expiration etc.
 */
export interface AppStatus {

  /** If true, the session check from server is in progress */
  sessionCheckInProgress: boolean;

  /**
   * Indicates that we are running in test environment with test data.
   * This information is typically available already before the session is available and may be used in login / register.
   * Null indicates that the information is not available. This should typically be considered false for security.
   */
  isTestData: boolean | null;

  /**
   * PLACEHOLDER: Indicates that the session has expired (as opposed to not checked yet).
   * This property has not yet been implemented.
   */
  sessionExpired?: boolean;

  /**
   * Application specific roles that are set on the client (browser code)
   * as opposed to normal server-side defined roles.
   */
  clientRoles: string[];
}
