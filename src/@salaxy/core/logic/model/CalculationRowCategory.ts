/**
 * Primary categorization of Calculation row types.
 */
export enum CalculationRowCategory {
  /** Base salary: hourly, monthly or compensation. */
  Salary = "salary",
  /** Additions to base salary: overtime, evenings, saturdays etc. */
  SalaryAdditions = "salaryAdditions",
  /** Advanced salary calcualtions: Net total or gross payment total. */
  SalaryCalc = "salaryCalc",
  /** Holidays */
  Holidays = "holidays",
  /** Benefits: car, accomodotaion etc.  */
  Benefits = "benefits",
  /** Expenses, typically tax-free. */
  Expenses = "expenses",
  /** Deductions at different levels. */
  Deductions = "deductions",
  /** Other salary items. */
  Other = "other",
}
