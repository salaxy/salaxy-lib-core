/**
 * Defines the current structure for the core dictionary for each language.
 * Lanugages are fi, en, sv and sx.
 */
export interface CoreDictionaryLang {
  /** SALAXY is the dictionary root object for each language. */
  SALAXY: {
    /** Enumerations */
    ENUM: any,

    /** Model coming from the server-side through API */
    MODEL: any,

    /** The common user interface terms */
    UI_TERMS: any,

    /** The validation error messages and other validation messages. */
    VALIDATION: any,
  }
}
