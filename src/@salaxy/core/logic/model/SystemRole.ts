/**
 * Enumerates the system roles:
 * The roles that are not returned from server as account roles but
 * rather as derived from properties of the current session.
 */
export enum SystemRole {

  /** Session check is in progress: The role of the user is unknown. */
  Unknown = "unknown",

  /** User is anonymous: The session has been checked and there is no token, token has expired or the token is invalid. */
  Anon =  "anon",

  /** User is authenticated either as Company or Person */
  Auth = "auth",

  /**
   * Initialization is in progress:
   * At the moment, this is the same as Session check period (SystemRole Unknown),
   * but there may later be other server-checks in the initialization.
   */
  Init = "init",

  /** User is in Worker role.  */
  Worker = "worker",

  /** User is in Employer role: Either Household or Company. */
  Employer = "employer",

  /** User is Household (Employer) */
  Household = "household",

  /** User is Company (Employer) */
  Company = "company",

  /** User is a Person: Employer and/or Worker (not Company) */
  Person = "person",

  /** Hidden always returns false. Used to hide items in navigation. */
  Hidden = "hidden",

  /** System is running with test data */
  Test = "test",

  /** System is runnin with production data */
  Prod = "prod",

  /** Finnish personal company ("Yksityinen elinkeinonharjoittaja"). */
  FiTm = "fiTm",

  /** Finnish Limited liability company ("Osakeyhtiö"). */
  FiOy = "fiOy",

  /** Finnish Association ("yhdistys") */
  FiRy = "fiRy",

  /**
   * Any user ("any" or "*") always returns true.
   * Note that in most scenarios also no roles ([], "" or null) are equivalent to Any.
   */
  Any = "any",

  /**
   * System role for account that is not verified.
   * Typically this means that the onboarding object should be shown e.g. before payment or on the fornt page.
   * The role is not set while account is not there (anonymous or session check in progress).
   */
  NonVerified = "nonVerified",

  /**
   * Roles which are coming from the primary partner.
   * Indicates that the partner messaging is enabled.
   */
  PartnerMessaging  = "partnerMessaging",

  /**
   * Roles which are coming from the primary partner.
   * Indicates that the partner services are enabled.
   */
  PartnerService = "partnerService",
}
