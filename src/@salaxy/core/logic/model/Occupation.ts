/**
 * Occupation according to Statistics Finland.
 * Used for insurance pricing and reporting.
 */
export interface Occupation {

  /** Salaxy identifier for the Occupation. Begins with code, but may have a suffix. */
  id?: string | null;

  /** Statistics Finland code. There may be duplicates with different id. */
  code?: string | null;

  /** Language for the entry */
  lang?: "fi" | "en" | "sv" | null;

  /** Label */
  label?: string | null;

  /** Lower case text that contains the id, label and potentially other text that should match in searches. */
  search?: string | null;

  /**
   * Free text
   * TODO: Do we need to expose this? Why?
   */
  freeText?: string | null;

  /** Keywords for searching etc. */
  keyword?: string | null;
}
