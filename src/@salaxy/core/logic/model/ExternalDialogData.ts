/**
 * Data that is sent to external dialog and received back from it.
 * Used in cross-site communication.
 */
export class ExternalDialogData<TItem> {

  /**
   * Result of from the external dialog is typically OK, Cancel.
   * Edit dialogs often also have "delete", but you may add custom actions.
   */
  public action: "ok" | "cancel" | "delete" | string;

  /**
   * Data that is being edited in the dialog.
   */
  public item: TItem;

  /**
   * If true, the item has been edited by the user and should typically be saved.
   * NOTE that this is calculated by the Salaxy calling logic: The external dialog does not need to set this.
   */
  public hasChanges?: boolean;
}
