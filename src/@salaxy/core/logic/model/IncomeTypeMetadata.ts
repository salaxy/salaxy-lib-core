import { TransactionCode } from "../../model";

/** Describes the properties and behavior of the Income types in Incomes register. */
export interface IncomeTypeMetadata {

  /** Code in incomes register. Language versioned. */
  code: number;

  /** Language versioned label for the income type. */
  label: string;

  /**
   * Description of the row.
   * Note that this is long text with line feeds / Markdown
   * Text is currently always in Finnish.
   */
  description: string;

  /**
   * Description of how the row is taken into widthholding tax
   * and how insurance payments (pension, unemployment, accident and health care) are made.
   * Note that this is long text with line feeds / Markdown
   * Text is currently always in Finnish.
   */
  taxAndSidecostsDescr: string;
  /** If true, teh row may have a negative value. */
  isNegativeSupported: boolean;
  /** If true Pension payments are made for this income type by default. */
  pensionInsurance: boolean;
  /** If true Accident insurance payments are made for this income type by default. */
  accidentInsurance: boolean;
  /** If true Unemployment insurance payments are made for this income type by default. */
  unemploymentInsurance: boolean;
  /** If true Health insurance payments are made for this income type by default. */
  healthInsurance: boolean;
  /** If true the default values for insurances may be overriden for a single row. */
  insuranceInformationAllowed: boolean;

  /** Describes the behavior of this row in calculating widthholding tax. */
  taxDefault: number;

  /** If false, the row type is not supported in this version */
  isSupported: boolean;

  /** When isSupported is false, there is a describtive text that tells why the type is not supported. Text is always in Finnish. */
  notSupportedError: any;

  /** Default behavior for payment through Salaxy Customer Funds account */
  paymentDefault: number;

  /** Grouping that is used in reporting etc. */
  calcGrouping: string;
  /** Transaction code enumeration */
  transactionCode?: TransactionCode,
}
