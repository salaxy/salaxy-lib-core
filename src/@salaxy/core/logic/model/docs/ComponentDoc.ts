import { AttributeDoc } from "./AttributeDoc";
import { RootDeclarationReflection } from "./RootDeclarationReflection";

/** Component documentation */
export interface ComponentDoc extends RootDeclarationReflection {

  /** The component tag / element in HTML */
  elem?: string;

  /** Name of the controller that implements the functionality of the component */
  controller?: string;

  /** The default template (view) used in the rendering of the component. */
  defaultTemplate?: string;

  /** HTML attributes available to specify options to the component. */
  attributes: AttributeDoc[];

  /** Required components in the outside scope */
  require?: any;

  /** Transclude slots */
  transclude?: any;
}
