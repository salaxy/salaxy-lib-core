/** Mirrors the typedoc ReflectionKind as union string type. */
export type ReflectionKind =
  "Global" |
  "ExternalModule" |
  "Module" |
  "Enum" |
  "EnumMember" |
  "Variable" |
  "Function" |
  "Class" |
  "Interface" |
  "Constructor" |
  "Property" |
  "Method" |
  "CallSignature" |
  "IndexSignature" |
  "ConstructorSignature" |
  "Parameter" |
  "TypeLiteral" |
  "TypeParameter" |
  "Accessor" |
  "GetSignature" |
  "SetSignature" |
  "ObjectLiteral" |
  "TypeAlias" |
  "Event" |
  "ClassOrInterface" |
  "VariableOrProperty" |
  "FunctionOrMethod" |
  "SomeSignature" |
  "SomeModule" |
  "SomeType" |
  "SomeValue"
  ;
