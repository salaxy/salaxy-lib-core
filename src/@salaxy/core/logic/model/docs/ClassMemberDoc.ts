import { DeclarationReflection } from "./DeclarationReflection";

/**
 * Documentation for a class member: Currently Controller member,
 * but may be later extended to other classes / containers.
 */
export interface ClassMemberDoc extends DeclarationReflection {

  /** Typedoc reflection type of the member. */
  type?: any;

  /** Display grouping for the member: E.g. accessors are considered properties here. */
  grouping: "Property" | "Method" | "Other";

  /**
   * For grouping type "Method", lists parameters
   * TODO: Consider making a separate type for properties.
   */
  parameters?: ClassMemberDoc[];
}
