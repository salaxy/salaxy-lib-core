/** Simplified read-only interface for typedoc type. */
export interface TypeDocType {
  /** Type of type */
  type: "intrinsic" | "union" | "array" | "stringLiteral" | "typeParameter" | "reference" | string;

  /** Name of type in intrinsic and typeParameter */
  name?: string;

  /** Collection of types in union */
  types?: TypeDocType[];

  /** Element type in array */
  elementType?: TypeDocType;

  /** Value if literal */
  value?: string;

  /** Identifier in reference */
  id?: number;

  /** Type arguments in reference */
  typeArguments?: TypeDocType[];
}
