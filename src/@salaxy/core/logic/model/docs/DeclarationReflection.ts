import { ReflectionKind } from "./ReflectionKind";

/**
 * Relevant information from typedoc reflection.
 */
export interface DeclarationReflection {

  /** Class name: Name of the component, controller etc. */
  name?: string;

  /** Raw typedoc level type of reflection. */
  rawKind?: ReflectionKind;

  /** The short text: First paragraph of the documentation or if not available (e.g. parameters), then the text. */
  shortText?: string;

  /** Full description: shortText + longer text field. */
  descr?: string;

  /** Short example(s) (inline in documentation) */
  example?: string;

}
