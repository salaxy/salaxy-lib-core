import { ClassMemberDoc } from "./ClassMemberDoc";
import { RootDeclarationReflection } from "./RootDeclarationReflection";
import { TypeDocType } from "./TypeDocType";

/**
 * Documentation of a Class or an Interface.
 * This also includes controllers and components.
 */
export interface ClassDoc extends RootDeclarationReflection {

  /** Public properties, fields and functions. */
  members: ClassMemberDoc[];

  /**
   * Collection of reference types that this class extends (extendedTypes).
   * Note that string may also contain type parameters as in CrudControllerBase<YearlyFeedback>.
   */
  extends: TypeDocType[];

  /**
   * Collection of interfaces that this class implements (implementedTypes).
   * Note that string may also contain type parameters as in CrudControllerBase<YearlyFeedback>.
   */
  implements: TypeDocType[];

  /** Types that extend this type (extendedBy) */
  extendedBy: TypeDocType[];

  /** Types that iplement this interface (implementedBy) */
  implementedBy: TypeDocType[];

  /**
   * Salaxy specific logic may add usages here.
   * Currently, components will add themselves to controllers.
   */
  usages?: string[];
}
