import { DeclarationReflection } from "./DeclarationReflection";

/**
 * Defines a root level declaration (Salaxy libraries point-of-view): Interface, class or enumeration.
 * Other declarations are children of these declarations: properties, methods, parameters etc.
 * This structure is a simplification of the general programming strucutres, but fits well with our documentation needs.
 */
export interface RootDeclarationReflection extends DeclarationReflection {

  /** Reflection ID for the library. Unique within one library. */
  id: number;

  /**
   * Type of item: "class", "interface" and "enum" are root level types,
   * "controller", "controllerBase" and "component" are specialized types classes.
   */
  kind: "class" | "interface" | "controller" | "controllerBase" | "component" | "enum";

  /** The library that defines the item */
  lib: string;

  /** Primary grouping depending on the library */
  group?: string;

  /** The source file that defines the component */
  source?: string;

  /** AngularJS views (templates) that contain more comprehensive example pages. */
  exampleUrls?: string[];

  /** Paths to screen shot pictures.  */
  screenShots?: string[];

}
