import { RootDeclarationReflection } from "./RootDeclarationReflection";

/**
 * Provides information about an enumeration. Currently, only string enumerations are supported.
 */
export interface EnumDoc extends RootDeclarationReflection {

  /** Member names */
  names: string[];

  /** Member values */
  values: string[];
}
