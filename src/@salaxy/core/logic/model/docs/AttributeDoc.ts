import { DeclarationReflection } from "./DeclarationReflection";

/** Component attribute */
export interface AttributeDoc extends DeclarationReflection {

  /** HTML attribute name e.g. my-prop for myProp. */
  attr?: string;

  /** Angular JS binding type */
  binding?: "<" | "@" | "=" | "&";

  /** Typedoc reflection type of the memeber. */
  type?: any;

  /** Controller that implements the property */
  controller?: string;

}
