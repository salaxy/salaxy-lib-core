import { generatedEnumData } from "../codegen";
import { EnumMetadata, EnumValueMetadata } from "./model";
import { Translations } from "./Translations";

/**
 * Provides metadata and other helpers for enumerations.
 */
export class EnumerationsLogic {

  /**
   * Gets a label text for an enumeration value.
   * @param enumType Name of the Type of the enumeration.
   * @param memberName Name of the member for which the label is fetched.
   * Also supports the value of the member if there is only difference in casing (which is the normal convention).
   * @param defaultValue The default value if the translation is not found.
   * Supports special keys "#name" and "name" to return the member name either with or without hash in the beginning.
   * Default is "#name", which returns "#[MemberName]" if the translation is not found.
   * @returns Language specific text form translations or the memberName text if no translation is found.
   */
  public static getEnumLabel(enumType: string, memberName: string, defaultValue: null | string | "#name" | "name" = "#name"): string {
    if (!enumType || !memberName) {
      return `#err ${enumType || "NoEnumType"}.${memberName || "NoEnumValue"}`;
    }
    enumType = this.upperCamelCase(enumType);
    memberName = this.lowerCamelCase(memberName);
    if (defaultValue === "name") {
      defaultValue = this.upperCamelCase(memberName);
    } else if (defaultValue === "#name") {
      defaultValue = this.upperCamelCase(memberName);
    }
    return Translations.getWithDefault(`SALAXY.ENUM.${enumType}.${memberName}.label`) || defaultValue;
  }

  /**
   * Gets a description text for an enumeration value in current language.
   * @param enumType - Name of the Type of the enumeration.
   * @param memberName Name of the member for which the description is fetched.
   * Also supports the value of the member if there is only difference in casing (which is the normal convention).
   * @param defaultValue The default value if the translation is not found. Default is null.
   * Supports special keys "#name" and "name" to return the member name either with or without hash in the beginning.
   * @returns Language specific text form translations or null if none is found.
   */
  public static getEnumDescr(enumType: string, memberName: string, defaultValue?: null | string | "#name" | "name"): string {
    if (!enumType || !memberName) {
      return `#err ${enumType || "NoEnumType"}.${memberName || "NoEnumValue"}`;
    }
    enumType = this.upperCamelCase(enumType);
    memberName = this.lowerCamelCase(memberName);
    if (defaultValue === "name") {
      defaultValue = this.upperCamelCase(memberName);
    } else if (defaultValue === "#name") {
      defaultValue = this.upperCamelCase(memberName);
    }
    return Translations.getWithDefault(`SALAXY.ENUM.${enumType}.${memberName}.description`) || defaultValue;
  }

  /**
   * Gets a very short user interface text for an enumeration.
   * Currently only suppotrted for Unit, but we expect more of these.
   * If the texts will be language-versioned, that logic will be here as well.
   * @param enumType - Name of the Type of the enumeration.
   * @param enumValue - Value for which the text is fetched.
   */
  public static getEnumShortText(enumType: string, enumValue: string): string {
    if (!enumType || !enumValue) {
      return `#err ${enumType || "NoEnumType"}.${enumValue || "NoEnumValue"}`;
    }
    enumType = this.upperCamelCase(enumType);
    enumValue = this.lowerCamelCase(enumValue);
    switch (enumType) {
      case "Unit": {
        const unitTexts = {
          undefined: "kpl?",
          hour: "h",
          day: "pv",
          week: "vko",
          period: "jaksoa",
          one: "",
          count: "kpl",
          percent: "%",
          kilometers: "km",
        };
        return unitTexts[enumValue];
      }
      default: {
        throw new Error(`Enum type ${enumType} is not supported in getEnumShortText().`);
      }
    }
  }

  /**
   * Gets the metadata (texts etc.) for an enumeration value.
   * @param enumType - Name of the Type of the enumeration.
   * @param enumValue - Value for which the label is fetched.
   */
  public static getEnumValueMetadata(enumType: string, enumValue: string): EnumValueMetadata {
    const typeMetaData = EnumerationsLogic.getEnumMetadata(enumType);
    if (!enumValue || !typeMetaData) {
      return null;
    }
    enumValue = this.lowerCamelCase(enumValue);
    const enumValueObj = typeMetaData.values.filter((x) => x.name === enumValue)[0];
    return enumValueObj;
  }

  /**
   * Gets the metadata (texts etc.) for an enumeration and its values.
   * @param enumType - Name of the Type of the enumeration.
   */
  public static getEnumMetadata(enumType: string): EnumMetadata {
    if (!enumType) {
      return null;
    }
    enumType = this.upperCamelCase(enumType);
    let enumObj = generatedEnumData.filter((x) => x.name === enumType)[0];
    const override = EnumerationsLogic.enumDataOverride.filter((x) => x.name === enumType)[0];
    enumObj = override || enumObj;
    if (!enumObj) {
      return null;
    }
    const result: EnumMetadata = {
      name: enumType,
      descr: enumObj.description,
      values: enumObj.values.map((x) => {
        return {
          name: x.name,
          order: x.order,
          label: this.getEnumLabel(enumType, x.name),
          descr: this.getEnumDescr(enumType, x.name),
        };
      }),
    };
    return result;
  }

  /** Gets the currently known enumerations. */
  public static getEnums(): string[] {
    return generatedEnumData.map((x) => x.name);
  }

  /**
   * You can override values of generatedEnumData without generating the entire set of enums from server.
   * TODO: These enums (if any) should be moved to C# code server-side and removed from here.
   */
  private static enumDataOverride = [{
    "name": "NewEnumName",
    "description": null,
    "values": [
      { "order": 0, "name": "undefined" },
      { "order": 1, "name": "enum1" },
    ],
  }, {
    "name": "SummaryTransactionCode",
    "description": "Income type code for Payment Summary Report",
    "values": [
      { "order": 0, "name": "undefined" },
      { "order": 101, "name": "noWagesPayable" },
      { "order": 102, "name": "employersHealthInsuranceContribution" },
      { "order": 103, "name": "deductionsToBeMadeFromTheEmployersHealthInsuranceContribution" },
    ],
  }, {
    "name": "ProfessionType",
    "description": "Profession type for Earnings Payment Report",
    "values": [
      { "order": 1, "name": "statisticsFinland" },
      { "order": 2, "name": "keva" },
      { "order": 3, "name": "bankOfFinland" },
      { "order": 4, "name": "trafi" },
    ],
  },
  {
    name: "EarningsPaymentReportStatus",
    description: "Status codes for Incomes registry Earnings Payment Reports",
    values: [
      { order: 0, name: "new" },
      { order: 1, name: "succeeded" },
      { order: 2, name: "canceled" },
      { order: 3, name: "error" },
      { order: 4, name: "invalid" },
      { order: 5, name: "scheduled" },
    ],
  },
  {
    name: "PayerSummaryReportStatus",
    description: "Status codes for Incomes registry Payment Summary Reports",
    values: [
      { order: 0, name: "new" },
      { order: 1, name: "succeeded" },
      { order: 2, name: "canceled" },
      { order: 3, name: "error" },
      { order: 4, name: "invalid" },
      { order: 5, name: "scheduled" },
    ],
  },
  ];

  private static lowerCamelCase(text: string) {
    return text.substr(0, 1).toLowerCase() + text.substr(1);
  }

  private static upperCamelCase(text: string) {
    return text.substr(0, 1).toUpperCase() + text.substr(1);
  }

}
