import { PensionCompany } from "../model";

/** Logic related to settings: Insurances, pension etc. */
export class SettingsLogic {

  /**
   * Returns pension number for test account
   * @param company The company enumerations.
   * Only the most common pension companies are supported.
   */
   public static getPensionNumberForTest(company: PensionCompany): string | null {

    switch (company) {
      case PensionCompany.Varma:
        return "55-1111111S";
      case PensionCompany.Elo:
        return "54-11111114";
      case PensionCompany.Ilmarinen:
        return "46-1111111Y";
      case PensionCompany.Veritas:
        return "56-1111111D";
      case PensionCompany.Apteekkien:
        return "80009-1111N";
      case PensionCompany.Verso:
        return "80008-11113";
      default:
        return null
    }

  }

}
