import { AvatarPictureType, ContractParty, ContractPartyType, LegalEntityType, WorkerAccount } from "../model";

/**
 * Provides business logic related to displaying / editing Contact objects
 */
export class ContractPartyLogic {

  /**
   * Gets a new blank Contact with default values, suitable for UI binding.
   */
  public static getBlank(): ContractParty {
    return {
      avatar: {
        entityType: null,
        lastName: null,
        pictureType: AvatarPictureType.Icon,
        isReadOnly: false,
      },
      contact: {
        countryCode: "fi",
      },
      isSelf: false,
      isSigned: false,
      contractPartyType: ContractPartyType.CustomContact,
      isEmployer: false,
    };
  }

  /** Converts a WorkerAccount object to a ContractParty object. */
  public static getFromWorker(worker: WorkerAccount): ContractParty {
    return {
      avatar: worker.avatar,
      contact: worker.contact,
      contractPartyType: ContractPartyType.CustomContact,
      iban: worker.ibanNumber,
      id: worker.id,
      isEmployer: false,
      isSelf: false,
      isSigned: false,
      officialId: worker.officialPersonId,
    };
  }

  /** Gets sample contacts for demonstration user interfaces. */
  public static getSampleContacts(): ContractParty[] {
    return [
      {
        id: "example-default",
        avatar: {
          entityType: LegalEntityType.Person,
          firstName: "Erkki",
          lastName: "Esimerkki",
          displayName: "Erkki Esimerkki",
          sortableName: "Esimerkki, Erkki",
          pictureType: AvatarPictureType.Icon,
          color: "lime",
          initials: "EE",
          url: null,
          description:
            "Esimerkkityöntekijä, oletus: 18-52 vuotta, veroprosentti 25%.",
          id: "example-default",
        },
        contact: {
          email: "erkki@esimerkki.fi",
          telephone: "555-5555555",
          street: "Mallikatu 3",
          postalCode: "333333",
          city: "Jokupaikka",
          countryCode: "fi",
        },
      },
      {
        id: "example-17",
        avatar: {
          entityType: LegalEntityType.Person,
          firstName: "Laura",
          lastName: "Lukiolainen",
          displayName: "Laura Lukiolainen",
          sortableName: "Lukiolainen, Laura",
          pictureType: AvatarPictureType.Icon,
          color: "pink",
          initials: "LL",
          url: null,
          description: "Esimerkki nuoresta työntekijästä: 17 vuotias, veroprosentti 0.",
          id: "example-17",
        },
        contact: {
          email: "laura@esimerkki.fi",
          telephone: "222-2222222",
          street: "Lukiolaisenkatu 5",
          postalCode: "222222",
          city: "Jokupaikka",
          countryCode: "fi",
        },
      },
      {
        id: "example-company",
        avatar: {
          entityType: LegalEntityType.Company,
          firstName: "Mallitakomo Oy",
          lastName: null,
          displayName: "Mallitakomo Oy",
          sortableName: "Mallitakomo Oy",
          pictureType: AvatarPictureType.Icon,
          color: "red",
          initials: "MT",
          url: null,
          description: "Esimerkkiyritys Tampereelta",
          id: "example-company",
        },
        contact: {
          email: "takomo@esimerkki.fi",
          telephone: "888-88888888",
          street: "Takomokatu 8",
          postalCode: "8888888",
          city: "Tampere",
          countryCode: "fi",
        },
      },
      {
        id: "example-pensioner",
        avatar: {
          entityType: LegalEntityType.Person,
          firstName: "Tanja",
          lastName: "Eläkeläinen",
          displayName: "Tanja Eläkeläinen",
          sortableName: "Eläkeläinen, Tanja",
          pictureType: AvatarPictureType.Icon,
          color: "blue",
          initials: "TE",
          url: null,
          description: "Esimerkki eläkeläisestä: 65-67 vuotias, veroprosentti 10%.",
          id: "example-pensioner",
        },
        contact: {
          email: "tanja@esimerkki.fi",
          telephone: "777-7777777",
          street: "Eläkeläisenkatu 7",
          postalCode: "111111",
          city: "Jokupaikka",
          countryCode: "fi",
        },
      },
    ];
  }
}
