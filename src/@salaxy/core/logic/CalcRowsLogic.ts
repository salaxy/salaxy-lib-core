import { CalculationRowType, CalculationRowUnit, Unit } from "../model";
import { EnumerationsLogic } from "./EnumerationsLogic";
import { CalcRowConfig, calcRowsMetadata, CalculationRowCategory } from "./model";
import { Translations } from "./Translations";

/**
 * Provides business logic for Calculation rows:
 * How to create user interfaces, reports etc. based on them and their categories.
 */
export class CalcRowsLogic {

  /**
   * Gets a single UI configuration for Calculation row.
   * Searches through all row types without role filtering.
   * @param type Type for which the configuration is fetched.
   */
  public static getRowConfig(type: CalculationRowType): CalcRowConfig {
    if (!type) {
      return null;
    }
    if (type == CalculationRowType.Unknown) {
      return this.getUnknownConfig();
    }
    const result = CalcRowsLogic.getAllRowConfigs().find((x) => x.name === type);
    if (result == null) {
      console.error("Unknown row type: " + type);
    }
    return result;
  }

  /**
   * Gets an indicator string (1-2 characters) for a unit.
   * @param unit Unit that should be described.
   * @returns Given unit indicator or null if none is matched (convert to empty string if necessary).
   */
  public static getUnitIndicator(unit: CalculationRowUnit | Unit | string): string {
    // TODO: Consider another location
    // TODO: After views fixed, remove non-plural strings.
    switch (unit) {
      case Unit.Euro:
      case "eur":
      case "euros":
      case "€":
        return "€";
      case Unit.Days:
      case CalculationRowUnit.Days:
      case "day":
        return "pv";
      case Unit.Hours:
      case CalculationRowUnit.Hours:
      case "hour":
        return "h";
      case Unit.Kilometers:
      case CalculationRowUnit.Kilometers:
      case "km":
        return "km";
      case Unit.Percent:
      case CalculationRowUnit.Percent:
      case "%":
        return "%";
      case Unit.Count:
      case CalculationRowUnit.Count:
        return "kpl";
      case Unit.Weeks:
      case "week":
        return "vko";
      case Unit.Period:
        return "jakso";
      case CalculationRowUnit.Undefined:
      case Unit.Undefined:
      case Unit.One:
      default:
        return null;
    }
  }

  /** Gets the row configuration for row type "unknown". */
  public static getUnknownConfig(): CalcRowConfig {
    if (!this.unknownRowConfig) {
      this.unknownRowConfig = this.getRowConfigInternal({
        order: 0,
        name: "unknown",
        category: CalculationRowCategory.Other,
        color: "#ffffff",
        amount: {
          default: 1,
          input: "hidden",
        },
        total: { input: "hidden" },
        price: { input: "hidden" },
      });
    }
    return this.unknownRowConfig;
  }

  /** Row configuration for unknown row type. */
  private static unknownRowConfig: CalcRowConfig;

  /**
   * Creates a new CalcRowsLogic helper wichh adapts to role logic (company/household).
   * @param role Role for which the rows are fetched.
   */
  constructor(private role: "household" | "company" = "company") {
  }

  /**
   * Gets the row types by category.
   * The instance methods filter types based on role.
   * @param category Category to fetch
   */
  public getRowTypesByCategory(category: CalculationRowCategory): CalculationRowType[] {
    return CalcRowsLogic.rowsByCategory[category] || [];
  }

  /**
   * Gets the row configurations by category(ies).
   * The instance methods filter types based on role.
   * @param categories List of categories to filter by.
   */
  public getRowConfigsByCategory(categories: CalculationRowCategory[]): CalcRowConfig[] {
    const types = [];
    for (const cat of categories || []) {
      types.push(...this.getRowTypesByCategory(cat));
    }
    return this.getRowConfigsByType(types);
  }

  /**
   * Gets the row configurations based on a list of row types.
   * The instance methods filter types based on role.
   * @param types List of types to filter by.
   */
  public getRowConfigsByType(types: CalculationRowType[]): CalcRowConfig[] {
    types = types || [];
    return this.getRowConfigs().filter((x) => types.indexOf(x.name as CalculationRowType) >= 0);
  }

  /**
   * Gets a single UI configuration for Calculation row.
   * This method filters for user role. Use static method to return any row configuration (also faster).
   * The instance methods filter types based on role.
   * @param type Type for which the configuration is fetched.
   */
  public getRowConfig(type: CalculationRowType): CalcRowConfig {
    if (!type) {
      return null;
    }
    if (type == CalculationRowType.Unknown) {
      return CalcRowsLogic.getUnknownConfig();
    }
    const result = this.getRowConfigs().find((x) => x.name === type);
    if (result == null) {
      console.error("Unknown row type: " + type);
    }
    return result;
  }

  /**
   * Gets all the UI configuration objects for calculation rows.
   * Caches the data based on role and language.
   */
  public getRowConfigs(): CalcRowConfig[] {
    const cacheKey = this.role + Translations.getLanguage();
    if (!CalcRowsLogic.rowsCache[cacheKey]) {
      if (this.role === "household") {
        CalcRowsLogic.rowsCache[cacheKey] = CalcRowsLogic.getAllRowConfigs().filter((x) =>
            x.name !== CalculationRowType.Compensation
            && x.name !== CalculationRowType.Board
            && x.name !== CalculationRowType.IrIncomeType
            && x.name !== CalculationRowType.OtherCompensation
            && x.name !== CalculationRowType.NonProfitOrg
            );
      } else {
        CalcRowsLogic.rowsCache[cacheKey] = CalcRowsLogic.getAllRowConfigs().filter((x) => x.name !== CalculationRowType.ChildCareSubsidy)
      }
    }
    return CalcRowsLogic.rowsCache[cacheKey];
  }

  /** Gets all the row configs cached for each language if language changes. */
  public static getAllRowConfigs(): CalcRowConfig[] {
    const lang = Translations.getLanguage();
    if (!CalcRowsLogic.rowsCache[lang]) {
      CalcRowsLogic.rowsCache[lang] = calcRowsMetadata.map((x) => CalcRowsLogic.getRowConfigInternal(x));
    }
    return CalcRowsLogic.rowsCache[lang];
  }

  /** Raw classification of row types to primary categories. */
  public static rowsByCategory = {

    /** Base salary types: the ones that you define directly */
    salary: [
      CalculationRowType.Salary,
      CalculationRowType.HourlySalary,
      CalculationRowType.MonthlySalary,
      CalculationRowType.Compensation,
      CalculationRowType.IrIncomeType,
      CalculationRowType.Board,
    ],

    /** Additions to base salary - typically a percentage of base salary */
    salaryAdditions: [
      CalculationRowType.Overtime,
      CalculationRowType.TesWorktimeShortening,
      CalculationRowType.EveningAddition,
      CalculationRowType.NightimeAddition,
      CalculationRowType.SaturdayAddition,
      CalculationRowType.SundayWork,
      CalculationRowType.OtherAdditions,
      CalculationRowType.Remuneration,
      CalculationRowType.OtherCompensation,
      CalculationRowType.WorkingTimeCompensation,
      CalculationRowType.EmploymentTermination,
      CalculationRowType.HourlySalaryWithWorkingTimeCompensation,
    ],

    /** Benefits: Not paid as money but taxable income */
    benefits: [
      CalculationRowType.MealBenefit,
      CalculationRowType.PhoneBenefit,
      CalculationRowType.SubsidisedCommute,
      CalculationRowType.CarBenefit,
      CalculationRowType.AccomodationBenefit,
      CalculationRowType.OtherBenefit,
      CalculationRowType.ChildCareSubsidy,
    ],

    /** Expenses and other non-taxed compensations */
    expenses: [
      CalculationRowType.DailyAllowance,
      CalculationRowType.Expenses,
      CalculationRowType.MilageOwnCar,
      CalculationRowType.MealCompensation,
      CalculationRowType.ToolCompensation,
      CalculationRowType.MilageDaily,
      CalculationRowType.MilageOther,
      CalculationRowType.NonProfitOrg,
      CalculationRowType.DailyAllowanceHalf,
    ],

    /** Holidays */
    holidays: [
      CalculationRowType.HolidayCompensation,
      CalculationRowType.HolidayBonus,
      CalculationRowType.HolidaySalary,
    ],

    /** Deductions */
    deductions: [
      CalculationRowType.UnionPayment,
      CalculationRowType.Advance,
      CalculationRowType.Foreclosure,
      CalculationRowType.OtherDeductions,
      CalculationRowType.PrepaidExpenses,
    ],

    /** Types that are used only as temporary / input types in Calculations - not in the results. */
    salaryCalc: [
      CalculationRowType.TotalWorkerPayment,
      CalculationRowType.TotalEmployerPayment,
    ],

    /** Other row types */
    other: [
      // CalculationRowType.ChildCareSubsidy,
    ],
  };

  private static rowsCache: any = {};

  /** Converts a raw excel generated row configuration to language-versioned JSON object. */
  private static getRowConfigInternal(rowConfig: CalcRowConfig): CalcRowConfig {
    if (!rowConfig) {
      return null;
    }
    let kind = rowConfig.kind?.name ? EnumerationsLogic.getEnumMetadata(rowConfig.kind.name) : null;
    if (kind?.values?.length < 2) {
      kind = null;
    }
    return {
      name: rowConfig.name,
      order: rowConfig.order,
      kind,
      label: Translations.get("SALAXY.ENUM.CalculationRowType." + rowConfig.name + ".label"),
      descr: Translations.get("SALAXY.ENUM.CalculationRowType." + rowConfig.name + ".description"),
      moreInfo: Translations.getWithDefault("SALAXY.ENUM.CalculationRowType." + rowConfig.name + ".moreInfo", null),
      category: rowConfig.category,
      favorite: rowConfig.favorite || 0,
      color: rowConfig.color || "#eee",
      iconText: Translations.getWithDefault("SALAXY.ENUM.CalculationRowType." + rowConfig.name + ".iconText", null) || "NN",
      price: {
        label: Translations.getWithDefault("SALAXY.ENUM.CalculationRowType." + rowConfig.name + ".priceLabel", null) || "Hinta",
        input: rowConfig.price.input || "number",
        default: rowConfig.price.default || null,
      },
      amount: {
        label: Translations.getWithDefault("SALAXY.ENUM.CalculationRowType." + rowConfig.name + ".amountLabel", null) || "Määrä",
        input: rowConfig.amount.input || "number",
        default: rowConfig.amount.default || null,
        unit: rowConfig.amount.unit || null,
      },
      total: {
        input: rowConfig.total.input || "readOnly",
      },
    };
  }
}
