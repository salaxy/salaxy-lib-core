import { Config } from "./Config";

/** This file assigns config to global salaxy namespace. */
declare const globalThis;
/** This file assigns config to global salaxy namespace. */
declare const global;

/**
 * Class for reading Environment specific configuration for Salaxy API's and JavaScript in general
 */
export class Configs {

  private static getGlobalConfig(): Config {
    const g = this.getGlobal();
    return (g.salaxy && g.salaxy.config) ? g.salaxy.config : null;
  }

  private static setGlobalConfig(config: Config) {
    const g = this.getGlobal();
    g.salaxy = g.salaxy || {};
    g.salaxy.config = config;
  }

  private static getGlobal(): any {
    if (typeof globalThis !== "undefined") { return globalThis; }
    if (typeof self !== "undefined") { return self; }
    if (typeof window !== "undefined") { return window; }
    if (typeof global !== "undefined") { return global; }
    throw new Error("unable to locate global object");
  }

  /**
   * Returns current configuration.
   */
  public static get current(): Config {
    return this.getGlobalConfig();
  }

  /**
   * Set current configuration.
   */
  public static set current(config: Config) {
    this.setGlobalConfig(config);
  }

  /** Get global, globalThis object */
  public static get global(): any {
    return this.getGlobal();
  }
}
