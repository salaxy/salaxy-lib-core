import { incomeTypeCodes } from "../codegen";
import { IncomeTypeMetadata } from "./model";
import { Translations } from "./Translations";
import { TransactionCode } from "../model";

/**
 * Provides business logic for Income types in Incomes Registry (Tulorekisteri).
 */
export class IncomeTypesLogic {

  /** Cached language-versioned list of all types - cached version of getAll() */
  public static allTypes: IncomeTypeMetadata[] = IncomeTypesLogic.getAll();

  /**
   * Cached language-versioned list of supported types.
   * This is the list of types that you typically use.
   */
  public static supportedTypes: IncomeTypeMetadata[] = IncomeTypesLogic.getAll().filter((x) => x.isSupported);

  /**
   * Searches the income type texts (supportedTypes) using the current language version.
   * Returns all types if the search string is less than 2 chars.
   * @param searchString Search text - will be used as case-insensitive. Will be trimmed. Minimum length 2.
   */
  public static search(searchString: string): IncomeTypeMetadata[] {
    if (!searchString || searchString.trim().length < 2) {
      return IncomeTypesLogic.supportedTypes;
    }
    searchString = searchString.trim().toLowerCase();
    return IncomeTypesLogic.supportedTypes.filter((x) => x.code.toString().indexOf(searchString) >= 0
      || x.label.toLowerCase().indexOf(searchString) >= 0);
  }

  /**
   * Gets all codes in Incomes register reporting.
   * This also includes the Salaxy internal codes eg. 1: "Ignored" / "Not reported"
   * For normal language-versioned behavior use cached allTypes field.
   * @param language Language for the label.
   */
  public static getAll(language: "fi" | "en" | "sv" | null = null): IncomeTypeMetadata[] {
    language = language || Translations.getLanguage() as any || "fi";
    const allCodes: IncomeTypeMetadata[]  = [{
      code: 1,
      label: (language === "en" ? "No IR reporting" : (language === "sv" ? "Ingen IR reporting" : "Ei ilmoiteta tulorekisteriin")),
      isNegativeSupported: true,
      pensionInsurance: false,
      accidentInsurance: false,
      unemploymentInsurance: false,
      healthInsurance: false,
      insuranceInformationAllowed: true,
      taxDefault: 0,
      notSupportedError: null,
      description: "Tätä riviä ei ilmoiteta tulorekisteriin. Tällaisia rivejä ovat esimerkiksi palkan yhteydessä.",
      taxAndSidecostsDescr: "Rivin summasta ai vähennetä ennakonpidätystä eikä työnantajan sosiaalivakuutusmaksuja.",
      isSupported: true,
      paymentDefault: 1,
      calcGrouping: "expenses",
    }].concat(this.getOriginal());
    return allCodes;
  }

  /**
   * Gets the original codes in Incomes register.
   * This does not include virtual codes, eg. 1: "Ignored" / "Not reported"
   * For normal language-versioned behavior use cached originalTypes field.
   * @param language Language for the label. Typically use null for automatic language versioning.
   */
  public static getOriginal(language: "fi" | "en" | "sv" | null = null): IncomeTypeMetadata[] {
    language = language || Translations.getLanguage() as any || "fi";

    return incomeTypeCodes.map((x) => ({
      code: x.code,
      label: (language === "en" ? x.nameEn : (language === "sv" ? x.nameSv : x.nameFi)),
      isNegativeSupported: x.isNegativeSupported,
      pensionInsurance: x.pensionInsurance,
      accidentInsurance: x.accidentInsurance,
      unemploymentInsurance: x.unemploymentInsurance,
      healthInsurance: x.healthInsurance,
      insuranceInformationAllowed: x.insuranceInformationAllowed,
      taxDefault: x.taxDefault,
      notSupportedError: x.notSupportedError,
      description: x.description,
      taxAndSidecostsDescr: x.taxAndSidecostsDescr,
      isSupported: x.isSupported,
      paymentDefault: x.paymentDefault,
      calcGrouping: x.calcGrouping,
      transactionCode: x.transactionCode as TransactionCode,
    }));
  }
}
