import * as occupationsData from "../i18n/occupationlist.json";
import { Translations } from "./Translations";
import { Occupation } from "./model";

/**
 * Helper for Statistics Finland Occupation codes and texts related to them.
 * Occupation is used for insurance pricing and reporting..
 */
export class Occupations {

  /** Gets all occupations for all languages. */
  public static getAllForAllLanguages(): Occupation[] {
    if (this.data.length === 0) {
      const occupations: Occupation[] = [];
      // JSON import does not seem to support directly importing an array => Need to put it inside an object.
      occupationsData.list.forEach((x) => {
        occupations.push({
          id: x.id,
          code: x.code,
          label: x.labelFi,
          freeText: x.freeTextFi,
          search: x.searchFi,
          keyword: x.keyword,
          lang: "fi",
        });
        occupations.push({
          id: x.id,
          code: x.code,
          label: x.labelSv,
          freeText: x.freeTextSv,
          search: x.searchSv,
          keyword: x.keyword,
          lang: "sv",
        });
        occupations.push({
          id: x.id,
          code: x.code,
          label: x.labelEn,
          freeText: x.freeTextEn,
          search: x.searchEn,
          keyword: x.keyword,
          lang: "en",
        });
      });
      this.data = occupations;
    }
    return this.data;
  }

  /**
   * Gets all the items of a given type.
   * @returns All occupation entries
   */
  public static getAll(language?: "fi" | "en" | "sv" | null): Occupation[] {
    language = language || Translations.getLanguage() as any || "fi";
    return this.getAllForAllLanguages().filter((x) => x.lang === language);
  }

  /**
   * Searches the occupations.
   * @param searchString String to search for. Typically user input.
   * @param language Language to search for.
   */
  public static search(searchString: string, language?: "fi" | "en" | "sv" | null): Occupation[] {
    searchString = (searchString || "").toLowerCase();
    if (searchString === "" || null) {
      this.getAll();
    }
    return this.getAll(language).filter((x) => (x.search || "").indexOf(searchString) >= 0);
  }
  /**
   * Gets a single occupation based on the original Statistics Finland code.
   * @param code - Statistics finland 5-number code.
   * Note that this is different than the Salaxy ID stored in Calculation.
   * Salaxy ID may specify additional suffix separated by dash. For Salaxy ID, use getById() instead.
   * @returns Occupation metadata or null if not found.
   */
  public static getByCode(code: string): Occupation {
    return this.getAll().find((x) => x.code === code);
  }

  /**
   * Gets a single occupation based on the its Salaxy ID, which is derived from the original  Statistics Finland code.
   * @param id - Salaxy ID for the occupation.
   * This is the id stored in the calculation, but not the one that is reported to Incomes Register.
   * @returns Occupation metadata or null if not found.
   */
  public static getById(id: string): Occupation {
    if (!id) {
      return null;
    }
    return this.getAll().find((x) => x.id === id);
  }

  /**
   * Fetches a list of occupations either based on list of IDs or a known keyword.
   * @param idListOrKeyword Either a comma separated list of ID's
   * or a known keyword: "household" and "company" currently supported.
   */
  public static getByIds(idListOrKeyword: string): Occupation[] {
    if (!idListOrKeyword) {
      return [];
    }
    const splittedIdList = idListOrKeyword.split(",").map((x) => x.trim());
    const hasNumber = /^\d{5}/;
    if (idListOrKeyword.search(hasNumber) > -1) {
      return this.getAll().filter((x) => splittedIdList.indexOf(x.id) > -1);
    } else {
      return this.getAll().filter((x) => splittedIdList.some((k) => x.keyword.indexOf(k.trim()) > -1));
    }
  }

  private static data: Occupation[] = [];

}
