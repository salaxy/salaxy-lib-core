import { CalculationResult, EmployerCalculationDTO, TotalCalculationDTO, WorkerCalculationDTO } from "../model";

/** Extension methods for CalculationResult */
export class CalculationResultLogic {

  /**
   * Adds Calculation Results together and returns a new CalculationResult
   * @param calculationResults CalculationResults to add.
   * @returns A new CalculationResult.
   */
  public static add(calculationResults: CalculationResult[] ): CalculationResult {
    if (calculationResults == null || calculationResults.length === 0) {
      return this.addResults(null, null);
    }
    return calculationResults.reduce( (sum, item) => this.addResults(sum, item), this.safeObj(null));
  }

  private static addResults(c1: CalculationResult, c2: CalculationResult): CalculationResult {
    c1 = this.safeObj(c1);
    c2 = this.safeObj(c2);
    return {
      totals: this.addTotals(c1.totals, c2.totals),
      workerCalc: this.addWorkerCalc(c1.workerCalc, c2.workerCalc),
      employerCalc: this.addEmployerCalc(c1.employerCalc, c2.employerCalc),
      rows: this.safeArr(c1.rows).concat(this.safeArr(c2.rows)),
      responsibilities: this.safeObj(c1.responsibilities),
      irRows: this.safeArr(c1.irRows).concat(this.safeArr(c2.irRows)),
    };
  }

  private static addTotals(c1: TotalCalculationDTO, c2: TotalCalculationDTO): TotalCalculationDTO {
    c1 = this.safeObj(c1);
    c2 = this.safeObj(c2);
    return  {
      pension: this.safeNum(c1.pension) + this.safeNum(c2.pension),
      total: this.safeNum(c1.total) + this.safeNum(c2.total),
      totalBaseSalary: this.safeNum(c1.totalBaseSalary) + this.safeNum(c2.totalBaseSalary),
      totalExpenses: this.safeNum(c1.totalExpenses) + this.safeNum(c2.totalExpenses),
      totalGrossSalary: this.safeNum(c1.totalGrossSalary) + this.safeNum(c2.totalGrossSalary),
      totalSocialSecurityBase: this.safeNum(c1.totalSocialSecurityBase) + this.safeNum(c2.totalSocialSecurityBase),
      totalTaxable: this.safeNum(c1.totalTaxable) + this.safeNum(c2.totalTaxable),
      unemployment: this.safeNum(c1.unemployment) + this.safeNum(c2.unemployment),
      totalAccidentInsuranceBase: this.safeNum(c1.totalAccidentInsuranceBase) + this.safeNum(c2.totalAccidentInsuranceBase),
      totalHealthInsuranceBase: this.safeNum(c1.totalHealthInsuranceBase) + this.safeNum(c2.totalHealthInsuranceBase),
      totalPayable: this.safeNum(c1.totalPayable) + this.safeNum(c2.totalPayable),
      totalPensionInsuranceBase: this.safeNum(c1.totalPensionInsuranceBase) + this.safeNum(c2.totalPensionInsuranceBase),
      totalUnemploymentInsuranceBase: this.safeNum(c1.totalUnemploymentInsuranceBase) + this.safeNum(c2.totalUnemploymentInsuranceBase),
    };
  }

  private static addWorkerCalc(c1: WorkerCalculationDTO, c2: WorkerCalculationDTO): WorkerCalculationDTO {
    c1 = this.safeObj(c1);
    c2 = this.safeObj(c2);
    return {
      benefits : this.safeNum(c1.benefits) + this.safeNum(c2.benefits),
      deductions : this.safeNum(c1.deductions) + this.safeNum(c2.deductions),
      fullTax : this.safeNum(c1.fullTax) + this.safeNum(c2.fullTax),
      fullOtherDeductions : this.safeNum(c1.fullOtherDeductions) + this.safeNum(c2.fullOtherDeductions),
      fullPension : this.safeNum(c1.fullPension) + this.safeNum(c2.fullPension),
      fullPrepaidExpenses : this.safeNum(c1.fullPrepaidExpenses) + this.safeNum(c2.fullPrepaidExpenses),
      fullSalaryAdvance : this.safeNum(c1.fullSalaryAdvance) + this.safeNum(c2.fullSalaryAdvance),
      fullUnemploymentInsurance : this.safeNum(c1.fullUnemploymentInsurance) + this.safeNum(c2.fullUnemploymentInsurance),
      fullUnionPayment : this.safeNum(c1.fullUnionPayment) + this.safeNum(c2.fullUnionPayment),
      otherDeductions : this.safeNum(c1.otherDeductions) + this.safeNum(c2.otherDeductions),
      pension : this.safeNum(c1.pension) + this.safeNum(c2.pension),
      prepaidExpenses : this.safeNum(c1.prepaidExpenses) + this.safeNum(c2.prepaidExpenses),
      foreclosure : this.safeNum(c1.foreclosure) + this.safeNum(c2.foreclosure),
      salaryAdvance : this.safeNum(c1.salaryAdvance) + this.safeNum(c2.salaryAdvance),
      salaryAfterTax : this.safeNum(c1.salaryAfterTax) + this.safeNum(c2.salaryAfterTax),
      salaryAfterTaxAndForeclosure : this.safeNum(c1.salaryAfterTaxAndForeclosure) + this.safeNum(c2.salaryAfterTaxAndForeclosure),
      salaryPayment : this.safeNum(c1.salaryPayment) + this.safeNum(c2.salaryPayment),
      tax : this.safeNum(c1.tax) + this.safeNum(c2.tax),
      totalWorkerPayment : this.safeNum(c1.totalWorkerPayment) + this.safeNum(c2.totalWorkerPayment),
      unemploymentInsurance : this.safeNum(c1.unemploymentInsurance) + this.safeNum(c2.unemploymentInsurance),
      unionPayment : this.safeNum(c1.unionPayment) + this.safeNum(c2.unionPayment),
      workerSideCosts : this.safeNum(c1.workerSideCosts) + this.safeNum(c2.workerSideCosts),
      totalWorkerPaymentLegacy : this.safeNum(c1.totalWorkerPaymentLegacy) + this.safeNum(c2.totalWorkerPaymentLegacy),
    };
  }

  private static addEmployerCalc(c1: EmployerCalculationDTO, c2: EmployerCalculationDTO): EmployerCalculationDTO {
    c1 = this.safeObj(c1);
    c2 = this.safeObj(c2);
    const result = {
      allSideCosts : this.safeNum(c1.allSideCosts) + this.safeNum(c2.allSideCosts),
      deductionPensionSelfPayment : this.safeNum(c1.deductionPensionSelfPayment) + this.safeNum(c2.deductionPensionSelfPayment),
      deductionUnemploymentSelfPayment : this.safeNum(c1.deductionUnemploymentSelfPayment) + this.safeNum(c2.deductionUnemploymentSelfPayment),
      deductionSalaryAdvance : this.safeNum(c1.deductionSalaryAdvance) + this.safeNum(c2.deductionSalaryAdvance),
      deductionForeclosure : this.safeNum(c1.deductionForeclosure) + this.safeNum(c2.deductionForeclosure),
      foreclosureByPalkkaus : this.safeNum(c1.foreclosureByPalkkaus) + this.safeNum(c2.foreclosureByPalkkaus),
      deductionUnionPayment : this.safeNum(c1.deductionUnionPayment) + this.safeNum(c2.deductionUnionPayment),
      deductionOtherDeductions : this.safeNum(c1.deductionOtherDeductions) + this.safeNum(c2.deductionOtherDeductions),

      finalCost : this.safeNum(c1.finalCost) + this.safeNum(c2.finalCost),
      householdDeduction : this.safeNum(c1.householdDeduction) + this.safeNum(c2.householdDeduction),
      mandatorySideCosts : this.safeNum(c1.mandatorySideCosts) + this.safeNum(c2.mandatorySideCosts),
      palkkaus : this.safeNum(c1.palkkaus) + this.safeNum(c2.palkkaus),
      pension : this.safeNum(c1.pension) + this.safeNum(c2.pension),
      socialSecurity : this.safeNum(c1.socialSecurity) + this.safeNum(c2.socialSecurity),
      totalDeductions : this.safeNum(c1.totalDeductions) + this.safeNum(c2.totalDeductions),
      totalPayment : this.safeNum(c1.totalPayment) + this.safeNum(c2.totalPayment),
      totalSalaryCost : this.safeNum(c1.totalSalaryCost) + this.safeNum(c2.totalSalaryCost),
      unemployment : this.safeNum(c1.unemployment) + this.safeNum(c2.unemployment),
      totalPaymentLegacy : this.safeNum(c1.totalPaymentLegacy) + this.safeNum(c2.totalPaymentLegacy),
    };

    (result as any).deductionTaxAndSocialSecuritySelfPayment = this.safeNum((c1 as any).deductionTaxAndSocialSecuritySelfPayment) + this.safeNum((c2 as any).deductionTaxAndSocialSecuritySelfPayment);
    (result as any).deductionWorkerSelfPayment = this.safeNum((c1 as any).deductionWorkerSelfPayment) + this.safeNum((c2 as any).deductionWorkerSelfPayment);

    return result;
  }

  private static safeNum(value: number): number {
    return value || 0;
  }
  private static safeArr(value: any[]): any[] {
    return value || [];
  }
  private static safeObj(value: any): any {
    return value || {};
  }
}
