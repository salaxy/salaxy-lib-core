import { EnumerationsLogic } from "./EnumerationsLogic";
import { Avatar, AvatarPictureType, Calculation, LegalEntityType, PaymentChannel, PayrollDetails } from "../model";

/** Common logic for invoices */
export class InvoicesLogic {
  /**
   * Custom type guard that determines whether the payment object is a payroll (based on whether it calcs collection).
   * @param paymentObject Payment object that is either PayrollDetails or Calculation OR does not have calcs / employer properites that is used to determine the type.
   */
  public static isPayroll(paymentObject: Calculation | PayrollDetails | any): paymentObject is PayrollDetails {
    return paymentObject && (paymentObject as PayrollDetails).calcs !== undefined;
  }

  /**
   * Custom type guard that determines whether the payment object is a calculation (based on whether it employer property).
   * @param paymentObject Payment object that is either PayrollDetails or Calculation OR does not have calcs / employer properites that is used to determine the type.
   */
  public static isCalculation(paymentObject: Calculation | PayrollDetails | any): paymentObject is Calculation {
    return paymentObject && (paymentObject as Calculation).employer !== undefined;
  }

  /**
   * Gets and avatar image for a payment channel
   * Enum name is id, label as displayName, descr as description and sort order as 6-char string in sortableName.
   * Picture is either color-initials combination or pic as usual.
   * @param channel The channel to fetch avatar for.
   */
  public static getChannelAvatar(channel: PaymentChannel): Avatar {
    return this.getAllChannels().find((x) => x.id === channel);
  }

  /**
   * Gets all the supported payment channels defined in PaymentChannel as avatars.
   * Enum name is id, label as displayName, descr as description and sort order as 6-char string in sortableName.
   * Picture is either color-initials combination or pic as usual.
   */
  public static getAllChannels(): Avatar[] {
    if (!this.avatars) {
      this.avatars = EnumerationsLogic.getEnumMetadata("PaymentChannel").values
        .map((e) => {
          const picData = this.avatarPics[e.name] || this.avatarPics[PaymentChannel.Undefined];
          return {
            id: e.name,
            displayName: e.label,
            firstName: e.label,
            sortableName: ("000000" + e.order).slice(-6),
            description: e.descr,
            color: picData.color,
            entityType: picData.entityType || LegalEntityType.Company,
            initials: picData.initials,
            pictureType: picData.url === "default" ? AvatarPictureType.Uploaded : picData.pictureType,
            url: picData.url === "default" ? `https://cdn.salaxy.com/ng1/img/paymentmethod/${e.name}.png` : picData.url,
          };
        });
    }
    return this.avatars;
  }

  private static avatars: Avatar[];

  private static avatarPics: { [key: string]: Avatar } = {

    [PaymentChannel.Undefined]: { color: "gray", initials: "?" },
    [PaymentChannel.Test]: { url: "default" },
    [PaymentChannel.ZeroPayment]: { url: "default" },
    [PaymentChannel.External]: { url: "default" },

    [PaymentChannel.PalkkausManual]: { url: "default" },
    [PaymentChannel.PalkkausWS]: { color: "#005f81", initials: "WS" },
    [PaymentChannel.PalkkausPersonal]: { url: "default",  },

    [PaymentChannel.PalkkausCfaPaytrail]: { url: "default" },
    [PaymentChannel.PalkkausCfaReference]: { url: "default" },
    [PaymentChannel.PalkkausCfaFinvoice]: { url: "default" },
    [PaymentChannel.PalkkausCfaTest]: { url: "default" },

    [PaymentChannel.AccountorGo]: { url: "default" },
    [PaymentChannel.TalenomOnline]: { url: "default" },
    [PaymentChannel.TalenomCfa]: { url: "default" },
    [PaymentChannel.HolviCfa]: { url: "default" },
    [PaymentChannel.FinagoSolo]: { url: "default" },
    [PaymentChannel.Procountor]: { url: "default" },
    [PaymentChannel.Kevytyrittaja]: { url: "default" },
    [PaymentChannel.VismaNetvisor]: { url: "default" },
  };
}
