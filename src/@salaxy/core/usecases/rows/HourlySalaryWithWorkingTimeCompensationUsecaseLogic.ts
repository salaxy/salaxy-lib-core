import { CalculationRowType, CalculationRowUnit, HourlySalaryWithWorkingTimeCompensationUsecase, UserDefinedRow, WorkingTimeCompensationKind } from "../../model";
import { EnumerationsLogic, IncomeTypeMetadata, IncomeTypesLogic } from "../../logic";
import { Numeric } from "../../util";
import { UsecaseValidationMessage } from "./UsecaseValidationMessage";

/** Client-side logic for HourlySalaryWithWorkingTimeCompensationUsecaseLogic  */
export class HourlySalaryWithWorkingTimeCompensationUsecaseLogic {

  /**
   * Updates the usecase values based onuser input without going to the server.
   * @param row Row that contains the usecase data.
   */
  public update(row: UserDefinedRow): void {
    const data: HourlySalaryWithWorkingTimeCompensationUsecase = row.data ?? {};

    if (!data.compensationRows || data.compensationRows.length < 1) {
      data.compensationRows = [{}];
    }
    for (const cr of data.compensationRows) {
      cr.unit = !cr.unit || cr.unit === CalculationRowUnit.Undefined ? CalculationRowUnit.Percent : cr.unit;
      cr.count = cr.count ?? 0;
      cr.price = cr.unit === CalculationRowUnit.Percent ? Numeric.round(cr.count * (row.price ?? 0), 2) : cr.price;
      cr.message = this.getCodeData(cr.rowKind ?? data.kind)?.label;
    }

    if (!row.message || row.message === EnumerationsLogic.getEnumLabel("CalculationRowType", CalculationRowType.HourlySalaryWithWorkingTimeCompensation)) {
      row.message = EnumerationsLogic.getEnumLabel("CalculationRowType", CalculationRowType.HourlySalary);
    }
  }

  /** Gets validation message */
  public getValidation(row: UserDefinedRow): UsecaseValidationMessage {
    const data: HourlySalaryWithWorkingTimeCompensationUsecase = row.data || {};

    if (!data.compensationRows || data.compensationRows.length < 1) {
      return;
    }

    for (const cr of data.compensationRows) {
      const codeData = this.getCodeData(cr.rowKind ?? data.kind);
      if (!codeData) {
        return {
          type: "error",
          msg: "Korvauksen laji puuttuu.",
        };
      }
    }
    const msgs = [];
    for (const cr of data.compensationRows) {
      const rowMsg = !cr.unit || cr.unit === CalculationRowUnit.Undefined || cr.unit === CalculationRowUnit.Percent ?
        `Lisä ${Numeric.formatPercent((cr.count ?? 0) * 100)}, ${Numeric.formatPrice(cr.price)} / tunti, lisä yhteensä ${Numeric.formatPrice(Numeric.round(row.count * cr.price, 2))}` :
        `Lisä ${Numeric.formatPrice(cr.price)} / tunti, lisä yhteensä ${Numeric.formatPrice(Numeric.round(row.count * cr.price, 2))}`;
      msgs.push(rowMsg);
    }
    return { type: "default", msg: msgs.join("\n") };
  }

  private getCodeData(kind: WorkingTimeCompensationKind): IncomeTypeMetadata {
    if (kind) {
      let code = null;
      switch (kind) {
        case WorkingTimeCompensationKind.EmergencyWorkCompensation:
          code = 205;
          break;
        case WorkingTimeCompensationKind.EveningWorkCompensation:
          code = 206;
          break;
        case WorkingTimeCompensationKind.EveningShiftAllowance:
          code = 207;
          break;
        case WorkingTimeCompensationKind.SaturdayPay:
          code = 211;
          break;
        case WorkingTimeCompensationKind.ExtraWorkPremium:
          code = 212;
          break;
        case WorkingTimeCompensationKind.OtherCompensation:
          code = 216;
          break;
        case WorkingTimeCompensationKind.WaitingTimeCompensation:
          code = 217;
          break;
        case WorkingTimeCompensationKind.SundayWorkCompensation:
          code = 221;
          break;
        case WorkingTimeCompensationKind.StandByCompensation:
          code = 230;
          break;
        case WorkingTimeCompensationKind.WeeklyRestCompensation:
          code = 232;
          break;
        case WorkingTimeCompensationKind.OvertimeCompensation:
          code = 235;
          break;
        case WorkingTimeCompensationKind.NightWorkAllowance:
          code = 236;
          break;
        case WorkingTimeCompensationKind.NightShiftCompensation:
          code = 237;
          break;
        case WorkingTimeCompensationKind.Undefined:
        default:
          code = 0;
          break;
      }
      return IncomeTypesLogic.getAll().find((x) => x.code === code);
    }
    return null;
  }
}
