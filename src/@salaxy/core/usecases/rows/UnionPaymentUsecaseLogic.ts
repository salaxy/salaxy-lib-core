import { Calculation, UnionPaymentKind, UnionPaymentUsecase, UserDefinedRow } from "../../model";
import { Years } from "../../util";
import { UsecaseValidationMessage } from "./UsecaseValidationMessage";

/** Client-side logic for UnionPaymentUsecase  */
export class UnionPaymentUsecaseLogic {

  /**
   * Updates the usecase values based onuser input without going to the server.
   * @param row Row that contains the usecase data.
   * @param calc Calculation.
   */
  public update(row: UserDefinedRow,  calc: Calculation): void {
    const data: UnionPaymentUsecase = row.data ?? {};
    data.kind = ( data.kind || UnionPaymentKind.Percentage);

    if (!data.kind || data.kind === UnionPaymentKind.Undefined) {
      data.kind = UnionPaymentKind.Percentage;
    }

    if (data.kind === UnionPaymentKind.Other) {
      if ((data as any).isFixedSum) {
        data.kind = UnionPaymentKind.Fixed;
        row.price = row.price * ( row.count ?? 1);
        row.count = 1;
      } else {
        data.kind = UnionPaymentKind.Percentage;
      }
    }

    const numbers = Years.getYearlyChangingNumbers(calc?.workflow?.salaryDate ?? new Date());
    const raksaIban = "FI6115553000120981";
    const raksaName = "Rakennusliitto";

    switch (data.kind) {
      case UnionPaymentKind.Fixed:
        data.iban =  data.iban === raksaIban ? null: data.iban;
        data.recipientFullName = data.recipientFullName === raksaName ? null: data.recipientFullName;
        row.count = 1;
        if (calc && calc.result && calc.result.totals && row.price === calc.result.totals.totalTaxable) {
          row.price = 0;
        }
        // row.price
       break;
       case UnionPaymentKind.Percentage:
        data.iban =  data.iban === raksaIban ? null: data.iban;
        data.recipientFullName = data.recipientFullName === raksaName ? null: data.recipientFullName;
        if (!row.count || row.count >= 1){
          row.count = 0;
        }
        if (calc && calc.result && calc.result.totals) {
          // row.count
          row.price = calc.result.totals.totalTaxable;
        }
       break;
      case UnionPaymentKind.RaksaNormal:
        data.iban = raksaIban;
        data.recipientFullName = raksaName;
        row.count = numbers.sideCosts.unionPaymentRaksaA;
        if (calc && calc.result && calc.result.totals) {
          row.price = calc.result.totals.totalTaxable;
        }
        break;
      case UnionPaymentKind.RaksaUnemploymentOnly:
        data.iban = raksaIban;
        data.recipientFullName = raksaName;
        row.count = numbers.sideCosts.unionPaymentRaksaAoTa;
        if (calc && calc.result && calc.result.totals) {
          row.price = calc.result.totals.totalTaxable;
        }
       break;
    }
  }

  /** Gets validation message for UnionPaymentUsecase  */
  public getValidation(row: UserDefinedRow): UsecaseValidationMessage {
    const data: UnionPaymentUsecase = row.data || {};

    if (data.kind === UnionPaymentKind.Other) {
      if ((data as any).isFixedSum) {
        data.kind = UnionPaymentKind.Fixed;
        row.price = row.price * ( row.count ?? 1);
        row.count = 1;
      } else {
        data.kind = UnionPaymentKind.Percentage;
      }
    }

    switch (data.kind) {
      case UnionPaymentKind.Fixed:
        if (!row.price) {
          return {
            type: "error",
            msg: "HUOM: Jäsenmaksua ei ole määritetty.",
          };
        }
        return {
          type: "default",
          msg: "Kiinteä AY-jäsenmaksun määrä. Työnantaja tilittää maksun itse.",
        };
      case UnionPaymentKind.Percentage:
        if (!row.count) {
          return {
            type: "error",
            msg: "HUOM: Jäsenmaksu-%:ia ei ole määritetty.",
          };
        }
          return {
            type: "default",
            msg: "AY-jäsenmaksu on %-osuus palkasta, palkkasumma päivitetään automaattisesti. Työnantaja tilittää maksun itse.",
          };
      case UnionPaymentKind.RaksaNormal:
      case UnionPaymentKind.RaksaUnemploymentOnly:
        if (!row.count) {
          return {
            type: "error",
            msg: "HUOM: Jäsenmaksu-%:ia ei ole määritetty.",
          };
        }
        return {
          type: "default",
          msg: "AY-jäsenmaksu on %-osuus palkasta, palkkasumma päivitetään automaattisesti. Työnantaja tilittää maksun itse.",
        };
      default:
        return {
          type: "error",
          msg: "Tuntematon AY-jäsenmaksun tyyppi.",
        };
    }
  }
}
