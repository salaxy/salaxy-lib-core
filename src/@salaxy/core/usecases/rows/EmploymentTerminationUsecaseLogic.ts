import { EmploymentTerminationKind, EmploymentTerminationUsecase, UserDefinedRow } from "../../model";
import { EnumerationsLogic, IncomeTypeMetadata, IncomeTypesLogic } from "../../logic";
import { UsecaseValidationMessage } from "./UsecaseValidationMessage";

/** Client-side logic for EmploymentTerminationUsecase  */
export class EmploymentTerminationUsecaseLogic {

  /**
   * Updates the usecase values based onuser input without going to the server.
   * @param row Row that contains the usecase data.
   */
  public update(row: UserDefinedRow): void {
    const data: EmploymentTerminationUsecase = row.data || {};
    const codeData = this.getCodeData(data.kind);
    if (codeData) {
      // TODO: check if this is the right way to set the message
      if (!row.message || row.message === EnumerationsLogic.getEnumLabel("CalculationRowType", row.rowType) || this.isIrIncomeTypeLabel(row.message)) {
        row.message = codeData.label;
      }
    }
  }

  /** Gets validation message for IrIncomeTypeUsecase  */
  public getValidation(row: UserDefinedRow): UsecaseValidationMessage {
    const data: EmploymentTerminationUsecase = row.data || {};
    const codeData = this.getCodeData(data.kind);
    if (!codeData) {
      return {
        type: "error",
        msg: "Tulolaji puuttuu.",
      };
    }
    return null;
  }

  private getCodeData(kind: EmploymentTerminationKind): IncomeTypeMetadata {
    if (kind) {
      let code = 0;
      switch (kind) {
        case EmploymentTerminationKind.NoticePeriodCompensation:
          code = 208;
          break;
        case EmploymentTerminationKind.MonetaryWorkingTimeBankCompensation:
          code = 224;
          break;
        case EmploymentTerminationKind.TerminationAndLayOffDamages:
          code = 229;
          break;
        case EmploymentTerminationKind.VoluntaryTerminationCompensation:
          code = 231;
          break;
        case EmploymentTerminationKind.PensionPaidByEmployer:
          code = 338;
          break;
        case EmploymentTerminationKind.Undefined:
        default:
          code = 0;
          break;
      }
      return IncomeTypesLogic.getAll().find((x) => x.code === code);
    }
    return null;
  }

  private isIrIncomeTypeLabel(label: string): boolean {
    return IncomeTypesLogic.getAll().some((x) => x.label === label);
  }
}
