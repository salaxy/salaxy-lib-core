import { UserDefinedRow } from "../../model";
import { UsecaseValidationMessage } from "./UsecaseValidationMessage";

/** Client-side logic for BoardUsecase  */
export class BoardUsecaseLogic {

  /**
   * Updates the usecase values based on user input without going to the server.
   * @param row Row that contains the usecase data.
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public update(row: UserDefinedRow): void {
    // Empty method stub
  }

  /** Gets validation message for BoardUsecaseLogic  */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public getValidation(row: UserDefinedRow): UsecaseValidationMessage {
    return null;
  }
}
