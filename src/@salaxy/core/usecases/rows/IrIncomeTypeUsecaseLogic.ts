import { IrIncomeTypeUsecase, TransactionCode, UserDefinedRow } from "../../model";
import { EnumerationsLogic, IncomeTypeMetadata, IncomeTypesLogic, } from "../../logic";
import { UsecaseValidationMessage } from "./UsecaseValidationMessage";

/** Client-side logic for IrIncomeTypeUsecase  */
export class IrIncomeTypeUsecaseLogic {

  /**
   * Updates the usecase values based onuser input without going to the server.
   * @param row Row that contains the usecase data.
   */
  public update(row: UserDefinedRow): void {
    const data: IrIncomeTypeUsecase = row.data;
    // Get code data by kind first (new version) and then using the old method.
    const codeData = this.getCodeDataByNumber(data?.irData?.code) ?? this.getCodeDataByTransaction(data?.kind);
    if (codeData) {
      data.irData = data.irData ?? {};
      data.irData.code = codeData.code;
      data.kind = codeData.transactionCode;
      if (!row.message || row.message ===  EnumerationsLogic.getEnumLabel("CalculationRowType", row.rowType) || this.isIrIncomeTypeLabel(row.message)) {
        row.message = codeData.label;
      }
    }
  }

  /** Gets validation message for IrIncomeTypeUsecase  */
  public getValidation(row: UserDefinedRow): UsecaseValidationMessage {
    const data: IrIncomeTypeUsecase = row.data;
    const codeData = this.getCodeDataByNumber(data?.irData?.code) ?? this.getCodeDataByTransaction(data?.kind)
    if (!codeData) {
      return {
        type: "error",
        msg: "Tulolaji puuttuu tai ei ole tuettu.",
      };
    }

    return null;
  }

  private getCodeDataByNumber(code: number): IncomeTypeMetadata {
    if (!code) {
      return null;
    }

    return IncomeTypesLogic.getAll().find( (x) => x.code === code);
  }

  private getCodeDataByTransaction(transactionCode: TransactionCode): IncomeTypeMetadata {
    if ( !transactionCode ||
        transactionCode === TransactionCode.Unknown ||
        (transactionCode as any) === "undefined" ) {
      return null;
    }
    return IncomeTypesLogic.getAll().find( (x) => x.transactionCode === transactionCode);
  }

  private isIrIncomeTypeLabel(label: string): boolean {
    return IncomeTypesLogic.getAll().some( (x) => x.label === label);
  }
}
