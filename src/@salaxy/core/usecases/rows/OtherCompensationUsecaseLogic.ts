import { OtherCompensationKind, OtherCompensationUsecase, UserDefinedRow } from "../../model";
import { EnumerationsLogic, IncomeTypeMetadata, IncomeTypesLogic, } from "../../logic";
import { UsecaseValidationMessage } from "./UsecaseValidationMessage";

/** Client-side logic for OtherCompensationUsecase  */
export class OtherCompensationUsecaseLogic {

  /**
   * Updates the usecase values based onuser input without going to the server.
   * @param row Row that contains the usecase data.
   */
  public update(row: UserDefinedRow): void {
    const data: OtherCompensationUsecase = row.data || {};
    const codeData = this.getCodeData(data.kind);
    if (codeData) {
      // TODO: check if this is the right way to set the message
      if (!row.message || row.message === EnumerationsLogic.getEnumLabel("CalculationRowType", row.rowType) || this.isIrIncomeTypeLabel(row.message)) {
        row.message = codeData.label;
      }
    }
  }

  /** Gets validation message for IrIncomeTypeUsecase  */
  public getValidation(row: UserDefinedRow): UsecaseValidationMessage {
    const data: OtherCompensationUsecase = row.data || {};
    const codeData = this.getCodeData(data.kind);
    if (!codeData) {
      return {
        type: "error",
        msg: "Tulolaji puuttuu.",
      };
    }
    return null;
  }

  private getCodeData(kind: OtherCompensationKind): IncomeTypeMetadata {
    if (kind) {
      let code = 0;
      switch (kind) {
        case OtherCompensationKind.MeetingFee:
          code = 210;
          break;
        case OtherCompensationKind.LectureFee:
          code = 214;
          break;
        case OtherCompensationKind.PositionOfTrustCompensation:
          code = 215;
          break;
        case OtherCompensationKind.AccruedTimeOffCompensation:
          code = 225;
          break;
        case OtherCompensationKind.MembershipOfGoverningBodyCompensation:
          code = 308;
          break;
        case OtherCompensationKind.MonetaryGiftForEmployees:
          code = 310;
          break;
        case OtherCompensationKind.UseCompensationAsEarnedIncome:
          code = 313;
          break;
        case OtherCompensationKind.UseCompensationAsCapitalIncome:
          code = 314;
          break;
        case OtherCompensationKind.OtherTaxableIncomeAsEarnedIncome:
          code = 316;
          break;
        case OtherCompensationKind.StockOptionsAndGrants:
          code = 320;
          break;
        case OtherCompensationKind.EmployeeInventionCompensation:
          code = 326;
          break;
        case OtherCompensationKind.CapitalIncomePayment:
          code = 332;
          break;
        case OtherCompensationKind.WorkEffortBasedDividendsAsWage:
          code = 339;
          break;
        case OtherCompensationKind.WorkEffortBasedDividendsAsNonWage:
          code = 340;
          break;
        case OtherCompensationKind.EmployeeStockOption:
          code = 343;
          break;
        case OtherCompensationKind.EmployeeStockOptionWithLowerPrice:
          code = 361;
          break;
        case OtherCompensationKind.Undefined:
        default:
          code = 0;
          break;
      }
      return IncomeTypesLogic.getAll().find((x) => x.code === code);
    }
    return null;
  }

  private isIrIncomeTypeLabel(label: string): boolean {
    return IncomeTypesLogic.getAll().some((x) => x.label === label);
  }
}
