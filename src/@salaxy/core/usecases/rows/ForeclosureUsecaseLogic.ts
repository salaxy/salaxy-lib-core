import { Calculation, ForeclosureKind, ForeclosureUsecase, UserDefinedRow } from "../../model";
import { Dates, Numeric, Years } from "../../util";
import { UsecaseValidationMessage } from "./UsecaseValidationMessage";

/** Client-side logic for ForeclosureUsecase  */
export class ForeclosureUsecaseLogic {

  /**
   * Updates the usecase values based onuser input without going to the server.
   * @param row Row that contains the usecase data.
   * @param calc Calculation.
   */
  public update(row: UserDefinedRow, calc: Calculation): void {
    const data: ForeclosureUsecase = row.data ?? {};

    if (!data.kind || data.kind === ForeclosureKind.Undefined) {
      data.kind = ForeclosureKind.Fixed;
    }

    if (data.kind !== ForeclosureKind.Periodic && data.kind !== ForeclosureKind.NonPeriodic) {
      return;
    }

    let salaryAfterTax = 0;

    if (calc && calc.result && calc.result.workerCalc) {
      salaryAfterTax = calc.result.workerCalc.salaryAfterTax - calc.result.workerCalc.pension - calc.result.workerCalc.unemploymentInsurance;
    }

    if (!data.protectedPortionBase) {
      const numbers = Years.getYearlyChangingNumbers(calc?.workflow?.salaryDate ?? new Date());
      data.protectedPortionBase = numbers.sideCosts.protectedPortionBase;
      data.protectedPortionDependant =  numbers.sideCosts.protectedPortionDependant;
    }

    let otherIncome = Numeric.round(data.incomeFromOtherSources || 0.0, 2);
    let leftToDebtor = Numeric.round(data.additionalAmountLeftToDebtor || 0.0, 2);
    let foreclosure = 0.0;
    if (salaryAfterTax > 0) {
      if (data.kind === ForeclosureKind.NonPeriodic) {
        foreclosure = salaryAfterTax / 3.0;
      } else {
        data.periodLength = this.calculatePeriodLength(calc);
        otherIncome = Numeric.round(data.periodLength * (otherIncome / 30.0),2);
        leftToDebtor = Numeric.round(data.periodLength * (leftToDebtor / 30.0), 2);

        const protectedPortion = Numeric.round(
          (data.periodLength || 0) *
          (
            (data.protectedPortionBase || 0.0)
            +
            ((data.dependantsCount || 0) * (data.protectedPortionDependant || 0))
          ), 2);

        const totalSalaryAfterTax = salaryAfterTax + otherIncome;
        if (protectedPortion > totalSalaryAfterTax) {
          foreclosure = 0.0;
        }
        else if (totalSalaryAfterTax <= (protectedPortion * 2.0)) {
          foreclosure = 2.0 * (totalSalaryAfterTax - protectedPortion) / 3.0;
        }
        else if (totalSalaryAfterTax <= (protectedPortion * 4.0)) {
          foreclosure = totalSalaryAfterTax / 3.0;
        }
        else {
          foreclosure = (protectedPortion * 4.0) / 3.0 +
            4.0 * (totalSalaryAfterTax - protectedPortion * 4.0) / 5.0;
          foreclosure = Math.min(foreclosure, totalSalaryAfterTax / 2.0);
        }
        foreclosure = Math.max(0.0, Math.min(foreclosure, salaryAfterTax) - leftToDebtor);
      }
    }
    row.count = 1;
    row.price = Numeric.round(foreclosure, 2);

  }

  /** Gets validation message for UnionPaymentUsecase  */
  public getValidation(row: UserDefinedRow): UsecaseValidationMessage {
    const data: ForeclosureUsecase = row.data || {};
    if (data.kind === ForeclosureKind.NonPeriodic || data.kind === ForeclosureKind.Periodic ) {
      return {
        type: "default",
        msg: `Ulosottosumma päivitetään automaattisesti, työnantaja tilittää maksun itse.`,
      };
    } else {
      if (!row.price) {
        return {
          type: "error",
          msg: "HUOM: Ulosoton summaa ei ole annettu.",
        };
      }
    }
    return null;
  }

  private calculatePeriodLength(calc: Calculation): number {

    const startDate = calc && calc.info && calc.info.workStartDate && calc.info.workEndDate ? calc.info.workStartDate : null;
    const endDate = calc && calc.info && calc.info.workStartDate && calc.info.workEndDate ? calc.info.workEndDate : null;


    if (startDate != null && endDate != null) {
      if (this.isOnceAMonthPeriod(startDate, endDate)) {
        return 30;
      }
      else if (this.isEvery2WeeksPeriod(startDate, endDate)) {
        return 14;
      }
      else if (this.isTwiceAMonthPeriod(startDate, endDate)) {
        return 15;
      } else {
        return Math.max(0, this.getDays(startDate, endDate));
      }
    }
    else {
      return 30;
    }
  }

  private isOnceAMonthPeriod(startDate: string, endDate: string): boolean {
    return endDate === Dates.asDate(Dates.getMoment(startDate).add(1, "month").subtract(1, "day"))
  }

  private isTwiceAMonthPeriod(startDate: string, endDate: string): boolean {
    return (Dates.getDuration(startDate, endDate).asDays() + 1 === 15)
    ||
    (Dates.getMoment(startDate).date() === 15 && endDate === Dates.asDate(Dates.getMoment(startDate).endOf("month")))
    ||
    (Dates.getMoment(startDate).date() === 16 && endDate === Dates.asDate(Dates.getMoment(startDate).endOf("month")))
  }

  private isEvery2WeeksPeriod(startDate: string, endDate: string): boolean {
    return endDate === Dates.asDate(Dates.getMoment(startDate).add(2, "week").subtract(1, "day"))
  }

  private getDays(startDate: string, endDate: string): number {
    return  Dates.getDuration(startDate, endDate).asDays() + 1;
  }
}
