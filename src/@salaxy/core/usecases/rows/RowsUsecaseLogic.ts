import {
  Calculation,
  CalculationRowType,
  CarBenefitKind,
  CarBenefitUsecase,
  DailyAllowanceKind,
  DailyAllowanceUsecase,
  NonProfitOrgKind,
  NonProfitOrgUsecase,
  UserDefinedRow
} from "../../model";
import { Years } from "../../util";

import { BoardUsecaseLogic } from "./BoardUsecaseLogic";
import { EmploymentTerminationUsecaseLogic } from "./EmploymentTerminationUsecaseLogic";
import { ForeclosureUsecaseLogic } from "./ForeclosureUsecaseLogic";
import { HourlySalaryWithWorkingTimeCompensationUsecaseLogic } from "./HourlySalaryWithWorkingTimeCompensationUsecaseLogic";
import { IrIncomeTypeUsecaseLogic } from "./IrIncomeTypeUsecaseLogic";
import { MealBenefitUsecaseLogic } from "./MealBenefitUsecaseLogic";
import { OtherCompensationUsecaseLogic } from "./OtherCompensationUsecaseLogic";
import { RemunerationUsecaseLogic } from "./RemunerationUsecaseLogic";
import { UnionPaymentUsecaseLogic } from "./UnionPaymentUsecaseLogic";
import { UsecaseValidationMessage } from "./UsecaseValidationMessage";
import { UsecaseValidations } from "./UsecaseValidations";
import { WorkingTimeCompensationUsecaseLogic } from "./WorkingTimeCompensationUsecaseLogic";
import { TotalWorkerPaymentUsecaseLogic } from "./TotalWorkerPaymentUsecaseLogic";

/**
 * Provides logic for usecase user interfaces based on calculation rows.
 */
export class RowsUsecaseLogic {

  /**
   * Returns a validation message for a row or null if none is required.
   * @param row The row that is validated.
   */
  public static getValidation(row: UserDefinedRow, calc: Calculation): UsecaseValidationMessage {
    return UsecaseValidations.getValidation(row, calc);
  }

  /**
   * Updates the usecase property values without going to the server.
   * @param row Row to update.
   */
  public static updateUsecase(row: UserDefinedRow, calc: Calculation): void {
    // TODO: Move all these logic to usecase specific classes similar to MealBenefitUsecaseLogic.
    switch (row.rowType) {
      case CalculationRowType.CarBenefit:
        return RowsUsecaseLogic.updateCarBenefitUsecase(row);
      case CalculationRowType.UnionPayment:
          return new UnionPaymentUsecaseLogic().update(row, calc);
      case CalculationRowType.DailyAllowance:
        return RowsUsecaseLogic.updateDailyAllowanceUsecase(row, calc);
      case CalculationRowType.MealBenefit:
        return new MealBenefitUsecaseLogic().update(row, calc);
      case CalculationRowType.NonProfitOrg:
        return RowsUsecaseLogic.updateNonProfitOrgUsecase(row);
      case CalculationRowType.IrIncomeType:
        return new IrIncomeTypeUsecaseLogic().update(row);
      case CalculationRowType.Board:
        return new BoardUsecaseLogic().update(row);
      case CalculationRowType.Remuneration:
        return new RemunerationUsecaseLogic().update(row);
      case CalculationRowType.OtherCompensation:
        return new OtherCompensationUsecaseLogic().update(row);
      case CalculationRowType.WorkingTimeCompensation:
        return new WorkingTimeCompensationUsecaseLogic().update(row);
      case CalculationRowType.EmploymentTermination:
        return new EmploymentTerminationUsecaseLogic().update(row);
      case CalculationRowType.HourlySalaryWithWorkingTimeCompensation:
        return new HourlySalaryWithWorkingTimeCompensationUsecaseLogic().update(row);
      case CalculationRowType.Foreclosure:
        return new ForeclosureUsecaseLogic().update(row, calc);
      case CalculationRowType.TotalWorkerPayment:
        return new TotalWorkerPaymentUsecaseLogic().update(row);
    }
  }

  /**
   * Updates the usecase values based onuser input without going to the server.
   * @param row Row that contains the usecase data.
   */
  public static updateDailyAllowanceUsecase(row: UserDefinedRow, calc: Calculation): void {

    const getDate = () => {
      if (calc && calc.workflow && calc.workflow.salaryDate) {
        return calc.workflow.salaryDate;
      }
      return new Date();
    };

    const data: DailyAllowanceUsecase = row.data;
    switch (data.kind) {
      case DailyAllowanceKind.FullDailyAllowance:
        row.price = Years.getYearlyChangingNumbers(getDate()).sideCosts.taxFreeDailyAllowance; // 43;
        break;
      case DailyAllowanceKind.PartialDailyAllowance:
        row.price =  Years.getYearlyChangingNumbers(getDate()).sideCosts.taxFreeDailyHalfAllowance; // 20;
        break;
      case DailyAllowanceKind.MealAllowance:
        row.price =  Years.getYearlyChangingNumbers(getDate()).sideCosts.taxFreeMealAllowance; // 10.75;
        break;
      case DailyAllowanceKind.InternationalDailyAllowance:
      case DailyAllowanceKind.Undefined:
      default:
        // Not known
        break;
    }
  }

  /**
   * Updates the usecase values based onuser input without going to the server.
   * @param row Row that contains the usecase data.
   */
  public static updateCarBenefitUsecase(row: UserDefinedRow): void {
    const data: CarBenefitUsecase = row.data;
    if (data.kind === CarBenefitKind.LimitedCarBenefit) {
      row.message = "Auton käyttöetu, " + (data.ageGroup || "-").toUpperCase();
    } else if (data.kind === CarBenefitKind.FullCarBenefit) {
      row.message = "Vapaa autoetu, " + (data.ageGroup || "-").toUpperCase();
    } else {
      row.message = "Autoetu, ei tyyppivalintaa, " + (data.ageGroup || "-").toUpperCase();
    }
    if (data.isKilometersBased) {
      row.message += `, ${data.kilometers}.`;
    }
  }

  /**
   * Updates the usecase values based onuser input without going to the server.
   * @param row Row that contains the usecase data.
   */
  public static updateNonProfitOrgUsecase(row: UserDefinedRow): void {
    const data: NonProfitOrgUsecase = row.data;
    switch (data.kind) {
      case NonProfitOrgKind.AccomodationAllowance:
        row.message = "Majoittumiskorvaus";
        break;
      case NonProfitOrgKind.KilometreAllowance:
        row.message = "Kilometrikorvaus";
        break;
      case NonProfitOrgKind.DailyAllowance:
        row.message = "Päiväraha";
        break;
    }
  }
}
