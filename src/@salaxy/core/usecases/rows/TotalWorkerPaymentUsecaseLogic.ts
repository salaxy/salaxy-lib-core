import { CalculationRowType, TotalWorkerPaymentUsecase, TotalWorkerPaymentKind, UserDefinedRow } from "../../model";
import { EnumerationsLogic } from "../../logic";
import { Numeric } from "../../util";
import { UsecaseValidationMessage } from "./UsecaseValidationMessage";

/** Client-side logic for TotalWorkerPayment  */
export class TotalWorkerPaymentUsecaseLogic {

  /**
   * Updates the usecase values based on user input without going to the server.
   * @param row Row that contains the usecase data.
   */
  public update(row: UserDefinedRow): void {
    const data: TotalWorkerPaymentUsecase = row.data || {};

    if (!data.kind || data.kind === TotalWorkerPaymentKind.Undefined) {
      data.kind = TotalWorkerPaymentKind.TotalWorkerPayment;
    }

    if (!data.rowType) {
      data.rowType = CalculationRowType.Salary;
    }

  }

  /** Gets validation message for BoardUsecaseLogic  */
  public getValidation(row: UserDefinedRow): UsecaseValidationMessage {
    const data: TotalWorkerPaymentUsecase = row.data || {};

    if (!data.kind || data.kind === TotalWorkerPaymentKind.Undefined) {
      data.kind = TotalWorkerPaymentKind.TotalWorkerPayment;
    }

    if (!data.rowType) {
      data.rowType = CalculationRowType.Salary;
    }

    return {
      type: "default",
      msg: `${EnumerationsLogic.getEnumLabel("CalculationRowType", data.rowType)} lasketaan annetun summan ${Numeric.formatPrice(row.price)} perusteella. Summan sisältämät erät: ${EnumerationsLogic.getEnumLabel("TotalWorkerPaymentKind", data.kind)}.`,
    };

    return null;
  }
}
