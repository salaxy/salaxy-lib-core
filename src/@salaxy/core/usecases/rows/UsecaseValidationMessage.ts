/** Client-side validation message for a usecase input. */
export interface UsecaseValidationMessage {

  /** Message text */
  msg: string;

  /** Type of the message. */
  type: "default" | "error";
}
