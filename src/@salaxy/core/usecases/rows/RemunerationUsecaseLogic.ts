import { RemunerationKind, RemunerationUsecase, UserDefinedRow } from "../../model";
import { EnumerationsLogic, IncomeTypeMetadata, IncomeTypesLogic, } from "../../logic";
import { UsecaseValidationMessage } from "./UsecaseValidationMessage";

/** Client-side logic for RemunerationUsecase  */
export class RemunerationUsecaseLogic {

  /**
   * Updates the usecase values based onuser input without going to the server.
   * @param row Row that contains the usecase data.
   */
  public update(row: UserDefinedRow): void {
    const data: RemunerationUsecase = row.data || {};
    const codeData = this.getCodeData(data.kind);
    if (codeData) {
      // TODO: check if this is the right way to set the message
      if (!row.message || row.message === EnumerationsLogic.getEnumLabel("CalculationRowType", row.rowType) || this.isIrIncomeTypeLabel(row.message)) {
        row.message = codeData.label;
      }
    }
  }

  /** Gets validation message for IrIncomeTypeUsecase  */
  public getValidation(row: UserDefinedRow): UsecaseValidationMessage {
    const data: RemunerationUsecase = row.data || {};
    const codeData = this.getCodeData(data.kind);
    if (!codeData) {
      return {
        type: "error",
        msg: "Tulolaji puuttuu.",
      };
    }
    return null;
  }

  private getCodeData(kind: RemunerationKind): IncomeTypeMetadata {
    if (kind) {
      let code = 0;
      switch (kind) {
        case RemunerationKind.InitiativeFee:
          code = 202;
          break;
        case RemunerationKind.BonusPay:
          code = 203;
          break;
        case RemunerationKind.Commission:
          code = 220;
          break;
        case RemunerationKind.PerformanceBonus:
          code = 223;
          break;
        case RemunerationKind.ShareIssueForEmployees:
          code = 226;
          break;
        case RemunerationKind.ProfitSharingBonus:
          code = 233;
          break;
        case RemunerationKind.Undefined:
        default:
          code = 0;
          break;
      }
      return IncomeTypesLogic.getAll().find((x) => x.code === code);
    }
    return null;
  }

  private isIrIncomeTypeLabel(label: string): boolean {
    return IncomeTypesLogic.getAll().some((x) => x.label === label);
  }
}
