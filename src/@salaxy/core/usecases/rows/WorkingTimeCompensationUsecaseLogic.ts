import { UserDefinedRow, WorkingTimeCompensationKind, WorkingTimeCompensationUsecase } from "../../model";
import { EnumerationsLogic, IncomeTypeMetadata, IncomeTypesLogic } from "../../logic";
import { UsecaseValidationMessage } from "./UsecaseValidationMessage";

/** Client-side logic for WorkingTimeCompensationUsecase  */
export class WorkingTimeCompensationUsecaseLogic {

  /**
   * Updates the usecase values based onuser input without going to the server.
   * @param row Row that contains the usecase data.
   */
  public update(row: UserDefinedRow): void {
    const data: WorkingTimeCompensationUsecase = row.data || {};
    const codeData = this.getCodeData(data.kind);
    if (codeData) {
      // TODO: check if this is the right way to set the message
      if (!row.message || row.message === EnumerationsLogic.getEnumLabel("CalculationRowType", row.rowType) || this.isIrIncomeTypeLabel(row.message)) {
        row.message = codeData.label;
      }
    }
  }

  /** Gets validation message for IrIncomeTypeUsecase  */
  public getValidation(row: UserDefinedRow): UsecaseValidationMessage {
    const data: WorkingTimeCompensationUsecase = row.data || {};
    const codeData = this.getCodeData(data.kind);
    if (!codeData) {
      return {
        type: "error",
        msg: "Tulolaji puuttuu.",
      };
    }
    return null;
  }

  private getCodeData(kind: WorkingTimeCompensationKind): IncomeTypeMetadata {
    if (kind) {
      let code = 0;
      switch (kind) {
        case WorkingTimeCompensationKind.EmergencyWorkCompensation:
          code = 205;
          break;
        case WorkingTimeCompensationKind.EveningWorkCompensation:
          code = 206;
          break;
        case WorkingTimeCompensationKind.EveningShiftAllowance:
          code = 207;
          break;
        case WorkingTimeCompensationKind.SaturdayPay:
          code = 211;
          break;
        case WorkingTimeCompensationKind.ExtraWorkPremium:
          code = 212;
          break;
        case WorkingTimeCompensationKind.OtherCompensation:
          code = 216;
          break;
        case WorkingTimeCompensationKind.WaitingTimeCompensation:
          code = 217;
          break;
        case WorkingTimeCompensationKind.SundayWorkCompensation:
          code = 221;
          break;
        case WorkingTimeCompensationKind.StandByCompensation:
          code = 230;
          break;
        case WorkingTimeCompensationKind.WeeklyRestCompensation:
          code = 232;
          break;
        case WorkingTimeCompensationKind.OvertimeCompensation:
          code = 235;
          break;
        case WorkingTimeCompensationKind.NightWorkAllowance:
          code = 236;
          break;
        case WorkingTimeCompensationKind.NightShiftCompensation:
          code = 237;
          break;
        case WorkingTimeCompensationKind.Undefined:
        default:
          code = 0;
          break;
      }
      return IncomeTypesLogic.getAll().find((x) => x.code === code);
    }
    return null;
  }

  private isIrIncomeTypeLabel(label: string): boolean {
    return IncomeTypesLogic.getAll().some((x) => x.label === label);
  }
}
