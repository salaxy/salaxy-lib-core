import {
  Calculation,
  CalculationRowType,
  CarBenefitUsecase,
  DailyAllowanceKind,
  DailyAllowanceUsecase,
  NonProfitOrgUsecase,
  SubsidisedCommuteKind,
  SubsidisedCommuteUsecase,
  UserDefinedRow,
} from "../../model";
import { EnumerationsLogic } from "../../logic";
import { Numeric } from "../../util";

import { BoardUsecaseLogic } from "./BoardUsecaseLogic";
import { EmploymentTerminationUsecaseLogic } from "./EmploymentTerminationUsecaseLogic";
import { ForeclosureUsecaseLogic } from "./ForeclosureUsecaseLogic";
import { HourlySalaryWithWorkingTimeCompensationUsecaseLogic } from "./HourlySalaryWithWorkingTimeCompensationUsecaseLogic";
import { IrIncomeTypeUsecaseLogic } from "./IrIncomeTypeUsecaseLogic";
import { MealBenefitUsecaseLogic } from "./MealBenefitUsecaseLogic";
import { OtherCompensationUsecaseLogic } from "./OtherCompensationUsecaseLogic";
import { RemunerationUsecaseLogic } from "./RemunerationUsecaseLogic";
import { UnionPaymentUsecaseLogic } from "./UnionPaymentUsecaseLogic";
import { UsecaseValidationMessage } from "./UsecaseValidationMessage";
import { WorkingTimeCompensationUsecaseLogic } from "./WorkingTimeCompensationUsecaseLogic";
import { TotalWorkerPaymentUsecaseLogic } from "./TotalWorkerPaymentUsecaseLogic";

/**
 * Provides logic for usecase user interfaces.
 */
export class UsecaseValidations {

  /**
   * Returns a validation message for a row or null if none is required.
   * @param row The row that is validated.
   * @param calc The calculation of the row.
   */
  public static getValidation(row: UserDefinedRow, calc: Calculation): UsecaseValidationMessage {
    if (row.price < 0) {
      if (!UsecaseValidations.canHaveNegativeRow(row.rowType)) {
        return {
          type: "error",
          msg: "Hinta ei voi olla negatiivinen: Tulorekisteriin ei voi ilmoittaa miinusmerkkisiä arvoja.",
        };
      }
    }
    if (row.count < 0) {
      return {
        type: "error",
        msg: "Määrä ei voi olla negatiivinen: Tulorekisteriin ei voi ilmoittaa miinusmerkkisiä arvoja.",
      };
    }
    switch (row.rowType) {
      case CalculationRowType.Expenses:
      case CalculationRowType.PrepaidExpenses:
        return UsecaseValidations.getExpensesValidation(row);
      case CalculationRowType.MealBenefit:
        return new MealBenefitUsecaseLogic().getValidation(row, calc);
      case CalculationRowType.DailyAllowance:
        return UsecaseValidations.getDailyAllowanceValidation(row);
      case CalculationRowType.NonProfitOrg:
        return UsecaseValidations.getNonProfitOrgValidation(row);
      case CalculationRowType.SubsidisedCommute:
        return UsecaseValidations.getSubsidisedCommuteValidation(row);
      case CalculationRowType.CarBenefit:
        return UsecaseValidations.getCarBenefitValidation(row);
      case CalculationRowType.UnionPayment:
        return new UnionPaymentUsecaseLogic().getValidation(row);
      case CalculationRowType.IrIncomeType:
        return new IrIncomeTypeUsecaseLogic().getValidation(row);
      case CalculationRowType.Board:
        return new BoardUsecaseLogic().getValidation(row);
      case CalculationRowType.Remuneration:
        return new RemunerationUsecaseLogic().getValidation(row);
      case CalculationRowType.OtherCompensation:
        return new OtherCompensationUsecaseLogic().getValidation(row);
      case CalculationRowType.WorkingTimeCompensation:
        return new WorkingTimeCompensationUsecaseLogic().getValidation(row);
      case CalculationRowType.EmploymentTermination:
        return new EmploymentTerminationUsecaseLogic().getValidation(row);
      case CalculationRowType.HourlySalaryWithWorkingTimeCompensation:
        return new HourlySalaryWithWorkingTimeCompensationUsecaseLogic().getValidation(row);
      case CalculationRowType.Foreclosure:
        return new ForeclosureUsecaseLogic().getValidation(row);
        case CalculationRowType.TotalWorkerPayment:
          return new TotalWorkerPaymentUsecaseLogic().getValidation(row);
      default:
        return null;
    }
  }

  /**
   * Returns true if the row type supports negative values.
   * Typically, this means that the sum of all rows must still be zero or positive.
   * @param rowType Row type to check
   */
  public static canHaveNegativeRow(rowType: CalculationRowType): boolean {
    // NOTE: This is also validated server-side, so you need to add it there as well.
    const supportsNegative: CalculationRowType[] = [
      // Note that there is a corresponding client-side method in UsecaseValidations.canHaveNegativeRow(), so you should update that as well.
      CalculationRowType.MonthlySalary,
      CalculationRowType.Salary,
      CalculationRowType.Compensation,
      CalculationRowType.Expenses,
    ];
    return !!supportsNegative.find((x) => x === rowType);
  }

  /** Gets validation message for DailyAllowanceUsecase  */
  public static getDailyAllowanceValidation(row: UserDefinedRow): UsecaseValidationMessage {
    const data: DailyAllowanceUsecase = row.data || {};
    let msg;
    switch (data.kind) {
      case DailyAllowanceKind.PartialDailyAllowance:
      case DailyAllowanceKind.InternationalDailyAllowance:
      case DailyAllowanceKind.MealAllowance:
        msg = EnumerationsLogic.getEnumLabel("DailyAllowanceKind", data.kind);
        break;
      case DailyAllowanceKind.Undefined:
      default:
        msg = "Kotimaan kokopäiväraha";
        break;
    }
    return { type: "default", msg };
  }

  /** Gets validation message for NonProfitOrgUsecase  */
  public static getNonProfitOrgValidation(row: UserDefinedRow): UsecaseValidationMessage {
    const data: NonProfitOrgUsecase = row.data || {};
    if (!data.kind) {
      return {
        type: "error",
        msg: "HUOM: Määritä korvauksen tyyppi. Oletuksena käytetään kilometrikorvausta.",
      };
    }
    return { type: "default", msg: `Yleishyödyllisen yhteisön ${EnumerationsLogic.getEnumLabel("NonProfitOrgKind", data.kind)}` };
  }

  /** Gets validation message for SubsidisedCommuteUsecase  */
  public static getSubsidisedCommuteValidation(row: UserDefinedRow): UsecaseValidationMessage {
    const data: SubsidisedCommuteUsecase = row.data || {};
    let msg;
    switch (data.kind) {
      case SubsidisedCommuteKind.SingleDeduction:
        msg = `Työsuhdematkalippu, vähennys palkasta: ${Numeric.formatPrice(data.deduction || 0)}`;
        break;
      case SubsidisedCommuteKind.PeriodicalDeduction:
        msg = `Työsuhdematkalippu, vähennys palkasta: ${Numeric.formatPrice(data.deduction || 0)} jaettuna ${data.periodDivider || 12} osaan.`;
        break;
      case SubsidisedCommuteKind.NoDeduction:
      default:
        msg = "Työsuhdematkalippu, ei vähennystä palkasta.";
        break;
    }
    return { type: "default", msg };
  }

  /** Gets validation message for CarBenefitUsecase  */
  public static getCarBenefitValidation(row: UserDefinedRow): UsecaseValidationMessage {
    const data: CarBenefitUsecase = row.data || {};
    if (!data.kind || !data.ageGroup || (data.isKilometersBased && data.kilometers == null)) {
      return {
        type: "error",
        msg: "Pakollisia tietoja puuttuu! Autoetu lasketaan todennäköisesti väärin.",
      };
    }
    const msg = EnumerationsLogic.getEnumLabel("CarBenefitCode", data.kind)
      + ", "
      + EnumerationsLogic.getEnumLabel("AgeGroupCode", data.ageGroup)
      + (data.isKilometersBased ? `, kilometripohjainen laskenta: ${data.kilometers || ""}km` : "")
      + (data.deduction ? `. Vähennys: ${Numeric.formatPrice(data.deduction)}.` : ". Ei vähennystä palkasta.")
      ;
    return { type: "default", msg };
  }

  /** Gets validation message for Expenses  */
  public static getExpensesValidation(row: UserDefinedRow): UsecaseValidationMessage {
    const data: {
      /** accounting data */
      accounting?: {
        /** vat percent, e.g. 0.24 */
        vatPercent?: number,
      },
    } = row.data || {};

    let msg = "Arvonlisäveroa ei määritetty";
    if (data && data.accounting) {
      if (data.accounting.vatPercent === 0) {
        msg = "Arvonlisävero 0 % - Nollaverokanta";
      } else if (data.accounting.vatPercent === 0.10) {
        msg = "Arvonlisävero 10 % - Kirjat, kulttuuri, henkilökuljetus ja majoitus";
      } else if (data.accounting.vatPercent === 0.14) {
        msg = "Arvonlisävero 14 % - Elintarvikkeet, ravintola- ja ateriapalvelut";
      } else if (data.accounting.vatPercent === 0.24) {
        msg = "Arvonlisävero 24 % - Yleinen verokanta";
      } else if (!data.accounting.vatPercent) {
        // let it be the default
      } else {
        msg = "Arvonlisävero " + (100 * data.accounting.vatPercent) + " %";
      }
    }
    return { type: "default", msg };
  }
}
