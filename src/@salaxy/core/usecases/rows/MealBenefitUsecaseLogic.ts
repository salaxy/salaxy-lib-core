import { Calculation, MealBenefitKind, MealBenefitUsecase, UserDefinedRow } from "../../model";
import { EnumerationsLogic  } from "../../logic";
import { Numeric, Years } from "../../util";
import { UsecaseValidationMessage } from "./UsecaseValidationMessage";

/** Client-side logic for MealBenefitUsecase  */
export class MealBenefitUsecaseLogic {

  // TODO: Create interface and probably a base class that has contructor with row and the data propety casted.

  /**
   * Updates the usecase values based onuser input without going to the server.
   * @param row Row that contains the usecase data.
   */
  public update(row: UserDefinedRow, calc: Calculation): void {
    const getDate = () => {
      if (calc && calc.workflow && calc.workflow.salaryDate) {
        return calc.workflow.salaryDate;
      }
      return new Date();
    };

    const data: MealBenefitUsecase = row.data;
    data.taxablePrice = this.getTaxablePrice(data.kind, row.price, getDate());
    switch (data.kind) {
      case MealBenefitKind.MealAllowance:
        row.price = Years.getYearlyChangingNumbers(getDate()).sideCosts.taxFreeMealAllowance; // 10.75;
        break;
      case MealBenefitKind.CateringContract:
      case MealBenefitKind.Institute:
      case MealBenefitKind.RestaurantWorker:
      case MealBenefitKind.Teacher:
        row.price = this.getTaxablePrice(data.kind, 1, getDate());
        break;
      case MealBenefitKind.MealTicket:
      case MealBenefitKind.TaxableAmount:
      case MealBenefitKind.Undefined:
      default:
        // Manual input
        break;
    }
  }

  /** Gets validation message for MealBenefitUsecase  */
  public getValidation(row: UserDefinedRow, calc: Calculation): UsecaseValidationMessage {

    const getDate = () => {
      if (calc && calc.workflow && calc.workflow.salaryDate) {
        return calc.workflow.salaryDate;
      }
      return new Date();
    };

    const data: MealBenefitUsecase = row.data || {};
    data.taxablePrice = this.getTaxablePrice(data.kind, row.price, getDate());
    switch (data.kind) {
      case MealBenefitKind.MealAllowance:
      case MealBenefitKind.CateringContract:
      case MealBenefitKind.Institute:
      case MealBenefitKind.MealTicket:
      case MealBenefitKind.RestaurantWorker:
      case MealBenefitKind.Teacher:
      case MealBenefitKind.TaxableAmount:
        return {
          type: "default",
          msg: `${EnumerationsLogic.getEnumLabel("MealBenefitKind", data.kind)}, verotusarvo ${Numeric.formatPrice(data.taxablePrice)}, vähennys palkasta ${Numeric.formatPrice(data.deduction || 0)}.`,
        };
      case MealBenefitKind.Undefined:
      default:
        data.taxablePrice = row.price;
        return {
          type: "error",
          msg: "HUOM: Ravintoedun tyyppiä ei ole määritetty. Oletus: verotusarvo, ei korvausta työntekijän palkasta.",
        };
    }
  }

  /**
   * Gets the taxable price for the row.
   * https://www.vero.fi/syventavat-vero-ohjeet/paatokset/47380/verohallinnon-p%C3%A4%C3%A4t%C3%B6s-vuodelta-2020-toimitettavassa-verotuksessa-noudatettavista-luontoisetujen-laskentaperusteista/
   * @param kind meal benefit type
   * @param price Price of the row.
   */
  public getTaxablePrice(kind: MealBenefitKind, price: number, datelyObject: any): number {

    switch (kind) {
      case MealBenefitKind.MealAllowance:
        return null;
      case MealBenefitKind.CateringContract:
        return Years.getYearlyChangingNumbers(datelyObject).sideCosts.mealBenefitCateringContract; // 6.80;
      case MealBenefitKind.Institute:
        return Years.getYearlyChangingNumbers(datelyObject).sideCosts.mealBenefitInstitute; // 5.10;
      case MealBenefitKind.MealTicket:
        return Numeric.round( 0.75 * price, 3); // Calculate using one extra decimal to preserve decimals
      case MealBenefitKind.RestaurantWorker:
        return Years.getYearlyChangingNumbers(datelyObject).sideCosts.mealBenefitRestaurantWorker; // 5.78;
      case MealBenefitKind.Teacher:
        return Years.getYearlyChangingNumbers(datelyObject).sideCosts.mealBenefitTeacher; // 4.08;
      case MealBenefitKind.TaxableAmount:
        return price;
      case MealBenefitKind.Undefined:
      default:
        return price;
    }
  }
}
