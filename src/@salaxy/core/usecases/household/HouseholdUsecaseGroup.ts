import { HouseholdUsecase } from "./HouseholdUsecase";
/** Group of usecases (user interfaces for defining a framework) */
export interface HouseholdUsecaseGroup {
    /** Identifier for the group */
    id: string;

    /**
     * Key for the usecase based on which it is resolved.
     * Later, this will resolve to a microservice.
     */
    uri?: string;

    /** Label in the list / tree selection */
    label?: string;

    /** Description of the use case group (mainly for the lists) */
    descr?: string;

    /** Path to the main icon (typically an SVG ) */
    icon: string;

    /** Path to a sub icon (typically a PNG)  */
    badge?: string;

    /** Collection of use cases - in the future, may also support sub groups. */
    children?: HouseholdUsecase[];
}
