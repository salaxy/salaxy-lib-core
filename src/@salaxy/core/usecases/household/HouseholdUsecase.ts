import { TaxDeductionWorkCategories } from "../../model";

/** Calculation usecase for households. */
export interface HouseholdUsecase {
    /** Unique identifier for the item in this user interface tree */
    id: string;

    /**
     * Key for the usecase based on which it is resolved.
     * Later, this will resolve to a microservice.
     */
    uri?: string;

    /** Label in the list / tree selection */
    label?: string;

    /** Description of the use case (mainly for the lists) */
    descr?: string;

    /** Path to the main icon (typically an SVG ), if null the default may be set by the group */
    icon?: string;

    /** Path to a sub icon (typically a PNG), if null the default may be set by the group  */
    badge?: string;

    /** If true, the selection is deductible in household taxation */
    isHouseholdDeductible?: boolean;

    /** If true, the selection is under childcare subsidy. */
    isChildcareSubsidy?: boolean;

    /** Indicates that the default for the contract length is less than a year. */
    isContractLessThanYear?: boolean,

    /**
     * If tax IsHouseholdDeductible is set to true, please also select the category (categories).
     * Note that "OwnPropety" is a default, it does not need to be set: Only set RelativesProperty if necessary.
     */
    taxDeductionCategories?: TaxDeductionWorkCategories | null;

    /** Occupation code for insurance purposes. */
    occupation?: string;
}
