import { HouseholdUsecase } from "./HouseholdUsecase";

/** Defines the usecase data for construction */
export interface UsecaseConstruction extends HouseholdUsecase {

    /**
     * If worktype is "other", the user should describe the work.
     * These descriptions may be used in insurance reports generation.
     */
    workTypeOtherMessage: boolean;

    /** If true, the TES additions are included in the agreed salary. */
    isTesIncludedInSalary: boolean;

    /**
     * If true, the expenses are agreed in a bispoken way.
     * Default is that expenses are based on Framework agreement (TES) tables.
     */
    isExpensesCustom: boolean;

    /** Daily distance (km) from worker home to construction site. */
    dailyTravelExpensesKm: number;

    /** Daily expenses (työkalukorvaus) if isExpensesCustom=true  */
    dailyExpenses: number;

    /** Daily travel expenses if isExpensesCustom=true  */
    dailyTravelExpenses: number;
}
