import { HouseholdUsecaseGroup } from "./HouseholdUsecaseGroup";
import { TaxDeductionWorkCategories } from "../../model";

/**
 * The base usecase tree for household customers.
 * The labels and descriptions are in SALAXY.ENUM.HouseHoldUseCaseTree.
 */
export const householdUsecaseTree: HouseholdUsecaseGroup[] = [
  {
    id: "childCare",
    icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/childcare.svg",
    children: [
      {
        id: "childCare/mll",
        uri: "https://secure.salaxy.com/v02/api/usecases/mll",
        isHouseholdDeductible: true,
        icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/childcare.svg",
        badge: "https://cdn.salaxy.com/ng1/household-usecases/badge/mll.png",
        occupation: "53112",
      },
      {
        id: "childCare/kela",
        uri: "https://secure.salaxy.com/v02/api/usecases/childCare",
        isChildcareSubsidy: true,
        isHouseholdDeductible: false,
        icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/childcare.svg",
        badge: "https://cdn.salaxy.com/ng1/household-usecases/badge/kela.png",
        occupation: "53112",
      },
      {
        id: "childCare/other",
        uri: "https://secure.salaxy.com/v02/api/usecases/childCare",
        isHouseholdDeductible: true,
        icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/childcare.svg",
        occupation: "53112",
      },
    ],
  },
  {
    id: "construction",
    icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/construction.svg",
    children: [
      {
        id: "construction/carpenter",
        icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/construction-carpenter.svg",
        uri: "https://secure.salaxy.com/v02/api/usecases/construction",
        occupation: "71150",
      },
      {
        id: "construction/painter",
        icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/construction-painter.svg",
        uri: "https://secure.salaxy.com/v02/api/usecases/construction",
        occupation: "71310",
      },
      {
        id: "construction/renovation",
        icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/construction-renovation.svg",
        uri: "https://secure.salaxy.com/v02/api/usecases/construction",
        isHouseholdDeductible: true,
        occupation: "71110",
      },
      {
        id: "construction/new",
        icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/construction-new.svg",
        uri: "https://secure.salaxy.com/v02/api/usecases/construction",
        isHouseholdDeductible: false,
        occupation: "71110",
      },
      {
        id: "construction/other",
        uri: "https://secure.salaxy.com/v02/api/usecases/other",
        icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/other.svg",
        occupation: "other",
      },
    ],
  },
  {
    id: "cleaning",
    uri: "https://secure.salaxy.com/v02/api/usecases/cleaning",
    icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/cleaning.svg",
    children: [
      {
        id: "cleaning/91110",
        isHouseholdDeductible: true,
        occupation: "91110",
        isContractLessThanYear: true
      },
      {
        id: "cleaning/51530",
        icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/cleaning-real-estate.svg",
        isHouseholdDeductible: true,
        occupation: "51530",
        isContractLessThanYear: true
      },
      {
        id: "cleaning/61132",
        icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/cleaning-garden.svg",
        isHouseholdDeductible: true,
        occupation: "61132",
        isContractLessThanYear: true
      },
      {
        id: "cleaning/53221",
        isHouseholdDeductible: true,
        occupation: "53221",
        isContractLessThanYear: true
      },
      {
        id: "cleaning/other",
        icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/cleaning-other.svg",
        isHouseholdDeductible: true,
        occupation: "other",
        isContractLessThanYear: true
      },
    ],
  },
  {
    id: "other",
    uri: "https://secure.salaxy.com/v02/api/usecases/other",
    icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/other.svg",
    children: [
      {
        id: "other/53222",
        isHouseholdDeductible: true,
        taxDeductionCategories: TaxDeductionWorkCategories.Carework,
        occupation: "53222",
      },
      {
        id: "other/61112",
        occupation: "61112",
      },
      {
        id: "other/91110",
        isHouseholdDeductible: true,
        taxDeductionCategories: TaxDeductionWorkCategories.Householdwork,
        occupation: "91110",
      },
      {
        id: "other/53213",
        isHouseholdDeductible: true,
        taxDeductionCategories: TaxDeductionWorkCategories.Carework,
        occupation: "53213",
      },
      {
        id: "other/53212",
        isHouseholdDeductible: true,
        taxDeductionCategories: TaxDeductionWorkCategories.Carework,
        occupation: "53212",
      },
      {
        id: "other/53112",
        isHouseholdDeductible: true,
        taxDeductionCategories: TaxDeductionWorkCategories.Carework,
        occupation: "53112",
      },
      {
        id: "other/53221",
        isHouseholdDeductible: true,
        taxDeductionCategories: TaxDeductionWorkCategories.Carework,
        occupation: "53221",
      },
      {
        id: "other/62100",
        icon: "https://cdn.salaxy.com/ng1/household-usecases/icon/other.svg",
        occupation: "62100",
      },
      {
        id: "other/51530",
        isHouseholdDeductible: true,
        taxDeductionCategories: TaxDeductionWorkCategories.Householdwork,
        occupation: "51530",
      },
      {
        id: "other/61132",
        isHouseholdDeductible: true,
        taxDeductionCategories: TaxDeductionWorkCategories.Householdwork,
        occupation: "61132",
      },
      {
        id: "other/undefined",
        occupation: "other",
      },
    ],
  },
];
