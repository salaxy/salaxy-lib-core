import { HouseholdUsecase } from "./HouseholdUsecase";

/** Defines the usecase data for cleaning */
export interface UsecaseCleaning extends HouseholdUsecase {

    /**
     * If salaries are compensated (when holidays are not kept, isFullTime=false),
     * the percentage is different depending on whether the person has been working for a full year or not.
     */
    isContractLessThanYear: boolean;

    /**
     * If true, this is a full time contract in the respect of holidays:
     * Holidays are kept during summer and salary is paid for these holidays.
     */
    isFullTime: boolean;

    /**
     * If worktype is "other", the user should describe the work.
     * These descriptions may be used in insurance reports generation.
     */
    workTypeOtherMessage: boolean;
}
