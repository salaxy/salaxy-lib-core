import { householdUsecaseTree } from "./householdUsecaseTree";
import { Calculation, CalculationRowType, TaxDeductionWorkCategories } from "../../model";
import { Dates } from "../../util";
import { CalculatorLogic, Translations } from "../../logic";
import { HouseholdUsecase } from "./HouseholdUsecase";
import { UsecaseChildCare } from "./UsecaseChildCare";
import { UsecaseCleaning } from "./UsecaseCleaning";
import { UsecaseConstruction } from "./UsecaseConstruction";
import { HouseholdUsecaseGroup } from "./HouseholdUsecaseGroup";

/**
 * Provides business logic for Calculation usecases:
 * Salary framework contracts ("työehtosopimus") and other salary recommendations
 * that are used as bases for salary calculation.
 */
export class HouseholdUsecasesLogic {

  /**
   * Known use cases in the first implementation.
   * These implementations replace the olf Calc.Framework.Type -based solution that is not very extensible.
   * The naming is still based on the old system for easier compatibility.
   */
  public static readonly knownUseCases = {

    /** Usecase has not been defined. Note that value is null, not "https://secure.salaxy.com/v02/api/usecases/notDefined" */
    notDefined: null,

    /** Generic child care usecase */
    childCare: "https://secure.salaxy.com/v02/api/usecases/childCare",

    /** Cleaning */
    cleaning: "https://secure.salaxy.com/v02/api/usecases/cleaning",

    /** Construction (Raksa) */
    construction: "https://secure.salaxy.com/v02/api/usecases/construction",

    /** Child care according to MLL recommendations. */
    mll: "https://secure.salaxy.com/v02/api/usecases/mll",

    /**
     * Santa Claus for Christmas campaigns: By default, this is not visible in the household UI:
     * It needs to be separately enabled for chirstmas campaigns.
     */
    santaClaus: "https://secure.salaxy.com/v02/api/usecases/santaClaus",

    /** Generic household salary payment usecase user interface. */
    other: "https://secure.salaxy.com/v02/api/usecases/other",
  };

  /**
   * Finds a parent group of a given usecase node
   * @param usecaseId - The usecase whose parent is fetched.
   * If you pass a group here, the function will return null which will typically mean the tree root in the user interface.
   *
   * @example
   * ```
   * // Creates a simple calculation
   * const usecase = UsecasesLogic.findUsecaseById("childCare/mll");
   * UsecasesLogic.setUsecase(this.context.calculations.current, usecase);
   * this.context.calculations.recalculateCurrent((calc) => {
   *     calc.worker.paymentData.telephone = "+358401234567";
   * });
   * ```
   */
  public static findUsecaseById(usecaseId: string): HouseholdUsecase {
    for (const group of HouseholdUsecasesLogic.getUsecaseTree()) {
      for (const usecase of group.children || []) {
        if (usecase.id === usecaseId) {
          return usecase;
        }
      }
    }
    return null;
  }

  /**
   * Gets a tree of usecases (user interfaces for defining a framework) for a given role.
   */
  public static getUsecaseTree(): HouseholdUsecaseGroup[] {
    const tree: HouseholdUsecaseGroup[] = householdUsecaseTree;
    for (const group of tree) {
      this.translate("HouseHoldUseCaseTree", group.id, group);
      for (const usecase of group.children) {
        usecase.badge = usecase.badge || group.badge;
        usecase.icon = usecase.icon || group.icon;
        usecase.uri = usecase.uri || group.uri;
        this.translate("HouseHoldUseCaseTree", usecase.id, usecase);
      }
    }
    return tree;
  }

  /**
   * Abstract the getting of the use case for the calculation.
   * We are expecting changes to the usecase implementation.
   * @param calc Calculation into which the usecase data is stored.
   */
  public static getUsecaseData(calc: Calculation): HouseholdUsecase {
    if (!calc.usecase?.uri) {
      const usecase = HouseholdUsecasesLogic.findUsecaseById("other/undefined");
      calc.usecase = {
        uri: usecase.uri,
        label: usecase.label,
        description: usecase.descr,
        data: usecase,
      };
    }
    return calc.usecase.data as HouseholdUsecase;
  }

  /**
   * Sets calculation properties based on selected usecase.
   * @param calc Calculation to set
   * @param usecase Usecase to apply.
   * @example
   * ```
   * // Creates a simple calculation
   * const usecase = UsecasesLogic.findUsecaseById("childCare/mll");
   * UsecasesLogic.setUsecase(this.calc, usecase);
   * this.calcApi.recalculate(this.calc).then((data) => {
   *   angular.copy(data, this.calc);
   * });
   * ```
   */
  public static setUsecase(calc: Calculation, usecase: HouseholdUsecase): HouseholdUsecase {
    calc.usecase = {
      uri: usecase.uri,
      label: usecase.label,
      description: usecase.descr,
      data: usecase,
    };
    this.applyUseCase(calc);
    return this.getUsecaseData(calc);
  }

  /**
   * Applies the usecase to a calculation, especially to the old Framework object.
   * Should be called before sending the calculation to the server (recalculate/save).
   * @param calc Calculation where usecase is set to calc.data.usecase.
   */
  public static applyUseCase(calc: Calculation): void {
    const usecase = this.getUsecaseData(calc);
    switch (usecase.uri) {
      case HouseholdUsecasesLogic.knownUseCases.childCare:
        this.applyUsecaseChildCare(calc);
        break;
      case HouseholdUsecasesLogic.knownUseCases.mll:
        this.applyUsecaseMll(calc);
        break;
      case HouseholdUsecasesLogic.knownUseCases.cleaning:
        this.applyUsecaseCleaning(calc);
        break;
      case HouseholdUsecasesLogic.knownUseCases.construction:
        this.applyUsecaseConstruction(calc);
        break;
      case HouseholdUsecasesLogic.knownUseCases.santaClaus:
        this.applyUsecaseSantaClaus(calc);
        break;
      case HouseholdUsecasesLogic.knownUseCases.other:
        this.applyCommonUsecaseProperties(calc);
        break;
      case HouseholdUsecasesLogic.knownUseCases.notDefined:
        // Nothing applied.
        break;
    }
  }

  /**
   * Applies the constructions salary calculation usecase.
   * @param calc Calculation to which the usecase is applied.
   */
  public static applyUsecaseConstruction(calc: Calculation): void {
    const usecase = this.getUsecaseData(calc) as UsecaseConstruction;
    const mainRow = CalculatorLogic.getSalaryRow(calc);
    switch (mainRow.rowType) {
      case CalculationRowType.HourlySalary:
      case CalculationRowType.MonthlySalary:
      case CalculationRowType.Salary:
        break;
      case CalculationRowType.TotalEmployerPayment:
        usecase.isTesIncludedInSalary = true;
        usecase.isExpensesCustom = true;
        usecase.dailyExpenses = null;
        usecase.dailyTravelExpenses = null;
        break;
      default:
        mainRow.rowType = CalculationRowType.Salary;
        break;
    }
    usecase.taxDeductionCategories = TaxDeductionWorkCategories.HomeImprovement;
    usecase.isTesIncludedInSalary = usecase.isTesIncludedInSalary || false;
    usecase.isExpensesCustom = usecase.isExpensesCustom || false;
    const numberOfDays = calc.framework.numberOfDays || 0;
    const totalBaseSalary = mainRow.count * mainRow.price;
    calc.rows = calc.rows.filter((row) => row.data?.usecase != usecase.uri );
    if (!usecase.isTesIncludedInSalary) {
      this.addUsecaseRow(calc, usecase.uri, CalculationRowType.HolidayCompensation, 0.185, totalBaseSalary, "Lomakorvaus");
      this.addUsecaseRow(calc, usecase.uri, CalculationRowType.TesWorktimeShortening, 0.077, totalBaseSalary, "Erillinen palkan osa");
    }
    if (usecase.isExpensesCustom) {
      this.addUsecaseRow(calc, usecase.uri, CalculationRowType.MilageOther, numberOfDays, usecase.dailyTravelExpenses,
        `Matkakustannusten korvaus ${usecase.dailyTravelExpenses || 0} €/päivä`);
      this.addUsecaseRow(calc, usecase.uri, CalculationRowType.ToolCompensation, numberOfDays, usecase.dailyExpenses,
        `Työkalukorvaus ${usecase.dailyExpenses || 0} €/päivä`);
    } else {
      const milage = HouseholdUsecasesLogic.raksaKmChart(usecase.dailyTravelExpensesKm);
      const toolCompensation = 1.68;
      this.addUsecaseRow(calc, usecase.uri, CalculationRowType.MilageOther, numberOfDays, milage, `Matkakustannusten korvaus ${milage || 0} €/päivä`);
      this.addUsecaseRow(calc, usecase.uri, CalculationRowType.ToolCompensation, numberOfDays, toolCompensation, `Työkalukorvaus ${toolCompensation || 0} €/päivä`);
    }
    this.applyCommonUsecaseProperties(calc);
  }

  /** Applies the childCare salary calculation usecase */
  public static applyUsecaseChildCare(calc: Calculation): void {
    const usecase = this.getUsecaseData(calc) as UsecaseChildCare;
    const mainRow = CalculatorLogic.getSalaryRow(calc);
    // Salary settings
    if (mainRow.rowType === CalculationRowType.HourlySalary) {
      mainRow.message = "Lastenhoitajan tuntipalkka";
    } else {
      mainRow.rowType = CalculationRowType.MonthlySalary;
      mainRow.message = "Lastenhoitajan kuukausipalkka";
    }
    // Child care subsidy and tax deduction
    calc.rows = calc.rows
      .filter((x) => x.rowType !== CalculationRowType.ChildCareSubsidy);
    if (usecase.isChildcareSubsidy) {
      calc.rows.push({
        count: 1,
        price: usecase.subsidyAmount,
        rowType: CalculationRowType.ChildCareSubsidy,
      });
    } else {
      usecase.subsidyAmount = null;
    }
    if (usecase.isChildcareSubsidy) {
      usecase.isHouseholdDeductible = false;
      usecase.taxDeductionCategories = null;
    }
    if (usecase.isHouseholdDeductible == null) {
      usecase.isHouseholdDeductible = true;
    }
    usecase.taxDeductionCategories = usecase.isHouseholdDeductible ? TaxDeductionWorkCategories.Carework : null;
    this.applyCommonUsecaseProperties(calc);
  }

  /** Applies the cleaning salary calculation usecase */
  public static applyUsecaseCleaning(calc: Calculation): void {
    const usecase = this.getUsecaseData(calc) as UsecaseCleaning;
    const mainRow = CalculatorLogic.getSalaryRow(calc);
    if (mainRow.rowType === CalculationRowType.HourlySalary) {
      // Potentially add custom row message here
    } else if (mainRow.rowType === CalculationRowType.MonthlySalary) {
      // Potentially add custom row message here
    } else {
      mainRow.rowType = CalculationRowType.Salary;
    }
    calc.rows = calc.rows
      .filter((x) => x.rowType !== CalculationRowType.HolidayCompensation);
    if (!usecase.isFullTime) {
      const salary = mainRow.count * mainRow.price;
      const percent = usecase.isContractLessThanYear ? 0.09 : 0.115;
      calc.rows.push({
        count: percent,
        price: salary,
        rowType: CalculationRowType.HolidayCompensation,
      });
    }
    if (usecase.isHouseholdDeductible == null) {
      usecase.isHouseholdDeductible = true;
    }
    usecase.taxDeductionCategories = TaxDeductionWorkCategories.Householdwork;
    this.applyCommonUsecaseProperties(calc);
  }

  /** Applies the MLL salary calculation usecase */
  public static applyUsecaseMll(calc: Calculation): void {
    const usecase = this.getUsecaseData(calc) as UsecaseChildCare;
    const mainRow = CalculatorLogic.getSalaryRow(calc, CalculationRowType.HourlySalary);
    mainRow.rowType = CalculationRowType.HourlySalary;
    mainRow.message = "Lastenhoitajan tuntipalkka";
    if (!mainRow.count || mainRow.count < 2) {
      mainRow.count = 2;
    }
    if (!usecase.useCustomPrice) {
      mainRow.price = 9.50;
    }
    calc.rows = calc.rows
      .filter((x) => x.rowType !== CalculationRowType.SundayWork);
    if (usecase.isSunday) {
      calc.rows.push({
        count: 1,
        price: mainRow.count * mainRow.price,
        rowType: CalculationRowType.SundayWork,
      });
    }

    // Deductions
    calc.framework.isYksityisenHoidonTuki = false;
    usecase.isHouseholdDeductible = true;
    usecase.taxDeductionCategories = TaxDeductionWorkCategories.Carework;

    // Defaults to Period
    if (!calc.info.workStartDate) {
      calc.info.workStartDate = Dates.getToday();
      calc.info.workEndDate = null; // workEndDate is no longer valid
      calc.framework.numberOfDays = null;
    }
    if (!calc.info.workEndDate) {
      calc.info.workEndDate = calc.info.workStartDate;
      calc.framework.numberOfDays = null;
    }
    if (!calc.framework.numberOfDays) {
      calc.framework.numberOfDays = Dates.getWorkdays(
        calc.info.workStartDate,
        calc.info.workEndDate,
      ).length;
    }
    this.applyCommonUsecaseProperties(calc);
  }

  private static applyUsecaseSantaClaus(calc: Calculation): void {
    const usecase = this.getUsecaseData(calc);
    const mainRow = CalculatorLogic.getSalaryRow(calc, CalculationRowType.Salary);
    mainRow.rowType = CalculationRowType.Salary;
    mainRow.message = "Joulupukin palkka";
    usecase.isHouseholdDeductible = false;
    usecase.taxDeductionCategories = null;
    const year = Dates.getMonth("today") < 6 ? Dates.getYear("today") - 1 : Dates.getYear("today");
    const nearestXmsEve = Dates.getDate(year, 12, 24);
    calc.info.workStartDate = nearestXmsEve;
    calc.info.workEndDate = nearestXmsEve;
    calc.framework.numberOfDays = 1;
    this.applyCommonUsecaseProperties(calc);
  }

  private static applyCommonUsecaseProperties(calc: Calculation) {
    const usecase = this.getUsecaseData(calc);
    const mainRow = CalculatorLogic.getSalaryRow(calc);
    calc.info.occupationCode = usecase.occupation;
    calc.salary.isHouseholdDeductible = usecase.isHouseholdDeductible || false;
    calc.salary.taxDeductionCategories = usecase.taxDeductionCategories;
    if (mainRow.rowType === CalculationRowType.HourlySalary) {
      // For hourly salary, default to null so that users really sets the value.
      if (mainRow.count === 0) {
        mainRow.count = null;
      }
    } else {
      // We currently do not support multiple months etc. in these user interfaces.
      // API does support them, but they would add unnecessary complexity to UX.
      mainRow.count = 1;
    }
    mainRow.rowType = mainRow.rowType || CalculationRowType.Salary;
  }

  private static translate(name: string, id: string, useCaseOrGroup: HouseholdUsecase | HouseholdUsecaseGroup) {
    useCaseOrGroup.label = Translations.get("SALAXY.ENUM." + name + "." + id.replace(/\//g, ".") + ".label") || useCaseOrGroup.label;
    useCaseOrGroup.descr = Translations.get("SALAXY.ENUM." + name + "." + id.replace(/\//g, ".") + ".description") || useCaseOrGroup.descr;
  }

  /** Adds a usecase row if price is not zero */
  private static addUsecaseRow(calc: Calculation, usecaseId: string, rowType: CalculationRowType, count: number , price: number, message?: string) {
    if (price) {
      calc.rows.push({
        rowType,
        price,
        message,
        count,
        data: { usecase: usecaseId }
      });
    }
  }

  /** Does the kilometers milage euros conversion */
  private static raksaKmChart(km: number): number
  {
      if ((km ?? 0) < 5) return 0;
      if (km < 10) return 1.94;
      if (km < 20) return 3.13;
      if (km < 30) return 5.63;
      if (km < 40) return 8.20;
      if (km < 50) return 10.10;
      if (km < 60) return 12.24;
      if (km < 70) return 16.08;
      if (km < 80) return 18.20;
      if (km < 90) return 20.68;
      if (km < 100) return 23.55;
      return 26.40;
  }
}
