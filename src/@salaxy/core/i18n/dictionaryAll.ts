import { CoreDictionaryLang } from "../logic/model";

import * as enumIrSx from "./sx/enum-ir.json";
import * as enumSx from "./sx/enum.json";
import * as modelIrSx from "./sx/model-ir.json";
import * as modelSx from "./sx/model.json";
import * as uiTermsSx from "./sx/uiterms.json";
import * as validationSx from "./sx/validation.json";

import * as enumIrFi from "./fi/enum-ir.json";
import * as enumFi from "./fi/enum.json";
import * as modelIrFi from "./fi/model-ir.json";
import * as modelFi from "./fi/model.json";
import * as uiTermsFi from "./fi/uiterms.json";
import * as validationFi from "./fi/validation.json";

import * as enumIrEn from "./en/enum-ir.json";
import * as enumEn from "./en/enum.json";
import * as modelIrEn from "./en/model-ir.json";
import * as modelEn from "./en/model.json";
import * as uiTermsEn from "./en/uiterms.json";
import * as validationEn from "./en/validation.json";

import * as enumIrSv from "./sv/enum-ir.json";
import * as enumSv from "./sv/enum.json";
import * as modelIrSv from "./sv/model-ir.json";
import * as modelSv from "./sv/model.json";
import * as uiTermsSv from "./sv/uiterms.json";
import * as validationSv from "./sv/validation.json";

/**
 * Links together the dictionary files.
 * NOTE: With typing (as any => CoreDictionaryLang) we try to make sure the Typedoc and other reflection methods
 * do not spend time in going through the json structures.
 */
export const dictionaryAll = {
  en: {
    SALAXY: {
      ENUM: { ...enumEn.SALAXY.ENUM, ...enumIrEn.SALAXY.ENUM } as any,
      MODEL: { ...modelEn.SALAXY.MODEL, ...modelIrEn.SALAXY.MODEL } as any,
      UI_TERMS: uiTermsEn.SALAXY.UI_TERMS as any,
      VALIDATION: validationEn.SALAXY.VALIDATION as any,
    },
  } as CoreDictionaryLang,
  fi: {
    SALAXY: {
      ENUM: { ...enumFi.SALAXY.ENUM, ...enumIrFi.SALAXY.ENUM } as any,
      MODEL: { ...modelFi.SALAXY.MODEL, ...modelIrFi.SALAXY.MODEL } as any,
      UI_TERMS: uiTermsFi.SALAXY.UI_TERMS as any,
      VALIDATION: validationFi.SALAXY.VALIDATION as any,
    },
  } as CoreDictionaryLang,
  sv: {
    SALAXY: {
      ENUM: { ...enumSv.SALAXY.ENUM, ...enumIrSv.SALAXY.ENUM } as any,
      MODEL: { ...modelSv.SALAXY.MODEL, ...modelIrSv.SALAXY.MODEL } as any,
      UI_TERMS: uiTermsSv.SALAXY.UI_TERMS as any,
      VALIDATION: validationSv.SALAXY.VALIDATION as any,
    },
  } as CoreDictionaryLang,
  sx: {
    SALAXY: {
      // TODO: Go through the structure: Should enum and enum-ir be separate or not.
      ENUM: { ...enumSx.SALAXY.ENUM, ...enumIrSx.SALAXY.ENUM } as any,
      MODEL: { ...modelSx.SALAXY.MODEL, ...modelIrSx.SALAXY.MODEL } as any,
      UI_TERMS: uiTermsSx.SALAXY.UI_TERMS as any,
      VALIDATION: validationSx.SALAXY.VALIDATION as any,
    },
  } as CoreDictionaryLang,
}