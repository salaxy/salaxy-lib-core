Developers should enable i18n-ally extension (https://github.com/antfu/i18n-ally)
in VSCode for seeing the language versioned texts within TypeScript or JavaScript files.
This also helps in editing the language versioned files.

The SX language is the Salaxy technical language and you should edit that.
You may also edit the end-languages (FI, EN and SV), but please be sure that
the texts are appoved by business users.

Business users maintain translations in https://crowdin.com/
Please ask access if necessary.

(PLEASE NOTE: As of writing 12/2020, the Crowdin process is not yet in use:
The developers may also edit the FI language to see the results in the UI.
However, this should change in Jan/2020)

TODO: occupationlist.json should be made compapatible with language versioning.
The texts come from Tilastokeskus, so they change seldomly though.