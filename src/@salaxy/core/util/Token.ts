import { Base64 } from 'js-base64';

/**
 * Helper class for JWT tokens.
 */
export class Token {

  /**
   * Validates the JWT token. Please note that the method does not check the signature.
   * Returns:
   * noToken - the token is null,
   * invalid - the token has invalid format, or some payload values (e.g. exp) are missing,
   * expired - the token has expired,
   * ok - token is valid.
   */
  public static validate(token: string): "noToken" | "ok" | "expired" | "invalid" {
    if (!token) {
      return "noToken";
    }
    const payload = this.parsePayload(token);
    if (!payload || !payload.exp) {
      return "invalid";
    }

    if (payload.exp * 1000 <= new Date().getTime()) {
      return "expired";
    }

    return "ok";
  }

  /**
   * Decodes and parses the payload from the string formatted token.
   * @param token Token as a string.
   * @returns Payload of the token as JSON object.
   */
  public static parsePayload(token: string): any {
    const tokenParts = token.split(".");
    if (tokenParts.length === 3) {
      let payload = tokenParts[1];
      payload = Base64.decode(unescape(payload));
      const data = JSON.parse(payload);
      return data;
    }
    return null;
  }

  /**
   * Decodes and parses the header from the string formatted token.
   * @param token Token as a string.
   * @returns Payload of the token as JSON object.
   */
  public static parseHeader(token: string): any {
    const tokenParts = token.split(".");
    if (tokenParts.length === 3) {
      let payload = tokenParts[0];
      payload = Base64.decode(unescape(payload));
      const data = JSON.parse(payload);
      return data;
    }
    return null;
  }
}
