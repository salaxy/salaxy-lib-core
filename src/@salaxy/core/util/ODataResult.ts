/** Result from OData query */
export interface ODataResult<T> {

  /** Actual results array */
  items: T[];

  /**
   * Total count of the items (server-side/storage) if updated in the previous query.
   * Note that this may be null if the count is not fetched.
   */
  count?: number;

  /** Next page link */
  nextPageLink?: string;
}
