
/** Defines the query options of OData */
export interface ODataQueryOptions {

  /** Tells the server to include the total count of matching entities in the response. (Useful for server-side paging.)  */
  $count?: boolean;

  /** Expands related entities inline. At the moment, this typically not implemented. */
  $expand?: string;

  /** Filters the results, based on a Boolean condition. */
  $filter?: string;

  /** Sorts the results. */
  $orderby?: string;

  /** Selects which properties to include in the response. */
  $select?: string;

  /** Skips the first n results. */
  $skip?: number;

  /** Returns only the first n the results. */
  $top?: number;

  /**
   * Performs free-text search using the in-built logic in the API.
   * The logic will consider this as a direct end-user input:
   * Wildcard search string is added and the text value is encoded.
   */
  $search?: string;

  /**
   * Performs a free-text search using the Lucene full Syntax and searchmode All (not Any).
   * The search must be in final valid format: No escaping or adding of wildcards is done to this search string.
   * https://docs.microsoft.com/en-us/azure/search/query-lucene-syntax
   * NOTE: Any search here will ignore $search parameter.
   */
  fullSearch?: string;

  /**
   * If true, perfoms the query also in anonymous mode
   * (no token, token expired or invalid token).
   * Default is that the query is done only if user has a valid token.
   */
  anon?: boolean;
}
