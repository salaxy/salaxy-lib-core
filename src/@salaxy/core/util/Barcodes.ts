import { Dates } from "./Dates";

/**
 * Helpers for generating barcode values.
 * Using format Code 128 (Uniform Symbology Specification)
 * with Modulo 103 algorithm for generating checksum.
 *
 * @see http://www.finanssiala.fi/maksujenvalitys/dokumentit/Bank_bar_code_guide.pdf
 * @see http://www.finanssiala.fi/maksujenvalitys/dokumentit/Pankkiviivakoodi-opas.pdf
 */
export class Barcodes {

  /**
   * Creates a value for barcode labels. Does not calculate the checksum by default.
   * Most libraries, which generate barcode labels, add the checksum and start and stop sequences automatically,
   * and those are not needed to be calculated externally.
   *
   * @param iban - IBAN bank account number. Must be a Finnish IBAN. Can have whitespace.
   * @param ref - Reference number as a string. Either fully numeric (max len 20), or starting with RF (max len 2+23).
   * Can have whitespace.
   * @param dueDate - Date as ISO date (yyyy-MM-dd). False fills values with zeros.
   * @param euroAmount - Euro amount. If string, e.g. 3030,33€, is converted to cents amount 303033.
   * Alternative can be passed cents amount as number.
   * @param includeChecksum - Calculates and appends the checksum to the end of the barcode value.
   *
   * @returns The barcode value (without start and end characters). The checksum is only calculated and appended to the value if the includeChecksum-parameter is true.
   */
  public static getValue(iban: string, ref: string, dueDate: string | false, euroAmount: number | string, includeChecksum = false): string {

    // --- Sanitize params

    // Remove white space from IBAN and ref
    iban = (iban + "").replace(/\s/g, "");
    ref = (ref + "").replace(/\s/g, "");

    // Format date to YYMMDD
    if (dueDate) {
      dueDate = Dates.format(dueDate, "YYMMDD");
    }

    // Format amount
    const amountInCents = (typeof euroAmount === "string") ? euroAmount.replace(/[, ]+/g, "") : euroAmount.toString();

    // --- Build value string

    let bcVal = "";

    // #1 segment: 1 char for version number - if refNo starts with "RF" => 5, else => 4
    const versionNo = ref.substring(0, 2).toUpperCase() === "RF" ? "5" : "4";
    bcVal += versionNo;

    // #2 segment: 16 chars for IBAN numeric part
    bcVal += iban.length === 18 ? iban.substr(2) : "0".repeat(18);

    // #3 segment: 6 chars euro amount without cents
    let euros = amountInCents.slice(0, -2);
    while (euros.length < 6) {
      euros = "0" + euros;
    }
    bcVal += euros;

    // #4 segment: 2 chars cents amount
    const cents = amountInCents.slice(-2);
    bcVal += (cents.length === 1) ? "0" + cents : cents;

    // #5 segment, when version 4: 3 zeros followed by 20 char national ref no
    if (versionNo === "4") {
      bcVal += "000";
      let refNo = ref.substr(0); // Copy value
      while (refNo.length < 20) {
        refNo = "0" + refNo;
      }
      bcVal += refNo;

    // #5 segment, when version 5: 23 chars RF ref no's numeric part (RF04 9991 -> 04000000000000000009991)
    } else {
      bcVal += ref.substr(2, 2);
      let refNo = ref.substr(4);
      while (refNo.length < 21) {
        refNo = "0" + refNo;
      }
      bcVal += refNo;
    }

    // #6 segment: 6 chars due date in YYMMDD format. If false, fill with zeros.
    bcVal += (dueDate) ? dueDate : "000000";

    if (includeChecksum) {
      // #7 segment: 2 chars Modulo 103 checksum
      let modulo = this.modulo103(bcVal).toString();
      modulo = modulo.length === 1 ? "0" + modulo : modulo;
      bcVal += modulo;
    }

    return bcVal;
  }

  /**
   * Takes in 54 characters string value and calculates modulo 103 value.
   *
   * @param val - 54 characters long string of numbers
   *
   * @returns A Number within range of 0-99. Reminder of dividing the sum total of weighted value pairs with 103.
   */
  public static modulo103(val: string): number {
    const pairs = val.match(/(..?)/g); // https://stackoverflow.com/questions/17286535/split-a-string-to-array-containing-pairs-of-two
    let total = 105;
    for (let i = 1; i <= 27; i++) {
      const num = Number(pairs[i - 1]);
      total += num * i;
    }
    return total % 103;
  }
}
