import moment from "moment";

import { Translations } from "../logic/Translations";
import { DatelyObject } from "./DatelyObject";
import { Dates } from "./Dates";

/**
 * Date/Time parsing, formatting and other operations within the Salaxy framework.
 * We use ISO 8601 as our Date/Time format even inside JavaScript objects.
 * This is because of the following reasons:
 *
 * 1. Generally, we do not want to use JavaScript Date because it presents so many problems.
 * 2. However, MomentJS is scheduled to be factored out to anothe JavaScript library
 * 3. We store dates in JSON as ISO 8601 date strings - this is universally the best practice
 * 4. ISO 8601 provides a way to present a date without time and most importantly timezone information
 * 5. ISO 8601 date also works best for most UI components (because of similar reasons as above).
 *
 * IMPORTANT: Use Date/Time only where TIME and/or timezone is relevant.
 * Otherwise use Date (Dates library) that does not have time or timezone.
 *
 * Implementation uses MomentJS, but we should avoid using MomentJS outside this library:
 * It will be written out in a near future!
 */
export class DateTimes {

  /**
   * Formats a datetime ISO string (or other dately object)
   * as time with texts "Today", "Yesterday", "D.M. HH:mm" for this year
   * and "D.M.YYYY" for other years (if necessary, add option for adding time to previous years).
   *
   * @param time Dately object to format
   */
  public static format(time: DatelyObject): string {
    const moment = Dates.getMoment(time);
    if (!moment) {
      return "-";
    }
    if (Dates.getTodayMoment().isSame(moment, "d")) {
      return Translations.getWithDefault("SALAXY.UI_TERMS.timeToday", "Today") + " " + moment.format("HH:mm");
    }
    if (Dates.getTodayMoment().add(-1, "d").isSame(moment, "d")) {
      return Translations.getWithDefault("SALAXY.UI_TERMS.timeYesterday", "Yesterday") + " " + moment.format("HH:mm");
    }
    if (Dates.getTodayMoment().isSame(moment, "y")) {
      return moment.format("D.M. HH:mm");
    }
    return moment.format("D.M.YYYY");
  }

  /**
   * Formats a date/time range to an end-user friendly duration text.
   * @param start Start of the range as ISO string.
   * @param end End of the range as ISO string.
   * @param nullValue Value if start or end is missing or cannot be parsed.
   */
  public static formatRangeToDuration(start: string, end: string, nullValue = "-"): string {
    if (!start || !end) {
      return nullValue;
    }
    if (end < start) {
      return this.formatDurationFromMoment(Dates.getDuration(end, start), nullValue) + " ennen";
    }
    return this.formatDurationFromMoment(Dates.getDuration(start, end), nullValue);
  }

  /**
   * Formats a date/time range to an end-user friendly duration text.
   * @param duration Duration as ISO duration string (e.g. P3Y6M4DT12H30M5S) or
   * .Net timespan (e.g. '7.23:59:59.999').
   * @param nullValue Value if duration is missing or cannot be parsed.
   */
  public static formatDuration(duration: string, nullValue = "-"): string {
    if (!duration) {
      return nullValue;
    }
    let postfix = "";
    if (duration.startsWith("-")) {
      postfix = " ennen";
      duration = duration.substr(1);
    }
    return this.formatDurationFromMoment(moment.duration(duration), nullValue) + postfix;
  }

  private static formatDurationFromMoment(duration: moment.Duration, nullValue: string): string {
    if (!duration.isValid) {
      return nullValue;
    }
    let result = "";
    if (duration.years() !== 0)
    {
      result += `${duration.years()} vuotta `;
    }
    if (duration.months() !== 0)
    {
      result += `${duration.months()} kk `;
    }
    if (duration.days() !== 0)
    {
      result += `${duration.days()} pv `;
    }
    if (duration.hours() !== 0)
    {
      result += `${duration.hours()} h `;
    }
    if (duration.minutes() !== 0)
    {
      result += `${duration.minutes()} min `;
    }
    return result.trim();
  }

}
