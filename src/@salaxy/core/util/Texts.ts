/**
 * Helpers for formatting and encodin / decoding texts.
 */
export class Texts {
  /**
   * Escapes HTML
   * @param html HTML text to escape
   * @returns HTML escaped (&<>!'/)
   */
  public static escapeHtml(html: string): string {
    const entityMap = {
      "&": "&amp;",
      "<": "&lt;",
      ">": "&gt;",
      '"': '&quot;',
      "'": '&#39;',
      "/": '&#x2F;'
    };
    // TODO: The escape is probably useless, but needs testing
    // eslint-disable-next-line no-useless-escape
    return String(html).replace(/[&<>"'\/]/g, (s) => {
      return entityMap[s];
    });
  }

  /**
   * Replaces characters in strings that are illegal/unsafe for filenames.
   * Unsafe characters are either removed or replaced by a substitute set
   * in the optional `options` object.
   *
   * Illegal Characters on Various Operating Systems
   * / ? < > \ : * | "
   * https://kb.acronis.com/content/39790
   *
   * Unicode Control codes
   * C0 0x00-0x1f & C1 (0x80-0x9f)
   * http://en.wikipedia.org/wiki/C0_and_C1_control_codes
   *
   * Reserved filenames on Unix-based systems (".", "..")
   * Reserved filenames in Windows ("CON", "PRN", "AUX", "NUL", "COM1",
   * "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9",
   * "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", and
   * "LPT9") case-insesitively and with or without filename extensions.
   *
   * Capped at 255 characters in length.
   * http://unix.stackexchange.com/questions/32795/what-is-the-maximum-allowed-filename-and-folder-size-with-ecryptfs
   *
   * @param name File name to escape.
   */
  public static escapeFileName(name: string): string {
    // External code: We do not want to fix and retest it. At least the second line is probably justified. The 1. and 3. perhaps not.
    const illegal = /[\/\?<>\\:\*\|"]/g; // eslint-disable-line
    const control = /[\x00-\x1f\x80-\x9f]/g; // eslint-disable-line
    const reserved = /^\.+$/;
    const windowsReserved = /^(con|prn|aux|nul|com[0-9]|lpt[0-9])(\..*)?$/i;
    const windowsTrailing = /[\. ]+$/;  // eslint-disable-line
    const r = "_";
    if (!name) {
      return name;
    }
    return name
      .replace(illegal, r)
      .replace(control, r)
      .replace(reserved, r)
      .replace(windowsReserved, r)
      .replace(windowsTrailing, r);
  }
}
