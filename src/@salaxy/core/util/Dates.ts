import moment from "moment";

import { DateRange, HolidayDate, HolidayGroup, Holidays } from "../model";
import { Objects } from "./Objects";
import { DatelyObject } from "./DatelyObject";
import { yearlyHolidaysYears } from "../codegen";

/**
 * Date parsing, formatting and other operations within the Salaxy framework.
 * We use ISO 8601 as our Date format even inside JavaScript objects.
 * This is because of the following reasons:
 *
 * 1. Generally, we do not want to use JavaScript Date because it presents so many problems.
 * 2. However, MomentJS is scheduled to be factored out to anothe JavaScript library
 * 3. We store dates in JSON as ISO 8601 date strings - this is universally the best practice
 * 4. ISO 8601 provides a way to present a date without time and most importantly timezone information
 * 5. ISO 8601 date also works best for most UI components (because of similar reasons as above).
 *
 * This library is only for Dates. Time / DateTime functionality is in DateTimes util library.
 *
 * Implementation uses MomentJS, but we should avoid using MomentJS outside this library:
 * It will be written out in a near future!
 */
export class Dates {

  /**
   * Gets an object and returns it as the date format that is stored in the data structure with ISO 8601 format.
   *
   * @param datelyObject - Object that should be converted to Salaxy date: A string, JS Date object, Moment etc.
   *
   * @returns The date in simplest possible ISO 8601 compliant format "YYYY-MM-DD", null if empty, not valid date etc.
   */
  public static asDate(datelyObject: DatelyObject): string {
    const asMoment = this.asMoment(datelyObject);
    return asMoment ? asMoment.format("YYYY-MM-DD") : null;
  }

  /**
   * Gets a date based on year, month and day (1-based).
   * NOTE: Unlike Date() constructor or Moment() constructor, all numbers are 1-based:
   * (2017, 2, 2) results to "2017-02-02" and not "2017-01-01T22:00:00.000Z" like in those constructors.
   *
   * @param year Year component of the date or "today" for today's year.
   * @param month Month component of the date (1-12) or "today" for today's month.
   * @param day Day of month component of the date (1-31) or "today" for today's day of month.
   */
  public static getDate(year: number | "today", month: number | "today", day: number | "today"): string {
    year = (year === "today") ? Dates.getTodayMoment().year() : year;
    month = (month === "today") ? Dates.getTodayMoment().month() : (month - 1);
    day = (day === "today") ? Dates.getTodayMoment().date() : day;
    if (!day || month == null || month < 0 || month > 11 || !year) {
      return null;
    }
    return this.asDate([year, month, day]);
  }

  /**
   * Converts the date to a JavaScript Date. Please note, that also time part is returned.
   * @param  datelyObject - Object that should be converted: A string, JS Date object, Moment etc.
   *
   * @returns JavaScript date corresponding to input or null.
   */
  public static asJSDate(datelyObject: DatelyObject): Date {
    const asMoment = this.asMoment(datelyObject);
    return asMoment ? asMoment.toDate() : null;
  }

  /**
   * Checks whether a string is in ISO format.
   *
   * @param dateCancidate Value that is evaluated.
   * @param required If set to true, the value is required. If required is null the false will be returned.
   * @param format Date or datetime format. Default is ISO_8601 (ISO datetime).
   *
   * @returns True if the given date is in the correct format.
   */
  public static isValidDateTime(dateCancidate: string, required = false, format = null): boolean {
    if (!dateCancidate) {
      return !required;
    }
    return moment(dateCancidate, format || moment.ISO_8601, true).isValid();
  }

  /**
   * Parses a date. In ISO format by default.
   *
   * @param dateCancidate Value that is parsed.
   * @param format Date or datetime format. Default is ISO_8601 (ISO datetime).
   * Note that result is just a date (not datetime).
   *
   * @returns True if the given date is in the correct format.
   */
  public static parseDate(dateCancidate: string, format = null): string {
    if (!dateCancidate) {
      return null;
    } else {
      return Dates.asDate(moment(dateCancidate, format || moment.ISO_8601, true));
    }
  }

  /** Get the date of today as ISO 8601 string format (YYYY-MM-DD)  */
  public static getToday(): string {
    return this.asDate(this.getTodayMoment());
  }

  /** Get the date of today as moment object. */
  public static getTodayMoment(): moment.Moment {
    return moment(0, "HH");
  }

  /**
   * Gets the dately object as a Moment. If empty, not valid etc., returns null.
   * If the object is already a returns the given objectg (no clone).
   *
   * @param datelyObject Object that should be converted to Moment: A string, JS Date object, Moment, array of numbers etc.
   * @param allowInvalid If true, returns also invalid moment object. By default returns null, if the dately object is parsed as invalid moment.
   *
   * @returns A Moment object or null.
   */
  public static asMoment(datelyObject: DatelyObject, allowInvalid = false): moment.Moment {
    if (!datelyObject) {
      return null;
    }
    if (moment.isMoment(datelyObject)) {
      if (datelyObject.isValid() || allowInvalid) {
        return datelyObject;
      }
      return null;
    }
    return this.getMoment(datelyObject, allowInvalid);
  }

  /**
   * Gets the dately object as a Moment. If empty, not valid etc., returns null.
   * If the object is already a Moment, clones it.
   *
   * @param datelyObject Object that should be converted to Moment: A string, JS Date object, Moment, array of numbers etc.
   * @param allowInvalid If true, returns also invalid moment object. By default returns null, if the dately object is parsed as invalid moment.
   *
   * @returns A Moment object or null.
   */
  public static getMoment(datelyObject: DatelyObject, allowInvalid = false): moment.Moment {
    if (!datelyObject) {
      return null;
    }

    let asMoment: moment.Moment;
    if (moment.isMoment(datelyObject)) {
      asMoment = datelyObject.clone();
    } else {
        if (moment.isDate(datelyObject) || Number.isInteger(datelyObject as number) || Array.isArray(datelyObject)) {
          asMoment = moment(datelyObject);
        } else if (datelyObject === "today") {
          asMoment = this.getTodayMoment();
        } else if (datelyObject === "yesterday") {
          asMoment = this.getTodayMoment().add(-1, "day");
        } else if (datelyObject === "tomorrow") {
          asMoment = this.getTodayMoment().add(1, "day");
        } else {
          asMoment = moment(datelyObject, moment.ISO_8601);
        }
    }
    if (asMoment.isValid() || allowInvalid) {
      return asMoment;
    }
    return null;
  }

  /**
   * Returns the duration  object. If parameter is empty or null, defaults to Today (not Now).
   *
   * @param startDatelyObject Duration start that is parsed using getMoment().
   * Null before conversion defaults to today. Invalid objects result to invalid duration.
   * @param endDatelyObject Duration start that is parsed using getMoment().
   * Null before conversion defaults to today. Invalid objects result to invalid duration.
   *
   * @returns A Duration object, which may also be invalid
   */
  public static getDuration(startDatelyObject: DatelyObject, endDatelyObject: DatelyObject): moment.Duration {
    startDatelyObject = this.asMoment(startDatelyObject, true) || this.getTodayMoment();
    endDatelyObject = this.asMoment(endDatelyObject, true) || this.getTodayMoment();
    return moment.duration(endDatelyObject.diff(startDatelyObject));
  }

  /**
   * Gets a date range based on start and end dates.
   * Days count is calculated using getWorkdays() (Add parameter if you need other day count methods).
   *
   * @param start DateRange start that should be converted to Salaxy date: A string, JS Date object, Moment etc.
   * @param end DateRange end that should be converted to Salaxy date: A string, JS Date object, Moment etc.
   *
   * @returns DateRange based on start and end dates.
   */
  public static getDateRange(start: DatelyObject, end: DatelyObject): DateRange {
    const result: DateRange = {
      start: this.asDate(start),
      end: this.asDate(end),
    };
    result.daysCount = this.getWorkdays(result.start, result.end).length;
    return result;
  }

  /**
   * Gets the workdays between two dates including both days.
   * Returns empty array if there is no start or if end is before start.
   * Returns 1 day if there is no end: we assume that the user selected just one day.
   *
   * @param startDate A dately value as the start date (a string, JS Date object, Moment etc.).
   * @param endDate Corresponding end date.
   * @param holidayGroup The holiday group for defining which days not to count as working day. The default group is Holiday.
   *
   * @returns The work days between the two dates (inclusive).
   */
  public static getWorkdays(startDate: DatelyObject, endDate: DatelyObject, holidayGroup: HolidayGroup = HolidayGroup.Holiday): string[] {
    startDate = this.asDate(startDate);
    endDate = this.asDate(endDate) || startDate;
    if (!startDate) {
      return [];
    }
    const result: string[] = [];
    while (startDate <= endDate) {
      if (this.isWorkday(startDate, holidayGroup)) {
        result.push(startDate);
      }
      startDate = this.asDate(this.getMoment(startDate).add(1, "day"));
    }
    return result;
  }

  /**
   * Gets the vacation days (6 day week Mon-Sat) between two dates including both days.
   * Returns empty array if there is no start or if end is before start.
   * Returns 1 day if there is no end: we assume that the user selected just one day.
   *
   * @param startDate A dately value as the start date (a string, JS Date object, Moment etc.).
   * @param endDate Corresponding end date.
   * @param holidayGroup The holiday group for defining which days not to count as working day. The default group is Holiday.
   *
   * @returns The work days between the two dates (inclusive).
   */
  public static getVacationDays(startDate: DatelyObject, endDate: DatelyObject, holidayGroup: HolidayGroup = HolidayGroup.Holiday): string[] {
    startDate = this.asDate(startDate);
    endDate = this.asDate(endDate) || startDate;
    if (!startDate) {
      return [];
    }
    const result: string[] = [];
    while (startDate <= endDate) {
      if (this.isVacationday(startDate, holidayGroup)) {
        result.push(startDate);
      }
      startDate = this.asDate(this.getMoment(startDate).add(1, "day"));
    }
    return result;
  }

  /**
   * Gets a formatted range of two dates.
   *
   * @param startDate A dately value as the start date (a string, JS Date object, Moment etc.).
   * @param endDate Corresponding end date.
   *
   * @returns A date range string with possible formats:
   * "12. - 25.11.2019", "12.11. - 02.12.2019", "12.11.2019 - 12.01.2020", "12.01.2020 -" or "- 12.01.2020".
   */
  public static getFormattedRange(startDate: DatelyObject, endDate: DatelyObject): string {
    const formatFull = "DD.MM.YYYY";
    const start = this.asMoment(startDate);
    const end = this.asMoment(endDate);
    if (!start && !end) {
      return "-";
    }
    if (!start && end) {
      return " - " + end.format(formatFull);
    }
    if (start && !end) {
      return start.format(formatFull) + " - ";
    }
    if (start.isSame(end)) {
      return start.format(formatFull);
    }
    const formatMonthDay = "DD.MM.";
    const formatDay = "DD.";
    const sameYear = start.year() === end.year();
    const sameMonth = start.month() === end.month();
    return start.format(sameYear ? (sameMonth ? formatDay : formatMonthDay) : formatFull) + " - " + end.format(formatFull);
  }

  /**
   * Output a formatted date with default date format (may later be localized).
   * @param date A dately value (a string, JS Date object, Moment etc.) that is formatted.
   * @returns A Simple date string e.g. "12.11.2019", if the value is empty or null returns "-".
   */
  public static getFormattedDate(date: DatelyObject): string {
    return this.format(date, "DD.MM.YYYY", "-");
  }

  /**
   * Formats a dately object using MomentJs formatting:
   * Default is "DD.MM.YYYY"
   * For all format string options, see https://momentjs.com/docs/#/displaying/format/
   */
  public static format(date: DatelyObject, format = "DD.MM.YYYY", nullValue: string = null): string {
    const asMoment = this.asMoment(date);
    return asMoment ? asMoment.format(format) : nullValue;
  }

  /**
   * Gets the month as 1-based number: 1-12.
   *
   * @param datelyObject - Object that represents month: A string, JS Date object, Moment etc.
   * Special string "today" can be used for fetching today's date.
   *
   * @returns Month between 1-12 or null if the object is not a dately object.
   */
  public static getMonth(datelyObject: DatelyObject): number {
    const asMoment = this.asMoment(datelyObject);
    return asMoment ? asMoment.month() + 1 : null;
  }

  /**
   * Gets the year of a date or today.
   *
   * @param datelyObject - Object that represents month: A string, JS Date object, Moment etc.
   * Special string "today" can be used for fetching today's date.
   *
   * @returns Year, e.g. 2019 or null if the object is not a dately object.
   */
  public static getYear(datelyObject: DatelyObject): number {
    const asMoment = this.asMoment(datelyObject);
    return asMoment ? asMoment.year() : null;
  }

  /**
   * Returns true if the given dately object is a holiday.
   * Please note that the weekend days are not by default holidays
   *
   * @param datelyObject - Object that should be converted to date: A string, JS Date object, Moment etc.
   * @param holidayGroup - The holiday group for defining which days to count as holiday day. The default group is Holiday.
   *
   * @returns True, if the given dately object is a holiday.
   */
  public static isHoliday(datelyObject: DatelyObject,  holidayGroup: HolidayGroup = HolidayGroup.Holiday ): boolean {
      const moment = this.asMoment(datelyObject);
      if (!moment) {
        return false;
      }
      if (!holidayGroup) {
        return false;
      }
      const holidays = this.getYearlyHolidays(moment);
      for (const holiday in holidays) {
        if (Objects.has(holidays, holiday)) {
          const holidayDate = holidays[holiday] as HolidayDate;
          if (this.asDate(holidayDate.date) === this.asDate(moment) &&
            holidayDate.holidayGroups.find((x) => x === holidayGroup)) {
            return true;
          }
        }
      }
      return false;
  }

  /**
   * Returns true if the given dately object is a working day (5 day week).
   * The day must not be Saturday or Sunday or not any day in the given holiday categoria.
   *
   * @param datelyObject - Object that should be converted to date: A string, JS Date object, Moment etc.
   * @param holidayGroup - The holiday group for defining which days to count as holiday day. The default group is Holiday.
   *
   * @returns True, if the given dately object is a working day.
   */
  public static isWorkday(datelyObject: DatelyObject, holidayGroup: HolidayGroup = HolidayGroup.Holiday): boolean {

    const moment = this.asMoment(datelyObject);
    if (!moment) {
      return false;
    }

    if (moment.isoWeekday() === 6 || // Saturday
        moment.isoWeekday() === 7) { // Sunday
      return false;
    }

    if (!holidayGroup) {
      return true;
    }

    return !this.isHoliday(moment, holidayGroup);
  }

  /**
   * Returns true if the given dately object is a vacation day (6 day week).
   * The day must not be Sunday or not any day in the given holiday categoria.
   *
   * @param datelyObject - Object that should be converted to date: A string, JS Date object, Moment etc.
   * @param holidayGroup - The holiday group for defining which days to count as holiday day. The default group is Holiday.
   *
   * @returns True, if the given dately object is a working day.
   */
  public static isVacationday(datelyObject: DatelyObject, holidayGroup: HolidayGroup = HolidayGroup.Holiday): boolean {

    const moment = this.asMoment(datelyObject);
    if (!moment) {
      return false;
    }

    if ( moment.isoWeekday() === 7) { // Sunday
      return false;
    }

    if (!holidayGroup) {
      return true;
    }

    return !this.isHoliday(moment, holidayGroup);
  }

/**
 * Adds given amount of working days to given day.
 *
 * @param datelyObject - Object that should be converted to date: A string, JS Date object, Moment etc.
 * @param days - Working days to add (positive or negative number).
 * If zero or not set, returns datelyObject or next workday if datelyObject is not a workday.
 * @param holidayGroup - The holiday group for defining which days to count as holiday day. The default group is Holiday.
 *
 * @returns New working day with amount of given working days ahead.
 */
  public static addWorkdays(datelyObject: DatelyObject, days: number, holidayGroup: HolidayGroup = HolidayGroup.Holiday): string {
    const newMoment = this.getMoment(datelyObject);
    if (!newMoment) {
      return null;
    }
    if (!days) {
      // eslint-disable-next-line no-constant-condition
      while (true) {
        if (this.isWorkday(newMoment, holidayGroup)) {
          return this.asDate(newMoment);
        }
        newMoment.add(1, "days");
      }
    }
    let count = 0;
    const step = days > 0 ? 1 : -1;
    // eslint-disable-next-line no-constant-condition
    while (true) {
      newMoment.add(step, "days");
      if (this.isWorkday(newMoment, holidayGroup)) {
        count += step;
        if (count === days) {
          return this.asDate(newMoment);
        }
      }
    }
  }


  /**
   * Parses the given date range string to a DateRange. Parsing is implemented for optimized date
   * range inputs. Optimized date range means that dates can be given in a shorter/truncated form,
   * for improved user efficiency, by omitting some components from the date strings:
   *
   * - For the end date the day is the only mandatory component, if month and/or year are omitted
   * they default to current month and year respectively
   * - For the start date all components are optional, any omitted components default to the
   * respective components from the end date.
   *
   * For example the result of parsing a user inputted optimized date range string "1 - 20" would be
   * { start: 2021-04-01, end: 2021-04-20, }.
   *
   * @param dateRange The optimized date range string to be parsed. The dates in the date range
   * should be in finnish format (dd.mm.yyyy or without leading zeros d.m.yyyy). Start and end dates
   * should be separated by a hyphen (-). Examples of valid date range strings could be e.g.
   * "1.4 - 1.5.2021" or "1" which would be parsed as { start: 2021-04-01, end: 2021-05-01 } and
   * { start: 2021-04-01, end: 2021-04-01 } respectively (at the time of writing this documentation).
   * @returns The parsed DateRange.
   */
  public static parseOptimizedDateRange(dateRange: string): DateRange {
    this.validateDaterangeInput(dateRange);
    return this.validateDaterange({
      start: this.parseStartDateFrom(dateRange),
      end: this.parseEndDateFrom(dateRange),
    });
  }

  /**
   * Parses a start date from an optimized date range string. Uses the end date as the default if
   * start date or some of its components are not given.
   */
  private static parseStartDateFrom(dateRange: string): string {
    if (dateRange.indexOf('-') < 0) {
      return this.parseEndDateFrom(dateRange);
    }
    const fromDate = dateRange.substring(0, dateRange.indexOf('-')).trim();
    return this.parseOptimizedDate(fromDate, this.parseEndDateFrom(dateRange));
  }

  /**
   * Parses an end date from an optimized date range string. Uses the current date as the default if
   * month and/or year are not given.
   */
  private static parseEndDateFrom(dateRange: string): string {
    const toDate = dateRange.substring(dateRange.indexOf('-') + 1).trim();
    return this.parseOptimizedDate(toDate);
  }

  /**
   * Parses an optimized date string. The date string should be in finnish format (dd.mm.yyyy with
   * or without leading zeros). Month and/or year component can be omitted from the date string
   * (optimized date string...) in which case they are taken from the default date. If default date
   * is not given the current date is used
   */
  private static parseOptimizedDate(date: string, defaultDate: string = this.getToday()): string {
    const dateComponents = date.split('.');
    dateComponents[0] = this.parseDay(dateComponents[0]);
    dateComponents[1] = this.parseMonth(dateComponents[1], defaultDate);
    dateComponents[2] = this.parseYear(dateComponents[2], defaultDate);
    return this.validateDate(dateComponents.reverse().join('-'));
  }

  private static parseDay(day: string): string {
    if (!day || day.length > 2) {
      return '';
    }
    return day.length === 1 ? '0' + day : day;
  }

  /**
   * Parses an optimized month string. If the month is not given it is taken from the default date.
   */
  private static parseMonth(month: string, defaultDate: string): string {
    if (month?.length > 2) {
      return '';
    }
    if (!month) {
      month = this.getMonth(defaultDate).toString();
    }
    return month.length === 1 ? '0' + month : month;
  }

  /**
   * Parses an optimized year string. Defaults to the 3rd millenium, if the given year doesn't
   * contain the millenia e.g. "21" --> "2021". If the year is not given at all, it is taken from
   * the default date.
   */
  private static parseYear(year: string, defaultDate: string): string {
    if (year?.length > 4) {
      return '';
    }
    if (!year) {
      return this.getYear(defaultDate).toString();
    }
    return (parseInt(year)%2000 + 2000).toString();
  }

  /**
   * Validates that the given date range string only contains allowed character (numbers, periods,
   * dashes, spaces).
   */
  private static validateDaterangeInput(dateRange: string): string {
    if (dateRange.match(/^[0-9 -.]*$/) === null) {
      throw new Error('Invalid date range! The given date range contains invalid characters.');
    }
    return dateRange;
  }

  /**
   * Validates that a date is real valid date (e.g. not 31.2.2021) by trying to convert it to ISO
   * string. The conversion will throw an exception if the given date is out or range.
   */
  private static validateDate(date: string): string {
    this.asMoment(date).toISOString();
    return date;
  }

  /**
   * Validates a date range by checking that the start date is not after the end date.
   */
  private static validateDaterange(dateRange: DateRange): DateRange {
    if (this.asMoment(dateRange.start).isAfter(dateRange.end)) {
      throw new Error('Invalid date range! Start date needs to be before the end date.');
    }
    return dateRange;
  }

  /**
   * Gets holidays for the given year.
   *
   * @param forYear - Dately object (JavaScript date, ISO string formatted date, or moment) for which the holidays are fetched.
   *
   * @returns The holidays for the given year.
   */
   public static getYearlyHolidays(forYear: DatelyObject): Holidays {
    const year = Dates.getYear(forYear);
    const yearlyHolidays = yearlyHolidaysYears.find((x) => x.year === year);
    if (!yearlyHolidays) {
      throw new Error(`Year ${year} not supported.`);
    }
    return yearlyHolidays.holidays;
  }

}
