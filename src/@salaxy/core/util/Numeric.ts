/**
 * Helpers for handling numbers: Mathematical operations, parsing and formatting numbers in our supported languages etc.
 */
export class Numeric {

  /**
   * Parses a user input to a float taking into consideration the Finnish culture.
   * Main case is that decimal separator is usually a comma (,).
   * If there is both comma (,) and point (.) in the same string, NaN is returned.
   * Whitespaces are removed and if string contains commas they are replaced with points before conversion to Number.
   * Null, undefined return original value, empty strings (or whitespace only) return null.
   *
   * @param numCandidate - text that should be converted to a number if possible.
   */
  public static parseNumber(numCandidate: string | number): number {
    if (numCandidate == null || typeof (numCandidate) === "number") {
      return numCandidate as number;
    }
    if (typeof (numCandidate) === "string") {
      if (numCandidate.indexOf(",") >= 0 && numCandidate.indexOf(".") >= 0) {
        return Number.NaN;
      }
      numCandidate = numCandidate.replace(/\s/, "");
      if (numCandidate === "") {
        return null;
      }
      return Number(numCandidate.replace(",", "."));
    }
    return Number(numCandidate);
  }

  /**
   * Formats a number as a euro price. Uses comma as decimal separator.
   * @param value - Numeric value to format
   * @param addSuffix - If true (default), adds the €-suffix.
   * @param nullChar The character or string for null or Nan value, the default is '-'.
   */
  public static formatPrice(value: any,  addSuffix = true, nullChar = "-" ): string {
    // TODO: Add localization for English language (dot instead of comma as decimal separator).
    // TODO: Add thousand separator?

    return Numeric.formatNumber(value, 2, (addSuffix ? "€" : ""), nullChar );
  }

  /**
   * Rounds a JavaScript number in a reliable way.
   * @param value Number that is rounded. If null given, returns null.
   * @param decimals Number of decimals after the decimal separator. Default is 2 as in money.
   * @returns Number properly rounded, Null/undefined as null, Non-numeric values return Number.NaN
   */
  public static round(value: number, decimals = 2): number {
    if (value == null) { // '==' handles both null and undefined values, do not use here '==='
      return value;
    }
    if (value < 0) {
      // Round away from zero
      return -1 * Numeric.round(-1 * value, decimals);
    }
    let sValue = `${value}`;
    if (sValue.indexOf("e") >= 0) {
      sValue = `${value.toFixed(15)}`;
    }
    return Number(Math.round(`${sValue}e${decimals}` as any).toString() + `e-${decimals}`);
  }

  /**
   * Fixes floating point problems in typical number.toString() scenarios.
   * E.g. `console.debug((0.07 * 100).toString());` returns "7.000000000000001".
   * Null/undefined returns empty string, numbers fixed and otherwise toString() is called.
   * @param value Number to show as string.
   * @returns Number rounded to specified number of decimals. Null/undefined returns empty string.
   * @example
   * console.debug(Numeric.toString(0.07 * 100)); // returns "7"
   * console.debug((0.07 * 100).toString());      // returns "7.000000000000001"
   */
  public static toString(value: number): string {
    if (value == null) {
      return "";
    }
    return value.toFixed ? parseFloat(value.toFixed(15)).toString() : value.toString() ;
  }

  /**
   * Fixes floating point problems in typical number.toFixed() scenarios.
   * E.g. `console.debug((1.005).toFixed(2));` returns "1.00"
   * @param value Number to show as string. Supports null/undefined, but otherwise should be a number.
   * @param fractionDigits Number of digits after the decimal point. Must be in the range 0 - 20, inclusive.
   * @returns Number rounded to specified number of decimals. Null/undefined returns empty string.
   * @example
   * console.debug(Numeric.toFixed(1.005, 2)) // returns "1.01"
   * console.debug((1.005).toFixed(2))        // returns 1.00
   */
  public static toFixed(value: number, fractionDigits = 0): string {
    value = Numeric.round(value, fractionDigits);
    return value == null ? "" : value.toFixed(fractionDigits);
  }

  /**
   * Rounds a JavaScript number in a reliable way in percentage format.
   *
   * @param value Number that is rounded.
   * @param decimals Number of decimals after the decimal separator. Default is 2 as in money.
   * If decimals is null, no rounding is done.
   * @param addSuffix If true, adds '%' sign at the end.
   * @param nullChar The character or string for null or Nan value, the default is '-'.
   */
  public static formatPercent(value: any, decimals = 2, addSuffix = true, nullChar = "-"): string {
    return Numeric.formatNumber(value, decimals, (addSuffix ? "%" : ""), nullChar  );
  }

  /**
   * Rounds a JavaScript number and formats it using comma as a separator and optional suffix.
   *
   * @param value Number that is rounded.
   * @param decimals Number of decimals after the decimal separator. Default is 2 as in money. If decimals is null, no rounding is done.
   * @param suffix The suffix to add to the number.
   * @param nullChar The character or string for null or Nan value, the default is '-'.
   */
  public static formatNumber(value: any, decimals = 2, suffix = "",  nullChar = "-"): string {
    const asFloat = parseFloat(value);
    if (isNaN(asFloat)) { return nullChar; }
    const asFixed = Numeric.toFixed(asFloat, decimals);
    return asFixed.replace(/\./, ",") + suffix;
  }

  /**
   * Checks if the given object is a number.
   * @param numberCandidate Object to check
   */
  public static isNumber(numberCandidate: any): boolean {
    return !isNaN(parseFloat(numberCandidate)) && isFinite(numberCandidate);
  }

}
