import { OpenAPIV3 } from "openapi-types";

/** Possible input types bases on JSON Schema generation. */
export type JsonInputType = OpenAPIV3.NonArraySchemaObjectType | OpenAPIV3.ArraySchemaObjectType | "error"