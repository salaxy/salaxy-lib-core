import { JsonInputType } from "./JsonInputType";

/**
 * Contains metadata about the default input that is determined
 * based on reflection model.
 */
export interface InputMetadata {

  /**
   * Property name if input is referenced as property, otherwise null.
   * Used in model path and sorting.
   */
  name: string,

  /** The JSON schema data type */
  type: JsonInputType,

  /**
   * The JSON schema data format: Subtype of type (e.g. string => multiline, number => float).
   * For type="object" and type="string"/enum=[] this is the Salaxy type/schema name of the object.
   */
  format: string,

  /** If true, the input is enumeration object. */
  isEnum: boolean;

  /** In data binding scenarios, the path from the current object. */
  path: string,

  /** Error message if the resolving fails: May be some other content in the future. */
  content?: string,
}
