import { IJsonSchema, OpenAPIV2, OpenAPIV3 } from "openapi-types";

import { Ajax } from "../../api/infra";
import { JsonSchemaCacheItem } from "./JsonSchemaCacheItem";
import { JsonSchemaUtils } from "./JsonSchemaUtils";

/**
 * Implemenatation of cache of resolved schemas and methods on reading their structure.
 */
export class JsonSchemaCache {

  /** Cache for loaded schema elements. */
  public schemaCache: JsonSchemaCacheItem[] = [];

  constructor(private ajax: Ajax) {
  }

  /**
   * Adds a local schema document and derefernces it.
   * @param url URL that identifies the schema
   * @param doc The schema document as object.
   * @returns A promise of the object.
   */
  public addSchemaDocument(url: string, doc: OpenAPIV3.Document | OpenAPIV2.Document | IJsonSchema): OpenAPIV3.Document {
    url = url.toLowerCase().trim();
    this.schemaCache = this.schemaCache.filter((x) => x.url !== url);
    if (JsonSchemaUtils.isOpenAPIV2(doc)) {
      const v3doc: OpenAPIV3.Document = {
        "openapi": "3.0.1",
        "info": doc.info,
        paths: doc.paths,
        components: {
          // HACK: There are differneces in two schema objects. Go through what they are exactly and migrate/mitigate if possible.
          schemas: doc.definitions as any,
        }
      };
      doc = v3doc;
    }
    if (JsonSchemaUtils.isSchemaDocument(doc)) {
      const v3doc: OpenAPIV3.Document = {
        openapi: "3.0.1",
        info: {
          title: doc.title,
          version: "1.0",
          description: doc.description,
        },
        paths: {},
        components: {
          // TODO: There may be differneces in two schema objects (depending on the version). Go through what they are exactly and migrate/mitigate if possible.
          schemas: doc.definitions as any,
        }
      };
      doc = v3doc;
    }
    if (JsonSchemaUtils.isOpenAPIV3(doc)) {
      this.prepareSchema(doc);
      // Add to cache
      this.schemaCache.push({
        url: url.toLowerCase(),
        doc,
        type: "OpenAPIV3",
      });
      return doc;
    }
    throw new Error("Unsupported JSON schema / open api: " + url);
  }

  /**
   * Prepares the scehma for our use:
   * Dereferences the internal references ($ref-attribute) as JavaScript references.
   * Also adds the schema names to the format attribute of schema.
   * External references are currently not handled.
   */
  public prepareSchema(doc: OpenAPIV3.Document): void {
    Object.keys(doc.components.schemas).forEach((schemaName) => {
      const schema = doc.components.schemas[schemaName];
      // TODO: Consider adding as a new property instead of recycling format attribute.
      (schema as OpenAPIV3.SchemaObject).format = schemaName;
      if (JsonSchemaUtils.isNonArraySchemaObject(schema)) {
        // JsonSchemaCache.sortSchemaProperties(schema);
        if (schema.type === "object") {
          if (schema.properties) {
            // First resolve references as JavaScript references
            Object.keys(schema.properties).forEach((propName) => {
              const prop = schema.properties[propName];
              let ref = (prop as OpenAPIV3.ReferenceObject).$ref;
              if (!ref && (prop as OpenAPIV3.NonArraySchemaObject).oneOf) {
                // HACK: Just a quick hack => Go through in more detail and make more generic (oneOf, allOf etc.)
                // Also add support for JsonSchema style oneOf["string", "null"] for nullable items.
                ref = ((prop as OpenAPIV3.NonArraySchemaObject).oneOf as OpenAPIV3.ReferenceObject[])[0].$ref;
              }
              if (ref) {
                if (ref.startsWith("#/components/schemas/")) {
                  schema.properties[propName] = doc.components.schemas[ref.replace("#/components/schemas/", "")];
                } else if (ref.startsWith("#/definitions/")) {
                  schema.properties[propName] = doc.components.schemas[ref.replace("#/definitions/", "")];
                } else {
                  console.error(`Unhandled $ref: ${ref}`);
                }
              } else if (JsonSchemaUtils.isArraySchemaObject(prop)) {
                const arrayRef = (prop.items as OpenAPIV3.ReferenceObject).$ref;
                if (arrayRef) {
                  if (arrayRef.startsWith("#/components/schemas/")) {
                    prop.items = doc.components.schemas[arrayRef.replace("#/components/schemas/", "")];
                  } else if (arrayRef.startsWith("#/definitions/")) {
                    prop.items = doc.components.schemas[arrayRef.replace("#/definitions/", "")];
                  } else {
                    console.error(`Unhandled array items $ref: ${ref}`);
                  }
                }
              }
            });
            // Handle differences in Swagger/OpenApi 2, OpenApi 3 and Json Schema
            Object.keys(schema.properties).forEach((propName) => {
              const prop = schema.properties[propName];
              if ((prop as any).readonly != null) {
                (prop as OpenAPIV3.NonArraySchemaObject | OpenAPIV3.ArraySchemaObject).readOnly = (prop as any).readonly;
              } else if ((prop as any)["x-readonly"] != null) {
                (prop as OpenAPIV3.NonArraySchemaObject | OpenAPIV3.ArraySchemaObject).readOnly = (prop as any)["x-readonly"];
              }
              // TODO: Consider other cases, at least nullability.
              // See logic in NJsonSchema: https://github.com/RicoSuter/NJsonSchema/blob/master/src/NJsonSchema/JsonSchema.Serialization.cs
            });
          }
        }
      }
    });
  }

  /**
   * Sorts the properties in JSON schema according to some misc. logic rules
   * (e.g. id first, objects and arrays lower and unknown last).
   * TODO: We could potentially put this in another place: Server-side? UI framework?
   */
  public static sortSchemaProperties(schema: OpenAPIV3.SchemaObject): void {
    if (schema.properties) {
      const orderedArray = Object.keys(schema.properties).map((key) => {
        const prop = schema.properties[key] as OpenAPIV3.SchemaObject;
        const orderResult = {
          key,
          prop,
          sort: 50,
        };
        switch (prop.type) {
          case "number":
          case "integer":
          case "boolean":
            break;
          case "string":
            if (key.endsWith("At")) {
              orderResult.sort = 20;
            } else if (key === "id") {
              orderResult.sort = 10;
            }
            break;
          case "array":
            orderResult.sort = 70;
            break;
          case "object":
            {
              switch (prop.format) {
                case "DateRange":
                case "Avatar":
                  orderResult.sort = 50;
                  break;
                default:
                  orderResult.sort = 60;
                  break;
              }
            }
            break;
          default:
            orderResult.sort = 80;
            break;
        }
        return orderResult;
      });
      const ordered = {};
      orderedArray.sort((a, b) => (a.sort.toString() + a.key).localeCompare(b.sort.toString() + b.key))
        .forEach((x) => {
          ordered[x.key] = x.prop;
        });
      schema.properties = ordered;
    }
  }

  /**
   * Assures that a schema document identified by URL is loaded from server to the cache.
   * Currently, supports only OpenApi 3, but we may later support OpenApi 2 and/or plain JSON schema.
   * @param openApiUrl URL to the open API document.
   * @returns A promise that resolves when the document has been loaded and you can call other methods on it.
   */
  public assureSchemaDocument(openApiUrl: string): Promise<OpenAPIV3.Document> {
    const cachedSchema = this.schemaCache.find((x) => x.url === openApiUrl);
    if (cachedSchema) {
      if (cachedSchema.loadingDonePromise != null) {
        return cachedSchema.loadingDonePromise;
      }
      return Promise.resolve(cachedSchema.doc);
    }
    const loadingDonePromise = new Promise<OpenAPIV3.Document>((resolve, reject) => {
      this.ajax.getJSON(openApiUrl).then((data: OpenAPIV2.Document | OpenAPIV3.Document) => {
        if (!data) {
          throw new Error("No data from the schema: " + openApiUrl);
        }
        resolve(this.addSchemaDocument(openApiUrl, data));
      }).catch((reason) => {
        reject(reason);
      });
    });
    this.schemaCache.push({
      url: openApiUrl,
      type: "OpenAPIV3",
      doc: null,
      loadingDonePromise,
    });
    return loadingDonePromise;
  }

  /**
   * Finds a schema document from the cache.
   * This method is syncronous base method for other schema operations
   * Make sure that the schme document has been loaded before calling as this will not load the document,
   * but will fail instead if the schema document is not there.
   * @param openApiUrl URL to the open API document.
   * @param throwIfNotFound If true, will throw an error if the document is not found in the cache.
   */
  public findSchemaDoc(openApiUrl: string, throwIfNotFound: boolean): JsonSchemaCacheItem {
    openApiUrl = openApiUrl.toLowerCase().trim();
    if (!openApiUrl) {
      throw new Error("Empty Open API URL.");
    }
    const result = this.schemaCache.find((x) => x.url === openApiUrl);
    if (!result && throwIfNotFound) {
      throw new Error("Schema document not loaded: " + openApiUrl);
    }
    return result;
  }

  /**
   * Finds a single schema (Data model) within a schema document
   * Make sure that the schme document has been loaded before calling as this will not load the document,
   * but will fail instead if the schema document is not there.
   * @param openApiUrl URL to the open API document.
   * @param name Name of the schema (data model)
   */
  public findSchema(openApiUrl: string, name: string): OpenAPIV3.SchemaObject | null {
    const schema = this.findSchemaDoc(openApiUrl, true)?.doc.components?.schemas[name];
    if (!schema) {
      return null;
    }
    if (JsonSchemaUtils.isReferenceObject(schema)) {
      throw new Error(`Unresolved reference in ${openApiUrl} - ${name}`);
    }
    return schema;
  }
}
