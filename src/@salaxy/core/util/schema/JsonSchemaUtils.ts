import { IJsonSchema, OpenAPIV2, OpenAPIV3 } from "openapi-types";
import { JsonSchemaProperty } from "./JsonSchemaProperty";

/** Static utilities for JSON Schemas an Open API documents. */
export class JsonSchemaUtils {

  /**
   * Gets a property from a schema document. Supports single property names or longer property paths.
   * @param schema The root type from which the property is found.
   * @param path Property path starting with the root schema.
   */
  public static getProperty(schema: OpenAPIV3.SchemaObject, path: string): JsonSchemaProperty {
    if (!schema || !path?.trim()) {
      return null;
    }
    const pathArr = path.split(".");
    let member: OpenAPIV3.SchemaObject = schema;
    let parentName: string = null;
    let propertyName: string = null;
    let isRequired = false;
    while (pathArr.length) {
      propertyName = pathArr.shift().trim();
      const memberOrRef = member.properties[propertyName];
      parentName = member.format;
      isRequired = (member.required?.indexOf(propertyName) >= 0) || false;
      if (JsonSchemaUtils.isReferenceObject(memberOrRef)) {
        throw new Error(`Unresolved reference in ${propertyName}`);
      }
      member = memberOrRef;
      if (!member) {
        return null;
      }
    }
    return {
      parentName,
      propertyName,
      schema: member,
      isRequired,
    };
  }

  /** Typeguard for OpenAPIV2 */
  public static isOpenAPIV3(doc: OpenAPIV3.Document | OpenAPIV2.Document | IJsonSchema): doc is OpenAPIV3.Document {
    return (doc as OpenAPIV3.Document)?.openapi?.startsWith("3.") || false;
  }

  /** Typeguard for OpenAPIV3 */
  public static isOpenAPIV2(doc: OpenAPIV3.Document | OpenAPIV2.Document | IJsonSchema): doc is OpenAPIV2.Document {
    return (doc as OpenAPIV2.Document)?.swagger?.startsWith("2.") || false;
  }

  /** Typeguard for JSON schema (TODO: consider adding more generic Schema typing).) */
  public static isSchemaDocument(doc: OpenAPIV3.Document | OpenAPIV2.Document | IJsonSchema): doc is IJsonSchema {
    return !!(doc as IJsonSchema)?.$schema;
  }

  /** Typeguard for OpenAPIV3.ReferenceObject  */
  public static isReferenceObject(schema: OpenAPIV3.SchemaObject | OpenAPIV3.ReferenceObject): schema is OpenAPIV3.ReferenceObject {
    return !!(schema as OpenAPIV3.ReferenceObject)?.$ref;
  }

  /** Typeguard for OpenAPIV3.NonArraySchemaObject  */
  public static isNonArraySchemaObject(schema: OpenAPIV3.SchemaObject | OpenAPIV3.ReferenceObject): schema is OpenAPIV3.NonArraySchemaObject {
    return ["boolean", "object", "number", "string", "integer"].indexOf((schema as OpenAPIV3.NonArraySchemaObject).type) >= 0;
  }

  /** Typeguard for OpenAPIV3.ArraySchemaObject  */
  public static isArraySchemaObject(schema: OpenAPIV3.SchemaObject | OpenAPIV3.ReferenceObject): schema is OpenAPIV3.ArraySchemaObject {
    return (schema as OpenAPIV3.ArraySchemaObject).type === "array";
  }
}
