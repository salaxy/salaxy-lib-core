import { OpenAPIV3 } from "openapi-types";

import { InputMetadata } from "./InputMetadata";

/**
 * Provides framework independent helpers for creating HTML forms
 * based on JSON schema and other metadata.
 */
export class FormHelpers {

  /**
   * Gets a valid input type, format and other input properties from schema.
   * @param schema The property schema based on which the input is created.
   * @param propertyName Name of property or null if the input is not created from a property, but from model directly.
   * @param dataBindingPath Path for the data model.
   * Default is "form": The current form. Could be a a longer path e.g. "form.result.employerCalc"
   */
  public static getInputMetadata(schema: OpenAPIV3.SchemaObject, propertyName: string, dataBindingPath = "form"): InputMetadata {
    const result: InputMetadata = {
      name: propertyName,
      path: null,
      type: schema.type,
      format: schema.format,
      isEnum: false,
    };
    propertyName = propertyName || "";
    result.path = result.name ? (dataBindingPath ? (dataBindingPath + "." + result.name) : result.name) : dataBindingPath;
    switch (result.type) {
      case "number":
      case "integer": // TODO: Add enum support to numbers.
      case "boolean":
      case "object":
      case "array":
        break;
      case "string":
        result.isEnum = schema.enum?.length > 0;
        break;
      default:
        result.type = "error";
        result.content = `Unhandled data type '${result.type}', format '${result.format}' in '${propertyName || "no property name"}'.`;
        return result;
    }
    return result;
  }

  /**
   * Gets the necessary metadata for generating the inputs for the specified model of type "object".
   * If the schema is not of type "object", returns null.
   * @param schema Schema for which the user interface is generated.
   * @param dataBindingPath Path for the data model.
   * Default is "form": The current form. Could be a a longer path e.g. "form.result.employerCalc"
   */
  public static getInputsForObject(schema: OpenAPIV3.SchemaObject, dataBindingPath = "form"): InputMetadata[] {
    if (schema.type === "object") {
      return Object.keys(schema.properties).map((key) => this.getInputMetadata(schema, key, dataBindingPath))
    } else {
      return null;
    }
  }

  /**
   * Gets input for the schema itself.
   * @param schema Schema for which the input component is generated.
   * @param propertyName Property name in relation to schema. This may be used in language versioning / texts.
   * @param dataBindingPath Path for the data binding within the form.
   */
  public static getInputForSelf(schema: OpenAPIV3.SchemaObject, propertyName: string, dataBindingPath: string): InputMetadata {
    return this.getInputMetadata(schema, propertyName, dataBindingPath);
  }

  /**
   * Gets the necessary metadata for generating the inputs for the array items.
   * If the schema is not of type "array", returns null.
   * @param schema Schema for which the user interface is generated.
   * @param dataBindingPath Path for the data binding within the form.
   */
  public static getInputsForArray(schema: OpenAPIV3.SchemaObject, dataBindingPath: string): InputMetadata[] {
    if (schema.type === "array") {
      return FormHelpers.getInputsForObject(schema.items as OpenAPIV3.SchemaObject, dataBindingPath)
        || [FormHelpers.getInputForSelf(schema.items as OpenAPIV3.SchemaObject, null, dataBindingPath)];
    }
    return null;
  }
}
