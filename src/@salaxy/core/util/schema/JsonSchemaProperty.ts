import { OpenAPIV3 } from "openapi-types";

/**
 * Defines a SchemaObject for the purposes of Salaxy UI generation
 * Besides the standard schema properties, adds property name and containing type for property.
 */
export interface JsonSchemaProperty {

  /**
   * If the schema object is fetched as a property (or items of array property),
   * this is the property name. Otherwise null.
   */
  propertyName: string;

  /**
   * If the schema object is fetched as a property (or items of array property),
   * this is the name of the immediate containing schema (parent). Otherwise null.
   */
  parentName: string;

  /** The actual schema type. */
  schema: OpenAPIV3.SchemaObject;

  /** If true, the parent specifies this property as required. */
  isRequired: boolean;

}
