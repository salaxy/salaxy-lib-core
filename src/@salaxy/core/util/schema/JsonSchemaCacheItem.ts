import { OpenAPIV3 } from "openapi-types";

/**
 * Represents a schema that has been loaded, resolved and cached.
 * COMPATIBILITY WARNING: The doc will be changed as Schema instead of OpenApi document.
 * This change might not be marked as breaking change!
 */
export interface JsonSchemaCacheItem {

  /**
   * URL of the schema. This is typically the unique ID.
   * Note that the URL here is all-lowercase for faster retrieval.
   */
  url: string;

  /** Type of the schema. */
  type: "OpenAPIV2" | "OpenAPIV3" | "unknown";

  /**
   * The schema document. If the doc is null/undefined, you should take reference to loadingDonePromise.
   * @deprecated The doc will be changed as Schema instead of OpenApi document.
   * This change might not be marked as breaking change!
   */
  doc?: OpenAPIV3.Document,

  /**
   * If set, the document is being loaded to the cache and this promise will be fired
   * when the document is ready and loaded as doc (a new item in cache array). */
  loadingDonePromise?: Promise<OpenAPIV3.Document>,

}