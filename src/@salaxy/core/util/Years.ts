import { DatelyObject } from "./DatelyObject";
import { Dates } from "./Dates";

import { yearlyChangingNumbersYears } from "../codegen";
import { YearlyChangingNumbers } from "../model";
import { Holidays } from "../model";

/**
 * Helper for yearly changing sidecost percentages and holidays.
 */
export class Years {

  /**
   * Gets salary calculation parameters that are changing yearly and holidays of the year.
   * The method is designed for the end-of-year and as such it only supports 2 years:
   * the current / previous year (from Jan to Nov) OR current / next year (in approx. December).
   *
   * @param forYear - Dately object (JavaScript date, ISO string formatted date, or moment) for which the numbers are fetched.
   *
   * @returns The yearly changing numbers if year supported. Error if year not supported.
   */
  public static getYearlyChangingNumbers(forYear: DatelyObject): YearlyChangingNumbers {
    const year = Dates.getYear(forYear);
    const numbers = yearlyChangingNumbersYears.find((x) => x.year === year);
    if (!numbers) {
      throw new Error(`Year ${year} not supported.`);
    }
    numbers.holidays = this.getYearlyHolidays(forYear);
    return numbers;
  }

  /**
   * Gets holidays for the given year.
   *
   * @param forYear - Dately object (JavaScript date, ISO string formatted date, or moment) for which the holidays are fetched.
   *
   * @returns The holidays for the given year.
   */
  public static getYearlyHolidays(forYear: DatelyObject): Holidays {
    // Implementation is moved to Dates to avoid circular dependencies.
    return Dates.getYearlyHolidays(forYear);
  }
}
