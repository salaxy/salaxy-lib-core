/**
 * Object that can be understood as date:
 *
 * - String is an ISO string "yyyy-mm-dd" without any time / timezone information. This is the default Date representation in Salaxy library and should typically be used.
 * - Date is a JavaScript date object
 * - Number is (as in Date constructor) a Unix time stamp - number of milliseconds since January 1, 1970, 00:00:00 UTC (the Unix epoch)
 * - Array of numbers is (as in Date constructor) [year, monthIndex, day].
 * - MomentJS object
 * - Special string "today" will set the value as today. Same thing for "yesterday" or "tomorrow", and these strings may be extended.
 *
 * If the object cannot be understood as date it will typically be converted to null. Sometimes this may also be an error.
 */
export type DatelyObject = string | Date | number | number[] | moment.Moment | "today" | "yesterday" | "tomorrow";
