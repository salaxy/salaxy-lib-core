
import { PensionCompany } from "../model";

import { Iban } from "./Iban";
import { Dates } from "./Dates";

/**
 * Provides base methods for validation.
 * These are typically used in user interface validator components.
 */
export class Validation {

  /**
   * Returns true if the given value is a valid Finnish Personal ID (HETU / SSN).
   * Checks the format and checksum.
   * @param value Candidate for Finnish Personal ID.
   */
  public static isPersonalIdFi(value: string): boolean {

    if (!value) {
      return true;
    }

    // TODO: The escape is probably useless, but needs testing
    // eslint-disable-next-line no-useless-escape
    const pattern = /^(\d{6})([\+\-A])(\d{3})([a-zA-Z0-9\*])$/;
    const groups = pattern.exec(value);

    if (!groups) {
      return false;
    }

    const date = groups[1];
    const centenry = groups[2];
    const person = groups[3];
    const checksum = groups[4];

    const cc = centenry === "-" ? "19" : "20";
    if (!Dates.isValidDateTime(date.substr(0, 4) + cc + date.substr(4, 2), true, "DDMMYYYY")) {
      return false;
    }

    if (checksum === "*") {
      return true;
    }

    const checksumLetters = "0123456789ABCDEFHJKLMNPRSTUVWXY";
    const calculatedChecksum = checksumLetters[parseInt(date + person, 10) % 31];
    return calculatedChecksum === checksum.toUpperCase();
  }

  /**
   * Returns true if the value is a valid e-mail.
   * Currently uses a simple regex (source?).
   * @param value E-mail candidate.
   */
  public static isEmail(value: string): boolean {
    // TODO: The escape is probably useless, but needs testing
    // eslint-disable-next-line no-useless-escape
    const regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regExp.test(value);
  }

  /**
   * Returns true if the value is a valid mobile phone number: 5-16 numbers and spaces (5-18 if there is a "+" in the beginning).
   * Currently uses a naive regex, but we could extend this to take into account the regional differences.
   * @param value Mobile phone number candidate.
   */
  public static isMobilePhone(value: string): boolean {
    // TODO: Check lengths.
    const regExp = /^(\+[\d ]{1,3}[\d ]{4,15})$|^([\d ]{5,16})$/;
    return regExp.test(value);
  }

  /**
   * Returns true if the value is a valid currency amount with maximum two decimals.
   * @param value Currency amount candidate
   */
  public static isCurrency(value: string): boolean {
    const regExp = /^\d+(([.]|[,])\d{1,2})?$/;
    return regExp.test(value);
  }

  /**
   * Returns true if the value is a valid number.
   * @param value Number candidate
   */
  public static isNumber(value: string): boolean {
    const regExp = /^\d+(([.]|[,])\d+)?$/;
    return regExp.test(value);
  }

  /**
   * Returns true if the value is a valid integer.
   * @param value Number candidate
   */
  public static isInteger(value: string): boolean {
    const regExp = /^\d+$/;
    return regExp.test(value);
  }

  /**
   * Returns true if the value is a valid tax percent amount with maximum one decimal.
   * @param value Tax percent candidate
   */
  public static isTaxPercent(value: string): boolean {
    const regExp = /^\d{1,2}(([.]|[,])\d{1})?$/;
    return regExp.test(value);
  }

  /**
   * Returns true if the value is a valid SMS verification code.
   * SMS verification code is 6 digits.
   * @param value Verification code candidate.
   */
  public static isSmsVerificationCode(value: string): boolean {
    const regExp = /^\d{6}$/;
    return regExp.test(value);
  }

  /** Returns true if the value is a valid Finnish Postal code: 5 numbers. */
  public static isPostalCodeFi(value: string): boolean {
    const regExp = /^\d{5}$/;
    return regExp.test(value);
  }

  /**
   * Returns true if the value is a valid Finnish Company ID. If the format is not correct, will return false.
   * @param companyIdCandidate String to validate / format
   * @param allowStarAsChecksum If true, allows the checksum character (the last char) to be an asterix (*).
   * @returns boolean
   */
  public static formatCompanyIdFi(companyIdCandidate: string, allowStarAsChecksum = true): boolean {
    if (!companyIdCandidate) {
      return false;
    }
    companyIdCandidate = companyIdCandidate.toUpperCase().trim().replace("-", "").replace("–", "").replace("—", ""); // dash, n-dash, m-dash
    if (companyIdCandidate.substr(0, 2) === "FI") {
      companyIdCandidate = companyIdCandidate.substr(2);
    }
    if (companyIdCandidate.length === 7) {
      // Some old companies may have 6+1 numbers
      companyIdCandidate = "0" + companyIdCandidate;
    }
    if (companyIdCandidate.length !== 8) {
      return false;
    }
    const multipliers = [7, 9, 10, 5, 8, 4, 2];
    let total = 0;
    for (let i = 0; i < 7; i++) {
      const num = Number(companyIdCandidate.charAt(i));
      if (num < 0 || num > 9) {
        return false;
      }
      total += num * multipliers[i];
    }
    const modulo = total % 11;
    if (modulo === 1) {
      return false;
    }
    const checksum = modulo === 0 ? 0 : (11 - modulo);
    return (allowStarAsChecksum && companyIdCandidate.charAt(7) === "*") || Number(companyIdCandidate[7]) === checksum;
  }

  /**
   * Returns true if the value is a valid IBAN number.
   * Checks for the country specific length as well as checksum.
   * @param value IBAN number number candidate.
   */
  public static isIban(value: string): boolean {
    return Iban.isIban(value);
  }

  /**
   * Returns PensionCompany for the given pension contract number.
   * @param value - Pension contract number.
   */
  public static isPensionContractNumber(value: string): PensionCompany {

    if (value == null) {
      return null;
    }

    let regExp: RegExp = null;

    // TODO: The escape is probably useless, but needs testing
    if (value.startsWith("7") || value.startsWith("8")) {
      // eslint-disable-next-line no-useless-escape
      regExp = /^(\d{5})\-?(\d{4})([a-zA-Z0-9\*])$/;
    } else {
      // eslint-disable-next-line no-useless-escape
      regExp = /^(\d{2})\-?(\d{7})([a-zA-Z0-9\*])$/;
    }
    const result = regExp.exec(value);
    if (!result) {
      return null;
    }

    const company = result[1];
    const contract = result[2];
    const check = result[3];

    const checksumLetters = "0123456789ABCDEFHJKLMNPRSTUVWXY";
    const calculatedChecksum = checksumLetters[parseInt(company + contract, 10) % 31];
    if (calculatedChecksum !== check) {
      return null;
    }

    switch (company) {
      case "80009":
        return PensionCompany.Apteekkien;
      case "80008":
        return PensionCompany.Verso;
      case "54":
        return PensionCompany.Elo;
      case "90":
        return PensionCompany.Etera;
      case "46":
        return PensionCompany.Ilmarinen;
      case "40":
        return PensionCompany.PensionsAlandia;
      case "55":
        return PensionCompany.Varma;
      case "56":
        return PensionCompany.Veritas;
    }
    return null;
  }

  /**
   * Returns PensionCompany for the given *temporary* pension contract number.
   * @param value - TemporaryPension contract number.
   */
  public static isTemporaryPensionContractNumber(value: string): PensionCompany {

    if (value == null) {
      return null;
    }

    switch (value) {
      case "55-3999990D":
        return PensionCompany.Varma;
      case "46-3000000V":
        return PensionCompany.Ilmarinen;
      case "54-0000000U":
        return PensionCompany.Elo;
      case "56-0049990P":
        return PensionCompany.Veritas;
    }
    return null;
  }
}
