/**
 * Helpers for handling arrays.
 */
export class Arrays {

    /**
     * Takes in an array or string or null and always returns an array.
     * If the incoming value is a string, splits it by commas. This is especaially useful for HTML attributes.
     * Note that null (or other falsy objects) are returned as an empty array.
     *
     * @param arrayCandidate - A string array or string that is split by comma.
     */
    public static assureArray(arrayCandidate: string | string[]): string[] {
        if (!arrayCandidate) {
            return [];
        }
        if (Array.isArray(arrayCandidate)) {
            return arrayCandidate;
        }
        return arrayCandidate.split(",");
    }

    /**
     * Calculates a sum of array items.
     * Null items will be added as zero.
     * @param array Array to sum
     * @param selector Selector for a number to sum.
     */
    public static sum<T>(array: T[], selector: (item: T) => number): number {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      return array.reduce((prev, current, ix) => current ? (prev + (selector(current) || 0)) : 0, 0);
    }

    /**
     * Sequence generator function gets an array between two numbers (inclusive).
     * (commonly referred to as "range", e.g. Clojure, PHP etc)
     * @param start Start number of the sequence
     * @param stop End number of the sequence
     * @param step Step between the numbers, default is 1.
     * @example
     * Arrays.getRange(2019, 2023);
     * // [2019, 2020, 2021, 2022, 2023]
     */
    public static getRange(start: number, stop: number, step = 1): number[] {
      const range = (start: number, stop: number, step: number) =>
        Array.from({ length: (stop - start) / step + 1}, (_, i) => start + (i * step));
      return range(start, stop, step);
    }
}
