/* eslint-disable no-undef */
module.exports = {
  extends: [
    "../../../.eslintrc.js",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
  ],
  parserOptions: {
    tsconfigRootDir: __dirname,
    project: "./tsconfig.json",
  },
  "reportUnusedDisableDirectives": true,
  rules: {
    /*****************
     * Added rules
     *****************/

     // TODO: Consider adding additional rules or recommended configurations.

     /*****************
     * Removed typed rules
     *****************/
    // Because we interact with legacy JavaScript we do use any extensively
     "@typescript-eslint/no-unsafe-member-access": "off",
     "@typescript-eslint/no-unsafe-assignment": "off",
     "@typescript-eslint/no-unsafe-call": "off",
     "@typescript-eslint/no-unsafe-return": "off",
     // Our NG1-framework handles most catches automatically => too much noise to catch all
     "@typescript-eslint/no-floating-promises": "off",
     // TODO: Reconsider these! Consider which ones of these to allow
     "@typescript-eslint/restrict-template-expressions": "off",
     "@typescript-eslint/restrict-plus-operands": "off",
  },
};