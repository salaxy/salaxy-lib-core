/** Codegen namespace is interfaces and data that is generated in Salaxy server */

/* eslint-disable */

/**
 * Generated products data from the server.
 * To update, go to: https://test-api.salaxy.com/v02/api/accounts/products/anon
 * @ignore
 * TODO: Add language versioning to the server side and generation.
 * TODO: Can we package this entire namespace in a way that it is hidden to be internal only?
 */
export const accountProductDefaults: any = {
    "baseService": {
        "campaignCode": null,
        "id": "baseService",
        "title": "Palkkaus.fi-tili",
        "status": "Palkkaamisen peruspalvelut",
        "description": "Digiajan palkanmaksupalvelu",
        "img": "https://cdn.salaxy.com/img/elems/product-logos/palkkaus.png",
        "articleId": "product_baseService",
        "enabled": true,
        "visible": true
    },
    "tax": {
        "paymentReferenceNumber": null,
        "isSalariesPaidThisYear": false,
        "isMonthlyReportingEnabled": true,
        "isYearlyReportingEnabled": true,
        "isNotificationsSelfHandling": false,
        "isPaymentsSelfHandling": false,
        "id": "tax",
        "title": "Yhteydet verottajaan",
        "status": "Ennakonpidätys ja ilmoitukset",
        "description": "Ennakonpidätykset ja Sotu-maksut",
        "img": "https://cdn.salaxy.com/img/elems/product-logos/tax.png",
        "articleId": "product_tax",
        "enabled": true,
        "visible": true
    },
    "unemployment": {
        "isSalariesPaidThisYear": false,
        "isPrepayments": false,
        "isNotificationsSelfHandling": false,
        "id": "unemployment",
        "title": "Työttömyysvakuutus (TVR)",
        "status": "Maksut ja ilmoitukset TVR:ään",
        "description": "Työttömyysturva rahoitetaan TVR-maksuilla",
        "img": "https://cdn.salaxy.com/img/elems/product-logos/tvr.png",
        "articleId": "product_unemployment",
        "enabled": true,
        "visible": true
    },
    "pension": {
        "status": "Ei sopimusta: TyEL maksetaan tilapäisenä työnantajana",
        "isPensionContractDone": false,
        "pensionCompany": "none",
        "pensionContractNumber": null,
        "isPensionSelfHandling": false,
        "isPendingContract": false,
        "enabled": true,
        "id": "pension",
        "title": "TyEL-eläkevakuutus",
        "description": "TyEL-vakuutus on työntekijän eläketurva",
        "img": "https://cdn.salaxy.com/img/elems/product-logos/varma-tyel.png",
        "articleId": "product_pension",
        "visible": true
    },
    "insurance": {
        "status": "Ei sopimusta - Huom! 1200 € :n sääntö.",
        "isPartnerInsurance": false,
        "isInsuranceContractDone": false,
        "insuranceCompany": "none",
        "insuranceContractNumber": null,
        "enabled": false,
        "id": "insurance",
        "title": "Työtapaturmavakuutus",
        "description": "Tapaturmavakuutus vakuuttaa työntekijäsi",
        "img": "https://cdn.salaxy.com/img/elems/product-logos/if.png",
        "articleId": "product_insurance",
        "visible": true
    },
    "accounting": {
        "status": "Ei käytössä",
        "accountantName": null,
        "accountantEmail": null,
        "accountantPhone": null,
        "enabled": false,
        "id": "accounting",
        "title": "Kirjanpitoraportit",
        "description": "Palkkalaskelmien tiedot helposti kirjanpitäjälle",
        "img": "https://cdn.salaxy.com/img/elems/product-logos/accounting.png",
        "articleId": "product_accounting",
        "visible": true
    },
    "healthCareHeltti": {
        "package": "notDefined",
        "industry": "notDefined",
        "status": "Ei sopimusta",
        "enabled": false,
        "id": "healthCareHeltti",
        "title": "Työterveyshuolto",
        "description": "Tuottavammat ja terveemmät työntekijät",
        "img": "https://cdn.salaxy.com/img/elems/product-logos/heltti.png",
        "articleId": "product_healthCareHeltti",
        "visible": true
    },
    "pensionEntrepreneur": {
        "status": "Ei YEL-sopimusta",
        "tempContractMarker": false,
        "enabled": false,
        "id": "pensionEntrepreneur",
        "title": "Yrittäjän eläke",
        "description": "YEL-vakuutus on yrittäjän eläketurva",
        "img": "https://cdn.salaxy.com/img/elems/product-logos/varma-yel.png",
        "articleId": "product_pensionEntrepreneur",
        "visible": true
    },
    "taxCard": {
        "currentCard": null,
        "status": "Ei verokorttia",
        "id": "taxCard",
        "title": "Verokortti (TT)",
        "description": "Työntekijän verokortti ja sen käsittely",
        "img": "https://cdn.salaxy.com/img/elems/product-logos/palkkaus.png",
        "articleId": "product_taxCard",
        "enabled": false,
        "visible": true
    }
}
