/* eslint-disable */

/**
 * Authentication methods for VismaSign.
 * This is the raw list of methods as returned by https://www.onnistuu.fi/api/v1/auth/methods
 * We will override most of these images as the ones below are pretty outdated.
 * @ignore
 */
export const vismaSignOriginalMethods = [{
  "identifier": "tupas-osuuspankki",
  "name": "Osuuspankki",
  "image": "https://www.onnistuu.fi/img/banks/op_white_bgr-14.gif"
},
{
  "identifier": "tupas-nordea",
  "name": "Nordea",
  "image": "https://www.onnistuu.fi/img/banks/nordea_button_70x70px.gif"
},
{
  "identifier": "tupas-danske",
  "name": "Danske Bank",
  "image": "https://www.onnistuu.fi/img/banks/danskebank.png"
},
{
  "identifier": "tupas-spankki",
  "name": "S-Pankki",
  "image": "https://www.onnistuu.fi/img/banks/s-pankki.png"
},
{
  "identifier": "tupas-handelsbanken",
  "name": "Handelsbanken",
  "image": "https://www.onnistuu.fi/img/banks/handelsbanken_painike.jpg"
},
{
  "identifier": "tupas-elisa",
  "name": "Mobiilivarmenne",
  "image": "https://www.onnistuu.fi/img/banks/elisa.png"
},
{
  "identifier": "tupas-aktia",
  "name": "Aktia",
  "image": "https://www.onnistuu.fi/img/banks/aktia.png"
},
{
  "identifier": "tupas-poppankki",
  "name": "POP Pankki",
  "image": "https://www.onnistuu.fi/img/banks/POPmaksunappi.png"
},
{
  "identifier": "tupas-sp",
  "name": "S\u00e4\u00e4st\u00f6pankki",
  "image": "https://www.onnistuu.fi/img/banks/Spmaksunappi.png"
},
{
  "identifier": "tupas-omasp",
  "name": "Oma S\u00e4\u00e4st\u00f6pankki",
  "image": "https://www.onnistuu.fi/img/banks/omasp.png"
},
{
  "identifier": "tupas-alandsbanken",
  "name": "\u00c5landsbanken",
  "image": "https://www.onnistuu.fi/img/banks/alandsbanken.gif"
}];
