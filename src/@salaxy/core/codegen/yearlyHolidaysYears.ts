/* eslint-disable */

import {YearlyHolidays} from "../model";

/**
 * Yearly holidays.
 */

export const yearlyHolidaysYears =
[
  {
    "year": 2018,
    "holidays": {
      "newYearsDay": {
        "date": "2018-01-01",
        "holiday": "newYearsDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "uudenvuodenpäivä",
        "name": "New Year's Day"
      },
      "epiphany": {
        "date": "2018-01-06",
        "holiday": "epiphany",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "loppiainen",
        "name": "Epiphany"
      },
      "goodFriday": {
        "date": "2018-03-30",
        "holiday": "goodFriday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pitkäperjantai",
        "name": "Good Friday"
      },
      "easterSaturday": {
        "date": "2018-03-31",
        "holiday": "easterSaturday",
        "holidayGroups": [
          "nonBanking",
          "holiday"
        ],
        "localName": "pääsiäislauantai",
        "name": "Easter Saturday"
      },
      "easterSunday": {
        "date": "2018-04-01",
        "holiday": "easterSunday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pääsiäispäivä",
        "name": "Easter Sunday"
      },
      "easterMonday": {
        "date": "2018-04-02",
        "holiday": "easterMonday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "2. pääsiäispäivä",
        "name": "Easter Monday"
      },
      "mayDay": {
        "date": "2018-05-01",
        "holiday": "mayDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "vappu",
        "name": "May Day"
      },
      "ascensionDay": {
        "date": "2018-05-10",
        "holiday": "ascensionDay",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "helatorstai",
        "name": "Ascension Day"
      },
      "pentecost": {
        "date": "2018-05-20",
        "holiday": "pentecost",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "helluntaipäivä",
        "name": "Pentecost"
      },
      "midsummerEve": {
        "date": "2018-06-22",
        "holiday": "midsummerEve",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "juhannusaatto",
        "name": "Midsummer Eve"
      },
      "midsummerDay": {
        "date": "2018-06-23",
        "holiday": "midsummerDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "juhannuspäivä",
        "name": "Midsummer Day"
      },
      "allSaintsDay": {
        "date": "2018-11-24",
        "holiday": "allSaintsDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pyhäinpäivä",
        "name": "All Saints' Day"
      },
      "independenceDay": {
        "date": "2018-12-06",
        "holiday": "independenceDay",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "itsenäisyyspäivä",
        "name": "Independence Day"
      },
      "christmasEve": {
        "date": "2018-12-24",
        "holiday": "christmasEve",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "jouluaatto",
        "name": "Christmas Eve"
      },
      "christmasDay": {
        "date": "2018-12-25",
        "holiday": "christmasDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "joulupäivä",
        "name": "Christmas Day"
      },
      "stStephensDay": {
        "date": "2018-12-26",
        "holiday": "stStephensDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "2. joulupäivä",
        "name": "St. Stephen's Day"
      }
    }
  },
  {
    "year": 2019,
    "holidays": {
      "newYearsDay": {
        "date": "2019-01-01",
        "holiday": "newYearsDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "uudenvuodenpäivä",
        "name": "New Year's Day"
      },
      "epiphany": {
        "date": "2019-01-06",
        "holiday": "epiphany",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "loppiainen",
        "name": "Epiphany"
      },
      "goodFriday": {
        "date": "2019-04-19",
        "holiday": "goodFriday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pitkäperjantai",
        "name": "Good Friday"
      },
      "easterSaturday": {
        "date": "2019-04-20",
        "holiday": "easterSaturday",
        "holidayGroups": [
          "nonBanking",
          "holiday"
        ],
        "localName": "pääsiäislauantai",
        "name": "Easter Saturday"
      },
      "easterSunday": {
        "date": "2019-04-21",
        "holiday": "easterSunday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pääsiäispäivä",
        "name": "Easter Sunday"
      },
      "easterMonday": {
        "date": "2019-04-22",
        "holiday": "easterMonday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "2. pääsiäispäivä",
        "name": "Easter Monday"
      },
      "mayDay": {
        "date": "2019-05-01",
        "holiday": "mayDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "vappu",
        "name": "May Day"
      },
      "ascensionDay": {
        "date": "2019-05-30",
        "holiday": "ascensionDay",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "helatorstai",
        "name": "Ascension Day"
      },
      "pentecost": {
        "date": "2019-06-09",
        "holiday": "pentecost",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "helluntaipäivä",
        "name": "Pentecost"
      },
      "midsummerEve": {
        "date": "2019-06-21",
        "holiday": "midsummerEve",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "juhannusaatto",
        "name": "Midsummer Eve"
      },
      "midsummerDay": {
        "date": "2019-06-22",
        "holiday": "midsummerDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "juhannuspäivä",
        "name": "Midsummer Day"
      },
      "allSaintsDay": {
        "date": "2019-11-23",
        "holiday": "allSaintsDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pyhäinpäivä",
        "name": "All Saints' Day"
      },
      "independenceDay": {
        "date": "2019-12-06",
        "holiday": "independenceDay",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "itsenäisyyspäivä",
        "name": "Independence Day"
      },
      "christmasEve": {
        "date": "2019-12-24",
        "holiday": "christmasEve",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "jouluaatto",
        "name": "Christmas Eve"
      },
      "christmasDay": {
        "date": "2019-12-25",
        "holiday": "christmasDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "joulupäivä",
        "name": "Christmas Day"
      },
      "stStephensDay": {
        "date": "2019-12-26",
        "holiday": "stStephensDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "2. joulupäivä",
        "name": "St. Stephen's Day"
      }
    }
  },
  {
    "year": 2020,
    "holidays": {
      "newYearsDay": {
        "date": "2020-01-01",
        "holiday": "newYearsDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "uudenvuodenpäivä",
        "name": "New Year's Day"
      },
      "epiphany": {
        "date": "2020-01-06",
        "holiday": "epiphany",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "loppiainen",
        "name": "Epiphany"
      },
      "goodFriday": {
        "date": "2020-04-10",
        "holiday": "goodFriday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pitkäperjantai",
        "name": "Good Friday"
      },
      "easterSaturday": {
        "date": "2020-04-11",
        "holiday": "easterSaturday",
        "holidayGroups": [
          "nonBanking",
          "holiday"
        ],
        "localName": "pääsiäislauantai",
        "name": "Easter Saturday"
      },
      "easterSunday": {
        "date": "2020-04-12",
        "holiday": "easterSunday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pääsiäispäivä",
        "name": "Easter Sunday"
      },
      "easterMonday": {
        "date": "2020-04-13",
        "holiday": "easterMonday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "2. pääsiäispäivä",
        "name": "Easter Monday"
      },
      "mayDay": {
        "date": "2020-05-01",
        "holiday": "mayDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "vappu",
        "name": "May Day"
      },
      "ascensionDay": {
        "date": "2020-05-21",
        "holiday": "ascensionDay",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "helatorstai",
        "name": "Ascension Day"
      },
      "pentecost": {
        "date": "2020-05-31",
        "holiday": "pentecost",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "helluntaipäivä",
        "name": "Pentecost"
      },
      "midsummerEve": {
        "date": "2020-06-19",
        "holiday": "midsummerEve",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "juhannusaatto",
        "name": "Midsummer Eve"
      },
      "midsummerDay": {
        "date": "2020-06-20",
        "holiday": "midsummerDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "juhannuspäivä",
        "name": "Midsummer Day"
      },
      "allSaintsDay": {
        "date": "2020-11-21",
        "holiday": "allSaintsDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pyhäinpäivä",
        "name": "All Saints' Day"
      },
      "independenceDay": {
        "date": "2020-12-06",
        "holiday": "independenceDay",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "itsenäisyyspäivä",
        "name": "Independence Day"
      },
      "christmasEve": {
        "date": "2020-12-24",
        "holiday": "christmasEve",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "jouluaatto",
        "name": "Christmas Eve"
      },
      "christmasDay": {
        "date": "2020-12-25",
        "holiday": "christmasDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "joulupäivä",
        "name": "Christmas Day"
      },
      "stStephensDay": {
        "date": "2020-12-26",
        "holiday": "stStephensDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "2. joulupäivä",
        "name": "St. Stephen's Day"
      }
    }
  },
  {
    "year": 2021,
    "holidays": {
      "newYearsDay": {
        "date": "2021-01-01",
        "holiday": "newYearsDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "uudenvuodenpäivä",
        "name": "New Year's Day"
      },
      "epiphany": {
        "date": "2021-01-06",
        "holiday": "epiphany",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "loppiainen",
        "name": "Epiphany"
      },
      "goodFriday": {
        "date": "2021-04-02",
        "holiday": "goodFriday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pitkäperjantai",
        "name": "Good Friday"
      },
      "easterSaturday": {
        "date": "2021-04-03",
        "holiday": "easterSaturday",
        "holidayGroups": [
          "nonBanking",
          "holiday"
        ],
        "localName": "pääsiäislauantai",
        "name": "Easter Saturday"
      },
      "easterSunday": {
        "date": "2021-04-04",
        "holiday": "easterSunday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pääsiäispäivä",
        "name": "Easter Sunday"
      },
      "easterMonday": {
        "date": "2021-04-05",
        "holiday": "easterMonday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "2. pääsiäispäivä",
        "name": "Easter Monday"
      },
      "mayDay": {
        "date": "2021-05-01",
        "holiday": "mayDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "vappu",
        "name": "May Day"
      },
      "ascensionDay": {
        "date": "2021-05-13",
        "holiday": "ascensionDay",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "helatorstai",
        "name": "Ascension Day"
      },
      "pentecost": {
        "date": "2021-05-23",
        "holiday": "pentecost",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "helluntaipäivä",
        "name": "Pentecost"
      },
      "midsummerEve": {
        "date": "2021-06-25",
        "holiday": "midsummerEve",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "juhannusaatto",
        "name": "Midsummer Eve"
      },
      "midsummerDay": {
        "date": "2021-06-26",
        "holiday": "midsummerDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "juhannuspäivä",
        "name": "Midsummer Day"
      },
      "allSaintsDay": {
        "date": "2021-11-20",
        "holiday": "allSaintsDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pyhäinpäivä",
        "name": "All Saints' Day"
      },
      "independenceDay": {
        "date": "2021-12-06",
        "holiday": "independenceDay",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "itsenäisyyspäivä",
        "name": "Independence Day"
      },
      "christmasEve": {
        "date": "2021-12-24",
        "holiday": "christmasEve",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "jouluaatto",
        "name": "Christmas Eve"
      },
      "christmasDay": {
        "date": "2021-12-25",
        "holiday": "christmasDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "joulupäivä",
        "name": "Christmas Day"
      },
      "stStephensDay": {
        "date": "2021-12-26",
        "holiday": "stStephensDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "2. joulupäivä",
        "name": "St. Stephen's Day"
      }
    }
  },
  {
    "year": 2022,
    "holidays": {
      "newYearsDay": {
        "date": "2022-01-01",
        "holiday": "newYearsDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "uudenvuodenpäivä",
        "name": "New Year's Day"
      },
      "epiphany": {
        "date": "2022-01-06",
        "holiday": "epiphany",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "loppiainen",
        "name": "Epiphany"
      },
      "goodFriday": {
        "date": "2022-04-15",
        "holiday": "goodFriday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pitkäperjantai",
        "name": "Good Friday"
      },
      "easterSaturday": {
        "date": "2022-04-16",
        "holiday": "easterSaturday",
        "holidayGroups": [
          "nonBanking",
          "holiday"
        ],
        "localName": "pääsiäislauantai",
        "name": "Easter Saturday"
      },
      "easterSunday": {
        "date": "2022-04-17",
        "holiday": "easterSunday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pääsiäispäivä",
        "name": "Easter Sunday"
      },
      "easterMonday": {
        "date": "2022-04-18",
        "holiday": "easterMonday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "2. pääsiäispäivä",
        "name": "Easter Monday"
      },
      "mayDay": {
        "date": "2022-05-01",
        "holiday": "mayDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "vappu",
        "name": "May Day"
      },
      "ascensionDay": {
        "date": "2022-05-26",
        "holiday": "ascensionDay",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "helatorstai",
        "name": "Ascension Day"
      },
      "pentecost": {
        "date": "2022-06-05",
        "holiday": "pentecost",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "helluntaipäivä",
        "name": "Pentecost"
      },
      "midsummerEve": {
        "date": "2022-06-24",
        "holiday": "midsummerEve",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "juhannusaatto",
        "name": "Midsummer Eve"
      },
      "midsummerDay": {
        "date": "2022-06-25",
        "holiday": "midsummerDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "juhannuspäivä",
        "name": "Midsummer Day"
      },
      "allSaintsDay": {
        "date": "2022-11-19",
        "holiday": "allSaintsDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pyhäinpäivä",
        "name": "All Saints' Day"
      },
      "independenceDay": {
        "date": "2022-12-06",
        "holiday": "independenceDay",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "itsenäisyyspäivä",
        "name": "Independence Day"
      },
      "christmasEve": {
        "date": "2022-12-24",
        "holiday": "christmasEve",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "jouluaatto",
        "name": "Christmas Eve"
      },
      "christmasDay": {
        "date": "2022-12-25",
        "holiday": "christmasDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "joulupäivä",
        "name": "Christmas Day"
      },
      "stStephensDay": {
        "date": "2022-12-26",
        "holiday": "stStephensDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "2. joulupäivä",
        "name": "St. Stephen's Day"
      }
    }
  },
  {
    "year": 2023,
    "holidays": {
      "newYearsDay": {
        "date": "2023-01-01",
        "holiday": "newYearsDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "uudenvuodenpäivä",
        "name": "New Year's Day"
      },
      "epiphany": {
        "date": "2023-01-06",
        "holiday": "epiphany",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "loppiainen",
        "name": "Epiphany"
      },
      "goodFriday": {
        "date": "2023-04-07",
        "holiday": "goodFriday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pitkäperjantai",
        "name": "Good Friday"
      },
      "easterSaturday": {
        "date": "2023-04-08",
        "holiday": "easterSaturday",
        "holidayGroups": [
          "nonBanking",
          "holiday"
        ],
        "localName": "pääsiäislauantai",
        "name": "Easter Saturday"
      },
      "easterSunday": {
        "date": "2023-04-09",
        "holiday": "easterSunday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pääsiäispäivä",
        "name": "Easter Sunday"
      },
      "easterMonday": {
        "date": "2023-04-10",
        "holiday": "easterMonday",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "2. pääsiäispäivä",
        "name": "Easter Monday"
      },
      "mayDay": {
        "date": "2023-05-01",
        "holiday": "mayDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "vappu",
        "name": "May Day"
      },
      "ascensionDay": {
        "date": "2023-05-18",
        "holiday": "ascensionDay",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "helatorstai",
        "name": "Ascension Day"
      },
      "pentecost": {
        "date": "2023-05-28",
        "holiday": "pentecost",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "helluntaipäivä",
        "name": "Pentecost"
      },
      "midsummerEve": {
        "date": "2023-06-23",
        "holiday": "midsummerEve",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "juhannusaatto",
        "name": "Midsummer Eve"
      },
      "midsummerDay": {
        "date": "2023-06-24",
        "holiday": "midsummerDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "juhannuspäivä",
        "name": "Midsummer Day"
      },
      "allSaintsDay": {
        "date": "2023-11-25",
        "holiday": "allSaintsDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "pyhäinpäivä",
        "name": "All Saints' Day"
      },
      "independenceDay": {
        "date": "2023-12-06",
        "holiday": "independenceDay",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "itsenäisyyspäivä",
        "name": "Independence Day"
      },
      "christmasEve": {
        "date": "2023-12-24",
        "holiday": "christmasEve",
        "holidayGroups": [
          "official",
          "holiday"
        ],
        "localName": "jouluaatto",
        "name": "Christmas Eve"
      },
      "christmasDay": {
        "date": "2023-12-25",
        "holiday": "christmasDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "joulupäivä",
        "name": "Christmas Day"
      },
      "stStephensDay": {
        "date": "2023-12-26",
        "holiday": "stStephensDay",
        "holidayGroups": [
          "official",
          "nonBanking",
          "holiday"
        ],
        "localName": "2. joulupäivä",
        "name": "St. Stephen's Day"
      }
    }
  },
] as any as YearlyHolidays[];
