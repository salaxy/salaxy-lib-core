/**
 * This data is generated from
 * ****************************************************
 * DEVELOPERS NOTE: COPY from https://test-api.salaxy.com/docs/EnumJson / http://localhost:82/docs/EnumJson
 * DO NOT EDIT DIRECTLY
 * @ignore
 */
export const generatedEnumData = [
  {
    "name": "AbcSection",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "doNotShow"
      },
      {
        "order": 10,
        "name": "employment"
      },
      {
        "order": 20,
        "name": "insurance"
      },
      {
        "order": 30,
        "name": "salary"
      },
      {
        "order": 40,
        "name": "examples"
      },
      {
        "order": 50,
        "name": "palkkausGeneral"
      },
      {
        "order": 60,
        "name": "palkkausInstructions"
      },
      {
        "order": 100,
        "name": "blog"
      },
      {
        "order": 110,
        "name": "press"
      },
      {
        "order": 200,
        "name": "personEmployer"
      },
      {
        "order": 201,
        "name": "householdEmployer"
      },
      {
        "order": 210,
        "name": "worker"
      },
      {
        "order": 211,
        "name": "employee"
      },
      {
        "order": 220,
        "name": "entrepreneur"
      },
      {
        "order": 221,
        "name": "association"
      },
      {
        "order": 222,
        "name": "businessOwner"
      },
      {
        "order": 301,
        "name": "productLongDescription"
      },
      {
        "order": 302,
        "name": "documentTemplates"
      },
      {
        "order": 303,
        "name": "instructionsAndExamples"
      }
    ]
  },
  {
    "name": "AbsenceCauseCode",
    "description": null,
    "values": [
      {
        "order": 10,
        "name": "unpaidLeave"
      },
      {
        "order": 11,
        "name": "personalReason"
      },
      {
        "order": 1001,
        "name": "illness"
      },
      {
        "order": 1002,
        "name": "partTimeSickLeave"
      },
      {
        "order": 1003,
        "name": "parentalLeave"
      },
      {
        "order": 1004,
        "name": "specialMaternityLeave"
      },
      {
        "order": 1005,
        "name": "rehabilitation"
      },
      {
        "order": 1006,
        "name": "childIllness"
      },
      {
        "order": 1007,
        "name": "partTimeChildCareLeave"
      },
      {
        "order": 1008,
        "name": "training"
      },
      {
        "order": 1009,
        "name": "jobAlternationLeave"
      },
      {
        "order": 1010,
        "name": "studyLeave"
      },
      {
        "order": 1011,
        "name": "industrialAction"
      },
      {
        "order": 1012,
        "name": "interruptionInWorkProvision"
      },
      {
        "order": 1013,
        "name": "leaveOfAbsence"
      },
      {
        "order": 1014,
        "name": "militaryRefresherTraining"
      },
      {
        "order": 1015,
        "name": "militaryService"
      },
      {
        "order": 1016,
        "name": "layOff"
      },
      {
        "order": 1017,
        "name": "childCareLeave"
      },
      {
        "order": 1018,
        "name": "midWeekHoliday"
      },
      {
        "order": 1019,
        "name": "accruedHoliday"
      },
      {
        "order": 1020,
        "name": "occupationalAccident"
      },
      {
        "order": 1021,
        "name": "annualLeave"
      },
      {
        "order": 1022,
        "name": "partTimeAbsenceDueToRehabilitation"
      },
      {
        "order": 1099,
        "name": "other"
      }
    ]
  },
  {
    "name": "AccountantType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 1,
        "name": "unlinkedPrimaryPartner"
      },
      {
        "order": 2,
        "name": "unlinkedAccountingOnly"
      },
      {
        "order": 3,
        "name": "pendingPrimaryPartner"
      },
      {
        "order": 10,
        "name": "primaryPartner"
      }
    ]
  },
  {
    "name": "AccountingChannel",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "manual"
      },
      {
        "order": 800,
        "name": "email"
      },
      {
        "order": 900,
        "name": "template"
      },
      {
        "order": 1001,
        "name": "paymentTest"
      },
      {
        "order": 1010,
        "name": "paymentAccountorGo"
      },
      {
        "order": 1020,
        "name": "paymentTalenomOnline"
      },
      {
        "order": 1040,
        "name": "paymentFinagoSolo"
      },
      {
        "order": 1050,
        "name": "paymentProcountor"
      },
      {
        "order": 1060,
        "name": "paymentKevytyrittaja"
      },
      {
        "order": 1070,
        "name": "paymentVismaNetvisor"
      },
      {
        "order": 1204,
        "name": "paymentCfaFinvoice"
      },
      {
        "order": 1210,
        "name": "paymentCfaTest"
      },
      {
        "order": 2010,
        "name": "procountor"
      },
      {
        "order": 2020,
        "name": "vismaNetvisor"
      },
      {
        "order": 2030,
        "name": "fennoa"
      },
      {
        "order": 2030,
        "name": "fennoa"
      }
    ]
  },
  {
    "name": "AccountingPeriodClosingOption",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "default"
      }
    ]
  },
  {
    "name": "AccountingReportRowType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "debit"
      },
      {
        "order": 1,
        "name": "credit"
      },
      {
        "order": 2,
        "name": "total"
      },
      {
        "order": 3,
        "name": "groupHeader"
      },
      {
        "order": 4,
        "name": "groupTotal"
      },
      {
        "order": 5,
        "name": "childRow"
      }
    ]
  },
  {
    "name": "AccountingReportTableType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "classic"
      },
      {
        "order": 1,
        "name": "simple"
      },
      {
        "order": 2,
        "name": "mapped"
      }
    ]
  },
  {
    "name": "AccountingTargetStatus",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "enabled"
      },
      {
        "order": 2,
        "name": "disabled"
      },
      {
        "order": 10,
        "name": "productTarget"
      },
      {
        "order": 11,
        "name": "sharedTarget"
      },
      {
        "order": 12,
        "name": "rulesetTemplate"
      },
      {
        "order": 13,
        "name": "paymentChannel"
      }
    ]
  },
  {
    "name": "AgeGroupCode",
    "description": null,
    "values": [
      {
        "order": 1,
        "name": "a"
      },
      {
        "order": 2,
        "name": "b"
      },
      {
        "order": 3,
        "name": "c"
      },
      {
        "order": 4,
        "name": "u"
      }
    ]
  },
  {
    "name": "AgeRange",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 15,
        "name": "age_15"
      },
      {
        "order": 16,
        "name": "age16"
      },
      {
        "order": 17,
        "name": "age17"
      },
      {
        "order": 18,
        "name": "age18_52"
      },
      {
        "order": 53,
        "name": "age53_62"
      },
      {
        "order": 63,
        "name": "age63_64"
      },
      {
        "order": 65,
        "name": "age65_67"
      },
      {
        "order": 68,
        "name": "age68AndOVer"
      }
    ]
  },
  {
    "name": "AllowanceCode",
    "description": null,
    "values": [
      {
        "order": 1,
        "name": "mealAllowance"
      },
      {
        "order": 2,
        "name": "partialDailyAllowance"
      },
      {
        "order": 3,
        "name": "fullDailyAllowance"
      },
      {
        "order": 4,
        "name": "internationalDailyAllowance"
      },
      {
        "order": 5,
        "name": "taxExemptReimbursementsAbroad"
      }
    ]
  },
  {
    "name": "AnnualLeavePaymentKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "planned"
      },
      {
        "order": 101,
        "name": "manualSalary"
      },
      {
        "order": 102,
        "name": "manualCompensation"
      },
      {
        "order": 103,
        "name": "manualBonus"
      },
      {
        "order": 201,
        "name": "paidCalc"
      },
      {
        "order": 202,
        "name": "draftCalc"
      }
    ]
  },
  {
    "name": "ApiListItemType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 10,
        "name": "accountBase"
      },
      {
        "order": 11,
        "name": "companyAccount"
      },
      {
        "order": 12,
        "name": "personAccount"
      },
      {
        "order": 13,
        "name": "workerAccount"
      },
      {
        "order": 14,
        "name": "certificate"
      },
      {
        "order": 15,
        "name": "sessionUserCredential"
      },
      {
        "order": 16,
        "name": "onboarding"
      },
      {
        "order": 17,
        "name": "profile"
      },
      {
        "order": 18,
        "name": "workerInvitation"
      },
      {
        "order": 20,
        "name": "calculation"
      },
      {
        "order": 21,
        "name": "calculationPaid"
      },
      {
        "order": 22,
        "name": "eSalaryPayment"
      },
      {
        "order": 23,
        "name": "payment"
      },
      {
        "order": 24,
        "name": "earningsPayment"
      },
      {
        "order": 30,
        "name": "employment"
      },
      {
        "order": 31,
        "name": "workerAbsences"
      },
      {
        "order": 32,
        "name": "holidayYear"
      },
      {
        "order": 40,
        "name": "taxcard"
      },
      {
        "order": 41,
        "name": "payrollDetails"
      },
      {
        "order": 42,
        "name": "invoice"
      },
      {
        "order": 43,
        "name": "report"
      },
      {
        "order": 44,
        "name": "blobFile"
      },
      {
        "order": 45,
        "name": "article"
      },
      {
        "order": 46,
        "name": "messageThread"
      },
      {
        "order": 47,
        "name": "emailMessage"
      },
      {
        "order": 48,
        "name": "accountProducts"
      },
      {
        "order": 49,
        "name": "varmaPensionOrder"
      },
      {
        "order": 50,
        "name": "ifInsuranceOrder"
      },
      {
        "order": 51,
        "name": "historical"
      }
    ]
  },
  {
    "name": "ApiTestErrorType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "default"
      },
      {
        "order": 10,
        "name": "userFriendly"
      }
    ]
  },
  {
    "name": "ApiItemType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "calculation"
      },
      {
        "order": 2,
        "name": "calculationPaid"
      },
      {
        "order": 10,
        "name": "payrollDetails"
      },
      {
        "order": 20,
        "name": "invoice"
      },
      {
        "order": 30,
        "name": "earningsPaymentReport"
      },
      {
        "order": 40,
        "name": "payerSummaryReport"
      },
      {
        "order": 50,
        "name": "employment"
      },
      {
        "order": 100,
        "name": "messageThread"
      },
      {
        "order": 110,
        "name": "calendarEvent"
      }
    ]
  },
  {
    "name": "ApiValidationErrorType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "general"
      },
      {
        "order": 10,
        "name": "required"
      },
      {
        "order": 20,
        "name": "invalid"
      },
      {
        "order": 30,
        "name": "warning"
      }
    ]
  },
  {
    "name": "AuthenticationMethod",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "emailPwdLocal"
      },
      {
        "order": 11,
        "name": "facebook"
      },
      {
        "order": 12,
        "name": "google"
      },
      {
        "order": 13,
        "name": "microsoft"
      },
      {
        "order": 14,
        "name": "linkedIn"
      },
      {
        "order": 15,
        "name": "auth0Database"
      },
      {
        "order": 16,
        "name": "x509"
      },
      {
        "order": 17,
        "name": "salaxy"
      },
      {
        "order": 18,
        "name": "test"
      },
      {
        "order": 1000,
        "name": "internalLocalhost"
      }
    ]
  },
  {
    "name": "AuthorizationStatus",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "oK"
      },
      {
        "order": -40,
        "name": "accessDenied"
      },
      {
        "order": -30,
        "name": "expiredOauthToken"
      },
      {
        "order": -20,
        "name": "invalidOAuthToken"
      },
      {
        "order": -10,
        "name": "noOauthToken"
      }
    ]
  },
  {
    "name": "AuthorizationType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "none"
      },
      {
        "order": 1,
        "name": "employerAuthorization"
      },
      {
        "order": 2,
        "name": "workerContract"
      },
      {
        "order": 3,
        "name": "companyContract"
      },
      {
        "order": 100,
        "name": "manual"
      },
      {
        "order": 999,
        "name": "temporary"
      }
    ]
  },
  {
    "name": "AvatarPictureType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "icon"
      },
      {
        "order": 1,
        "name": "uploaded"
      },
      {
        "order": 2,
        "name": "gravatar"
      }
    ]
  },
  {
    "name": "BankPaymentType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 1,
        "name": "error"
      },
      {
        "order": 10,
        "name": "paytrailBatch"
      },
      {
        "order": 11,
        "name": "netvisorPayment"
      },
      {
        "order": 20,
        "name": "salaryGrossPayment"
      },
      {
        "order": 21,
        "name": "refundPayment"
      },
      {
        "order": 22,
        "name": "partialRefundPayment"
      },
      {
        "order": 30,
        "name": "insuranceGrossPayment"
      },
      {
        "order": 50,
        "name": "netSalary"
      },
      {
        "order": 51,
        "name": "salaryAdvance"
      },
      {
        "order": 100,
        "name": "taxAccount"
      },
      {
        "order": 200,
        "name": "tvr"
      },
      {
        "order": 210,
        "name": "tvrRfnd"
      },
      {
        "order": 300,
        "name": "palkkausFees"
      },
      {
        "order": 310,
        "name": "bankFees"
      },
      {
        "order": 320,
        "name": "paytrailFees"
      },
      {
        "order": 501,
        "name": "insuranceLahiTapiola"
      },
      {
        "order": 1000,
        "name": "tyel"
      },
      {
        "order": 1101,
        "name": "tyelEteraContract"
      },
      {
        "order": 1102,
        "name": "tyelEteraTemp"
      },
      {
        "order": 1103,
        "name": "tyelEteraUnknown"
      },
      {
        "order": 1201,
        "name": "tyelEloContract"
      },
      {
        "order": 1202,
        "name": "tyelEloTemp"
      },
      {
        "order": 1203,
        "name": "tyelEloInitialImport"
      },
      {
        "order": 1301,
        "name": "tyelVarmaContract"
      },
      {
        "order": 1302,
        "name": "tyelVarmaTemp"
      },
      {
        "order": 1401,
        "name": "tyelIlmarinenContract"
      },
      {
        "order": 1402,
        "name": "tyelIlmarinenTemp"
      },
      {
        "order": 2000,
        "name": "union"
      },
      {
        "order": 2001,
        "name": "unionRaksa"
      },
      {
        "order": 2010,
        "name": "foreclosure"
      },
      {
        "order": 10001,
        "name": "saldoYearBegin"
      },
      {
        "order": 10002,
        "name": "saldoYearEnd"
      }
    ]
  },
  {
    "name": "BenefitCode",
    "description": null,
    "values": [
      {
        "order": 1,
        "name": "accommodationBenefit"
      },
      {
        "order": 2,
        "name": "telephoneBenefit"
      },
      {
        "order": 3,
        "name": "mealBenefit"
      },
      {
        "order": 4,
        "name": "otherBenefits"
      }
    ]
  },
  {
    "name": "BlobFileType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 1,
        "name": "authorizationPdf"
      },
      {
        "order": 2,
        "name": "avatar"
      },
      {
        "order": 3,
        "name": "taxCard"
      },
      {
        "order": 4,
        "name": "expensesReceipt"
      },
      {
        "order": 5,
        "name": "raksaloki"
      },
      {
        "order": 6,
        "name": "template"
      },
      {
        "order": 7,
        "name": "eInvoice"
      },
      {
        "order": 8,
        "name": "temporary"
      },
      {
        "order": 9,
        "name": "settings"
      },
      {
        "order": 20,
        "name": "messages"
      },
      {
        "order": 100,
        "name": "monthlyReport"
      },
      {
        "order": 101,
        "name": "yearlyReport"
      },
      {
        "order": 102,
        "name": "calcReport"
      }
    ]
  },
  {
    "name": "BlobRepository",
    "description": null,
    "values": [
      {
        "order": 1,
        "name": "userFiles"
      },
      {
        "order": 2,
        "name": "versionHistory"
      },
      {
        "order": 3,
        "name": "systemFiles"
      },
      {
        "order": 4,
        "name": "payload"
      },
      {
        "order": 5,
        "name": "reports"
      },
      {
        "order": 6,
        "name": "cdnImages"
      },
      {
        "order": 100,
        "name": "gitContent"
      },
      {
        "order": 200,
        "name": "fileSystemContent"
      }
    ]
  },
  {
    "name": "CalcGroup",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "baseSalary"
      },
      {
        "order": 2,
        "name": "salaryAdditions"
      },
      {
        "order": 3,
        "name": "benefits"
      },
      {
        "order": 4,
        "name": "expenses"
      },
      {
        "order": 5,
        "name": "deductions"
      },
      {
        "order": 6,
        "name": "otherNoPayment"
      },
      {
        "order": 100,
        "name": "totals"
      },
      {
        "order": 200,
        "name": "disabled"
      }
    ]
  },
  {
    "name": "CalculationFlag",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "exclude"
      },
      {
        "order": 1,
        "name": "pensionInsurance"
      },
      {
        "order": 2,
        "name": "accidentInsurance"
      },
      {
        "order": 3,
        "name": "unemploymentInsurance"
      },
      {
        "order": 4,
        "name": "healthInsurance"
      },
      {
        "order": 10,
        "name": "insurancesDeduction"
      },
      {
        "order": 100,
        "name": "noTax"
      },
      {
        "order": 101,
        "name": "tax"
      },
      {
        "order": 102,
        "name": "taxDeduction"
      },
      {
        "order": 200,
        "name": "cfNoPayment"
      },
      {
        "order": 201,
        "name": "cfPayment"
      },
      {
        "order": 202,
        "name": "cfDeduction"
      },
      {
        "order": 203,
        "name": "cfDeductionAtSalaxy"
      }
    ]
  },
  {
    "name": "CalculationResultRowType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "totalBenefits"
      },
      {
        "order": 2,
        "name": "totalExpenses"
      },
      {
        "order": 3,
        "name": "totalSocialSecurityEmployer"
      },
      {
        "order": 4,
        "name": "totalSocialSecurityEmployerDebt"
      },
      {
        "order": 5,
        "name": "totalSocialSecurityEmployerPayment"
      },
      {
        "order": 6,
        "name": "totalPension"
      },
      {
        "order": 7,
        "name": "totalPensionPayment"
      },
      {
        "order": 8,
        "name": "totalPensionEmployer"
      },
      {
        "order": 9,
        "name": "totalPensionEmployerDebt"
      },
      {
        "order": 10,
        "name": "totalPensionWorker"
      },
      {
        "order": 11,
        "name": "totalUnemployment"
      },
      {
        "order": 12,
        "name": "totalUnemploymentPayment"
      },
      {
        "order": 13,
        "name": "totalUnemploymentEmployer"
      },
      {
        "order": 14,
        "name": "totalUnemploymentEmployerDebt"
      },
      {
        "order": 15,
        "name": "totalUnemploymentWorker"
      },
      {
        "order": 16,
        "name": "totalPalkkaus"
      },
      {
        "order": 17,
        "name": "totalSalary"
      },
      {
        "order": 18,
        "name": "totalTax"
      },
      {
        "order": 19,
        "name": "totalTaxPayment"
      },
      {
        "order": 20,
        "name": "totalPayment"
      },
      {
        "order": 21,
        "name": "totalWorkerPayment"
      }
    ]
  },
  {
    "name": "CalculationRowSource",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "salaryPage"
      },
      {
        "order": 1,
        "name": "tesSelection"
      },
      {
        "order": 2,
        "name": "manualRow"
      }
    ]
  },
  {
    "name": "CalculationRowType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 1,
        "name": "salary"
      },
      {
        "order": 2,
        "name": "hourlySalary"
      },
      {
        "order": 3,
        "name": "monthlySalary"
      },
      {
        "order": 4,
        "name": "totalWorkerPayment"
      },
      {
        "order": 5,
        "name": "totalEmployerPayment"
      },
      {
        "order": 9,
        "name": "compensation"
      },
      {
        "order": 10,
        "name": "overtime"
      },
      {
        "order": 11,
        "name": "tesWorktimeShortening"
      },
      {
        "order": 12,
        "name": "eveningAddition"
      },
      {
        "order": 13,
        "name": "nightimeAddition"
      },
      {
        "order": 14,
        "name": "saturdayAddition"
      },
      {
        "order": 15,
        "name": "sundayWork"
      },
      {
        "order": 19,
        "name": "otherAdditions"
      },
      {
        "order": 101,
        "name": "accomodationBenefit"
      },
      {
        "order": 102,
        "name": "mealBenefit"
      },
      {
        "order": 103,
        "name": "phoneBenefit"
      },
      {
        "order": 104,
        "name": "carBenefit"
      },
      {
        "order": 199,
        "name": "otherBenefit"
      },
      {
        "order": 201,
        "name": "holidayCompensation"
      },
      {
        "order": 202,
        "name": "holidayBonus"
      },
      {
        "order": 203,
        "name": "holidaySalary"
      },
      {
        "order": 301,
        "name": "dailyAllowance"
      },
      {
        "order": 302,
        "name": "dailyAllowanceHalf"
      },
      {
        "order": 303,
        "name": "mealCompensation"
      },
      {
        "order": 304,
        "name": "milageOwnCar"
      },
      {
        "order": 305,
        "name": "toolCompensation"
      },
      {
        "order": 306,
        "name": "expenses"
      },
      {
        "order": 307,
        "name": "milageDaily"
      },
      {
        "order": 308,
        "name": "milageOther"
      },
      {
        "order": 401,
        "name": "unionPayment"
      },
      {
        "order": 402,
        "name": "foreclosure"
      },
      {
        "order": 403,
        "name": "advance"
      },
      {
        "order": 404,
        "name": "foreclosureByPalkkaus"
      },
      {
        "order": 405,
        "name": "prepaidExpenses"
      },
      {
        "order": 406,
        "name": "otherDeductions"
      },
      {
        "order": 901,
        "name": "childCareSubsidy"
      },
      {
        "order": 902,
        "name": "chainsawReduction"
      },
      {
        "order": 1010,
        "name": "nonProfitOrg"
      },
      {
        "order": 1020,
        "name": "subsidisedCommute"
      },
      {
        "order": 1030,
        "name": "irIncomeType"
      },
      {
        "order": 1040,
        "name": "board"
      },
      {
        "order": 1050,
        "name": "remuneration"
      },
      {
        "order": 1060,
        "name": "otherCompensation"
      },
      {
        "order": 1070,
        "name": "workingTimeCompensation"
      },
      {
        "order": 1080,
        "name": "employmentTermination"
      },
      {
        "order": 1090,
        "name": "hourlySalaryWithWorkingTimeCompensation"
      },
      {
        "order": 9999,
        "name": "totals"
      }
    ]
  },
  {
    "name": "CalculationRowUnit",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "count"
      },
      {
        "order": 2,
        "name": "percent"
      },
      {
        "order": 3,
        "name": "days"
      },
      {
        "order": 4,
        "name": "kilometers"
      },
      {
        "order": 5,
        "name": "hours"
      }
    ]
  },
  {
    "name": "CalculationStatus",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "draft"
      },
      {
        "order": 1,
        "name": "paymentStarted"
      },
      {
        "order": 2,
        "name": "paymentSucceeded"
      },
      {
        "order": 3,
        "name": "paymentCanceled"
      },
      {
        "order": 4,
        "name": "paymentError"
      },
      {
        "order": 5,
        "name": "paymentWorkerCopy"
      },
      {
        "order": 6,
        "name": "workerRequested"
      },
      {
        "order": 7,
        "name": "workerRequestAccepted"
      },
      {
        "order": 8,
        "name": "workerRequestDeclined"
      },
      {
        "order": 9,
        "name": "paymentRefunded"
      },
      {
        "order": 100,
        "name": "waitingApproval"
      },
      {
        "order": 200,
        "name": "payrollDraft"
      },
      {
        "order": 220,
        "name": "proDraft"
      },
      {
        "order": 300,
        "name": "sharedWaiting"
      },
      {
        "order": 310,
        "name": "sharedApproved"
      },
      {
        "order": 320,
        "name": "sharedRejected"
      },
      {
        "order": 1000,
        "name": "template"
      }
    ]
  },
  {
    "name": "CalculationStatusCategory",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "all"
      },
      {
        "order": 1,
        "name": "readonly"
      },
      {
        "order": 2,
        "name": "editable"
      },
      {
        "order": 3,
        "name": "sent"
      },
      {
        "order": 4,
        "name": "shared"
      },
      {
        "order": 5,
        "name": "received"
      }
    ]
  },
  {
    "name": "CalendarActionType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "display"
      },
      {
        "order": 2,
        "name": "email"
      },
      {
        "order": 3,
        "name": "audio"
      },
      {
        "order": 10,
        "name": "createItem"
      },
      {
        "order": 11,
        "name": "script"
      }
    ]
  },
  {
    "name": "CalendarEventStatus",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 100,
        "name": "cancelled"
      },
      {
        "order": 101,
        "name": "confirmed"
      },
      {
        "order": 102,
        "name": "tentative"
      },
      {
        "order": 111,
        "name": "needsAction"
      },
      {
        "order": 112,
        "name": "completed"
      },
      {
        "order": 113,
        "name": "inProcess"
      }
    ]
  },
  {
    "name": "CarBenefitCode",
    "description": null,
    "values": [
      {
        "order": 1,
        "name": "limitedCarBenefit"
      },
      {
        "order": 2,
        "name": "fullCarBenefit"
      }
    ]
  },
  {
    "name": "CompanyType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 100,
        "name": "fiOy"
      },
      {
        "order": 101,
        "name": "fiTm"
      },
      {
        "order": 102,
        "name": "fiRy"
      },
      {
        "order": 103,
        "name": "fiYy"
      }
    ]
  },
  {
    "name": "ContractPartyType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "person"
      },
      {
        "order": 1,
        "name": "customContact"
      }
    ]
  },
  {
    "name": "CustomerWebApp",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "palkkaus"
      },
      {
        "order": 1,
        "name": "customSso"
      },
      {
        "order": 2,
        "name": "customPalkkaus"
      },
      {
        "order": 999,
        "name": "noCustomerUi"
      }
    ]
  },
  {
    "name": "EmployerType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 10,
        "name": "miniEmployer"
      },
      {
        "order": 20,
        "name": "smallEmployer"
      },
      {
        "order": 40,
        "name": "fullTimeEmployer"
      },
      {
        "order": 41,
        "name": "tyelOwnContractEmployer"
      }
    ]
  },
  {
    "name": "EmploymentRelationStatus",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 1,
        "name": "primary"
      },
      {
        "order": 2,
        "name": "secondaryCurrent"
      },
      {
        "order": 100,
        "name": "archived"
      }
    ]
  },
  {
    "name": "EmploymentRelationType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "salary"
      },
      {
        "order": 2,
        "name": "hourlySalary"
      },
      {
        "order": 3,
        "name": "monthlySalary"
      },
      {
        "order": 100,
        "name": "compensation"
      },
      {
        "order": 101,
        "name": "boardMember"
      },
      {
        "order": 200,
        "name": "entrepreneur"
      },
      {
        "order": 201,
        "name": "farmer"
      },
      {
        "order": 301,
        "name": "employedByStateEmploymentFund"
      },
      {
        "order": 304,
        "name": "keyEmployee"
      },
      {
        "order": 308,
        "name": "athlete"
      },
      {
        "order": 309,
        "name": "performingArtist"
      }
    ]
  },
  {
    "name": "EmploymentSalaryType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 1,
        "name": "hourlyPay"
      },
      {
        "order": 2,
        "name": "montlyPay"
      },
      {
        "order": 3,
        "name": "otherPay"
      }
    ]
  },
  {
    "name": "EmploymentType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 1,
        "name": "job"
      },
      {
        "order": 2,
        "name": "partTime"
      },
      {
        "order": 3,
        "name": "fullTime"
      }
    ]
  },
  {
    "name": "EmploymentWorkHoursType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "hoursPerDay"
      },
      {
        "order": 1,
        "name": "hoursPerWeek"
      },
      {
        "order": 2,
        "name": "hourPerTwoWeeks"
      },
      {
        "order": 3,
        "name": "hourPerThreeWeeks"
      }
    ]
  },
  {
    "name": "ESalaryPaymentStatus",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "created"
      },
      {
        "order": 10,
        "name": "shared"
      },
      {
        "order": 100,
        "name": "opened"
      },
      {
        "order": 200,
        "name": "employerResolved"
      },
      {
        "order": 210,
        "name": "employmentResolved"
      },
      {
        "order": 220,
        "name": "taxcardResolved"
      },
      {
        "order": 230,
        "name": "paymentMethodResolved"
      },
      {
        "order": 300,
        "name": "calculationCreated"
      },
      {
        "order": 400,
        "name": "paymentStarted"
      },
      {
        "order": 410,
        "name": "paymentSucceeded"
      },
      {
        "order": 450,
        "name": "paymentCanceled"
      }
    ]
  },
  {
    "name": "ExportMethod",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "fileExcel"
      },
      {
        "order": 2,
        "name": "fileCsv"
      },
      {
        "order": 3,
        "name": "filePdf"
      },
      {
        "order": 101,
        "name": "copyExcel"
      },
      {
        "order": 102,
        "name": "copyCsv"
      },
      {
        "order": 200,
        "name": "paymentChannel"
      },
      {
        "order": 201,
        "name": "api"
      }
    ]
  },
  {
    "name": "DateOfBirthAccuracy",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "assumption"
      },
      {
        "order": 1,
        "name": "ageBased"
      },
      {
        "order": 2,
        "name": "ageGroupBased"
      },
      {
        "order": 3,
        "name": "monthCorrect"
      },
      {
        "order": 10,
        "name": "exact"
      },
      {
        "order": 20,
        "name": "verified"
      }
    ]
  },
  {
    "name": "FrameworkAgreement",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "notDefined"
      },
      {
        "order": 1,
        "name": "construction"
      },
      {
        "order": 3,
        "name": "mll"
      },
      {
        "order": 4,
        "name": "childCare"
      },
      {
        "order": 8,
        "name": "cleaning"
      },
      {
        "order": 10,
        "name": "santaClaus"
      },
      {
        "order": 1000,
        "name": "entrepreneur"
      },
      {
        "order": 9999,
        "name": "other"
      }
    ]
  },
  {
    "name": "Gender",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 1,
        "name": "male"
      },
      {
        "order": 2,
        "name": "female"
      }
    ]
  },
  {
    "name": "HelttiIndustry",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "notDefined"
      },
      {
        "order": 100,
        "name": "i"
      },
      {
        "order": 110,
        "name": "j"
      },
      {
        "order": 120,
        "name": "k"
      },
      {
        "order": 140,
        "name": "m"
      },
      {
        "order": 160,
        "name": "o"
      },
      {
        "order": 170,
        "name": "n"
      },
      {
        "order": 170,
        "name": "n"
      },
      {
        "order": 180,
        "name": "q"
      },
      {
        "order": 190,
        "name": "r"
      },
      {
        "order": 200,
        "name": "s"
      },
      {
        "order": 999,
        "name": "other"
      }
    ]
  },
  {
    "name": "HelttiProductPackage",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "notDefined"
      },
      {
        "order": 10,
        "name": "small"
      },
      {
        "order": 20,
        "name": "medium"
      },
      {
        "order": 30,
        "name": "large"
      }
    ]
  },
  {
    "name": "IncomeEarnerType",
    "description": null,
    "values": [
      {
        "order": 1,
        "name": "employedByStateEmploymentFund"
      },
      {
        "order": 2,
        "name": "jointOwnerWithPayer"
      },
      {
        "order": 3,
        "name": "partialOwner"
      },
      {
        "order": 4,
        "name": "keyEmployee"
      },
      {
        "order": 5,
        "name": "leasedEmployeeLivingAbroad"
      },
      {
        "order": 6,
        "name": "personWorkingInFrontierDistrict"
      },
      {
        "order": 7,
        "name": "personWorkingAbroad"
      },
      {
        "order": 8,
        "name": "athlete"
      },
      {
        "order": 9,
        "name": "performingArtist"
      },
      {
        "order": 10,
        "name": "restrictedPeriodInFinland"
      },
      {
        "order": 11,
        "name": "netOfTaxContract"
      },
      {
        "order": 12,
        "name": "organization"
      },
      {
        "order": 13,
        "name": "personWorkingOnAlandFerry"
      },
      {
        "order": 14,
        "name": "entrepreneurOrFarmerNoPensionRequired"
      }
    ]
  },
  {
    "name": "IncomeLogDiff",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "default"
      },
      {
        "order": 1,
        "name": "new"
      },
      {
        "order": 2,
        "name": "changed"
      },
      {
        "order": 3,
        "name": "removed"
      }
    ]
  },
  {
    "name": "InvoiceStatus",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 20,
        "name": "forecast"
      },
      {
        "order": 30,
        "name": "preview"
      },
      {
        "order": 50,
        "name": "waitingPalkkaus"
      },
      {
        "order": 100,
        "name": "unread"
      },
      {
        "order": 110,
        "name": "read"
      },
      {
        "order": 120,
        "name": "waitingConfirmation"
      },
      {
        "order": 130,
        "name": "paymentStarted"
      },
      {
        "order": 140,
        "name": "paid"
      },
      {
        "order": 150,
        "name": "canceled"
      },
      {
        "order": 160,
        "name": "error"
      }
    ]
  },
  {
    "name": "InvoiceType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 100,
        "name": "net"
      },
      {
        "order": 110,
        "name": "tax"
      },
      {
        "order": 111,
        "name": "taxWithholding"
      },
      {
        "order": 112,
        "name": "taxSocialSecurity"
      },
      {
        "order": 120,
        "name": "unemployment"
      },
      {
        "order": 130,
        "name": "pension"
      },
      {
        "order": 140,
        "name": "union"
      },
      {
        "order": 150,
        "name": "foreclosure"
      },
      {
        "order": 200,
        "name": "gross"
      }
    ]
  },
  {
    "name": "Holiday",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "newYearsDay"
      },
      {
        "order": 1,
        "name": "epiphany"
      },
      {
        "order": 2,
        "name": "goodFriday"
      },
      {
        "order": 3,
        "name": "easterSunday"
      },
      {
        "order": 4,
        "name": "easterSaturday"
      },
      {
        "order": 5,
        "name": "easterMonday"
      },
      {
        "order": 6,
        "name": "mayDay"
      },
      {
        "order": 7,
        "name": "ascensionDay"
      },
      {
        "order": 8,
        "name": "pentecost"
      },
      {
        "order": 9,
        "name": "midsummerEve"
      },
      {
        "order": 10,
        "name": "midsummerDay"
      },
      {
        "order": 11,
        "name": "allSaintsDay"
      },
      {
        "order": 12,
        "name": "independenceDay"
      },
      {
        "order": 13,
        "name": "christmasEve"
      },
      {
        "order": 14,
        "name": "christmasDay"
      },
      {
        "order": 15,
        "name": "stStephensDay"
      }
    ]
  },
  {
    "name": "HolidayAccrualSource",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "initial"
      },
      {
        "order": 1,
        "name": "manual"
      },
      {
        "order": 10,
        "name": "calcDraft"
      },
      {
        "order": 11,
        "name": "calcPaid"
      }
    ]
  },
  {
    "name": "HolidayBonusPaymentMethod",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "none"
      },
      {
        "order": 1,
        "name": "payForHolidaySalary"
      },
      {
        "order": 2,
        "name": "paySummerBonus"
      },
      {
        "order": 24,
        "name": "pay24Days"
      },
      {
        "order": 100,
        "name": "payAllBonus"
      }
    ]
  },
  {
    "name": "HolidayCode",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 10,
        "name": "permanent14Days"
      },
      {
        "order": 11,
        "name": "permanent35Hours"
      },
      {
        "order": 20,
        "name": "temporaryTimeOff"
      },
      {
        "order": 21,
        "name": "holidayCompensation"
      },
      {
        "order": 22,
        "name": "holidayCompensationIncluded"
      },
      {
        "order": 99,
        "name": "noHolidays"
      }
    ]
  },
  {
    "name": "HolidayGroup",
    "description": null,
    "values": [
      {
        "order": 1,
        "name": "official"
      },
      {
        "order": 2,
        "name": "nonBanking"
      },
      {
        "order": 4,
        "name": "holiday"
      }
    ]
  },
  {
    "name": "InsuranceCompany",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "none"
      },
      {
        "order": 1,
        "name": "lähiTapiola"
      },
      {
        "order": 2,
        "name": "pohjola"
      },
      {
        "order": 3,
        "name": "if"
      },
      {
        "order": 4,
        "name": "fennia"
      },
      {
        "order": 5,
        "name": "aVakuutus"
      },
      {
        "order": 6,
        "name": "aktia"
      },
      {
        "order": 7,
        "name": "pohjantähti"
      },
      {
        "order": 8,
        "name": "tryg"
      },
      {
        "order": 9,
        "name": "ålands"
      },
      {
        "order": 10,
        "name": "turva"
      },
      {
        "order": 11,
        "name": "redarnas"
      },
      {
        "order": 12,
        "name": "folksam"
      },
      {
        "order": 13,
        "name": "alandia"
      },
      {
        "order": 999999,
        "name": "other"
      },
      {
        "order": 1000000,
        "name": "pending"
      }
    ]
  },
  {
    "name": "InvoicePaymentType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "default"
      },
      {
        "order": 1,
        "name": "salary"
      }
    ]
  },
  {
    "name": "IrFlags",
    "description": null,
    "values": [
      {
        "order": 10,
        "name": "noMoney"
      },
      {
        "order": 20,
        "name": "oneOff"
      },
      {
        "order": 30,
        "name": "unjustEnrichment"
      }
    ]
  },
  {
    "name": "IrInsuranceExceptions",
    "description": null,
    "values": [
      {
        "order": 501,
        "name": "includeAll"
      },
      {
        "order": 502,
        "name": "includePension"
      },
      {
        "order": 503,
        "name": "includeHealthInsurance"
      },
      {
        "order": 505,
        "name": "includeUnemployment"
      },
      {
        "order": 506,
        "name": "includeAccidentInsurance"
      },
      {
        "order": 601,
        "name": "excludeAll"
      },
      {
        "order": 602,
        "name": "excludePension"
      },
      {
        "order": 603,
        "name": "excludeHealthInsurance"
      },
      {
        "order": 605,
        "name": "excludeUnemployment"
      },
      {
        "order": 606,
        "name": "excludeAccidentInsurance"
      }
    ]
  },
  {
    "name": "IrRowSourceType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "manual"
      },
      {
        "order": 1,
        "name": "usecase"
      },
      {
        "order": 2,
        "name": "usecaseV02"
      }
    ]
  },
  {
    "name": "LegalEntityType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "person"
      },
      {
        "order": 2,
        "name": "company"
      },
      {
        "order": 3,
        "name": "personCreatedByEmployer"
      },
      {
        "order": 4,
        "name": "partner"
      }
    ]
  },
  {
    "name": "LegacyTaxcardType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "example"
      },
      {
        "order": 1,
        "name": "noTaxCard"
      },
      {
        "order": 2,
        "name": "sideIncome"
      },
      {
        "order": 3,
        "name": "noTaxPrepayment"
      },
      {
        "order": 4,
        "name": "taxCardA"
      },
      {
        "order": 5,
        "name": "taxCardB"
      },
      {
        "order": 6,
        "name": "freelancer"
      },
      {
        "order": 7,
        "name": "steps"
      },
      {
        "order": 99,
        "name": "others"
      }
    ]
  },
  {
    "name": "MessageFrom",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 10,
        "name": "owner"
      },
      {
        "order": 20,
        "name": "otherParty"
      },
      {
        "order": 90,
        "name": "system"
      }
    ]
  },
  {
    "name": "MessageType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "email"
      },
      {
        "order": 1,
        "name": "sms"
      }
    ]
  },
  {
    "name": "OnboardingStatus",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "created"
      },
      {
        "order": 10,
        "name": "open"
      },
      {
        "order": 100,
        "name": "done"
      },
      {
        "order": 100,
        "name": "done"
      }
    ]
  },
  {
    "name": "PartnerSite",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 1,
        "name": "palkkaus"
      },
      {
        "order": 2,
        "name": "palkkausIdentified"
      },
      {
        "order": 3,
        "name": "palkkausEmployerFillIn"
      },
      {
        "order": 10,
        "name": "example"
      },
      {
        "order": 100,
        "name": "dev"
      },
      {
        "order": 110,
        "name": "refugeeJobs"
      },
      {
        "order": 120,
        "name": "talkkarit"
      },
      {
        "order": 130,
        "name": "raksa"
      },
      {
        "order": 140,
        "name": "palkkaapakolainen"
      },
      {
        "order": 1010,
        "name": "careDotCom"
      },
      {
        "order": 1020,
        "name": "duunitori"
      },
      {
        "order": 1030,
        "name": "mol"
      }
    ]
  },
  {
    "name": "PaymentChannel",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "test"
      },
      {
        "order": 10,
        "name": "zeroPayment"
      },
      {
        "order": 20,
        "name": "external"
      },
      {
        "order": 101,
        "name": "palkkausManual"
      },
      {
        "order": 102,
        "name": "palkkausWS"
      },
            {
        "order": 103,
        "name": "palkkausPersonal"
      },
      {
        "order": 201,
        "name": "palkkausCfaPaytrail"
      },
      {
        "order": 203,
        "name": "palkkausCfaReference"
      },
      {
        "order": 204,
        "name": "palkkausCfaFinvoice"
      },
      {
        "order": 210,
        "name": "palkkausCfaTest"
      },
      {
        "order": 1010,
        "name": "accountorGo"
      },
      {
        "order": 1020,
        "name": "talenomOnline"
      },
      {
        "order": 1021,
        "name": "talenomCfa"
      },
      {
        "order": 1030,
        "name": "holviCfa"
      },
      {
        "order": 1040,
        "name": "finagoSolo"
      },
      {
        "order": 1050,
        "name": "procountor"
      },
      {
        "order": 1060,
        "name": "kevytyrittaja"
      },
      {
        "order": 1070,
        "name": "vismaNetvisor"
      }
    ]
  },
  {
    "name": "PaymentMethod",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "sepa"
      },
      {
        "order": 2,
        "name": "siirto"
      },
      {
        "order": 3,
        "name": "test"
      }
    ]
  },
  {
    "name": "PaymentStatus",
    "description": null,
    "values": [
      {
        "order": 1,
        "name": "new"
      },
      {
        "order": 100,
        "name": "sentToBank"
      },
      {
        "order": 101,
        "name": "bankTechApproval"
      },
      {
        "order": 102,
        "name": "bankDelivered"
      },
      {
        "order": 109,
        "name": "bankPartialError"
      },
      {
        "order": 110,
        "name": "bankError"
      },
      {
        "order": 1000,
        "name": "paid"
      },
      {
        "order": 8999,
        "name": "unknown"
      },
      {
        "order": 9999,
        "name": "cancelled"
      }
    ]
  },
  {
    "name": "PayrollStatus",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "draft"
      },
      {
        "order": 1,
        "name": "paymentStarted"
      },
      {
        "order": 2,
        "name": "paymentSucceeded"
      },
      {
        "order": 3,
        "name": "paymentCanceled"
      },
      {
        "order": 4,
        "name": "paymentError"
      },
      {
        "order": 100,
        "name": "waitingApproval"
      },
      {
        "order": 1000,
        "name": "template"
      }
    ]
  },
  {
    "name": "PensionCalculation",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "employee"
      },
      {
        "order": 2,
        "name": "entrepreneur"
      },
      {
        "order": 3,
        "name": "farmer"
      },
      {
        "order": 4,
        "name": "partialOwner"
      },
      {
        "order": 5,
        "name": "athlete"
      },
      {
        "order": 6,
        "name": "compensation"
      },
      {
        "order": 7,
        "name": "boardRemuneration"
      },
      {
        "order": 8,
        "name": "smallEntrepreneur"
      },
      {
        "order": 9,
        "name": "smallFarmer"
      }
    ]
  },
  {
    "name": "PensionCompany",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "none"
      },
      {
        "order": 2,
        "name": "etera"
      },
      {
        "order": 3,
        "name": "ilmarinen"
      },
      {
        "order": 4,
        "name": "elo"
      },
      {
        "order": 5,
        "name": "pensionsAlandia"
      },
      {
        "order": 6,
        "name": "varma"
      },
      {
        "order": 7,
        "name": "veritas"
      },
      {
        "order": 8,
        "name": "apteekkien"
      },
      {
        "order": 9,
        "name": "verso"
      },
      {
        "order": 999999,
        "name": "other"
      }
    ]
  },
  {
    "name": "PeriodDateKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "paidAtDate"
      },
      {
        "order": 1,
        "name": "salaryDate"
      }
    ]
  },
  {
    "name": "PeriodType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "month"
      },
      {
        "order": 1,
        "name": "quarter"
      },
      {
        "order": 2,
        "name": "year"
      },
      {
        "order": 99,
        "name": "custom"
      }
    ]
  },
  {
    "name": "PricingModel",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "palkkausFee"
      },
      {
        "order": 1,
        "name": "noFee"
      },
      {
        "order": 2,
        "name": "fixedFee"
      }
    ]
  },
  {
    "name": "ProductPackage",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "default"
      },
      {
        "order": 1,
        "name": "partnerBasic"
      },
      {
        "order": 2,
        "name": "partnerPro"
      }
    ]
  },
  {
    "name": "ProductListFilter",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "available"
      },
      {
        "order": 1,
        "name": "all"
      }
    ]
  },
  {
    "name": "ReportCategory",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "all"
      },
      {
        "order": 1,
        "name": "monthly"
      },
      {
        "order": 2,
        "name": "yearly"
      }
    ]
  },
  {
    "name": "ReportType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "example"
      },
      {
        "order": 100,
        "name": "monthlyDetails"
      },
      {
        "order": 110,
        "name": "taxMonthly4001"
      },
      {
        "order": 111,
        "name": "payerSummaryReport"
      },
      {
        "order": 120,
        "name": "monthlyPension"
      },
      {
        "order": 190,
        "name": "monthlyLiikekirjuri"
      },
      {
        "order": 200,
        "name": "monthlyRapko"
      },
      {
        "order": 201,
        "name": "monthlyAccounting"
      },
      {
        "order": 1000,
        "name": "yearlyDetails"
      },
      {
        "order": 1001,
        "name": "yearEndReport"
      },
      {
        "order": 1010,
        "name": "yearlyWorkerSummary"
      },
      {
        "order": 1100,
        "name": "taxYearly7801"
      },
      {
        "order": 1200,
        "name": "unemployment"
      },
      {
        "order": 1210,
        "name": "insurance"
      },
      {
        "order": 1800,
        "name": "householdDeduction"
      },
      {
        "order": 1814,
        "name": "taxHouseholdDeduction14B"
      },
      {
        "order": 1815,
        "name": "taxHouseholdDeduction14BSpouseA"
      },
      {
        "order": 1816,
        "name": "taxHouseholdDeduction14BSpouseB"
      },
      {
        "order": 2000,
        "name": "salarySlip"
      },
      {
        "order": 2001,
        "name": "salarySlipPaid"
      },
      {
        "order": 2002,
        "name": "salarySlipCopy"
      },
      {
        "order": 2010,
        "name": "employerReport"
      },
      {
        "order": 2020,
        "name": "paymentReport"
      },
      {
        "order": 2030,
        "name": "earningsPaymentReport"
      },
      {
        "order": 2200,
        "name": "employmentContract"
      }
    ]
  },
  {
    "name": "Role",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "accountant"
      },
      {
        "order": 2,
        "name": "trustedPartner"
      },
      {
        "order": 3,
        "name": "accountantCandidate"
      },
      {
        "order": 4,
        "name": "pricingPartner"
      }
    ]
  },
  {
    "name": "SalaryKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "fixedSalary"
      },
      {
        "order": 2,
        "name": "hourlySalary"
      },
      {
        "order": 3,
        "name": "monthlySalary"
      },
      {
        "order": 50,
        "name": "compensation"
      },
      {
        "order": 100,
        "name": "totalWorkerPayment"
      },
      {
        "order": 101,
        "name": "totalEmployerPayment"
      }
    ]
  },
  {
    "name": "SalaryType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "tyel"
      },
      {
        "order": 1,
        "name": "yel"
      },
      {
        "order": 2,
        "name": "compensation"
      }
    ]
  },
  {
    "name": "SalaryPerMonthRange",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 50,
        "name": "noPensionRequired"
      },
      {
        "order": 60,
        "name": "normal"
      }
    ]
  },
  {
    "name": "SalaryPerYearRange",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "zeroSalary"
      },
      {
        "order": 1,
        "name": "noReportingNecessary"
      },
      {
        "order": 200,
        "name": "noMonthlyReporting"
      },
      {
        "order": 1500,
        "name": "normal"
      }
    ]
  },
  {
    "name": "ServiceModel",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "shared"
      },
      {
        "order": 1,
        "name": "partnerOnly"
      }
    ]
  },
  {
    "name": "SettingsStatus",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "checked"
      },
      {
        "order": 10,
        "name": "pending"
      }
    ]
  },
  {
    "name": "SharingUriType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "url"
      },
      {
        "order": 3,
        "name": "employer"
      }
    ]
  },
  {
    "name": "TaxcardApiIncomeType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 24,
        "name": "salary"
      },
      {
        "order": 27,
        "name": "dividendsForWorkAsSalary"
      },
      {
        "order": 28,
        "name": "personnelFundIncome"
      },
      {
        "order": 29,
        "name": "salaryOneOff"
      },
      {
        "order": 33,
        "name": "compensationForUseEarnedIncome"
      },
      {
        "order": 34,
        "name": "compensationForUseCapitalIncome"
      },
      {
        "order": 35,
        "name": "compensationVatResponsible"
      },
      {
        "order": 36,
        "name": "compensation"
      },
      {
        "order": 37,
        "name": "dividendsForWorkAsCompensation"
      },
      {
        "order": 38,
        "name": "athletePay"
      },
      {
        "order": 39,
        "name": "otherTaxableIncome"
      },
      {
        "order": 40,
        "name": "otherTaxableIncomeOneOff"
      },
      {
        "order": 42,
        "name": "employeeStockOption"
      }
    ]
  },
  {
    "name": "TaxcardApprovalMethod",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "assureWaiting"
      },
      {
        "order": 1,
        "name": "approve"
      },
      {
        "order": 2,
        "name": "reject"
      }
    ]
  },
  {
    "name": "TaxcardCalcDiffCheck",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "getDiff"
      },
      {
        "order": 1,
        "name": "noCheck"
      },
      {
        "order": 2,
        "name": "commitDiff"
      }
    ]
  },
  {
    "name": "TaxCardIncomeType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 1,
        "name": "salaxyCalculation"
      },
      {
        "order": 2,
        "name": "previousEmployerSalaries"
      },
      {
        "order": 3,
        "name": "externalSalaries"
      },
      {
        "order": 10,
        "name": "sharedCardExtSum"
      },
      {
        "order": 100,
        "name": "diff"
      }
    ]
  },
  {
    "name": "TaxcardState",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "new"
      },
      {
        "order": 10,
        "name": "approved"
      },
      {
        "order": 100,
        "name": "employerAdded"
      },
      {
        "order": 200,
        "name": "verifiedVero"
      },
      {
        "order": 300,
        "name": "sharedWaiting"
      },
      {
        "order": 310,
        "name": "sharedApproved"
      },
      {
        "order": 320,
        "name": "sharedRejected"
      },
      {
        "order": 330,
        "name": "sharedRejectedWithoutOpen"
      },
      {
        "order": 500,
        "name": "shared"
      }
    ]
  },
  {
    "name": "TaxcardKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "noTaxCard"
      },
      {
        "order": 2,
        "name": "auto"
      },
      {
        "order": 10,
        "name": "defaultYearly"
      },
      {
        "order": 11,
        "name": "replacement"
      },
      {
        "order": 20,
        "name": "noWithholdingHousehold"
      },
      {
        "order": 99,
        "name": "others"
      },
      {
        "order": 900,
        "name": "historical"
      }
    ]
  },
  {
    "name": "TaxcardValidity",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "valid"
      },
      {
        "order": 2,
        "name": "validJanuary"
      },
      {
        "order": 3,
        "name": "future"
      },
      {
        "order": 4,
        "name": "expired"
      }
    ]
  },
  {
    "name": "TaxDeductionWorkCategories",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "none"
      },
      {
        "order": 1,
        "name": "householdwork"
      },
      {
        "order": 2,
        "name": "carework"
      },
      {
        "order": 4,
        "name": "homeImprovement"
      },
      {
        "order": 8,
        "name": "ownPropety"
      },
      {
        "order": 16,
        "name": "relativesProperty"
      }
    ]
  },
  {
    "name": "TaxReportHandling",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "default"
      },
      {
        "order": 1,
        "name": "noZeroSalaryReport"
      },
      {
        "order": 2,
        "name": "noMonthlyReport"
      },
      {
        "order": 3,
        "name": "always"
      }
    ]
  },
  {
    "name": "TesSubtype",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "notSelected"
      },
      {
        "order": 100,
        "name": "constructionCarpenter"
      },
      {
        "order": 101,
        "name": "constructionFloor"
      },
      {
        "order": 105,
        "name": "constructionOther"
      },
      {
        "order": 109,
        "name": "constructionFreeContract"
      }
    ]
  },
  {
    "name": "TestEnum",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "valueZero"
      },
      {
        "order": 1,
        "name": "valueOne"
      },
      {
        "order": 2,
        "name": "valueTwo"
      },
      {
        "order": 3,
        "name": "valueThree"
      },
      {
        "order": 4,
        "name": "valueFour"
      },
      {
        "order": 5,
        "name": "valueFive"
      },
      {
        "order": 6,
        "name": "valueSix"
      },
      {
        "order": 7,
        "name": "valueSeven"
      },
      {
        "order": 8,
        "name": "valueEight"
      },
      {
        "order": 9,
        "name": "valueNine"
      },
      {
        "order": 10,
        "name": "valueTen"
      }
    ]
  },
  {
    "name": "ThreadedMessageType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "text"
      },
      {
        "order": 1,
        "name": "blobFile"
      },
      {
        "order": 10,
        "name": "calculation"
      },
      {
        "order": 20,
        "name": "payroll"
      },
      {
        "order": 30,
        "name": "workerAccount"
      },
      {
        "order": 500,
        "name": "email"
      },
      {
        "order": 510,
        "name": "sms"
      }
    ]
  },
  {
    "name": "UnionPaymentType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "notSelected"
      },
      {
        "order": 100,
        "name": "raksaNormal"
      },
      {
        "order": 101,
        "name": "raksaUnemploymentOnly"
      },
      {
        "order": 9999,
        "name": "other"
      }
    ]
  },
  {
    "name": "Unit",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "hours"
      },
      {
        "order": 2,
        "name": "days"
      },
      {
        "order": 3,
        "name": "weeks"
      },
      {
        "order": 5,
        "name": "period"
      },
      {
        "order": 1000,
        "name": "one"
      },
      {
        "order": 1001,
        "name": "count"
      },
      {
        "order": 1002,
        "name": "percent"
      },
      {
        "order": 1004,
        "name": "kilometers"
      },
      {
        "order": 2000,
        "name": "euro"
      }
    ]
  },
  {
    "name": "VarmaPensionOrderAction",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "newYel"
      },
      {
        "order": 2,
        "name": "newTyel"
      },
      {
        "order": 3,
        "name": "move"
      }
    ]
  },
  {
    "name": "VarmaPensionOrderYelPayer",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "entrepreneur"
      },
      {
        "order": 2,
        "name": "company"
      }
    ]
  },
  {
    "name": "WageBasis",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 10,
        "name": "monthly"
      },
      {
        "order": 20,
        "name": "hourly"
      },
      {
        "order": 30,
        "name": "performanceBased"
      },
      {
        "order": 999,
        "name": "other"
      }
    ]
  },
  {
    "name": "WebSiteUserRole",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "none"
      },
      {
        "order": 1,
        "name": "household"
      },
      {
        "order": 2,
        "name": "worker"
      },
      {
        "order": 4,
        "name": "company"
      }
    ]
  },
  {
    "name": "WorkflowEventFeatures",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "none"
      },
      {
        "order": 1,
        "name": "isActive"
      },
      {
        "order": 2,
        "name": "isEditable"
      },
      {
        "order": 4,
        "name": "isRemovable"
      },
      {
        "order": 8,
        "name": "isOnHold"
      }
    ]
  },
  {
    "name": "YearEndAdminCheck",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "notChecked"
      },
      {
        "order": 1,
        "name": "ok"
      },
      {
        "order": 2,
        "name": "okWithWarning"
      },
      {
        "order": 3,
        "name": "errors"
      }
    ]
  },
  {
    "name": "YearEndFeedbackCalculationType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 1,
        "name": "salaxyPayment"
      },
      {
        "order": 10,
        "name": "external"
      },
      {
        "order": 100,
        "name": "correction"
      }
    ]
  },
  {
    "name": "YearEndUserFeedback",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "notChecked"
      },
      {
        "order": 1,
        "name": "ok"
      },
      {
        "order": 2,
        "name": "okWithModifications"
      },
      {
        "order": 3,
        "name": "willHandleMyself"
      },
      {
        "order": 4,
        "name": "noYearlyReports"
      }
    ]
  },
  {
    "name": "YtjCompanyType",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "noCompanyType"
      },
      {
        "order": 1,
        "name": "housingCooperative"
      },
      {
        "order": 2,
        "name": "condominium"
      },
      {
        "order": 4,
        "name": "asoAssociation"
      },
      {
        "order": 5,
        "name": "openCompany"
      },
      {
        "order": 6,
        "name": "association"
      },
      {
        "order": 9,
        "name": "hypoAssociation"
      },
      {
        "order": 10,
        "name": "mutualRealEstateCompany"
      },
      {
        "order": 13,
        "name": "ky"
      },
      {
        "order": 14,
        "name": "osuuskunta"
      },
      {
        "order": 15,
        "name": "cooperativeBank"
      },
      {
        "order": 16,
        "name": "oy"
      },
      {
        "order": 18,
        "name": "foundation"
      },
      {
        "order": 20,
        "name": "savingsBank"
      },
      {
        "order": 21,
        "name": "financialAssociation"
      },
      {
        "order": 22,
        "name": "stateEstablishment"
      },
      {
        "order": 25,
        "name": "insuranceAssociation"
      },
      {
        "order": 26,
        "name": "privateEntrepreneur"
      },
      {
        "order": 29,
        "name": "otherAssociation"
      },
      {
        "order": 31,
        "name": "specialPurposeAssociation"
      },
      {
        "order": 35,
        "name": "forestCareAssociation"
      },
      {
        "order": 38,
        "name": "otherFinancialAssociation"
      },
      {
        "order": 46,
        "name": "religiousCommunity"
      },
      {
        "order": 51,
        "name": "taxableGrouping"
      },
      {
        "order": 58,
        "name": "mutualInsuranceAssociation"
      },
      {
        "order": 60,
        "name": "foreignOrganisation"
      },
      {
        "order": 61,
        "name": "municipalEstablishment"
      },
      {
        "order": 62,
        "name": "federationOfMunicipalitiesEstablishment"
      },
      {
        "order": 64,
        "name": "alandFederation"
      },
      {
        "order": 83,
        "name": "europeanCooperative"
      },
      {
        "order": 84,
        "name": "europeanCooperativeBank"
      },
      {
        "order": 90,
        "name": "reindeerHerdingCooperative"
      },
      {
        "order": 999,
        "name": "unknown"
      }
    ]
  },
  {
    "name": "CarBenefitKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "limitedCarBenefit"
      },
      {
        "order": 2,
        "name": "fullCarBenefit"
      }
    ]
  },
  {
    "name": "UnionPaymentKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "fixed"
      },
      {
        "order": 2,
        "name": "percentage"
      },
      {
        "order": 100,
        "name": "raksaNormal"
      },
      {
        "order": 101,
        "name": "raksaUnemploymentOnly"
      },
      {
        "order": 9999,
        "name": "other"
      }
    ]
  },
  {
    "name": "NonProfitOrgKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 357,
        "name": "kilometreAllowance"
      },
      {
        "order": 358,
        "name": "dailyAllowance"
      },
      {
        "order": 1358,
        "name": "accomodationAllowance"
      }
    ]
  },
  {
    "name": "SubsidisedCommuteKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "noDeduction"
      },
      {
        "order": 2,
        "name": "singleDeduction"
      },
      {
        "order": 10,
        "name": "periodicalDeduction"
      }
    ]
  },
  {
    "name": "MealBenefitKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "cateringContract"
      },
      {
        "order": 2,
        "name": "mealTicket"
      },
      {
        "order": 3,
        "name": "taxableAmount"
      },
      {
        "order": 4,
        "name": "mealAllowance"
      },
      {
        "order": 5,
        "name": "institute"
      },
      {
        "order": 6,
        "name": "teacher"
      },
      {
        "order": 7,
        "name": "restaurantWorker"
      }
    ]
  },
  {
    "name": "DailyAllowanceKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "fullDailyAllowance"
      },
      {
        "order": 2,
        "name": "partialDailyAllowance"
      },
      {
        "order": 3,
        "name": "internationalDailyAllowance"
      },
      {
        "order": 4,
        "name": "mealAllowance"
      }
    ]
  },
  {
    "name": "BoardKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      }
    ]
  },
  {
    "name": "RemunerationKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 202,
        "name": "initiativeFee"
      },
      {
        "order": 203,
        "name": "bonusPay"
      },
      {
        "order": 220,
        "name": "commission"
      },
      {
        "order": 223,
        "name": "performanceBonus"
      },
      {
        "order": 226,
        "name": "shareIssueForEmployees"
      },
      {
        "order": 233,
        "name": "profitSharingBonus"
      }
    ]
  },
  {
    "name": "OtherCompensationKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 210,
        "name": "meetingFee"
      },
      {
        "order": 214,
        "name": "lectureFee"
      },
      {
        "order": 215,
        "name": "positionOfTrustCompensation"
      },
      {
        "order": 225,
        "name": "accruedTimeOffCompensation"
      },
      {
        "order": 308,
        "name": "membershipOfGoverningBodyCompensation"
      },
      {
        "order": 310,
        "name": "monetaryGiftForEmployees"
      },
      {
        "order": 313,
        "name": "useCompensationAsEarnedIncome"
      },
      {
        "order": 314,
        "name": "useCompensationAsCapitalIncome"
      },
      {
        "order": 316,
        "name": "otherTaxableIncomeAsEarnedIncome"
      },
      {
        "order": 320,
        "name": "stockOptionsAndGrants"
      },
      {
        "order": 326,
        "name": "employeeInventionCompensation"
      },
      {
        "order": 332,
        "name": "capitalIncomePayment"
      },
      {
        "order": 339,
        "name": "workEffortBasedDividendsAsWage"
      },
      {
        "order": 340,
        "name": "workEffortBasedDividendsAsNonWage"
      },
      {
        "order": 343,
        "name": "employeeStockOption"
      },
      {
        "order": 361,
        "name": "employeeStockOptionWithLowerPrice"
      }
    ]
  },
  {
    "name": "WorkingTimeCompensationKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 205,
        "name": "emergencyWorkCompensation"
      },
      {
        "order": 206,
        "name": "eveningWorkCompensation"
      },
      {
        "order": 207,
        "name": "eveningShiftAllowance"
      },
      {
        "order": 211,
        "name": "saturdayPay"
      },
      {
        "order": 212,
        "name": "extraWorkPremium"
      },
      {
        "order": 216,
        "name": "otherCompensation"
      },
      {
        "order": 217,
        "name": "waitingTimeCompensation"
      },
      {
        "order": 221,
        "name": "sundayWorkCompensation"
      },
      {
        "order": 230,
        "name": "standByCompensation"
      },
      {
        "order": 232,
        "name": "weeklyRestCompensation"
      },
      {
        "order": 235,
        "name": "overtimeCompensation"
      },
      {
        "order": 236,
        "name": "nightWorkAllowance"
      },
      {
        "order": 237,
        "name": "nightShiftCompensation"
      }
    ]
  },
  {
    "name": "EmploymentTerminationKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 208,
        "name": "noticePeriodCompensation"
      },
      {
        "order": 224,
        "name": "monetaryWorkingTimeBankCompensation"
      },
      {
        "order": 229,
        "name": "terminationAndLayOffDamages"
      },
      {
        "order": 231,
        "name": "voluntaryTerminationCompensation"
      },
      {
        "order": 338,
        "name": "pensionPaidByEmployer"
      }
    ]
  },
  {
    "name": "ForeclosureKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "fixed"
      },
      {
        "order": 2,
        "name": "periodic"
      },
      {
        "order": 3,
        "name": "nonPeriodic"
      }
    ]
  },
  {
    "name": "TotalWorkerPaymentKind",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "undefined"
      },
      {
        "order": 1,
        "name": "totalWorkerPayment"
      },
      {
        "order": 2,
        "name": "totalWorkerWorkerPaymentExcludingExpenses"
      },
      {
        "order": 3,
        "name": "totalWorkerPaymentAndTax"
      },
      {
        "order": 4,
        "name": "totalWorkerWorkerPaymentAndTaxExcludingExpenses"
      }
    ]
  },
  {
    "name": "TransactionCode",
    "description": null,
    "values": [
      {
        "order": 0,
        "name": "unknown"
      },
      {
        "order": 1,
        "name": "ignored"
      },
      {
        "order": 201,
        "name": "timeRatePay"
      },
      {
        "order": 202,
        "name": "initiativeFee"
      },
      {
        "order": 203,
        "name": "bonusPay"
      },
      {
        "order": 204,
        "name": "complementaryWagePaidDuringBenefitPeriod"
      },
      {
        "order": 205,
        "name": "emergencyWorkCompensation"
      },
      {
        "order": 206,
        "name": "eveningWorkCompensation"
      },
      {
        "order": 207,
        "name": "eveningShiftAllowance"
      },
      {
        "order": 208,
        "name": "noticePeriodCompensation"
      },
      {
        "order": 209,
        "name": "kilometreAllowanceTaxable"
      },
      {
        "order": 210,
        "name": "meetingFee"
      },
      {
        "order": 211,
        "name": "saturdayPay"
      },
      {
        "order": 212,
        "name": "extraWorkPremium"
      },
      {
        "order": 213,
        "name": "holidayBonus"
      },
      {
        "order": 214,
        "name": "lectureFee"
      },
      {
        "order": 215,
        "name": "compensationForTrustPosition"
      },
      {
        "order": 216,
        "name": "otherCompensation"
      },
      {
        "order": 217,
        "name": "waitingTimeCompensation"
      },
      {
        "order": 218,
        "name": "workingConditionCompensation"
      },
      {
        "order": 219,
        "name": "partialPayDuringSickLeave"
      },
      {
        "order": 220,
        "name": "commission"
      },
      {
        "order": 221,
        "name": "sundayWorkCompensation"
      },
      {
        "order": 222,
        "name": "benefitArisingFromSyntheticOption"
      },
      {
        "order": 223,
        "name": "performanceBonus"
      },
      {
        "order": 224,
        "name": "monetaryCompensationFromWorkingTimeBank"
      },
      {
        "order": 225,
        "name": "compensationForAccruedTimeOff"
      },
      {
        "order": 226,
        "name": "shareIssueForEmployees"
      },
      {
        "order": 227,
        "name": "contractPay"
      },
      {
        "order": 229,
        "name": "damagesInConjunctionWithTermination"
      },
      {
        "order": 230,
        "name": "standByCompensation"
      },
      {
        "order": 231,
        "name": "voluntaryCompensationInConjunctionWithTermination"
      },
      {
        "order": 232,
        "name": "weeklyRestCompensation"
      },
      {
        "order": 233,
        "name": "profitSharingBonus"
      },
      {
        "order": 234,
        "name": "annualHolidayCompensation"
      },
      {
        "order": 235,
        "name": "overtimeCompensation"
      },
      {
        "order": 236,
        "name": "nightWorkAllowance"
      },
      {
        "order": 237,
        "name": "nightShiftCompensation"
      },
      {
        "order": 238,
        "name": "otherRegularCompensation"
      },
      {
        "order": 239,
        "name": "compensationForUnusedCompensatoryLeave"
      },
      {
        "order": 301,
        "name": "accommodationBenefit"
      },
      {
        "order": 302,
        "name": "interestBenefitForHousingLoan"
      },
      {
        "order": 303,
        "name": "mealAllowance"
      },
      {
        "order": 304,
        "name": "carBenefit"
      },
      {
        "order": 308,
        "name": "compensationForGoverningBodyMembership"
      },
      {
        "order": 309,
        "name": "shareOfReserveDrawnFromPersonnelFund"
      },
      {
        "order": 310,
        "name": "monetaryGiftForEmployees"
      },
      {
        "order": 311,
        "name": "kilometreAllowanceTaxExempt"
      },
      {
        "order": 312,
        "name": "treatmentFeeForMunicipalVeterinarian"
      },
      {
        "order": 313,
        "name": "compensationForUseEarnedIncome"
      },
      {
        "order": 314,
        "name": "compensationForUseCapitalIncome"
      },
      {
        "order": 315,
        "name": "otherTaxableBenefitForEmployees"
      },
      {
        "order": 316,
        "name": "otherTaxableIncomeDeemedEarnedIncome"
      },
      {
        "order": 317,
        "name": "otherFringeBenefit"
      },
      {
        "order": 319,
        "name": "kinshipCarersFee"
      },
      {
        "order": 320,
        "name": "stockOptions"
      },
      {
        "order": 321,
        "name": "wagesPaidBySubstitutePayerIncludingSocialInsurance"
      },
      {
        "order": 322,
        "name": "wagesPaidBySubstitutePayerIncludingEarningsRelatedPensionInsurance"
      },
      {
        "order": 323,
        "name": "wagesPaidBySubstitutePayerIncludingUnemploymentInsurance"
      },
      {
        "order": 324,
        "name": "wagesPaidBySubstitutePayerIncludingAccidentAndOccupationalDiseaseInsurance"
      },
      {
        "order": 325,
        "name": "wagesPaidBySubstitutePayerIncludingHealthInsurance"
      },
      {
        "order": 326,
        "name": "compensationForEmployeeInvention"
      },
      {
        "order": 327,
        "name": "reimbursementOfPrivateCaretakersExpenses"
      },
      {
        "order": 328,
        "name": "privateCaretakersFee"
      },
      {
        "order": 329,
        "name": "reimbursementOfFamilyDayCareProvidersExpenses"
      },
      {
        "order": 330,
        "name": "telephoneBenefit"
      },
      {
        "order": 331,
        "name": "dailyAllowance"
      },
      {
        "order": 332,
        "name": "capitalIncomePayment"
      },
      {
        "order": 334,
        "name": "mealBenefit"
      },
      {
        "order": 335,
        "name": "reimbursementOfCostsPaidToConciliator"
      },
      {
        "order": 336,
        "name": "nonWageCompensationForWork"
      },
      {
        "order": 337,
        "name": "supplementaryDailyAllowancePaidByEmployerSpecificHealthInsuranceFund"
      },
      {
        "order": 338,
        "name": "pensionPaidByEmployer"
      },
      {
        "order": 339,
        "name": "dividendsBasedOnWages"
      },
      {
        "order": 340,
        "name": "dividendsBasedOnNonWage"
      },
      {
        "order": 341,
        "name": "employerSubsidisedCommuterTicketTaxExempt"
      },
      {
        "order": 342,
        "name": "employerSubsidisedCommuterTicketTaxable"
      },
      {
        "order": 343,
        "name": "employeeStockOption"
      },
      {
        "order": 350,
        "name": "wagesTransferredToAthletesSpecialFund"
      },
      {
        "order": 351,
        "name": "wagesPaidFromAthletesSpecialFund"
      },
      {
        "order": 352,
        "name": "wagesForInsurancePurposes"
      },
      {
        "order": 353,
        "name": "taxableReimbursementOfExpenses"
      },
      {
        "order": 354,
        "name": "privateDayCareAllowanceMunicipalSupplement"
      },
      {
        "order": 355,
        "name": "privateDayCareAllowanceWages"
      },
      {
        "order": 356,
        "name": "privateDayCareAllowanceNonWage"
      },
      {
        "order": 357,
        "name": "kilometreAllowancePaidByNonProfitOrganisation"
      },
      {
        "order": 358,
        "name": "dailyAllowancePaidByNonProfitOrganisation"
      },
      {
        "order": 359,
        "name": "unjustEnrichment"
      },
      {
        "order": 361,
        "name": "employeeStockOptionLowerMarketPrice"
      },
      {
        "order": 362,
        "name": "royaltyPaidToNonResidentTaxpayer"
      },
      {
        "order": 363,
        "name": "bicycleBenefitTaxExempt"
      },
      {
        "order": 364,
        "name": "bicycleBenefitTaxable"
      },
      {
        "order": 365,
        "name": "conditionalStockOptions"
      },
      {
        "order": 401,
        "name": "compensationCollectedForCarBenefit"
      },
      {
        "order": 402,
        "name": "withholdingTax"
      },
      {
        "order": 403,
        "name": "electedOfficialFee"
      },
      {
        "order": 404,
        "name": "taxAtSource"
      },
      {
        "order": 405,
        "name": "taxAtSourceDeduction"
      },
      {
        "order": 406,
        "name": "wagesPaid"
      },
      {
        "order": 407,
        "name": "reimbursementCollectedForOtherFringeBenefits"
      },
      {
        "order": 408,
        "name": "otherItemDeductibleFromNetWage"
      },
      {
        "order": 409,
        "name": "netWage"
      },
      {
        "order": 410,
        "name": "employerPaidPremiumForCollectiveAdditionalPensionInsurance"
      },
      {
        "order": 411,
        "name": "employerPaidPremiumForCollectiveAdditionalPensionInsuranceEmployeesContribution"
      },
      {
        "order": 412,
        "name": "employeesHealthInsuranceContribution"
      },
      {
        "order": 413,
        "name": "employeesPensionInsuranceContribution"
      },
      {
        "order": 414,
        "name": "employeesUnemploymentInsuranceContribution"
      },
      {
        "order": 415,
        "name": "reimbursementForEmployerSubsidisedCommuterTicket"
      },
      {
        "order": 416,
        "name": "taxPaidAbroad"
      },
      {
        "order": 417,
        "name": "distraint"
      },
      {
        "order": 418,
        "name": "voluntaryIndividualPensionInsurancePremium"
      },
      {
        "order": 419,
        "name": "deductionBeforeWithholding"
      },
      {
        "order": 420,
        "name": "reimbursementCollectedForBicycleBenefit"
      }
    ]
  }
];
