/* eslint-disable */

import { YearlyChangingNumbers } from "../model";

/**
 * Yearly changing numbers. Contains side cost percentages.
 *
 */

export const yearlyChangingNumbersYears =
  [
    {
      "year": 2018,
      "sideCosts": {
        "unemploymentTotalPercent": 0.0255,
        "unemploymentEmployerPercent": 0.0065,
        "unemploymentWorkerPercent": 0.019,
        "tyelIncomeLimit": 58.27,
        "tyelBasePercent": 0.253,
        "tyelWorkerPercent": 0.0635,
        "tyel53Percent": 0.0785,
        "tyelContractEmployerLimit": 8346,
        "socSecPercent": 0.0086,
        "illnessInsurancePercent": 0.0153,
        "taxFreeDailyAllowance": 42,
        "taxFreeDailyHalfAllowance": 19,
        "taxFreeMealAllowance": 10.5,
        "taxFreeKmAllowance": 0.42,
        "unionPaymentRaksaA": 0.0155,
        "unionPaymentRaksaAoTa": 0.0055,
        "stepsTaxCardLimit1": 10500,
        "stepsTaxCardLimit2": 17950,
        "householdDeduction": 0.20,
      }
    },
    {
      "year": 2019,
      "sideCosts": {
        "unemploymentTotalPercent": 0.02,
        "unemploymentEmployerPercent": 0.005,
        "unemploymentWorkerPercent": 0.015,
        "tyelIncomeLimit": 59.36,
        "tyelBasePercent": 0.252,
        "tyelWorkerPercent": 0.0675,
        "tyel53Percent": 0.0825,
        "tyelContractEmployerLimit": 8502,
        "socSecPercent": 0.0077,
        "illnessInsurancePercent": 0.0154,
        "taxFreeDailyAllowance": 42,
        "taxFreeDailyHalfAllowance": 19,
        "taxFreeMealAllowance": 10.5,
        "taxFreeKmAllowance": 0.43,
        "unionPaymentRaksaA": 0.0149,
        "unionPaymentRaksaAoTa": 0.0052,
        "stepsTaxCardLimit1": 10500,
        "stepsTaxCardLimit2": 17950,
        "householdDeduction": 0.20,
      }
    },
    {
      "year": 2020,
      "sideCosts": {
        "unemploymentEmployerPercent": 0.0045,
        "unemploymentWorkerPercent": 0.0125,
        "unemploymentPartialOwnerWorkerPercent": 0.0065,
        "tyelIncomeLimit": 60.57,
        "tyelBasePercent": 0.253,
        "tyelWorkerPercent": 0.0715,
        "tyel53Percent": 0.0865,
        "tyelContractEmployerLimit": 8676.0,
        "socSecPercent": 0.0134,
        "illnessInsurancePercent": 0.0186,
        "taxFreeDailyAllowance": 43.0,
        "taxFreeDailyHalfAllowance": 20.0,
        "taxFreeMealAllowance": 10.75,
        "taxFreeKmAllowance": 0.43,
        "unionPaymentRaksaA": 0.0149,
        "unionPaymentRaksaAoTa": 0.0052,
        "palkkausVatPercent": 0.24,
        "householdDeduction": 0.15,
        "mealBenefitCateringContract": 6.8,
        "mealBenefitInstitute": 5.1,
        "mealBenefitRestaurantWorker": 5.78,
        "mealBenefitTeacher": 4.08,
        "mealTicketLowerLimit": 6.8,
        "mealTicketUpperLimit": 10.7,
        "mealTicketTaxable": 0.75,
        "protectedPortionBase": 22.63,
        "protectedPortionDependant": 8.12
      },
    },
    {
      "year": 2021,
      "sideCosts": {
        "unemploymentEmployerPercent": 0.005,
        "unemploymentWorkerPercent": 0.014,
        "unemploymentPartialOwnerWorkerPercent": 0.0065,
        "tyelIncomeLimit": 61.37,
        "tyelBasePercent": 0.248,
        "tyelWorkerPercent": 0.0715,
        "tyel53Percent": 0.0865,
        "tyelContractEmployerLimit": 8790.0,
        "socSecPercent": 0.0153,
        "illnessInsurancePercent": 0.0136,
        "taxFreeDailyAllowance": 44.0,
        "taxFreeDailyHalfAllowance": 20.0,
        "taxFreeMealAllowance": 11.0,
        "taxFreeKmAllowance": 0.44,
        "unionPaymentRaksaA": 0.0149,
        "unionPaymentRaksaAoTa": 0.0052,
        "palkkausVatPercent": 0.24,
        "householdDeduction": 0.15,
        "mealBenefitCateringContract": 6.9,
        "mealBenefitInstitute": 5.18,
        "mealBenefitRestaurantWorker": 5.87,
        "mealBenefitTeacher": 4.14,
        "mealTicketLowerLimit": 6.9,
        "mealTicketUpperLimit": 10.9,
        "mealTicketTaxable": 0.75,
        "protectedPortionBase": 22.71,
        "protectedPortionDependant": 8.15
      },
    },
  ] as any as YearlyChangingNumbers[];
