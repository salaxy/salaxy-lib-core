import {AccountInIndex} from "../v01";

/** Generic index for all User Object Containers. */
export interface IUserObjectIndex {
  /** Unique key for the object. Unique within an Index.
   * In the case of UserObject, this is [Guid]_[ContainerType]_[Owner].
   * For non-user objects, it should typically be just a Guid.
   */
  id?: string | null;
  /** Guid of the container.
   * This is not always unique within the index.
   * Howver, within the container this is unique.
   */
  containerGuid?: string | null;
  /** The container type that is the source of this index item. */
  containerType?: string | null;
  /** Version number. May be used in conflicts */
  versionNumber?: number | null;
  /** Updated at time: Note that in typical implementations, this is the UserUpdatedAt time. */
  updatedAt?: string | null;
  /** Created at time */
  createdAt?: string | null;
  /** Latest partner information. */
  partner?: string | null;
  /**
   * Owner of the object is also the Partition key.
   * Mandatory in most storage scenarios.
   */
  owner?: string | null;
  /** Classifications for the object. First implementation supports BusinessEventType enumeration for values. */
  flags?: string[] | null;
  /** The business object type that is the source of this event. */
  payloadType?: string | null;
  /** Person GUID for the owner of the object. */
  ownerId?: string | null;
  /** Metadata for the owner */
  ownerInfo?: AccountInIndex | null;
  /** The main status depending on the type of the object. */
  status?: string | null;
  /** When the event started. Typically, this is the CreatedAt date, but it may be something else. */
  startAt?: string | null;
  /** This is the end date of the event.
   * Typically, it is the UserUpdatedAt date, but it may be something else - e.g PaidAt for the calculation.
   */
  endAt?: string | null;
  /** Gross salary if that is relevant to the transaction. */
  grossSalary?: number | null;
  /** This is the payment from the Owner point-of-view:
   * Total payment for the Employer and Net salary for the Worker in the case of a calculation.
   * Only add here the payment, if the payment is really made.
   */
  payment?: number | null;
  /** Estimated revenue that the transaction generated to Palkkaus.fi. Only add here the revenue, if the payment is really made. */
  revenue?: number | null;
  /** The GUID for the other party. Currently, this is always the PersonID. */
  otherId?: string | null;
  /** The other party (usually a Person) that is involved in the event (e.g Worker if this is a Salary payment by Employer). */
  otherPartyInfo?: AccountInIndex | null;
  /** A very short description describing the object as an event. E.g. "Paid salary" */
  shortText?: string | null;
  /** A short detail describing the transaction.
   * Should contain the relevant information for administration purposes.
   */
  additionalInfo?: string | null;
  /** Messages (Error, Warning, OnHold, Note) that are active in the system.
   * Shown in format "[MessageType]:[Message] ([User] at [UTC-time])"
   */
  messages?: string[] | null;
  /** Sub category for the payload. E.g. Payment Category, MoneyTransfer Source. */
  entityType?: string | null;
  /** Reference information. E.g. Payment or MoneyTransfer reference number. */
  reference?: string | null;
  /** External id for the object in 3rd party system. E.g. Filing code in MoneyTransfer. */
  externalId?: string | null;
  /** Business object ids related to this object. E.g. calculations and payrolls in the payment. */
  businessObjects?: string[] | null;
  /** Icon for the purposes of listings. */
  icon?: string | null;
  /** This is the recorded handling date for the object. E.g. when the calculation was paid to worker. */
  handledAt?: string | null;
  /** This is valid for calculations only. The estimated date of salary in worker. */
  salaryDate?: string | null;
  /** Business attributes to include further information of the object. */
  businessAttributes?: { [key: string]: any; } | null;
  /** The date for the actual period for which this object is done. */
  logicalDate?: string | null;
}
