import { PaymentChannel } from "../v01";

/**
 * Data that is used in editing Payment Channel settings in an external window.
 * Combines Payment channel settings and attributes in to one object.
 */
export interface PaymentChannelSettingsInfo {

  /** Identifier of the channel: The PaymentChannel enum value. */
  id: PaymentChannel,

  /** If true, the channel is the default channel for the current user */
  isDefault: boolean,

  /** The settings object for the channel */
  settings: {

    /** If true, the channel is enabled for the current user */
    isEnabled: boolean,

    /** If true, the channel is available for selection */
    isAvailable: boolean,

    /** Identifier of the accounting target that specifies how the accounting data is exported as part of payment. */
    accountingTargetId?: string,

    /** Channel specific data stored in the settings.  */
    data: any,

  }

  /** The customer funds settings. Sent only to Palkkaus CFA setup screens. */
  customerFundsSettings?: {

    /** If true, worker salary and expense payments are paid directly by the employer. */
    isWorkerSelfHandling: boolean,

    /** If true, the user handles the reporting and payments himself/herself. */
    isPensionSelfHandling: boolean,

    /**
     * True for partner pension companies.
     * If false, isPensionSelfHandling is always true: it cannot be changed without chaning the pension company.
     */
    isPensionIncludedSupported: boolean,

    /** If true, tax and social security payments are paid directly by the employer.  */
    isTaxAndSocialSecuritySelfHandling: boolean,

  }

  /** The invoice settings. Sent only to channels that are allowed to modify bank account number etc.  */
  invoiceSettings?: {

    /** Receiver's address for the e-invoice. */
    eInvoiceReceiver: string,

    /** Receiver's operator for the e-invoice. */
    eInvoiceIntermediator: string,

    /** SEPA maksatustunnus */
    sepaBankPartyId: string,

    /** Bank account IBAN number used in generating invoices. */
    ibanNumber: string,

  }

}
