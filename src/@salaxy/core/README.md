General plain JavaScript / TypeScript libraries
-----------------------------------------------

Plain JavaScript / TypeScript stack logic for Salaxy. 
These libraries are independent from libraries such as Angular, Angular 2, ReactJS etc.
They may be used inside these libraries.

Usage:

- Run `npm install --save @salaxy/core@latest`
- Reference or copy TypeScript files from `node_modules/@salaxy/core`

| File                   | Description                                       |
| ---------------------- | ------------------------------------------------- |
| index.js               | ES6 module compilation                            |
| salaxy-core-all.js     | Browser friendly (browserify) compilation         | 
| salaxy-core-all.min.js | Browser friendly (browserify) compilation  (min)  | 

For more information and documentation about Salaxy platform, please go to https://developers.salaxy.com

Please note that the salaxy-core-all.js includes the following libraries:

- moment.js (https://www.npmjs.com/package/moment)
- js-base64 (https://www.npmjs.com/package/js-base64)
- babel-polyfill (https://www.npmjs.com/package/babel-polyfill)
